package com.electroworld.android.ui.main.home.adapter

import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.electroworld.android.R
import com.electroworld.android.model.ImagesModel
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.products.list.ProductListFragment
import com.electroworld.android.ui.main.products.offerpreview.OfferPreviewFragment
import com.electroworld.android.utils.ScreenType
import kotlinx.android.synthetic.main.image_item.view.*

class ImagesAdapter(
    val list: ArrayList<ImagesModel>,
    navController: NavController,
    type: String,
    screenType: ScreenType
) :
    PagerAdapter() {

    private var mNavController: NavController = navController
    private var type: String = type
    private var screenType: ScreenType = screenType

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object` as View

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val ctx = container.context
        val view =
            (ctx as MainActivity).layoutInflater.inflate(R.layout.image_item, container, false)
        view.setOnClickListener {
            if (type == "1" || type == "2") {
                val bundle = ProductListFragment.getPlaceHolderBundles(
                    list[position].id,
                    list[position].title
                )
                mNavController.navigate(R.id.action_navigation_home_to_productListFragment, bundle)
            } else if (type == "4") {
                val bundle = OfferPreviewFragment.getPlaceHolderBundles(
                    list[position].id, list[position].title
                )
                if (screenType == ScreenType.CART) {
                    mNavController.navigate(
                        R.id.action_cartListFragment_to_offerPreviewFragment,
                        bundle
                    )
                } else {
                    val bundle = OfferPreviewFragment.getPlaceHolderBundles(
                        list[position].id,
                        list[position].title
                    )
                    mNavController.navigate(
                        R.id.action_navigation_home_to_offerPreviewFragment,
                        bundle
                    )
                }
            }
        }
        Glide.with(ctx)
            .load(list[position].image)
            .into(view.imageView)
        container.addView(view)
        return view
    }

    override fun getCount(): Int = list.size

    override fun getItemPosition(`object`: Any): Int = super.getItemPosition(`object`)

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}