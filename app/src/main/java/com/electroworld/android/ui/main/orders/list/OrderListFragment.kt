package com.electroworld.android.ui.main.orders.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.customclass.LoadMoreNestedScrollView
import com.electroworld.android.databinding.FragmentOrderListBinding
import com.electroworld.android.databinding.RowOrderListBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.dpToPx
import com.electroworld.android.extentions.getColorCompat
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.OrderStatus
import com.electroworld.android.model.Orientation
import com.electroworld.android.model.TimeLineModel
import com.electroworld.android.model.TimelineAttributes
import com.electroworld.android.model.orderlist.OrderListItem
import com.electroworld.android.model.orderlist.OrderListModel
import com.electroworld.android.model.orderlist.OrderListProductItem
import com.electroworld.android.ui.main.orders.TimeLineAdapter
import com.electroworld.android.ui.main.orders.detail.OrderDetailFragment.Companion.ARG_ORDER_ID
import com.electroworld.android.utils.ScreenType
import com.github.vipulasri.timelineview.TimelineView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import java.util.*
import javax.inject.Inject

class OrderListFragment : DaggerFragment() {

    private lateinit var viewModel: OrderListViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentOrderListBinding

    private val disposable = CompositeDisposable()
    private var mAdapter: GenericAdapter<OrderListItem, RowOrderListBinding>? = null
    private var isLoadMore = true

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager
    private var page = 1
    private var status = 0
    private var is_return = 0
    private lateinit var mAttributes: TimelineAttributes
    var screenType = ScreenType.ALL_ORDERS
    val mDataList = ArrayList<TimeLineModel>()
    var statusAccept: OrderStatus = OrderStatus.ACTIVE
    var statusShipped: OrderStatus = OrderStatus.INACTIVE
    var processing: OrderStatus = OrderStatus.INACTIVE
    var statusOutForDelivery: OrderStatus = OrderStatus.INACTIVE
    var statusDelivered: OrderStatus = OrderStatus.INACTIVE

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_order_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(OrderListViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentOrderListBinding.bind(view)
        binding.ab.tvABTitle.text = getString(R.string.profile_my_orders)


        mAttributes = TimelineAttributes(
            markerSize = dpToPx(15f),
            markerColor = getColorCompat(R.color.colorPrimary),
            markerInCenter = true,
            markerLeftPadding = dpToPx(0f),
            markerTopPadding = dpToPx(0f),
            markerRightPadding = dpToPx(0f),
            markerBottomPadding = dpToPx(0f),
            linePadding = dpToPx(2f),
            startLineColor = getColorCompat(R.color.colorPrimary),
            endLineColor = getColorCompat(R.color.colorPrimary),
            lineStyle = TimelineView.LineStyle.NORMAL,
            lineWidth = dpToPx(2f),
            lineDashWidth = dpToPx(4f),
            lineDashGap = dpToPx(2f)
        )
        mAttributes.orientation = Orientation.HORIZONTAL
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        initRecyclerView()
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("All Orders"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Cancelled"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Return Orders"))
        binding.tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        page = 1
        makeReq()
        when (screenType) {
            ScreenType.ALL_ORDERS -> {
                binding.tabLayout.getTabAt(0)?.select()
            }
            ScreenType.CANCELLED -> {
                binding.tabLayout.getTabAt(1)?.select()
            }
            ScreenType.RETURNED -> {
                binding.tabLayout.getTabAt(2)?.select()
            }
        }
        binding.tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> {
                        screenType = ScreenType.ALL_ORDERS
                        page = 1
                        makeReq()
                    }
                    1 -> {
                        screenType = ScreenType.CANCELLED
                        page = 1
                        makeReq()
                    }
                    2 -> {
                        screenType = ScreenType.RETURNED
                        page = 1
                        makeReq()
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun updateAdapter() {
        if (mAdapter?.itemCount == 0) {
            binding.tvEmptyMsg.setVisible()
            binding.recyclerView.setGone()
        } else {
            binding.tvEmptyMsg.setGone()
            binding.recyclerView.setVisible()
        }
    }


    private fun makeReq() {
        when (screenType) {
            ScreenType.ALL_ORDERS -> {
                status = 0
                is_return = 0
            }
            ScreenType.CANCELLED -> {
                status = 10
                is_return = 0
            }
            ScreenType.RETURNED -> {
                status = 9
                is_return = 0
            }
        }
        if (page == 1) {
            binding.recyclerView.setGone()
            binding.tvEmptyMsg.setGone()
        }

        disposable.add(
            viewModel.makeOrderListReq(
                page,
                status,
                is_return
            )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<OrderListModel>>() {
                    override fun onSuccess(res: ApiResponse<OrderListModel>) {
                        if (res.http_code == 200) {
                            if (page == 1) {
                                mAdapter?.clearAll()
                            }
                            mAdapter?.addItems(res.data.order_list)
                            isLoadMore = res.data.order_list.isNotEmpty()
                            updateAdapter()
                        } else {
                            binding.tvEmptyMsg.setVisible()
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun initRecyclerView() {
        mAdapter = object :
            GenericAdapter<OrderListItem, RowOrderListBinding>(R.layout.row_order_list) {
            override fun onBindData(
                model: OrderListItem,
                position: Int,
                binding: RowOrderListBinding
            ) {
                binding.item = model
                binding.tvProductNames.text = getProductNames(model.items)

                mDataList.clear()
                val orderStatusId = model.status_id
                if (orderStatusId == 2 || orderStatusId == 3 || orderStatusId == 4
                    || orderStatusId == 11 || orderStatusId == 13
                ) {
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.accepted),
                            "",
                            OrderStatus.COMPLETED
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.shipped),
                            "",
                            OrderStatus.ACTIVE
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.out_for_delivery),
                            "",
                            OrderStatus.INACTIVE
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.delivered),
                            "",
                            OrderStatus.INACTIVE
                        )
                    )
                } else if (orderStatusId == 5 || orderStatusId == 6) {
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.accepted),
                            "",
                            OrderStatus.COMPLETED
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.shipped),
                            "",
                            OrderStatus.COMPLETED
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.out_for_delivery),
                            "",
                            OrderStatus.ACTIVE
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.delivered),
                            "",
                            OrderStatus.INACTIVE
                        )
                    )
                } else if (orderStatusId == 7) {
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.accepted),
                            "",
                            OrderStatus.COMPLETED
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.shipped),
                            "",
                            OrderStatus.COMPLETED
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.out_for_delivery),
                            "",
                            OrderStatus.COMPLETED
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.delivered),
                            "",
                            OrderStatus.ACTIVE
                        )
                    )
                } else if (orderStatusId == 8) {
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.accepted),
                            "",
                            OrderStatus.COMPLETED
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.shipped),
                            "",
                            OrderStatus.COMPLETED
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.out_for_delivery),
                            "",
                            OrderStatus.COMPLETED
                        )
                    )
                    mDataList.add(
                        TimeLineModel(
                            binding.tvProductNames.context.resources.getString(R.string.delivered),
                            "",
                            OrderStatus.COMPLETED
                        )
                    )
                }

                binding.rvTimeLine.apply {
                    layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
                    adapter = TimeLineAdapter(mDataList, mAttributes, "order_list")
                }


                if (orderStatusId == 1 || orderStatusId == 12 || orderStatusId == 14 || orderStatusId == 15
                    || orderStatusId == 20 || orderStatusId == 10
                ) {
                    binding.rvTimeLine.setGone()
                } else {
                    binding.rvTimeLine.setVisible()
                }


//                if (model.status_id in 2..14) {
//                    binding.rvTimeLine.setVisible()
//                    statusAccept = OrderStatus.INACTIVE
//                    statusShipped = OrderStatus.INACTIVE
//                    statusOutForDelivery = OrderStatus.INACTIVE
//                    statusDelivered = OrderStatus.INACTIVE
//                    model.order_status_history?.forEach {
//                        if (it.status_id >= 2) {
//                            statusAccept = OrderStatus.COMPLETED
//                            statusShipped = OrderStatus.ACTIVE
//                            if (it.status_id == 3) {
//                                processing = OrderStatus.ACTIVE
//                            }
//                            if (it.status_id >= 5) {
//                                statusShipped = OrderStatus.COMPLETED
//                                statusOutForDelivery = OrderStatus.ACTIVE
//                                if (it.status_id >= 7) {
//                                    statusOutForDelivery = OrderStatus.COMPLETED
//                                    statusDelivered = OrderStatus.ACTIVE
//                                    if (it.status_id == 8) {
//                                        statusDelivered = OrderStatus.COMPLETED
//                                    }
//                                }
//
//                            }
//                        }
//                    }
//                    mDataList.clear()
//                    mDataList.add(
//                        TimeLineModel(
//                            binding.tvProductNames.context.resources.getString(R.string.accepted),
//                            "",
//                            statusAccept
//                        )
//                    )
//                    mDataList.add(
//                        TimeLineModel(
//                            binding.tvProductNames.context.resources.getString(R.string.shipped),
//                            "",
//                            statusShipped
//                        )
//                    )
//                    mDataList.add(
//                        TimeLineModel(
//                            binding.tvProductNames.context.resources.getString(R.string.out_for_delivery),
//                            "",
//                            statusOutForDelivery
//                        )
//                    )
//                    mDataList.add(
//                        TimeLineModel(
//                            binding.tvProductNames.context.resources.getString(R.string.delivered),
//                            "",
//                            statusDelivered
//                        )
//                    )
//                    binding.rvTimeLine.apply {
//                        layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
//                        adapter = TimeLineAdapter(mDataList, mAttributes, "order_list")
//                    }
//                } else {
//                    binding.rvTimeLine.setGone()
//                }

                when (screenType) {
                    ScreenType.RETURNED -> {
                        binding.rvTimeLine.setGone()
                    }
                    ScreenType.CANCELLED -> {
                        binding.rvTimeLine.setGone()
                    }
                }
            }

            override fun onItemClick(model: OrderListItem, position: Int) {
                openDetailsScreen(model)
            }
        }
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
        val loadMoreNestedScrollView =
            LoadMoreNestedScrollView(binding.recyclerView.layoutManager as LinearLayoutManager)
        loadMoreNestedScrollView.setOnLoadMoreListener {
            if (isLoadMore) {
                page = page.plus(1)
                makeReq()
            }
        }
        binding.nestedScrollView.setOnScrollChangeListener(loadMoreNestedScrollView)
    }

    private fun getProductNames(list: List<OrderListProductItem>?): String {
        val sb = StringBuilder()
        list?.forEachIndexed { index, orderListProductItem ->
            sb.append(orderListProductItem.name_en)
            if (index != list.size - 1) {
                sb.append(", ")
            }
        }
        return sb.toString()
    }


    fun openDetailsScreen(item: OrderListItem) {
        val bundle = Bundle()
        bundle.putString(ARG_ORDER_ID, item.orderId)
        mNavController.navigate(R.id.action_orderListFragment_to_orderDetailFragment, bundle)
    }

    override fun onDestroyView() {
        disposable.clear()
        super.onDestroyView()
    }
}