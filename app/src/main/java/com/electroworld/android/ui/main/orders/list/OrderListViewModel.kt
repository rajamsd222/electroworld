package com.electroworld.android.ui.main.orders.list

import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.model.orderlist.OrderListModel
import com.electroworld.android.repository.OMSRepository
import io.reactivex.Single
import javax.inject.Inject

class OrderListViewModel @Inject constructor(private val repoOMS: OMSRepository) : ViewModel() {

    fun makeOrderListReq(page: Int, status: Int, is_return: Int): Single<ApiResponse<OrderListModel>> {
        return repoOMS.getOrderList(page, status,is_return)
    }
}