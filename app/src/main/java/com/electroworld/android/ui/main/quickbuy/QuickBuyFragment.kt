package com.electroworld.android.ui.main.quickbuy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.customclass.LoadMoreNestedScrollView
import com.electroworld.android.databinding.FragmentQuickBuyBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.HomeModel
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.SetFavModel
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.products.ProductListAdapter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.toolbar_title_cart.view.*
import javax.inject.Inject

class QuickBuyFragment : DaggerFragment() {

    private lateinit var viewModel: QuickBuyViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding : FragmentQuickBuyBinding

    var mAdapter: ProductListAdapter? = null
    @Inject
    lateinit var providersFactory: ViewModelProviderFactory
    @Inject
    lateinit var myPref: PreferencesManager

    var currentModel: ProductModel? = null
    var currentPos = -1
    var setfavValue = 0
    private var pageNo = 1
    private var isLoadMore = true
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_quick_buy, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pageNo = 1
        viewModel = ViewModelProvider(this,providersFactory).get(QuickBuyViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentQuickBuyBinding.bind(view)
        binding.ab.tvABTitle.text = getString(R.string.title_quick_buy)
        binding.ab.ivBack.setOnClickListener(View.OnClickListener {
            mNavController.navigateUp()
        })
        initRecyclerView()
        binding.ab.ivSearch.setGone()
        binding.ab.iconCart.setOnClickListener(View.OnClickListener {
            mNavController.navigate(R.id.action_navigation_quick_buy_to_cartListFragment)
        })
        getPlaceHolderDetails()
        viewModel.getFavResponse().observe(viewLifecycleOwner) { res -> consumeFavResponse(res) }
    }

    private fun initRecyclerView(){
        mAdapter = ProductListAdapter(this, requireActivity() as MainActivity, myPref)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = mAdapter
        val loadMoreNestedScrollView = LoadMoreNestedScrollView(binding.recyclerView.layoutManager as LinearLayoutManager)
        loadMoreNestedScrollView.setOnLoadMoreListener {
            if (isLoadMore) {
                pageNo = pageNo.plus(1)
                getPlaceHolderDetails()
            }
        }
        binding.nestedScrollView.setOnScrollChangeListener(loadMoreNestedScrollView)
    }
    private fun getPlaceHolderDetails(){
        viewModel.getQuickBuyDataRes(
            requireActivity(),
            "quick_buy",
                pageNo
        )?.observe(viewLifecycleOwner,
            Observer<ApiResponse<List<HomeModel>>?> { res ->
                if (res != null) {
                    if (res.http_code == 200) {
                        res.data.forEach {
                            if (it.type.equals("3")) {
                               // showProducts(binding.contentLayout, it.placeholders,it)
                                if (it.placeholders != null && it.placeholders.isNotEmpty()) {
                                    mAdapter?.setItems(it.placeholders.get(0).products)
                                    isLoadMore = true
                                }
                                else{
                                    isLoadMore = false
                                }
                            }
                        }

                    } else {
                        CustomDialog.show(requireActivity(),res.message, AlertType.ERROR)
                    }
                }
                updateUI()
            })
    }
    private fun updateUI() {
        if(mAdapter?.itemCount == 0){
            binding.tvEmptyMsg.setVisible()
        }
        else{
            binding.tvEmptyMsg.setGone()
        }
    }


    fun setAsFavouriteProduct(pos: Int, item: ProductModel) {
        currentModel = item
        currentPos = pos
        setfavValue = if (item.is_favourite == 1) 0 else 1
        viewModel.setAsFav(
            item.warehouse_product_id,
            item.product_id,
            setfavValue,
            item.group_id
        )
    }

    private fun consumeFavResponse(res: ApiResource<ApiResponse<SetFavModel>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            if (currentModel != null && currentPos != -1) {
                                currentModel!!.is_favourite = setfavValue
                                mAdapter?.updateItem(currentPos, currentModel!!)
                                (activity as MainActivity?)?.setFavouriteCount(res.data.data.total_fav_count)
                            }
                        } else {
                            CustomDialog.show(requireActivity(),res.data.message,AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(),res.message,AlertType.SERVER_ERROR)
                }
            }
        }
    }

    fun doAddToCart(model: ProductModel, isAdd: Boolean, isBulkUpdate: Boolean, selectedQty: Int) {
        CartUtils.addToCartProductList(requireActivity() as MainActivity,myPref, model, isAdd, isBulkUpdate, selectedQty)
        mAdapter?.notifyDataSetChanged()
        updateCartCountUI()
    }

    private fun updateCartCountUI() {
         binding.ab.tvCartCount.text = myPref.getCartCount().toString()
         when {
             myPref.getCartCount()>0 -> {
                 binding.ab.tvCartCount.visibility = View.VISIBLE
             }
             else -> {
                 binding.ab.tvCartCount.visibility = View.GONE
             }
         }
    }

    override fun onResume() {
        super.onResume()
        updateCartCountUI()
    }

    fun openDetailsScreen(model: ProductModel){
        val bundle = Bundle()
        bundle.putString("name", model.name_en)
        bundle.putString("group_id", model.group_id)
        bundle.putString("warehouse_product_id", model.warehouse_product_id)
        mNavController.navigate(R.id.action_navigation_quick_buy_to_productDetailFragment, bundle)
    }

    fun getProductInfoByVariantId(pos: Int, model: ProductModel, warehouseProductId: String) {
        viewModel.getProductInfoRes(
            requireActivity(),
            warehouseProductId
        )?.observe(viewLifecycleOwner, object : Observer<ApiResponse<ProductModel>?> {
            override fun onChanged(res: ApiResponse<ProductModel>?) {
                if (res != null) {
                    if (res.http_code === 200) {
                        if (res.data != null) {
                            val item: ProductModel? = res.data
                            if (item != null) {
                                item.group_variants = model.group_variants
                                mAdapter?.updateItem(pos, item)
                            }
                            mAdapter?.notifyDataSetChanged()
                        }
                    } else {
                        CustomDialog.show(requireActivity(),res.message,AlertType.ERROR)
                    }
                }
            }
        })
    }

}