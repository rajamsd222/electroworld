package com.electroworld.android.ui.main.cart

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.electroworld.android.model.ProductModel
import com.electroworld.android.ui.main.cart.models.CartItem

object CartRepo {

    private var mutableCart = MutableLiveData<List<CartItem>?>()

    fun getCart(): LiveData<List<CartItem>?> {
        if(mutableCart.value == null){
            initCart()
        }
        mutableCart.value?.forEach {
            Log.e(""+it.productModel.name_en," => "+ it.quantity)
        }
        return mutableCart
    }

    private fun initCart() {
        mutableCart.value = ArrayList()
    }

    fun addItemsToCart(productModel: ProductModel, quantity: Int): Boolean {
        if (mutableCart.value == null) {
            initCart()
        }
        val cartItemsList = mutableCart.value?.toMutableList()
        cartItemsList?.forEachIndexed { index, cartItem ->
            if (cartItem.productModel.warehouse_product_id == productModel.warehouse_product_id) {
                if(cartItem.quantity<=0){
                    removeCartItem(index)
                    return true
                }
                cartItem.quantity = cartItem.quantity + quantity
                mutableCart.value = cartItemsList
                return true
            }
        }
        cartItemsList?.add(CartItem(productModel, quantity))
        mutableCart.value = cartItemsList
        return true
    }

    private fun removeCartItem(cartItemIndex: Int){
        if (mutableCart.value == null) {
            return
        }
        val cartItemsList = mutableCart.value?.toMutableList()
        cartItemsList?.removeAt(cartItemIndex)
        mutableCart.value = cartItemsList
    }

    fun removeItemsFromCart(cartItem: CartItem){
        if (mutableCart.value == null) {
            return
        }
        val cartItemsList = mutableCart.value?.toMutableList()
        cartItemsList?.remove(cartItem)
        mutableCart.value = cartItemsList
    }

}