package com.electroworld.android.ui.main.notifications

import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.repository.OMSRepository
import io.reactivex.Single
import javax.inject.Inject

class NotificationViewModel  @Inject constructor(private val repo : OMSRepository) : ViewModel() {

    fun getNotificationList(): Single<ApiResponse<List<NotificationModel>>> {
        return repo.makeNotificationReq()
    }
}