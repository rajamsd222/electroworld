package com.electroworld.android.ui.main.products.search

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.ProductApiModel
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.SearchHistoryModel
import com.electroworld.android.model.SetFavModel
import com.electroworld.android.utils.AppUtils
import com.electroworld.android.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class SearchViewModel @Inject constructor(val mainApi: MainApi,val myPref: PreferencesManager) : ViewModel() {


    private val disposable = CompositeDisposable()
    private val responseLiveData = MutableLiveData<ApiResource<ApiResponse<ProductApiModel>>>()

    private val responseLiveDataFav = MutableLiveData<ApiResource<ApiResponse<SetFavModel>>>()

    private fun searchHistory(
        context: Context,
        android_device_id: String
    ): MutableLiveData<ApiResponse<List<SearchHistoryModel>>?>? {
        val res: MutableLiveData<ApiResponse<List<SearchHistoryModel>>?> =
            MutableLiveData<ApiResponse<List<SearchHistoryModel>>?>()
        val dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.getSearchHistory(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            android_device_id,
            ""
        )
            .enqueue(object : Callback<ApiResponse<List<SearchHistoryModel>>?> {
                override fun onResponse(
                    call: Call<ApiResponse<List<SearchHistoryModel>>?>,
                    response: Response<ApiResponse<List<SearchHistoryModel>>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponse<List<SearchHistoryModel>>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun searchHistoryRes(
        context: Context,
        android_device_id: String
    ): LiveData<ApiResponse<List<SearchHistoryModel>>?>? {
        return searchHistory(
            context,
            android_device_id
        )
    }

    fun setAsFav(
        warehouse_product_id: String,
        product_id: String,
        is_favourite: Int,
        group_id: String
    ){
        disposable.clear()
        disposable.add(mainApi.setFav(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            warehouse_product_id,
            product_id,
            is_favourite,
            group_id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveDataFav.setValue(
                    ApiResource.loading<ApiResponse<SetFavModel>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveDataFav.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveDataFav.setValue(
                    ApiResource.error<ApiResponse<SetFavModel>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getFavResponse(): MutableLiveData<ApiResource<ApiResponse<SetFavModel>>> {
        return responseLiveDataFav
    }

    private fun getProductInfo(
        context: Context,
        warehouse_product_id: String
    ): MutableLiveData<ApiResponse<ProductModel>?>? {
        val res: MutableLiveData<ApiResponse<ProductModel>?> =
            MutableLiveData<ApiResponse<ProductModel>?>()
        val dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.getProductInfoByVariant(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            warehouse_product_id
        )
            .enqueue(object : Callback<ApiResponse<ProductModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<ProductModel>?>,
                    response: Response<ApiResponse<ProductModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponse<ProductModel>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun getProductInfoRes(
        context: Context,
        warehouse_product_id: String
    ): LiveData<ApiResponse<ProductModel>?>? {
        return getProductInfo(
            context,
            warehouse_product_id
        )
    }

    fun getList(category_id: String, level: String
        , page: Int, search_product: String, android_device_id: String
    ){
        disposable.add(mainApi.getProductListBySearch(
            Constants.HEADER_TOKEN,myPref.getPrefKeyAccessToken(),myPref.getPrefKeyCustomerId(),myPref.getPrefKeyStoreId(),category_id,
            level,page,search_product,android_device_id,myPref.getPrefKeyVendorId(), myPref.getPrefKeyLanguage())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.setValue(
                    ApiResource.loading<ApiResponse<ProductApiModel>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveData.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveData.setValue(
                    ApiResource.error<ApiResponse<ProductApiModel>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getApiResponse(): MutableLiveData<ApiResource<ApiResponse<ProductApiModel>>> {
        return responseLiveData
    }

    override fun onCleared() {
        disposable.clear()
    }

}