package com.electroworld.android.ui.main.products.filter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.databinding.RowRadioButtonBinding
import com.electroworld.android.model.filter.FilterData

class FilterSingleSelectionAdapter (
    val context: Activity,
    list: List<FilterData>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items = ArrayList<FilterData>()

    init {
        items = list as ArrayList<FilterData>
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowRadioButtonBinding =
            RowRadioButtonBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(private val binding: RowRadioButtonBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            val item: FilterData = items[position]
            binding.item = item
            binding.executePendingBindings()
            binding.rb.tag = position
            binding.rb.setOnClickListener {
                val pos = it.tag as Int
                items.forEachIndexed { index, filterData ->
                    filterData.isSelected = pos == index
                }
                notifyDataSetChanged()
            }
        }
    }
}