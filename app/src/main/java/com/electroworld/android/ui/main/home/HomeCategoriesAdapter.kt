package com.electroworld.android.ui.main.home

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.electroworld.android.R
import com.electroworld.android.databinding.RowHomeCategoryBinding
import com.electroworld.android.model.Placeholder
import com.electroworld.android.ui.main.products.GroupVariantsModel
import com.electroworld.android.ui.main.products.list.ProductListFragment
import java.util.*

class HomeCategoriesAdapter(
    mNavController: NavController,
    posts: List<Placeholder>,
    fragment: Fragment,
    val context: Activity
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var posts: List<Placeholder> = ArrayList<Placeholder>()
    var parentFragment: Fragment
    var bottomSheetDialog: BottomSheetDialog? = null
    val list: MutableList<GroupVariantsModel> = ArrayList()
    var mNavController : NavController = mNavController
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowHomeCategoryBinding =
            RowHomeCategoryBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    inner class ViewHolder(binding: RowHomeCategoryBinding) : RecyclerView.ViewHolder(binding.root) {
        private val binding: RowHomeCategoryBinding = binding
        fun bind(position: Int) {
            val model: Placeholder = posts[position]
            binding.item = model
            binding.executePendingBindings()
            binding.parentView.setTag(model)
            binding.parentView.setOnClickListener {
                val tag = it.tag as Placeholder?
                val bundle = ProductListFragment.getPlaceHolderBundles(tag?.id,tag?.title)
                mNavController.navigate(R.id.action_navigation_home_to_productListFragment, bundle)
            }
        }

    }

    init {
        this.posts = posts
        parentFragment = fragment
    }

}
