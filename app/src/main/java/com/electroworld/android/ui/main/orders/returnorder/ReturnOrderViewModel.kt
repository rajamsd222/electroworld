package com.electroworld.android.ui.main.orders.returnorder

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.ImagesModel
import com.electroworld.android.model.UserModel
import com.electroworld.android.repository.OMSRepository
import com.electroworld.android.repository.PIMSRepository
import com.electroworld.android.utils.AppUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import javax.inject.Inject
import okhttp3.MultipartBody





class ReturnOrderViewModel @Inject constructor(
    val mainApi: MainApi, val myPref: PreferencesManager,
    private val repoPIMS: PIMSRepository, private val repoOMS: OMSRepository
) : ViewModel() {

    fun initReturnReqDynamicNew(
        context: Context?,
        token: String?,
        userId: String?,
        vendorId: String?,
        order_id: String?,
        order_product_ids: String?,
        comments: String?,
        image_count: String?,
        imageList: MutableList<ImagesModel>
    ): MutableLiveData<ApiResponse<UserModel>?>? {
        val res: MutableLiveData<ApiResponse<UserModel>?> =
            MutableLiveData<ApiResponse<UserModel>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()

        var items = arrayListOf<MultipartBody.Part?>()
        var imageFile: MultipartBody.Part? = null
        var imageName = ""
        imageList.forEachIndexed { index, imagesModel ->
            val uploadedFile = File(imagesModel.image) as File
            if (uploadedFile != null) {
                if (uploadedFile.exists()) {
                    imageName = "image" + (index+1)
                    val requestFile =
                        uploadedFile.asRequestBody("multipart/form-data".toMediaTypeOrNull())
                    imageFile =
                        MultipartBody.Part.createFormData(imageName, uploadedFile.name, requestFile)

                    items.add(imageFile)
                }
            }
        }

        val token_ = token!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val UserId_ = userId!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val vendorId_ = vendorId!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val order_id_ = order_id!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val order_product_ids = order_product_ids!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val comments = comments!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val image_count = image_count!!.toRequestBody("text/plain".toMediaTypeOrNull())
        mainApi.makeReturnOrderNew(
            token_,
            UserId_,
            vendorId_,
            order_id_,
            order_product_ids,
            comments,
            image_count,
            items
        )
            .enqueue(object : Callback<ApiResponse<UserModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<UserModel>?>,
                    response: Response<ApiResponse<UserModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful()) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponse<UserModel>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun makeReturnOrderRes(
        context: Context?,
        token: String?,
        userId: String?,
        vendorId: String?,
        order_id: String?,
        order_product_ids: String?,
        comments: String?,
        image_count: String?,
        imageList: MutableList<ImagesModel>
    ): MutableLiveData<ApiResponse<UserModel>?>? {
        return initReturnReqDynamicNew(
            context, token, userId, vendorId, order_id,
            order_product_ids, comments, image_count, imageList
        )
    }

}