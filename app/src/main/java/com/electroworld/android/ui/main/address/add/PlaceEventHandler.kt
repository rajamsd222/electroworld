package com.electroworld.android.ui.main.address.add

interface PlaceEventHandler {
    fun handle(placeid: String?, address: String?)
}