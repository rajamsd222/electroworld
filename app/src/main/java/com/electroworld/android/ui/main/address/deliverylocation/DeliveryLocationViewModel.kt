package com.electroworld.android.ui.main.address.deliverylocation

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.StoreModel
import com.electroworld.android.utils.AppUtils
import com.electroworld.android.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class DeliveryLocationViewModel  @Inject constructor(val mainApi: MainApi, val myPref: PreferencesManager) : ViewModel() {



    private fun getStoreInfo(
        context: Context,
        latitude: Double,
        longitude: Double
    ): MutableLiveData<ApiResponse<StoreModel>?>? {
        val res: MutableLiveData<ApiResponse<StoreModel>?> =
            MutableLiveData<ApiResponse<StoreModel>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.getStoreInfo(
            Constants.HEADER_TOKEN,
            latitude,
            longitude,
            myPref.getPrefKeyVendorId()
        ).enqueue(object : Callback<ApiResponse<StoreModel>?> {
            override fun onResponse(
                call: Call<ApiResponse<StoreModel>?>,
                response: Response<ApiResponse<StoreModel>?>
            ) {
                dialog.dismiss()
                if (response.isSuccessful) {
                    res.setValue(response.body())
                }
            }

            override fun onFailure(call: Call<ApiResponse<StoreModel>?>, t: Throwable) {
                dialog.dismiss()
                res.setValue(null)
            }
        })
        return res
    }

    fun getStoreInfoResponse(
        context: Context,
        latitude: Double,
        longitude: Double
    ): LiveData<ApiResponse<StoreModel>?>? {
        return getStoreInfo(
            context, latitude, longitude
        )
    }

    private val disposable = CompositeDisposable()
    private val resGetPlaceInfoById = MutableLiveData<ApiResource<JsonObject>>()
    fun getInfoByPlaceId(url: String){
        disposable.clear()
        disposable.add(mainApi.getPlaceInfoByUrl(url)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                resGetPlaceInfoById.setValue(
                    ApiResource.loading<JsonObject?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    resGetPlaceInfoById.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                resGetPlaceInfoById.setValue(
                    ApiResource.error<JsonObject?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getResultByPlaceId(): MutableLiveData<ApiResource<JsonObject>> {
        return resGetPlaceInfoById
    }



    private fun getAddressList(url: String?): MutableLiveData<JsonObject>? {
        val res: MutableLiveData<JsonObject> =
            MutableLiveData<JsonObject>()
        mainApi.doPlaceSearchByUrlNew(url)
            .enqueue(object : Callback<JsonObject> {
                override fun onResponse(
                    call: Call<JsonObject>,
                    response: Response<JsonObject>
                ) {
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    res.setValue(null)
                }
            })
        return res
    }
    fun getAddressListRes(url: String?): LiveData<JsonObject>? {
        return getAddressList(url)
    }

}