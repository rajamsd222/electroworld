package com.electroworld.android.ui.main.cart.models

import com.electroworld.android.model.ProductModel

data class CartSingleItem(
    var productModel: ProductModel,
    var quantity: Int = 0
)