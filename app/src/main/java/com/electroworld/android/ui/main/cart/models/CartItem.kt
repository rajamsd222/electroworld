package com.electroworld.android.ui.main.cart.models

import com.electroworld.android.model.ProductModel

data class CartItem(
    var productModel: ProductModel,
    var quantity : Int
)