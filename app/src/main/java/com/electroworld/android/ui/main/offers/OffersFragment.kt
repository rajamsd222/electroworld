package com.electroworld.android.ui.main.offers

import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.alert.ProgressUtil
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.customclass.LoadMoreNestedScrollView
import com.electroworld.android.databinding.FragmentOffersBinding
import com.electroworld.android.databinding.ListItemSortByBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.SetFavModel
import com.electroworld.android.model.filter.FilterApiRes
import com.electroworld.android.model.filter.SortBy
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.products.ProductListAdapter
import com.electroworld.android.ui.main.products.filter.FilterDialog
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.ScreenType
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.toolbar_title_cart.view.*
import javax.inject.Inject

class OffersFragment : DaggerFragment() {

    private lateinit var viewModel: OffersViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentOffersBinding

    var bottomSheetDialog: BottomSheetDialog? = null
    var mAdapter: ProductListAdapter? = null

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager
    private var currentModel: ProductModel? = null
    private var currentPos = -1
    private var setfavValue = 0
    private var pageNo = 1
    private var isLoadMore = true


    private var sort_by = "most"
    private var sortTitle = "Most Popular"
    private var order_by = ""
    private var brand_id = ""
    private var dynamicParams: HashMap<String, String> = HashMap()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_offers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pageNo = 1
        setupView(view)
        setupClickListeners(view)
        viewModel = ViewModelProvider(this, providersFactory).get(OffersViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentOffersBinding.bind(view)
        binding.ab.tvABTitle.text = getString(R.string.title_offers)
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        initRecyclerView()
        getPlaceHolderDetails()

        viewModel.getFavResponse().removeObservers(viewLifecycleOwner)
        viewModel.getFavResponse().observe(viewLifecycleOwner) { res -> consumeFavResponse(res) }

        binding.ab.ivSearch.setGone()
        binding.ab.iconCart.setOnClickListener(View.OnClickListener {
            mNavController.navigate(R.id.action_navigation_offers_to_cartListFragment)
        })

        viewModel.getFiltersResponse().observe(viewLifecycleOwner) { res ->
            consumeFilterResponse(
                res
            )
        }


        if ((activity as MainActivity).mFiltersList.isEmpty() ||
            (activity as MainActivity).mSortByList.isEmpty()
        ) {
            viewModel.getFiltersList()
        } else {
            for (item in (activity as MainActivity).mSortByList) {
                if (item.default == "1") {
                    sortTitle = item.title
                    sort_by = item.key
                    order_by = item.order_by
                }
            }
        }

        binding.layoutSortBy.setOnClickListener {
            showSortByDialog()
        }


        binding.ivGridListToggle1.setOnClickListener {
            toggleViews()
        }


        binding.layoutFilterBy.setOnClickListener {
            if ((activity as MainActivity).mFiltersList != null) {
                val avcDialog = FilterDialog(
                    requireActivity() as MainActivity,
                    object : FilterDialog.FilterOKClickListener {
                        override fun onOKClick(view: View?, tag: String?) {
                            if (tag.equals("apply")) {
                                dynamicParams.clear()
                                (activity as MainActivity).mFiltersList.forEach {
                                    var key = it.key
                                    val ids = ArrayList<String>()//Creating an empty arraylist
                                    it.filter_data.forEach { data ->
                                        if (data.isSelected) {
                                            ids.add(data.id.toString())
                                        }
                                    }
                                    if (ids.size > 0) {
                                        if (it.type.equals("single")) {
                                            dynamicParams[key] =
                                                TextUtils.join(",", ids)
                                        } else {
                                            dynamicParams[key] =
                                                "[" + TextUtils.join(",", ids) + "]"
                                        }

                                    }
                                }
                                if (dynamicParams.size > 0) {
                                    binding.txtFilterBy.text =
                                        getString(R.string.filter_by) + " ( " + dynamicParams.size + " )"
                                } else {
                                    binding.txtFilterBy.text = getString(R.string.filter_by)
                                }
                                sortTitle = Constants.SORT_BY_TITLE
                                sort_by = Constants.SORT_BY
                                order_by = Constants.ORDER_BY
                                pageNo = 1
                                getPlaceHolderDetails()

                            } else if (tag.equals("clear")) {
                                clearFilter()
                                binding.txtFilterBy.text = getString(R.string.filter_by)
                                getPlaceHolderDetails()
                            }
                        }
                    }, ScreenType.PLACE_HOLDER
                )
                avcDialog.setItems(
                    (activity as MainActivity).mFiltersList,
                    (activity as MainActivity).mSortByList,
                    sortTitle
                )
                avcDialog.showDialog()
            }
        }


    }

    public fun toggleViews() {

        if (mAdapter != null) {
            val isSwitched = mAdapter!!.toggleItemViewType()
            binding.recyclerView.layoutManager =
                if (isSwitched) LinearLayoutManager(requireContext()) else GridLayoutManager(
                    requireContext(),
                    2
                )
            mAdapter?.notifyDataSetChanged()

            binding.ivGridListToggle1.setImageResource(
                if (isSwitched) R.mipmap.ic_stat_grid_view else R.mipmap.ic_stat_list
            )
        }
    }


    fun showSortByDialog() {
        val dialogView: View =
            requireActivity().layoutInflater.inflate(R.layout.dialog_variant, null)
        bottomSheetDialog =
            BottomSheetDialog(requireActivity(), R.style.CustomBottomSheetDialog)
        bottomSheetDialog!!.setContentView(dialogView)
        val tvTitle = bottomSheetDialog!!.findViewById<TextView>(R.id.tvTitle)
        val AppTitle1 = bottomSheetDialog!!.findViewById<TextView>(R.id.AppTitle1)
        val recyclerView = bottomSheetDialog!!.findViewById<RecyclerView>(R.id.recycler_view)

        val adapter =
            object : GenericAdapter<SortBy, ListItemSortByBinding>(R.layout.list_item_sort_by) {
                override fun onBindData(
                    model: SortBy?,
                    position: Int,
                    dataBinding: ListItemSortByBinding?
                ) {
                    dataBinding!!.item = model
                    if (sortTitle == model!!.title) {
                        dataBinding.parentLayout.setBackgroundResource(R.drawable.variant_selected_bg)
                        dataBinding.tvName.setTextColor(Color.WHITE)
                        dataBinding.ivLeft.setImageResource(R.mipmap.circle_tick_icon)
                    } else {
                        dataBinding.parentLayout.setBackgroundResource(R.drawable.variant_default_bg)
                        dataBinding.ivLeft.setImageResource(R.mipmap.dot_icon)
                    }

                }

                override fun onItemClick(model: SortBy?, position: Int) {
                    sort_by = model!!.key
                    sortTitle = model.title
                    binding.tvSortBy.text = sortTitle
                    order_by = model.order_by
                    bottomSheetDialog!!.dismiss()
                    pageNo = 1
                    getPlaceHolderDetails()
                }
            }
        recyclerView?.adapter = adapter
        adapter.items = (requireActivity() as MainActivity).mSortByList
        AppTitle1!!.setGone()
        tvTitle!!.text = getString(R.string.sort_by)
        bottomSheetDialog!!.show()
    }

    private fun clearFilter() {
        dynamicParams.clear()
        Constants.SORT_BY = ""
        (activity as MainActivity).mFiltersList.forEach {
            it.filter_data.forEach { data ->
                data.isSelected = false
            }
        }
    }


    private fun setupClickListeners(view: View) {

    }

    private fun setupView(view: View) {

    }


    private fun consumeFilterResponse(res: ApiResource<ApiResponse<FilterApiRes>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            (activity as MainActivity).mFiltersList = res.data.data.filter_list
                            (activity as MainActivity).mSortByList = res.data.data.sort_by
                            for (item in (activity as MainActivity).mSortByList) {
                                if (item.default == "1") {
                                    sortTitle = item.title
                                    sort_by = item.key
                                    order_by = item.order_by
                                }
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }


    private fun initRecyclerView() {
        mAdapter = ProductListAdapter(this, requireActivity() as MainActivity, myPref)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = mAdapter
        val loadMoreNestedScrollView =
            LoadMoreNestedScrollView(binding.recyclerView.layoutManager as LinearLayoutManager)
        loadMoreNestedScrollView.setOnLoadMoreListener {
            if (isLoadMore) {
                pageNo = pageNo.plus(1)
                getPlaceHolderDetails()
            }
        }
        binding.nestedScrollView.setOnScrollChangeListener(loadMoreNestedScrollView)
    }

    private fun getPlaceHolderDetails() {
        ProgressUtil.showLoading(requireActivity())
        viewModel.getOffersDataRes(
            requireActivity(),
            "offer_zone",
            pageNo,
            sort_by,
            order_by,
            brand_id,
            dynamicParams
        )?.observe(viewLifecycleOwner,
            { res ->
                ProgressUtil.hideLoading()
                if (res != null) {
                    if (res.http_code == 200) {
                        if (pageNo == 1) {
                            mAdapter?.clearAll()
                        }
                        res.data.forEach {
                            if (it.type.equals("3")) {
                                if (it.placeholders != null && it.placeholders.isNotEmpty()) {
                                    mAdapter?.setItems(it.placeholders[0].products)
                                    isLoadMore = it.placeholders[0].products.isNotEmpty()
                                }
                            }
                        }
                    } else {
                        CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                    }
                }
                updateUI()
            }
        )


    }

    private fun updateUI() {
        if (mAdapter?.itemCount == 0) {
            binding.tvEmptyMsg.setVisible()
        } else {
            binding.tvEmptyMsg.setGone()
        }
    }

    fun setAsFavouriteProduct(pos: Int, item: ProductModel) {
        currentModel = item
        currentPos = pos
        setfavValue = if (item.is_favourite == 1) 0 else 1
        viewModel.setAsFav(
            item.warehouse_product_id,
            item.product_id,
            setfavValue,
            item.group_id
        )
    }

    private fun consumeFavResponse(res: ApiResource<ApiResponse<SetFavModel>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            if (currentModel != null && currentPos != -1) {
                                currentModel!!.is_favourite = setfavValue
                                mAdapter?.updateItem(currentPos, currentModel!!)
                                (activity as MainActivity?)?.setFavouriteCount(res.data.data.total_fav_count)
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }

    fun doAddToCart(model: ProductModel, isAdd: Boolean, isBulkUpdate: Boolean, selectedQty: Int) {
        CartUtils.addToCartProductList(
            requireActivity() as MainActivity,
            myPref,
            model,
            isAdd,
            isBulkUpdate,
            selectedQty
        )
        mAdapter?.notifyDataSetChanged()
        updateCartCountUI()
    }

    private fun updateCartCountUI() {
        binding.ab.tvCartCount.text = myPref.getCartCount().toString()
        when {
            myPref.getCartCount() > 0 -> {
                binding.ab.tvCartCount.visibility = View.VISIBLE
            }
            else -> {
                binding.ab.tvCartCount.visibility = View.GONE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        updateCartCountUI()
    }

    fun openDetailsScreen(model: ProductModel) {
        val bundle = Bundle()
        bundle.putString("name", model.name_en)
        bundle.putString("group_id", model.group_id)
        bundle.putString("warehouse_product_id", model.warehouse_product_id)
        mNavController.navigate(R.id.action_navigation_offers_to_productDetailFragment, bundle)
    }

    fun getProductInfoByVariantId(pos: Int, model: ProductModel, warehouseProductId: String) {
        viewModel.getProductInfoRes(
            requireActivity(),
            warehouseProductId
        )?.observe(viewLifecycleOwner, object : Observer<ApiResponse<ProductModel>?> {
            override fun onChanged(res: ApiResponse<ProductModel>?) {
                if (res != null) {
                    if (res.http_code === 200) {
                        if (res.data != null) {
                            val item: ProductModel? = res.data
                            if (item != null) {
                                item.group_variants = model.group_variants
                                mAdapter?.updateItem(pos, item)
                            }
                            mAdapter?.notifyDataSetChanged()
                        }
                    } else {
                        CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                    }
                }
            }
        })
    }

}