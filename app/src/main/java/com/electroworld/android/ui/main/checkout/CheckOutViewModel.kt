package com.electroworld.android.ui.main.checkout

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.*
import com.electroworld.android.repository.OMSRepository
import com.electroworld.android.ui.UIHandler
import com.electroworld.android.utils.AppUtils
import com.electroworld.android.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class CheckOutViewModel @Inject constructor(
    val mainApi: MainApi, val myPref: PreferencesManager, private val omsRepository: OMSRepository
) : ViewModel() {



    val errorMessage = MutableLiveData<String>()
    val uiHandlerLiveData = MutableLiveData<UIHandler>()

    val deliverySlotApi = MutableLiveData<DeliverySlotApiModel>()

    private val disposable = CompositeDisposable()
    private val responseLiveDataPayments =
        MutableLiveData<ApiResource<ApiResponse<List<PaymentModel>>>>()
    private val responseLiveData =
        MutableLiveData<ApiResource<ApiResponse<ShippingChargesResponse>>>()

    private fun checkValidOrder(
        context: Context,
        addressId: String,
        cartList: JSONArray,
        warehouse_id: String
    ): MutableLiveData<ApiResponse<CheckValidOrderModel>?>? {
        val res: MutableLiveData<ApiResponse<CheckValidOrderModel>?> =
            MutableLiveData<ApiResponse<CheckValidOrderModel>?>()
        val dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.chkValidOrder(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            addressId,
            cartList,
            warehouse_id,
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyLanguage()
        )
            .enqueue(object : Callback<ApiResponse<CheckValidOrderModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<CheckValidOrderModel>?>,
                    response: Response<ApiResponse<CheckValidOrderModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(
                    call: Call<ApiResponse<CheckValidOrderModel>?>,
                    t: Throwable
                ) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun checkValidOrderRes(
        context: Context,
        addressId: String,
        cartList: JSONArray,
        warehouse_id: String
    ): LiveData<ApiResponse<CheckValidOrderModel>?>? {
        return checkValidOrder(
            context,
            addressId,
            cartList,
            warehouse_id
        )
    }



    fun getDeliverySlots(){
        disposable.add(
            omsRepository.getDeliverySlots()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<DeliverySlotApiModel>>() {
                    override fun onSuccess(res: ApiResponse<DeliverySlotApiModel>) {
                        loading.value = false
                        uiHandlerLiveData.value = UIHandler("","",false)
                        if(res.http_code==200){
                            deliverySlotApi.value = res.data
                        }
                        else{
                            apiError.value = res.message
                        }
                    }

                    override fun onError(e: Throwable) {
                        uiHandlerLiveData.value = UIHandler("","",false)
                        errorMessage.value = e.message
                        loading.value = false
                    }
                })
        )
    }


/*

    fun getShippingCharges(subtotal: Double, order_type: String
    ){
        disposable.add(mainApi.getShippingSlots(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            subtotal,
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyWareHouseId(),
            order_type
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.setValue(
                    ApiResource.loading<ApiResponse<ShippingModel>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveData.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveData.setValue(
                    ApiResource.error<ApiResponse<ShippingModel>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getShippingSlotApiResponse(): MutableLiveData<ApiResource<ApiResponse<ShippingModel>>> {
        return responseLiveData
    }*/

    private fun getPromocodeInfo(
        context: Context,
        promocode: String,
        subtotal: Double
    ): MutableLiveData<ApiResponse<PromoCodeModel>?>? {
        val res: MutableLiveData<ApiResponse<PromoCodeModel>?> =
            MutableLiveData<ApiResponse<PromoCodeModel>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.getPromocodeInfo(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            promocode,
            subtotal,
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyLanguage()
        )
            .enqueue(object : Callback<ApiResponse<PromoCodeModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<PromoCodeModel>?>,
                    response: Response<ApiResponse<PromoCodeModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponse<PromoCodeModel>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun getPromocodeInfoRes(
        context: Context,
        promocode: String,
        subtotal: Double
    ): LiveData<ApiResponse<PromoCodeModel>?>? {
        return getPromocodeInfo(
            context,
            promocode,
            subtotal
        )
    }


    private fun doPlaceOrder(
        context: Context,
        address_id: String,
        paymentMode: String,
        grand_total: Double,
        cfTokenNeed: String
    ): MutableLiveData<ApiResponse<OrderPlaceModel>?> {
        val res: MutableLiveData<ApiResponse<OrderPlaceModel>?> =
            MutableLiveData<ApiResponse<OrderPlaceModel>?>()
        val dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.placeOrder(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            myPref.getDeliveryAddressStoreId(),
            address_id,
            myPref.getDeliveryTimeType(),
            myPref.getFromTime(),
            myPref.getToTime(),
            paymentMode,
            myPref.getMobileNumber(),
            myPref.getPrefKeyAuthenticationId(),
            Constants.LATITUDE,
            Constants.LONGITUDE,
            myPref.getDeliveryCharge(),
            myPref.getSubTotal(),
            grand_total,
            myPref.getCartArray(),
            myPref.getWalletValue(),
            myPref.getPromoCode(),
            myPref.getGiftValue(),
            myPref.getGiftAmount(),
            myPref.getGiftFromMessage(),
            myPref.getGiftToMessage(),
            myPref.getGiftMessage(),
            myPref.getPickupFromStoreValue(),
            myPref.getGSTEnabledValue(),
            myPref.getGSTNumber(),
            myPref.getCompanyName(),
            cfTokenNeed,
            myPref.getDeliveryInstruction(),
            myPref.getBillingAddress(),
            myPref.getBillingPhone(),
            myPref.getBillingContactName(),
            if (myPref.getBillingChecked()) "1" else ""
        )
            .enqueue(object : Callback<ApiResponse<OrderPlaceModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<OrderPlaceModel>?>,
                    response: Response<ApiResponse<OrderPlaceModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(
                    call: Call<ApiResponse<OrderPlaceModel>?>,
                    t: Throwable
                ) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun doPlaceOrderRes(
        context: Context,
        address_id: String,
        paymentMode: String,
        grand_total: Double,
        cfTokenNeed: String
    ): LiveData<ApiResponse<OrderPlaceModel>?> {
        return doPlaceOrder(
            context,
            address_id,
            paymentMode,
            grand_total,
            cfTokenNeed
        )
    }


    val apiError = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()
    val _liveDataPaymentList = MutableLiveData<List<PaymentModel>>()

    fun makePaymentListReq(): MutableLiveData<List<PaymentModel>> {
        disposable.add(
            mainApi.getPayments(
                Constants.HEADER_TOKEN,
                myPref.getPrefKeyAccessToken(),
                myPref.getPrefKeyVendorId()
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<PaymentModeResponse>() {
                    override fun onSuccess(res: PaymentModeResponse) {
                        loading.value = false
                        if (res.http_code == 200) {
                            _liveDataPaymentList.value = res.data
                        } else {
                            apiError.value = res.message
                        }
                    }

                    override fun onError(e: Throwable) {
                        apiError.value = e.message
                        loading.value = false
                    }
                })
        )
        return _liveDataPaymentList
    }


    fun getShippingCharges(
        subtotal: Double, order_type: String
    ) {
        disposable.add(mainApi.getShippingSlots(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            subtotal,
            myPref.getPrefKeyVendorId(),
            myPref.getDeliveryAddressStoreId(),
            order_type,
            myPref.getCartDetailsForFOCProduct()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.setValue(
                    ApiResource.loading<ApiResponse<ShippingChargesResponse>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveData.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveData.setValue(
                    ApiResource.error<ApiResponse<ShippingChargesResponse>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getShippingSlotApiResponse(): MutableLiveData<ApiResource<ApiResponse<ShippingChargesResponse>>> {
        return responseLiveData
    }


    /* fun getList(){
         disposable.add(mainApi.getPayments(Constants.HEADER_TOKEN, myPref.getPrefKeyAccessToken(), myPref.getPrefKeyVendorId())
             .subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .doOnSubscribe {
                 responseLiveDataPayments.setValue(
                     ApiResource.loading<ApiResponse<List<PaymentModel>>?>(null)
                 )
             }
             .subscribe(
                 { userModelApiResponse ->
                     responseLiveDataPayments.setValue(
                         ApiResource.success(
                             userModelApiResponse
                         )
                     )
                 }
             ) { throwable ->
                 responseLiveDataPayments.setValue(
                     ApiResource.error<ApiResponse<List<PaymentModel>>?>(
                         throwable.message!!,
                         null
                     )
                 )
             })
     }*/

    fun getApiResponse(): MutableLiveData<ApiResource<ApiResponse<List<PaymentModel>>>> {
        return responseLiveDataPayments
    }

    override fun onCleared() {
        disposable.clear()
    }

}