package com.electroworld.android.ui.main.cart

import android.graphics.Color
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.ProductModel
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.models.CartSingleItem
import com.electroworld.android.ui.main.cart.models.CompareItem
import com.electroworld.android.ui.main.cart.models.LocalCartModel
import com.electroworld.android.ui.main.cart.models.LocalCompareModel
import com.electroworld.android.ui.main.orders.SummaryModel
import com.electroworld.android.ui.main.products.detail.ProductDetailFragment
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.MyUtils
import com.electroworld.android.utils.ToastUtil
import com.electroworld.android.utils.Utils
import com.google.android.material.snackbar.Snackbar
import java.util.*

object CartUtils {


    fun getOrderSummary(
        mainActivity: MainActivity,
        myPref: PreferencesManager,
        promotion_product_total_price: Double
    ): List<SummaryModel> {
        val list: MutableList<SummaryModel> = ArrayList()
        val model = myPref.getLocalCart()
        if (model != null) {
            val cartList = model.cartItemList
            var subTotal = 0.0
            var mrpTotal = 0.0
            cartList.forEach {
                subTotal += AppValidator.toDouble(it.productModel.discounted_price) * it.quantity
                mrpTotal += AppValidator.toDouble(it.productModel.price) * it.quantity
            }
            subTotal = MyUtils.formatCurrencyFromDoubleWithoutSymbol(subTotal)
            mrpTotal = MyUtils.formatCurrencyFromDoubleWithoutSymbol(mrpTotal)

            val discountAmount = subTotal - mrpTotal
            var promocodeDiscount = 0.0
            var promocodeTextLabel = mainActivity.getString(R.string.promocode_discount)
            if (model.promocode.length > 0) {
                if (model.promocodeDiscountPercentage > 0) {
                    promocodeDiscount = subTotal * model.promocodeDiscountPercentage / 100
                    promocodeTextLabel =
                        promocodeTextLabel + " (" + model.promocodeDiscountPercentage + "%)"
                } else if (model.promocodeDiscountAmount > 0) {
                    promocodeDiscount = model.promocodeDiscountAmount
                    promocodeTextLabel =
                        promocodeTextLabel + " (" + Utils.formatCurrency(promocodeDiscount) + ")"
                }
            }
            list.add(SummaryModel(mainActivity.getString(R.string.os_lbl_sub_total), mrpTotal))
            if (discountAmount != 0.0) {
                list.add(
                    SummaryModel(
                        mainActivity.getString(R.string.os_lbl_discount),
                        discountAmount
                    )
                )
            }
            list.add(
                SummaryModel(
                    mainActivity.getString(R.string.os_lbl_delivery_charges),
                    myPref.getDeliveryCharge()
                )
            )
            if (model.isGiftEnabled) {
                //Gift to others calculation
                list.add(
                    SummaryModel(
                        mainActivity.getString(R.string.os_lbl_gift_charges),
                        model.giftAmount
                    )
                )
            }
            if (promotion_product_total_price > 0.0) {
                //Promotion product
                list.add(
                    SummaryModel(
                        mainActivity.getString(R.string.os_lbl_promotion_product),
                        promotion_product_total_price
                    )
                )
            }
            if (promocodeDiscount > 0.0) {
                promocodeDiscount = promocodeDiscount * -1
                //Promo code
                list.add(SummaryModel(promocodeTextLabel, promocodeDiscount))
            }
            var grandTotal: Double = 0.0
            val giftAmount = if (model.isGiftEnabled) model.giftAmount else 0.0
            var walletAmount = myPref.getWalletAmount()
            if (model.isWalletEnabled) {
                grandTotal =
                    subTotal + promotion_product_total_price + myPref.getDeliveryCharge() + promocodeDiscount + giftAmount
                if (grandTotal < 0) {
                    grandTotal = 0.0
                }
                if (grandTotal >= walletAmount) {
                    grandTotal = grandTotal - walletAmount
                } else {
                    walletAmount = grandTotal
                    grandTotal = 0.0
                }
                if (walletAmount > 0.0) {
                    walletAmount = walletAmount * -1
                    list.add(
                        SummaryModel(
                            mainActivity.getString(R.string.os_lbl_wallet_amount),
                            walletAmount
                        )
                    )
                }
            } else {
                //Grand Total calculation
                grandTotal =
                    subTotal + promotion_product_total_price + myPref.getDeliveryCharge() + promocodeDiscount + giftAmount
                if (grandTotal < 0) {
                    grandTotal = 0.0
                }
            }
            list.add(
                SummaryModel(
                    mainActivity.getString(R.string.os_lbl_amount_payable),
                    grandTotal
                )
            )
        }
        return list
    }

    fun addPromocode(
        myPref: PreferencesManager,
        promocode: String,
        discountAmount: Double,
        discountPercent: Double
    ) {
        val model = myPref.getLocalCart()
        if (model != null) {
            model.promocode = promocode
            model.promocodeDiscountAmount = discountAmount
            model.promocodeDiscountPercentage = discountPercent
        }
        myPref.saveCartItems(model)
    }

    fun addDeliveryNotes(
        myPref: PreferencesManager,
        delivery_notes: String
    ) {
        val model = myPref.getLocalCart()
        if (model != null) {
            model.orderInstruction = delivery_notes
        }
        myPref.saveCartItems(model)
    }

    fun removeDeliveryNotes(
        myPref: PreferencesManager
    ) {
        val model = myPref.getLocalCart()
        if (model != null) {
            model.orderInstruction = ""
        }
        myPref.saveCartItems(model)
    }


    fun removePromoCode(myPref: PreferencesManager) {
        val model = myPref.getLocalCart()
        if (model != null) {
            model.promocode = ""
            model.promocodeDiscountAmount = 0.0
            model.promocodeDiscountPercentage = 0.0
        }
        myPref.saveCartItems(model)
    }

    fun addToCartProductList(
        mainActivity: MainActivity,
        myPref: PreferencesManager,
        data: ProductModel,
        isAdd: Boolean,
        isBulkUpdate: Boolean,
        selectedQty: Int
    ): Boolean {
        var model = myPref.getLocalCart()
        if (model == null) {
            model = LocalCartModel()
        }
        val cartList: MutableList<CartSingleItem> = model.cartItemList
        val existingRecordPosition = getOldRecordPosition(cartList, data.warehouse_product_id)
        val item: CartSingleItem
        val doInsert: Boolean
        var qty = 0
        if (existingRecordPosition == -1) { //insert new item in to the cart.
            item = CartSingleItem(data, selectedQty)
            doInsert = true
        } else {
            item = cartList[existingRecordPosition]
            qty = item.quantity
            doInsert = false

            if (isAdd && qty >= myPref.getMaxQuantityLimit()) {
                ToastUtil.show(mainActivity, "You have reached max qty")
                return false
            }
        }



        qty = if (isBulkUpdate) {
            selectedQty
        } else {
            if (isAdd) {
                qty + 1
            } else {
                qty - 1
            }
        }
        item.quantity = qty
        if (doInsert) {
            cartList.add(item)
        } else {
            if (qty <= 0) {
                cartList.remove(item)
            } else {
                cartList.set(existingRecordPosition, item)
            }
        }
        model.cartItemList = cartList
        myPref.saveCartItems(model)
        return true
    }

    fun updateCartQuantity(
        mainActivity: MainActivity,
        myPref: PreferencesManager,
        wareHouseProductId: String,
        isInc: Boolean,
        isBulkUpdate: Boolean,
        selectedQty: Int
    ): Boolean {
        val model = myPref.getLocalCart()
        if (model != null) {
            val cartList: MutableList<CartSingleItem> = model.cartItemList
            val existingRecordPosition = getOldRecordPosition(cartList, wareHouseProductId)
            if (existingRecordPosition != -1) {
                var finalQty = cartList[existingRecordPosition].quantity

                if (isInc && finalQty >= myPref.getMaxQuantityLimit()) {
                    ToastUtil.show(mainActivity, "You have reached max qty")
                    return false
                }

                finalQty = if (isBulkUpdate) {
                    selectedQty
                } else {
                    if (isInc) finalQty + 1 else finalQty - 1
                }
                if (finalQty <= 0) {
                    cartList.removeAt(existingRecordPosition)
                } else {
                    cartList[existingRecordPosition].quantity = finalQty
                }
            }
            model.cartItemList = cartList
            myPref.saveCartItems(model)
        }

        return true
    }


    fun addToCompare(
        adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>?,
        context: MainActivity,
        myPref: PreferencesManager,
        productId: String,
        groupId: String,
        second_level_category_id: String,
        wareHouseProductId: String
    ): Boolean {
        var model = myPref.getAddToCompareData()
        if (model == null) {
            model = LocalCompareModel()
        }
        val cartList: MutableList<CompareItem> = model.cartItemList
        val existsPos = getExistsPos(cartList, wareHouseProductId)
        if (existsPos == -1) {

            if (cartList.size >= 4) {
                Utils.showWarning(
                    context,
                    "Maximum four products allowed to compare at the same time"
                )
                return false
            } else if (cartList.size > 0 && cartList.get(0).second_level_category_id
                != second_level_category_id
            ) {
                val snackbar = Snackbar.make(
                    context.window.decorView.rootView, "You can only compare similar products",
                    Snackbar.LENGTH_LONG
                ).setAction("CLEAR LIST") {
                    myPref.clearCompareProducts()
                    context.refreshAddToCompareBadge()
                    if (adapter != null) {
                        adapter.notifyDataSetChanged()
                    } else {
                        if (context.getCurrentFragment() != null) {
                            (context.getCurrentFragment() as ProductDetailFragment).refresh(
                                wareHouseProductId
                            )
                        }
                    }
                }
                snackbar.setActionTextColor(Color.RED)
                val snackbarView = snackbar.view
                snackbarView.setBackgroundColor(Color.LTGRAY)
                val textView =
                    snackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
                textView.setTextColor(Color.BLUE)
                textView.textSize = 12f
                snackbar.show()
                return false
            }


            val item = CompareItem(
                productId,
                groupId,
                second_level_category_id,
                wareHouseProductId,
                myPref.getPrefKeyVendorId(),
                myPref.getPrefKeyStoreId()
            )
            cartList.add(item)
        } else {
            cartList.removeAt(existsPos)
        }


        model.cartItemList = cartList
        myPref.saveAddToCompare(model)
        context.refreshAddToCompareBadge()
        Utils.showApiSuccess(
            context,
            if (existsPos == -1) "Added to compare" else " Removed from compare"
        )
        return existsPos == -1
    }

    fun isExists(myPref: PreferencesManager, wareHouseProductId: String): Boolean {
        val model = myPref.getAddToCompareData()
        if (model != null) {
            val pos = getExistsPos(model.cartItemList, wareHouseProductId)
            return pos != -1
        }
        return false
    }

    fun deleteCompareItem(
        mainActivity: MainActivity,
        myPref: PreferencesManager,
        wareHouseProductId: String
    ) {
        val model = myPref.getAddToCompareData()
        if (model != null) {
            val cartList: MutableList<CompareItem> = model.cartItemList
            val existingRecordPosition = getExistsPos(cartList, wareHouseProductId)
            if (existingRecordPosition != -1) {
                cartList.remove(cartList[existingRecordPosition])
            }
            model.cartItemList = cartList
            myPref.saveAddToCompare(model)
            mainActivity.refreshAddToCompareBadge()
        }
    }


    fun deleteCartItem(myPref: PreferencesManager, wareHouseProductId: String) {
        val model = myPref.getLocalCart()
        if (model != null) {
            val cartList: MutableList<CartSingleItem> = model.cartItemList
            val existingRecordPosition = getOldRecordPosition(cartList, wareHouseProductId)
            if (existingRecordPosition != -1) {
                cartList.remove(cartList[existingRecordPosition])
            }
            model.cartItemList = cartList
            myPref.saveCartItems(model)
        }
    }

    fun getSingleItemQuantity(myPref: PreferencesManager, wareHouseProductId: String): Int {
        var qty = 0
        val model = myPref.getLocalCart()
        if (model != null) {
            val cartList: MutableList<CartSingleItem> = model.cartItemList
            val existingRecordPosition = getOldRecordPosition(cartList, wareHouseProductId)
            if (existingRecordPosition != -1) {
                qty = cartList[existingRecordPosition].quantity
            }
        }
        return qty
    }

    private fun getOldRecordPosition(
        cartList: MutableList<CartSingleItem>,
        wareHouseProductId: String
    ): Int {
        cartList.forEachIndexed { index, cartSingleItem ->
            if (cartSingleItem.productModel.warehouse_product_id == wareHouseProductId) {
                return index
            }
        }
        return -1
    }

    private fun getExistsPos(
        cartList: MutableList<CompareItem>,
        wareHouseProductId: String
    ): Int {
        cartList.forEachIndexed { index, cartSingleItem ->
            if (cartSingleItem.warehouse_product_id == wareHouseProductId) {
                return index
            }
        }
        return -1
    }

    fun getSubTotal(myPref: PreferencesManager): Double {
        var subTotal = 0.0
        val model = myPref.getLocalCart()
        if (model != null) {
            val cartList: MutableList<CartSingleItem> = model.cartItemList
            cartList.forEach {
                subTotal += AppValidator.toDouble(it.productModel.discounted_price) * it.quantity
            }
        }
        if (subTotal < 0)
            subTotal = 0.0
        return subTotal
    }

    fun updateCartDetails(myPref: PreferencesManager, info: ProductModel) {
        val model = myPref.getLocalCart()
        if (model != null) {
            val cartList: MutableList<CartSingleItem> = model.cartItemList
            val existingRecordPosition = getOldRecordPosition(cartList, info.warehouse_product_id)
            if (existingRecordPosition != -1) {
                cartList[existingRecordPosition].productModel = info
            }
            model.cartItemList = cartList
            myPref.saveCartItems(model)
        }
    }
}