package com.electroworld.android.ui.main.cashfree

import android.text.TextWatcher
import android.text.Editable
import java.lang.StringBuilder

class CardNumberFormattingTextWatcher : TextWatcher {
    private var lastSource = EMPTY_STRING
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    override fun afterTextChanged(s: Editable) {
        var source = s.toString()
        if (lastSource != source) {
            source = source.replace(WHITE_SPACE, EMPTY_STRING)
            val stringBuilder = StringBuilder()
            for (i in 0 until source.length) {
                if (i > 0 && i % 4 == 0) {
                    stringBuilder.append(WHITE_SPACE)
                }
                stringBuilder.append(source[i])
            }
            lastSource = stringBuilder.toString()
            s.replace(0, s.length, lastSource)
        }
    }

    companion object {
        private const val EMPTY_STRING = ""
        private const val WHITE_SPACE = " "
    }
}