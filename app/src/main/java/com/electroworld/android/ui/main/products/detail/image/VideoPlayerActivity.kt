package com.electroworld.android.ui.main.products.detail.image

import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import com.electroworld.android.R
import com.electroworld.android.api.Urls
import com.electroworld.android.databinding.ActivityVideoPlayerBinding
import com.electroworld.android.utils.Utils
import com.electroworld.android.utils.Utils.getVideoId
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import dagger.android.support.DaggerAppCompatActivity


class VideoPlayerActivity : DaggerAppCompatActivity() {


    lateinit var binding: ActivityVideoPlayerBinding

    public var url: String = ""
    public var title: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_player)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_video_player)

        url = intent.getStringExtra("url").toString()
        title = intent.getStringExtra("title").toString()

        if (url == null || url.isEmpty()) {
            Utils.showWarning(this, "Video url can't be empty")
            onBackPressed()
        }

        val isYoutubeUrl = url.contains("www.youtube.com")

        binding.youtubePlayerView.visibility = if (isYoutubeUrl) View.VISIBLE else View.GONE
        binding.andExoPlayerView.visibility = if (isYoutubeUrl) View.GONE else View.VISIBLE

        if (isYoutubeUrl) {
            lifecycle.addObserver(binding.youtubePlayerView)
            binding.youtubePlayerView.enterFullScreen();

            binding.youtubePlayerView.addYouTubePlayerListener(object :
                AbstractYouTubePlayerListener() {
                override fun onReady(@NonNull youTubePlayer: YouTubePlayer) {
                    if (url.contains("v=")) {
                        val videoId: String = getVideoId(url)
                        youTubePlayer.loadVideo(videoId, 0f)
                    }
                }
            })


        } else {
            binding.andExoPlayerView.setSource(url);
            binding.andExoPlayerView.setShowController(true);
            binding.andExoPlayerView.setPlayWhenReady(true);
        }

        binding.back.setOnClickListener {
            onBackPressed()
        }


    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.youtubePlayerView.release()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()
    }

}