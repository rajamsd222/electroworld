package com.electroworld.android.ui.main.products.compare

data class SpecificationsModel (
    val specification_name: String,
    val name_en: String
)