package com.electroworld.android.ui.main.products.filter

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.filter.FilterApiRes
import com.electroworld.android.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FilterViewModel @Inject constructor(val mainApi: MainApi,val myPref: PreferencesManager) : ViewModel() {
    private val disposable = CompositeDisposable()
    private val responseLiveDataFilter = MutableLiveData<ApiResource<ApiResponse<FilterApiRes>>>()

    fun getFiltersList() {
        disposable.add(mainApi.getFiltersList(
            Constants.HEADER_TOKEN, myPref.getPrefKeyVendorId(),myPref.getPrefKeyLanguage()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveDataFilter.setValue(
                    ApiResource.loading<ApiResponse<FilterApiRes>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveDataFilter.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveDataFilter.setValue(
                    ApiResource.error<ApiResponse<FilterApiRes>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getFiltersResponse(): MutableLiveData<ApiResource<ApiResponse<FilterApiRes>>> {
        return responseLiveDataFilter
    }
}