package com.electroworld.android.ui.main.products.list

import android.app.Activity
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.databinding.RowItemProductSubCategoryBinding
import com.electroworld.android.model.Subcategory
import java.util.*
import kotlin.collections.ArrayList

class ProductListSubCategoryAdapter(fragment: ProductListFragment,val context: Activity) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items = arrayListOf<Subcategory>()
    var parentFragment: ProductListFragment = fragment


    inner class ViewHolder(binding: RowItemProductSubCategoryBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        private val binding: RowItemProductSubCategoryBinding = binding
        fun bind(position: Int) {
            val model: Subcategory = items[position]
            binding.item = model
            binding.executePendingBindings()
            if(model.isSelected) binding.tvProductName.setTextColor(ContextCompat.getColor(context, R.color.color_sub_category_selection)) else binding.tvProductName.setTextColor(
                Color.WHITE
            )
            binding.rowParent.setTag(position)
            binding.rowParent.setOnClickListener {
                val pos = it.tag as Int
                for (i in items.indices) {
                    items[i].isSelected = i == pos
                }
                notifyDataSetChanged()
                parentFragment.getListBySubcategoryId(items[pos].id,items[pos].name)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowItemProductSubCategoryBinding =
            RowItemProductSubCategoryBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun getItems(): ArrayList<Subcategory> {
        return this.items
    }

    fun setItems(list: List<Subcategory>?) {
        if (list == null) {
            this.items.clear()
        } else {
            this.items = ArrayList(list)
        }
        notifyDataSetChanged()
    }

    fun clearAll() {
        this.items.clear()
        notifyDataSetChanged()
    }
    fun removeItem(item: Subcategory?) {
        items.remove(item)
        notifyDataSetChanged()
    }

}
