package com.electroworld.android.ui

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Base64
import android.util.Log
import android.view.WindowManager
import com.electroworld.android.R
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.utils.Constants
import dagger.android.support.DaggerAppCompatActivity
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import javax.inject.Inject


class SplashActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var myPref: PreferencesManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash)
        myPref.setPrefKeyLanguage("en")
        myPref.setPrefKeyVendorId(Constants.VENDOR_ID)
        Constants.COMES_FROM_SPLASH = true;
        printHashKey()
        Handler(Looper.getMainLooper()).postDelayed({
            //startActivity(Intent(this@SplashActivity, SampleActivity::class.java))
            //startActivity(Intent(this@SplashActivity, AuthActivity::class.java))
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            /* val intent = Intent(this@SplashActivity, CashFreePaymentActivity::class.java)
             intent.putExtra("amount", "100")
             startActivityForResult(intent, 151)*/
            finish()
        }, 2000)

        //CustomDialog.showMessageDialog(this,"Profile updated successfully",AlertType.SUCCESS)
        //CustomDialog.showMessageDialog(this,"We are not serving in this area",AlertType.ERROR)
    }


    private fun printHashKey() {

        // Add code to print out the key hash
        try {
            val info = packageManager.getPackageInfo(
                this.packageName,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.e(
                    "KeyHash:",
                    Base64.encodeToString(
                        md.digest(),
                        Base64.DEFAULT
                    )
                )
            }
        } catch (e: PackageManager.NameNotFoundException) {
        } catch (e: NoSuchAlgorithmException) {
        }
    }

}