package com.electroworld.android.ui.main.orders.detail

data class ItemsModel(
    val discounted_price: String,
    val image: String,
    val name: String,
    val price: String,
    val variant: String
)