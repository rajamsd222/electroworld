package com.electroworld.android.ui.main.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.databinding.FragmentNotificationBinding
import com.electroworld.android.databinding.RowNotificationListBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import javax.inject.Inject


class NotificationFragment : DaggerFragment() {

    private lateinit var viewModel: NotificationViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentNotificationBinding

    private var mAdapter: GenericAdapter<NotificationModel, RowNotificationListBinding>? = null

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    private val disposable = CompositeDisposable()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(NotificationViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentNotificationBinding.bind(view)
        binding.ab.tvABTitle.text = resources.getString(R.string.profile_notification)
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        initRecyclerView()
        makeNotificationReq()
    }

    private fun initRecyclerView() {
        mAdapter = object :
            GenericAdapter<NotificationModel, RowNotificationListBinding>(R.layout.row_notification_list) {
            override fun onBindData(model: NotificationModel?,position: Int,dataBinding: RowNotificationListBinding?) {
                dataBinding?.item = model
            }
            override fun onItemClick(model: NotificationModel?, position: Int) {}
        }
        binding.recyclerView.apply {
            adapter = mAdapter
        }
    }

    private fun makeNotificationReq() {
        binding.tvEmptyMsg.setGone()
        disposable.add(
            viewModel.getNotificationList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<NotificationModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<NotificationModel>>) {
                        if (res.http_code == 200) {
                            mAdapter?.addItems(res.data)
                            if (mAdapter?.itemCount == 0) {
                                binding.tvEmptyMsg.setVisible()
                            } else {
                                binding.tvEmptyMsg.setGone()
                            }
                        } else {
                            CustomDialog.show(requireActivity(),res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(),e.message,AlertType.SERVER_ERROR)
                    }
                })
        )
    }

}