package com.electroworld.android.ui.main.address.add;

import java.io.Serializable;

public class PlaceModel implements Serializable {

    String description;
    String pId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }
}
