package com.electroworld.android.ui.main.products

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.databinding.RowItemProductGridBinding
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.ProductModel
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartListFragment
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.home.HomeFragment
import com.electroworld.android.ui.main.products.detail.ProductDetailFragment
import com.electroworld.android.utils.ScreenType
import com.google.android.material.bottomsheet.BottomSheetDialog


class ProductsGridAdapter(
    fragment: Fragment,
    val context: MainActivity,
    private val myPref: PreferencesManager,
    screenName: ScreenType,
    productInterface: ProductInterface,
    homeModelName: String
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items = arrayListOf<ProductModel>()
    var parentFragment: Fragment = fragment
    var bottomSheetDialog: BottomSheetDialog? = null
    var mAdapter: ProductsGridAdapter? = null
    var screenType: ScreenType
    var homeModelName: String
    var productInterface: ProductInterface = productInterface

    init {
        mAdapter = this
        screenType = screenName
        this.homeModelName = homeModelName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowItemProductGridBinding =
            RowItemProductGridBinding.inflate(layoutInflater, parent, false)
        itemBinding.productInterface = productInterface
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun getItems(): ArrayList<ProductModel> {
        return this.items
    }

    inner class ViewHolder(binding: RowItemProductGridBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val binding: RowItemProductGridBinding = binding
        fun bind(position: Int) {
            val model: ProductModel = items[position]
            binding.item = model
            binding.tvMRP.paintFlags = binding.tvMRP.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            binding.executePendingBindings()
            binding.ivProduct.setBackgroundResource(0)

            if (!homeModelName.isNullOrEmpty() && homeModelName == "EML5") {
                binding.productsGroups.setGone()
                binding.hotDealsGroups.setVisible()
            } else {
                binding.hotDealsGroups.setGone()
                binding.productsGroups.setVisible()
            }
            binding.btnViewProduct.setOnClickListener {
                if (parentFragment is HomeFragment) {
                    (parentFragment as HomeFragment).openDetailsScreen(model)
                }
            }



            when (screenType) {
                ScreenType.CART -> {
                    //binding.ivType.setGone()
                    if (!model.discount_label.isNullOrEmpty()) {
                        binding.tvOffer.setGone()
                        binding.tvOfferRight.setVisible()
                    } else {
                        binding.tvOffer.setGone()
                        binding.tvOfferRight.setGone()
                    }

                }
                ScreenType.PRODUCT_DETAIL_FREQUENTLY_BROUGHT -> {
                    binding.ivProduct.setBackgroundResource(R.drawable.image_border_thin)
                    //binding.ivTypeLeft.setGone()
                    if (!model.discount_label.isNullOrEmpty()) {
                        binding.tvOffer.setVisible()
                        binding.tvOfferRight.setGone()
                    } else {
                        binding.tvOffer.setGone()
                        binding.tvOfferRight.setGone()
                    }
                }
                ScreenType.PRODUCT_DETAIL_SIMILAR_PRODUCT -> {
                    binding.ivProduct.setBackgroundResource(R.drawable.image_border_thin)
                    //binding.ivTypeLeft.setGone()
                    if (!model.discount_label.isNullOrEmpty()) {
                        binding.tvOffer.setVisible()
                        binding.tvOfferRight.setGone()
                    } else {
                        binding.tvOffer.setGone()
                        binding.tvOfferRight.setGone()
                    }
                }
                else -> {
                    /* binding.ivTypeLeft.setGone()*/
                    if (!model.discount_label.isNullOrEmpty()) {
                        binding.tvOffer.setVisible()
                        binding.tvOfferRight.setGone()
                    } else {
                        binding.tvOffer.setGone()
                        binding.tvOfferRight.setGone()
                    }

                }
            }
//            //if (model.group_variants?.size?: 0 > 1) {
//            if (model.group_variants != null && model.group_variants.size > 1) {
//                binding.tvGroupVariant.visibility = View.VISIBLE
//                binding.tvVariantName.visibility = View.GONE
//            } else {
//                binding.tvGroupVariant.visibility = View.GONE
//                binding.tvVariantName.visibility = View.VISIBLE
//            }

            binding.ivCompare.setColorFilter(
                ContextCompat.getColor(
                    context,
                    if (CartUtils.isExists(
                            myPref,
                            if (model.warehouse_product_id == null) "0" else model.warehouse_product_id
                        )
                    ) R.color.bottom_tab_color_selected else R.color.bottom_tab_color_default
                )
            )

            binding.compareLayout.setOnClickListener {
                val isAdded = CartUtils.addToCompare(
                    this@ProductsGridAdapter,
                    context,
                    myPref,
                    model.product_id,
                    model.group_id,
                    model.second_level_category_id,
                    if (model.warehouse_product_id == null) "0" else model.warehouse_product_id
                )
                notifyDataSetChanged()
            }

            /* val singleItemQuantity: Int =
                 CartUtils.getSingleItemQuantity(
                     myPref,
                     AppValidator.toStr(model.warehouse_product_id)
                 )
             if (singleItemQuantity > 0) {
                 binding.tvAdd.visibility = View.GONE
                 binding.layCount.visibility = View.VISIBLE
             } else {
                 binding.tvAdd.visibility = View.VISIBLE
                 binding.layCount.visibility = View.GONE
             }

             binding.tvQty.text = singleItemQuantity.toString()*/

            binding.favLayout.setOnClickListener {
                if (!myPref.isLogin()) {
                    context.showLoginDialog(context.getString(R.string.login_alert_for_fav))
                } else {
                    if (parentFragment is ProductDetailFragment) {
                        (parentFragment as ProductDetailFragment).setAsFavouriteProduct(
                            items.get(position)
                        )
                    } else if (parentFragment is HomeFragment) {
                        (parentFragment as HomeFragment).setAsFavouriteProduct(
                            mAdapter, position, items.get(position)
                        )
                    } else if (parentFragment is CartListFragment) {
                        (parentFragment as CartListFragment).setAsFavouriteProduct(
                            mAdapter, position, items.get(position)
                        )
                    }
                }
            }

            /* binding.tvAdd.setOnClickListener {
                 if (parentFragment is ProductDetailFragment) {
                     (parentFragment as ProductDetailFragment).doAddToCart(
                         items[position],
                         true,
                         false,
                         1
                     )
                 } else if (parentFragment is HomeFragment) {
                     (parentFragment as HomeFragment).doAddToCart(
                         mAdapter,
                         items[position],
                         true,
                         false,
                         1
                     )
                 } else if (parentFragment is CartListFragment) {
                     (parentFragment as CartListFragment).doAddToCart(
                         items[position],
                         true,
                         false,
                         1
                     )
                 }
             }
             binding.tvIncrement.setOnClickListener {
                 if (parentFragment is ProductDetailFragment) {
                     (parentFragment as ProductDetailFragment).doAddToCart(
                         items[position], true, false, 1
                     )
                 } else if (parentFragment is HomeFragment) {
                     (parentFragment as HomeFragment).doAddToCart(
                         mAdapter,
                         items[position], true, false, 1
                     )
                 } else if (parentFragment is CartListFragment) {
                     (parentFragment as CartListFragment).doAddToCart(
                         items[position], true, false, 1
                     )
                 }
             }
             binding.tvDecrement.setOnClickListener {
                 if (parentFragment is ProductDetailFragment) {
                     (parentFragment as ProductDetailFragment).doAddToCart(
                         items[position], false, false, 1
                     )
                 } else if (parentFragment is HomeFragment) {
                     (parentFragment as HomeFragment).doAddToCart(
                         mAdapter,
                         items[position], false, false, 1
                     )
                 }else if (parentFragment is CartListFragment) {
                     (parentFragment as CartListFragment).doAddToCart(
                         items[position], false, false, 1
                     )
                 }
             }
 */
            binding.tvGroupVariant.setOnClickListener {
                showVariantDialog(model, binding.tvGroupVariant, position, screenType)
            }

            binding.parentView.setOnClickListener {
                if (parentFragment is ProductDetailFragment) {
                    (parentFragment as ProductDetailFragment).openDetailsScreen(model)
                } else if (parentFragment is HomeFragment) {
                    (parentFragment as HomeFragment).openDetailsScreen(model)
                } else if (parentFragment is CartListFragment) {
                    (parentFragment as CartListFragment).openDetailsScreen(model)
                }
            }
        }

    }


    fun setItems(list: List<ProductModel>?) {
        if (list == null) {
            this.items.clear()
        } else {
            this.items.addAll(list)
        }
        notifyDataSetChanged()
    }

    fun clearAll() {
        this.items.clear()
        notifyDataSetChanged()
    }

    fun updateItem(pos: Int, item: ProductModel) {
        this.items.set(pos, item)
        notifyDataSetChanged()
    }

    fun removeItem(item: ProductModel?) {
        items.remove(item)
        notifyDataSetChanged()
    }

    fun showVariantDialog(
        model: ProductModel,
        tvGroupVariant: TextView,
        position: Int,
        screenType: ScreenType
    ) {
        val dialogView: View = context.getLayoutInflater().inflate(R.layout.dialog_variant, null)
        bottomSheetDialog = BottomSheetDialog(context, R.style.CustomBottomSheetDialog)
        bottomSheetDialog!!.setContentView(dialogView)
        var tvTitle = bottomSheetDialog!!.findViewById<TextView>(R.id.tvTitle)
        var recyclerView = bottomSheetDialog!!.findViewById<RecyclerView>(R.id.recycler_view)
        var adapter = VariantListAdapter(
            model.group_variants,
            parentFragment,
            context,
            bottomSheetDialog,
            tvGroupVariant,
            model,
            position,
            screenType
        )
        recyclerView?.adapter = adapter
        tvTitle!!.text = model.name_en /*+ " - " + model.variant_en*/
        bottomSheetDialog!!.show()
    }

    interface ProductInterface {
        fun addItem(productModel: ProductModel, quantity: Int)
    }
}
