package com.electroworld.android.ui.main.address.add.locationutil
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import java.io.IOException
import java.util.*

class LocationAddress {
    private val tag = "LocationAddress"
    fun getAddressFromLocation(
        latitude: Double,
        longitude: Double, context: Context, handler: Handler
    ) {
        val thread = object : Thread() {
            override fun run() {
                val geoCoder = Geocoder(
                    context,
                    Locale.getDefault()
                )
                var result: String = null.toString()
                try {
                    val addresses: List<Address> = geoCoder.getFromLocation(latitude, longitude, 1)
                    if (addresses.isNotEmpty()) {
                        val returnedAddress = addresses[0]
                        val strReturnedAddress = StringBuilder()
                        for (i in 0..returnedAddress.maxAddressLineIndex) {
                            strReturnedAddress.append(returnedAddress.getAddressLine(i))
                                .append("\n")
                        }
                        result = strReturnedAddress.toString()
                        Log.e("TAG", ""+result)
                    }
                } catch (e: IOException) {
                    Log.e(tag, "Unable connect to GeoCoder", e)
                } finally {
                    val message = Message.obtain()
                    message.target = handler
                    message.what = 1
                    val bundle = Bundle()
                    bundle.putString("address", result)
                    message.data = bundle
                    message.sendToTarget()
                }
            }
        }
        thread.start()
    }
}