package com.electroworld.android.ui.auth.webview

import androidx.lifecycle.ViewModel
import com.electroworld.android.api.auth.AuthApi
import javax.inject.Inject

class WebViewModel @Inject constructor(private val authApi: AuthApi) : ViewModel() {
    companion object {
        private const val TAG = "WebViewModel"
    }
}