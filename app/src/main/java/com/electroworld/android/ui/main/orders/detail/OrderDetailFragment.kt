package com.electroworld.android.ui.main.orders.detail

import android.app.Dialog
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.AlertViewSimple
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.bind.AppBinder
import com.electroworld.android.databinding.FragmentOrderDetailBinding
import com.electroworld.android.databinding.RowOrderDetailItemsBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.dpToPx
import com.electroworld.android.extentions.getColorCompat
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.DefaultData
import com.electroworld.android.model.OrderStatus
import com.electroworld.android.model.TimeLineModel
import com.electroworld.android.model.TimelineAttributes
import com.electroworld.android.model.orderdetail.OrderDetailModel
import com.electroworld.android.model.orderlist.OrderListProductItem
import com.electroworld.android.ui.main.cashfree.CashFreePaymentFragment
import com.electroworld.android.ui.main.orders.SummaryModel
import com.electroworld.android.ui.main.orders.TimeLineAdapter
import com.electroworld.android.utils.*
import com.github.vipulasri.timelineview.TimelineView
import com.google.gson.Gson
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import java.util.*
import javax.inject.Inject

class OrderDetailFragment : DaggerFragment() {

    companion object {
        const val ARG_ORDER_ID = "order_id"
        const val ARG_DATA = "data"
    }

    private lateinit var viewModel: OrderDetailViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentOrderDetailBinding

    var mAdapterOrderDetail: GenericAdapter<OrderListProductItem, RowOrderDetailItemsBinding>? =
        null
    private var orderId: String? = null

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    private var subTotal = 0.0
    private var grandTotal = 0.0
    private var discountedPriceTotal = 0.0
    private var deliveryCharge = 0.0
    private var promocodeDiscount = 0.0
    private var gift_to_others_amount = 0.0
    private var wallet_amount = 0.0
    private var showRatingButton = false
    private val disposable = CompositeDisposable()
    private lateinit var dialog: Dialog
    private lateinit var mAttributes: TimelineAttributes
    lateinit var parentData: OrderDetailModel
    var listReturn: MutableList<OrderListProductItem>? = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_order_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(OrderDetailViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentOrderDetailBinding.bind(view)
        binding.ab.tvABTitle.text = getString(R.string.title_order_detail)
        orderId = arguments?.getString(ARG_ORDER_ID)
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        mAttributes = TimelineAttributes(
            markerSize = dpToPx(12f),
            markerColor = getColorCompat(R.color.colorPrimary),
            markerInCenter = true,
            markerLeftPadding = dpToPx(0f),
            markerTopPadding = dpToPx(0f),
            markerRightPadding = dpToPx(0f),
            markerBottomPadding = dpToPx(0f),
            linePadding = dpToPx(2f),
            startLineColor = getColorCompat(R.color.colorPrimary),
            endLineColor = getColorCompat(R.color.colorPrimary),
            lineStyle = TimelineView.LineStyle.NORMAL,
            lineWidth = dpToPx(2f),
            lineDashWidth = dpToPx(4f),
            lineDashGap = dpToPx(2f)
        )

        initRecyclerView()
        makeOrderDetailsReq()
    }

    private fun makeOrderDetailsReq() {
        binding.promotionItemLayout.setGone()
        binding.tvCancelOrder.setGone()
        binding.contentLayout.setGone()
        val pId = orderId ?: return
        disposable.add(
            viewModel.makeOrderDetailReq(pId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<OrderDetailModel>>() {
                    override fun onSuccess(res: ApiResponse<OrderDetailModel>) {
                        if (res.http_code == 200) {
                            binding.tvEmptyMsg.setGone()
                            binding.contentLayout.setVisible()
                            binding.item = res.data
                            showRatingButton = res.data.order_status_id == 8
                            mAdapterOrderDetail?.clearAll()
                            mAdapterOrderDetail?.addItems(res.data.items)
                            deliveryCharge = res.data.delivery_charge
                            updateViews(res.data)
                            calculateSummary(res.data)
                        } else {
                            binding.tvEmptyMsg.setVisible()
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun showCancelAlert() {
        val avcDialog =
            AlertViewSimple(requireActivity(), object : AlertViewSimple.OKClickListener {
                override fun onOKClick(view: View?) {
                    doCancelOrder()
                }
            })
        avcDialog.setCancelable(true)
        avcDialog.setDialogTitle("Cancel Order")
        avcDialog.setDialogMessage("Are you sure want to cancel the order?")
        avcDialog.setPositiveText("Yes")
        avcDialog.setNegativeText("No")
        avcDialog.setShowNegativeButton(true)
        avcDialog.showDialog()
    }

    private fun doCancelOrder() {
        val pId = orderId ?: return
        disposable.add(
            viewModel.makeOrderCancelReq(pId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<DefaultData>>() {
                    override fun onSuccess(res: ApiResponse<DefaultData>) {
                        if (res.http_code == 200) {
                            Utils.showApiSuccess(activity, res.message)
                            mNavController.navigateUp()
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun doOrderFavAPI() {
        val pId = orderId ?: return
        disposable.add(
            viewModel.makeOrderFavouriteReq(pId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<DefaultData>>() {
                    override fun onSuccess(res: ApiResponse<DefaultData>) {
                        if (res.http_code === 200) {
                            Utils.showApiSuccess(activity, res.message)
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    /*private fun doReOrderAPI() {
        val pId = orderId ?: return
        disposable.add(
            viewModel.makeReOrderReq(pId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<ReOrderModel>>() {
                    override fun onSuccess(res: ApiResponse<ReOrderModel>) {
                        if (res.http_code == 200) {
                            if (res.getData() != null) {
                                myPref.clearLocalCart()
                                for (info in res.getData().products!!) {
                                    CartUtils.addToCartProductList(myPref, info, true, false, 1)
                                }
                                if (myPref.getCartCount() > 0) {
                                    mNavController.navigate(R.id.action_orderDetailFragment_to_cartListFragment)
                                }
                                Utils.showApiSuccess(activity, res.message)
                            } else {
                        CustomDialog.showMessageDialog(requireActivity(),res.message,AlertType.ERROR)
                            }
                        } else {
                        CustomDialog.showMessageDialog(requireActivity(),res.message,AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.showMessageDialog(requireActivity(),e.message,AlertType.SERVER_ERROR)
                    }
                })
        )
    }
*/
    private fun updateViews(data: OrderDetailModel) {
        parentData = data



        binding.tvViewInvoice.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("screenType", "order_detail")
            bundle.putString("url", data.invoice_url)
            mNavController.navigate(R.id.action_orderDetailFragment_to_webViewFragment, bundle)
        }
        binding.tvTrack.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable(ARG_DATA, data)
            mNavController.navigate(R.id.action_orderDetailFragment_to_trackOrderFragment, bundle)
        }

        binding.tvReOrder.setOnClickListener {
            doOrderFavAPI()
        }
        binding.btnRetryPayment.setOnClickListener {
            openCashFreeScreen(
                data.amount_payable,
                data.cashfree_reference_id.toString(),
                data.order_id.toString()
            )
        }
        binding.tvCancelOrder.setOnClickListener {
            showCancelAlert()
        }
        binding.returnOrderLayout.setOnClickListener {
            val dataList = Gson().toJson(data.items)
            val bundle = Bundle()
            bundle.putString(ARG_DATA, dataList)
            bundle.putString(ARG_ORDER_ID, data.order_id.toString())
            mNavController.navigate(R.id.action_orderDetailFragment_to_returnOrderFragment, bundle)
        }


        val mDataList = ArrayList<TimeLineModel>()
        val orderStatusId = data.order_status_id
        if (orderStatusId == 2 || orderStatusId == 3 || orderStatusId == 4
            || orderStatusId == 11 || orderStatusId == 13
        ) {
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.accepted),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.shipped),
                    "",
                    OrderStatus.ACTIVE
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.out_for_delivery),
                    "",
                    OrderStatus.INACTIVE
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.delivered),
                    "",
                    OrderStatus.INACTIVE
                )
            )
        } else if (orderStatusId == 5 || orderStatusId == 6) {
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.accepted),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.shipped),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.out_for_delivery),
                    "",
                    OrderStatus.ACTIVE
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.delivered),
                    "",
                    OrderStatus.INACTIVE
                )
            )
        } else if (orderStatusId == 7) {
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.accepted),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.shipped),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.out_for_delivery),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.delivered),
                    "",
                    OrderStatus.ACTIVE
                )
            )
        } else if (orderStatusId == 8) {
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.accepted),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.shipped),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.out_for_delivery),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.delivered),
                    "",
                    OrderStatus.COMPLETED
                )
            )
        } else if (orderStatusId == 14) {
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.accepted),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.shipped),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.out_for_delivery),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.delivered),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.return_requested),
                    "",
                    OrderStatus.ACTIVE
                )
            )
        }else if (orderStatusId == 12) {
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.accepted),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.shipped),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.out_for_delivery),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.delivered),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.return_requested),
                    "",
                    OrderStatus.COMPLETED
                )
            )
            mDataList.add(
                TimeLineModel(
                    resources.getString(R.string.partially_returned),
                    "",
                    OrderStatus.ACTIVE
                )
            )
        }

        if ((data.order_status_id in 2..8) || data.order_status_id == 11 || data.order_status_id == 12
            || data.order_status_id == 13 || data.order_status_id == 14 || data.order_status_id == 15
        ) {
            binding.cvTrackOrder.setVisible()
        } else {
            binding.cvTrackOrder.setGone()
        }

        if (data.shipment_method == "Pickup") {
            binding.cvDeliveryAddress.setGone()
        } else {
            binding.cvDeliveryAddress.setVisible()
        }

        binding.detailLayout.visibility =
            if (binding.cvTrackOrder.visibility == View.VISIBLE || binding.cvDeliveryAddress.visibility == View.VISIBLE)
                View.VISIBLE else View.GONE


        binding.rvTimeLine.apply {
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            adapter = TimeLineAdapter(mDataList, mAttributes, "order_detail")
        }

    }

    private fun openCashFreeScreen(amount: String, orderId: String, id: String) {
        val bundle = CashFreePaymentFragment.updateBundle(
            "",
            amount,
            orderId,
            id,
            PaymentType.DEFAULT,
            ScreenType.ORDER_DETAIL
        )
        mNavController.navigate(R.id.action_orderDetailFragment_to_cashFreePaymentFragment, bundle)
    }

    private fun initRecyclerView() {
        mAdapterOrderDetail = object :
            GenericAdapter<OrderListProductItem, RowOrderDetailItemsBinding>(R.layout.row_order_detail_items) {
            override fun onBindData(
                model: OrderListProductItem,
                position: Int,
                dataBinding: RowOrderDetailItemsBinding
            ) {
                dataBinding.item = model
                dataBinding.parentItem = parentData
                dataBinding.tvMRP.paintFlags =
                    dataBinding.tvMRP.paintFlags.or(Paint.STRIKE_THRU_TEXT_FLAG)
                //dataBinding.tvAddRating.visibility = if(AppValidator.toFloat(model.rating)>0) View.GONE else  View.VISIBLE
                dataBinding.ratingBar.visibility =
                    if (AppValidator.toFloat(model.rating) > 0) View.VISIBLE else View.GONE
                dataBinding.ratingBar.rating = AppValidator.toFloat(model.rating)

                if (model.rating.isNullOrEmpty()) {
                    dataBinding.tvAddRating.text = getString(R.string.add_rating)
                } else {
                    dataBinding.tvAddRating.text = getString(R.string.edit_rating)
                }
                if (showRatingButton && model.quantity > 0) {
                    dataBinding.tvAddRating.setVisible()
                } else {
                    dataBinding.tvAddRating.setGone()
                }
                dataBinding.tvAddRating.setOnClickListener {
                    showRatingDialog(model)
                }
            }

            override fun onItemClick(model: OrderListProductItem?, position: Int) {}
        }
        binding.recyclerView.apply {
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            adapter = mAdapterOrderDetail
        }
    }

    private fun showRatingDialog(model: OrderListProductItem?) {
        if (model != null) {
            showDialog(model)
            //RatingDialogFragment().show(requireActivity().getSupportFragmentManager(),"DialogFragment")
        }
    }

    private fun calculateSummary(data: OrderDetailModel) {
        subTotal = 0.0
        discountedPriceTotal = 0.0
        data.items?.forEach {
            discountedPriceTotal += it.quantity * AppValidator.toDouble(it.discounted_price)
            subTotal += it.quantity * AppValidator.toDouble(it.price)
        }
        loadSummary(data)
    }

    private fun loadSummary(data: OrderDetailModel) {
        wallet_amount = data.wallet_amount
        val list: MutableList<SummaryModel> = ArrayList()

        //Sub Total
        list.add(SummaryModel(getString(R.string.os_lbl_sub_total), subTotal))
        //Discount
        list.add(SummaryModel(getString(R.string.os_lbl_discount), discountedPriceTotal - subTotal))
        //Delivery charges
        list.add(SummaryModel(getString(R.string.os_lbl_delivery_charges), deliveryCharge))

        if (data.gift_to_others == 1) {
            gift_to_others_amount = data.gift_to_others_amount
        }

        if (gift_to_others_amount > 0.0) {
            //Gift to others calculation
            list.add(SummaryModel(getString(R.string.os_lbl_gift_charges), gift_to_others_amount))
        }

        if (data.promotion_product_total_price > 0.0) {
            //Promotion product
            list.add(
                SummaryModel(
                    getString(R.string.os_lbl_promotion_product),
                    data.promotion_product_total_price
                )
            )
        }

        var promocodeTextLabel = getString(R.string.promocode_discount)
        if (!data.promoCode.isNullOrEmpty()) {
            if (data.promo_code_discount_type.toString().toLowerCase().equals("percentage")) {
                promocodeDiscount = subTotal * data.promo_code_discount / 100
                promocodeTextLabel =
                    promocodeTextLabel + " (" + data.promo_code_discount + "%)"
            } else if (data.promo_code_discount_type.toString().toLowerCase().equals("amount")) {
                promocodeDiscount = data.promo_code_discount
                promocodeTextLabel =
                    promocodeTextLabel + " (" + MyUtils.formatCurrencyfromDouble(promocodeDiscount) + ")"
            }

        }

        if (data.promo_code_discount_amount > 0.0) {
            promocodeDiscount = data.promo_code_discount_amount * -1
            //Promo code
            list.add(SummaryModel(promocodeTextLabel, promocodeDiscount))
        }
        if (wallet_amount > 0.0) {
            //"Wallet Amount"
            list.add(SummaryModel(getString(R.string.os_lbl_wallet_amount), wallet_amount * -1))
        }

        //Grand Total calculation
        grandTotal = AppValidator.toDouble(data.grand_total)
        list.add(SummaryModel(getString(R.string.os_lbl_total_amount), grandTotal))

        list.add(
            SummaryModel(
                getString(R.string.os_lbl_amount_payable),
                AppValidator.toDouble(data.amount_payable)
            )
        )
        AppBinder.setOrderSummary(binding.orderSummaryLayout, list)
    }

    fun showDialog(model: OrderListProductItem) {
        dialog = Dialog(requireActivity(), R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.alert_rating)
        Objects.requireNonNull(dialog.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val appCompatImageView3 = dialog.findViewById<ImageView>(R.id.appCompatImageView3)
        val tvGroupName = dialog.findViewById<TextView>(R.id.tvGroupName)
        val tvProductName = dialog.findViewById<TextView>(R.id.tvProductName)
        val tvVariantName = dialog.findViewById<TextView>(R.id.tvVariantName)
        val tvSubmit = dialog.findViewById<TextView>(R.id.tvSubmit)
        val ratingBar = dialog.findViewById<RatingBar>(R.id.ratingBar)
        val edMessage = dialog.findViewById<EditText>(R.id.ed_message)

        ratingBar.rating = AppValidator.toFloat(model.rating)
        edMessage.setText(model.rating_descriptions)

        tvGroupName.text = model.group_name_en
        tvProductName.text = model.name_en
        tvVariantName.text = model.variant_en
        AppBinder.setglideFitXY(appCompatImageView3, model.image1)
        tvSubmit.setOnClickListener {
            val rating = ratingBar.rating
            if (rating > 0) {
                val comments = edMessage.text.toString()
                updateRating("" + rating, comments, model.product_id)
            } else {
                UiUtils.showToast(requireActivity(), "Please enter rating")
            }
        }
        dialog.show()
    }

    private fun updateRating(
        rating: String,
        comments: String,
        productId: String
    ) {
        disposable.add(
            viewModel.updateProductRatingReq(rating, comments, productId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<ApiResponseDefault>>() {
                    override fun onSuccess(res: ApiResponse<ApiResponseDefault>) {
                        if (res.http_code == 200) {
                            dialog.dismiss()
                            makeOrderDetailsReq()
                            CustomDialog.show(requireActivity(), res.message, AlertType.SUCCESS)
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    override fun onDestroyView() {
        disposable.clear()
        super.onDestroyView()
    }
}