package com.electroworld.android.ui.main.address.add.locationutil;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.electroworld.android.utils.Constants;

import java.util.List;
import java.util.Locale;

public class GetAddressFromLatLng extends AsyncTask<Double, Integer, String> {

    private static final String TAG = "GetAddressFromLatLng";
    private Context context;
    double latitude;
    double longitude;
    public GetAddressFromLatLng(Context context) {
        this.context = context;
    }

    public GetAddressFromLatLng(Context context,double lat,double lng) {
        this.context = context;
        this.latitude = lat;
        this.longitude = lng;
    }

    public void setLatLng(double lat,double lng){
        this.latitude = lat;
        this.longitude = lng;
    }

    @Override
    protected String doInBackground(Double... latlng) {
        String strAdd = "";
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.e(TAG, strReturnedAddress.toString());
            } else {
                Log.e(TAG, "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Can not get Address!");
        }

        return strAdd;
    }

    @Override
    protected void onPostExecute(String strAdd) {
        super.onPostExecute(strAdd);
        Constants.ADDRESS_NAME = strAdd;
        Constants.LATITUDE = latitude;
        Constants.LONGITUDE = longitude;
        Intent intent = new Intent("location_update");
        intent.putExtra(Constants.KEY_LAT_LNG_UPDATED, true);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}