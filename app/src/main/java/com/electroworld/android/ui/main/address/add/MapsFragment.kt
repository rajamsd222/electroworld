package com.electroworld.android.ui.main.address.add

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.os.StrictMode
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.customclass.CustomTypingEditText
import com.electroworld.android.databinding.FragmentMapsBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.AddressListModel
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.address.add.locationutil.AppLocationService
import com.electroworld.android.ui.main.address.add.locationutil.GetAddressFromLatLng
import com.electroworld.android.ui.main.address.add.locationutil.LocationAddress
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.UiUtils
import com.electroworld.android.utils.Utils
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener
import com.google.android.gms.maps.GoogleMap.OnCameraMoveListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnCompleteListener
import com.google.gson.JsonObject
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import javax.inject.Inject

class MapsFragment : DaggerFragment() {

    private val TAG = "MapScreenFragment"
    private var model: AddressListModel? = null
    private lateinit var binding: FragmentMapsBinding
    private lateinit var mNavController: NavController
    private lateinit var viewModel: MapViewModel

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    private val FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
    private val COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
    private val LOCATION_PERMISSION_REQUEST_CODE = 1234
    private val DEFAULT_ZOOM = 15f


    private var mLocationPermissionsGranted = false
    private var mMap: GoogleMap? = null
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null


    var listener: PlaceEventHandler? = null
    var placeModelArrayList: ArrayList<PlaceModel>? = null

    var isEditAddress = false

    var asyncTaskGetAddress: GetAddressFromLatLng? = null

    var addressType = ""
    var address = ""
    var houseNo = ""
    var apartment_name = ""
    var street_details = ""
    var landmark = ""
    var mobile_number = ""
    var contact_person = ""
    var lat: Double = Constants.LATITUDE
    var lng: Double = Constants.LONGITUDE
    var address_id = ""

    // lateinit var location: Location
    lateinit var appLocationService: AppLocationService
    private fun getDeviceLocation() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(
            requireActivity()
        )
        try {
            if (mLocationPermissionsGranted) {
                val tLocation = mFusedLocationProviderClient!!.getLastLocation()

                tLocation.addOnCompleteListener(requireActivity(),
                    OnCompleteListener<Location?> { task ->
                        if (task.isSuccessful) {
                            val location = tLocation.result
                            if (location != null) {
                                moveCamera(
                                    LatLng(
                                        location.latitude,
                                        location.longitude
                                    ),
                                    DEFAULT_ZOOM
                                )
                            } else {
//                                (requireActivity() as MainActivity).locationHelper!!.checkGPSEnabled()
//                                val mLastLocation =
//                                    (requireActivity() as MainActivity).locationHelper!!.location
//                                if (mLastLocation != null) {
//                                    moveCamera(
//                                        LatLng(
//                                            mLastLocation.latitude,
//                                            mLastLocation.longitude
//                                        ),
//                                        DEFAULT_ZOOM
//                                    )
//                                }
                            }
                        }
                    })
            }
        } catch (e: SecurityException) {
        }
    }

    private fun moveCamera(latLng: LatLng, zoom: Float) {
        lat = latLng.latitude
        lng = latLng.longitude
        if (mMap != null) {
            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
        }


        GetAddressFromLatLng(requireContext(), lat, lng).execute()
//        asyncTaskGetAddress!!.setLatLng(lat, lng)
//        asyncTaskGetAddress!!.execute()
    }

    private fun initMapViews() {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        getLocationPermission()
        initListeners()
    }

    private fun initListeners() {
        listener = object : PlaceEventHandler {
            override fun handle(placeid: String?, address: String?) {
                try {
                    val string: String =
                        "https://maps.googleapis.com/maps/api/place/details/json?input=&placeid=" + placeid + "&key=" + getString(
                            R.string.place_search_key
                        )
                    viewModel.getInfoByPlaceId(string)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun SetsPin(latLng: LatLng) {
        lat = latLng.latitude
        lng = latLng.longitude
        if (mMap != null) {
            mMap?.clear()
            mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13f))
        }
        val imm =
            requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.edSearch.windowToken, 0)
        placeModelArrayList = ArrayList<PlaceModel>()
    }

    private fun consumePlaceInfoByPlaceIdResponse(res: ApiResource<JsonObject>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        val json = JSONObject(res.data.toString())
                        val result = json.getJSONObject("result")
                        val geometry = result.getJSONObject("geometry")
                        val location = geometry.getJSONObject("location")
                        val formatted_address = result.getString("formatted_address")
                        updateAddressFields(formatted_address)
                        binding.recyclerView.setGone()

                        val latlng = LatLng(
                            location.getString("lat").toDouble(),
                            location.getString("lng").toDouble()
                        )
                        SetsPin(latlng)
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }

    private fun consumePlaceSearchResponse(res: ApiResource<ResponseBody>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        val json = JSONObject(res.data.toString())
                        try {
                            try {
                                val jsonArray = json.getJSONArray("predictions")
                                for (i in 0 until jsonArray.length()) {
                                    val placeModel = PlaceModel()
                                    placeModel.setDescription(
                                        jsonArray.getJSONObject(i).getString("description")
                                    )
                                    placeModel.setpId(
                                        jsonArray.getJSONObject(i).getString("place_id")
                                    )
                                    placeModelArrayList!!.add(placeModel)
                                }
                                if (placeModelArrayList!!.size > 0) {
                                    val placeListAdapter = PlaceListAdapter(
                                        activity,
                                        placeModelArrayList,
                                        listener
                                    )
                                    val mLayoutManager: RecyclerView.LayoutManager =
                                        LinearLayoutManager(activity)
                                    binding.recyclerView.addItemDecoration(
                                        DividerItemDecoration(
                                            activity,
                                            LinearLayoutManager.VERTICAL
                                        )
                                    )
                                    binding.recyclerView.layoutManager = mLayoutManager
                                    binding.recyclerView.adapter = placeListAdapter
                                    binding.recyclerView.visibility = View.VISIBLE
                                }
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }

    private fun makeUpdateReq(
        address_type: String,
        ID: String,
        house_no: String,
        apartment_name: String,
        street_details: String,
        landmark: String,
        mobile_number: String,
        contact_person: String,
        address: String,
        latitude: Double,
        longitude: Double
    ) {
        viewModel.getList(
            address_type,
            ID,
            house_no,
            apartment_name,
            street_details,
            landmark, mobile_number,
            contact_person,
            address, latitude, longitude,
        )
    }

    private fun consumeResponse(res: ApiResource<ApiResponse<ApiResponseDefault>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            Utils.showApiSuccess(activity, res.data.message)
                            binding.ab.ivBack.performClick()
                        } else {
                            CustomDialog.show(requireActivity(), res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }


    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val isGotLocation = intent.getBooleanExtra(Constants.KEY_LAT_LNG_UPDATED, false)
            if (isGotLocation) {
                updateAddress()
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if (!isEditAddress) {
            (requireActivity() as MainActivity).locationHelper!!.checkGPSEnabled()
            val mLastLocation =
                (requireActivity() as MainActivity).locationHelper!!.location
            if (mLastLocation != null) {
                moveCamera(
                    LatLng(
                        mLastLocation.latitude,
                        mLastLocation.longitude
                    ),
                    DEFAULT_ZOOM
                )
            }
        }
    }

    private fun updateAddress() {
        binding.edFullAddress.setText(Constants.ADDRESS_NAME)
        lat = Constants.LATITUDE
        lng = Constants.LONGITUDE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appLocationService = AppLocationService(requireActivity())
        viewModel = ViewModelProvider(this, providersFactory).get(MapViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentMapsBinding.bind(view)
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(
            mMessageReceiver,
            IntentFilter("location_update")
        )
        //location = appLocationService.getLocation(LocationManager.GPS_PROVIDER)!!

        //asyncTaskGetAddress = GetAddressFromLatLng(activity)
        model = arguments?.getSerializable("model") as AddressListModel?
        updateUI()
        initMapViews()
        if (isEditAddress) {
            binding.ab.tvABTitle.text = getString(R.string.edit_delivery_address)
        } else {
            binding.ab.tvABTitle.text = getString(R.string.add_delivery_address)
        }
        binding.ab.ivBack.setOnClickListener(View.OnClickListener {
            mNavController.navigateUp()
        })

        binding.ivClose.setOnClickListener {
            binding.edSearch.setText("")
            binding.recyclerView.setGone()
        }
        viewModel.getApiResponse().observe(viewLifecycleOwner) { res -> consumeResponse(res) }
        viewModel.searchPlaceResponse().observe(viewLifecycleOwner) { res ->
            consumePlaceSearchResponse(
                res
            )
        }
        viewModel.getResultByPlaceId().observe(viewLifecycleOwner) { res ->
            consumePlaceInfoByPlaceIdResponse(
                res
            )
        }
        binding.rgAddressType.setOnCheckedChangeListener { arg0, id ->
            when (id) {
                -1 -> addressType = ""
                R.id.rb_home -> addressType = "Home"
                R.id.rb_work -> addressType = "Work"
                R.id.rb_other -> addressType = "Other"
                else -> addressType = ""
            }
        }
        binding.btnSaveAddress.setOnClickListener(View.OnClickListener {
            if (!myPref.isLogin()) {
                (requireActivity() as MainActivity).showLoginDialog(requireActivity().getString(R.string.alert_login_address))
            } else {
                address = binding.edFullAddress.text.toString().trim()
                houseNo = binding.edHouseNo.text.toString().trim()
                apartment_name = binding.edApartmentName.text.toString().trim()
                street_details = binding.edStreetDetails.text.toString().trim()
                landmark = binding.edLandmark.text.toString().trim()
                mobile_number = binding.edPhoneNumber.text.toString().trim()
                contact_person = binding.edContactPerson.text.toString().trim()
                // String phoneNumber = binding.edPhoneNumber.getText().toString().trim();
                if (address.length == 0) {
                    UiUtils.showInputAlert(
                        activity,
                        getString(R.string.error_enter_address),
                        binding.edFullAddress
                    )
                } else if (houseNo.length == 0) {
                    UiUtils.showInputAlert(
                        activity,
                        getString(R.string.error_enter_house_no),
                        binding.edHouseNo
                    )
                } else if (addressType.length == 0) {
                    Utils.showWarning(
                        activity,
                        getString(R.string.error_select_any_one_address_type)
                    )
                } else {
                    makeUpdateReq(
                        addressType,
                        address_id,
                        houseNo,
                        apartment_name,
                        street_details,
                        landmark,
                        mobile_number,
                        contact_person,
                        address,
                        lat,
                        lng
                    )
                }
            }
        })

    }

    var textWatcher =
        CustomTypingEditText.OnTypingModified { view, isTyping ->
            if (isTyping) {
                Log.i("TAG", "onIsTypingModified: User started typing.")
            } else {
                Log.i("TAG", "onIsTypingModified: User stopped typing")
                handleTextChanges()
            }
        }

    var onFocusChangeListener =
        View.OnFocusChangeListener { view, b ->
            if (b) {
                binding.edSearch.text?.let { binding.edSearch.setSelection(it.length) }
                binding.ivClose.setVisible()
            } else {
                binding.ivClose.setGone()
            }
        }

    private fun handleTextChanges() {
        try {
            requireActivity().runOnUiThread {

                if (TextUtils.isEmpty(binding.edSearch.text.toString())) {
                    binding.recyclerView.setGone()
                    binding.ivClose.setGone()
                } else {
                    binding.ivClose.setVisible()
                    placeModelArrayList = ArrayList()
                    val url = ("https://maps.googleapis" +
                            ".com/maps/api/place/autocomplete/json?input=" + binding.edSearch.text
                        .toString()
                            + "&key=" + getString(R.string.place_search_key))

                    //viewModel.doSearchPlace(url)
                    viewModel.getAddressListRes(url)?.observe(
                        viewLifecycleOwner,
                        object : Observer<JsonObject> {
                            override fun onChanged(res: JsonObject) {
                                val json = JSONObject(res.toString())
                                try {
                                    try {
                                        val jsonArray =
                                            json.getJSONArray("predictions")
                                        for (i in 0 until jsonArray.length()) {
                                            val placeModel = PlaceModel()
                                            placeModel.setDescription(
                                                jsonArray.getJSONObject(i)
                                                    .getString(
                                                        "description"
                                                    )
                                            )
                                            placeModel.setpId(
                                                jsonArray.getJSONObject(i)
                                                    .getString(
                                                        "place_id"
                                                    )
                                            )
                                            placeModelArrayList!!.add(placeModel)
                                        }
                                        if (placeModelArrayList!!.size > 0) {
                                            val placeListAdapter = PlaceListAdapter(
                                                activity,
                                                placeModelArrayList,
                                                listener
                                            )
                                            val mLayoutManager: RecyclerView.LayoutManager =
                                                LinearLayoutManager(activity)
                                            binding.recyclerView.addItemDecoration(
                                                DividerItemDecoration(
                                                    activity,
                                                    LinearLayoutManager.VERTICAL
                                                )
                                            )
                                            binding.recyclerView.layoutManager =
                                                mLayoutManager
                                            binding.recyclerView.adapter =
                                                placeListAdapter
                                            binding.recyclerView.visibility =
                                                View.VISIBLE
                                        }
                                    } catch (e: JSONException) {
                                        e.printStackTrace()
                                    }
                                } catch (e: java.lang.Exception) {
                                    e.printStackTrace()
                                }
                            }
                        })
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun updateUI() {
        if (model != null) {
            isEditAddress = true
            address_id = model!!.address_id
            addressType = model!!.address_type
            binding.edFullAddress.setText(model!!.address)
            binding.edHouseNo.setText(model!!.house_no)
            binding.edApartmentName.setText(model!!.apartment_name)
            binding.edStreetDetails.setText(model!!.street)
            binding.edLandmark.setText(model!!.landmark)
            binding.edPhoneNumber.setText(model!!.mobile_number)
            binding.edContactPerson.setText(model!!.contact_person)
            if (TextUtils.isEmpty(model!!.contact_person)) {
                binding.edContactPerson.setText(myPref.getFullName())
            }
            if (TextUtils.isEmpty(model!!.mobile_number)) {
                binding.edPhoneNumber.setText(myPref.getMobileNumber())
            }
            binding.typeGroup.visibility = View.GONE
            lat = model!!.latitude
            lng = model!!.longitude
            address = model!!.address
        } else {
            binding.edPhoneNumber.setText(myPref.getMobileNumber())
            binding.edContactPerson.setText(myPref.getFullName())
            binding.typeGroup.visibility = View.VISIBLE
            //binding.edPhoneNumber.setText(pojo.getPhoneNumber());
        }
    }


    private val callback = OnMapReadyCallback { googleMap ->
        mMap = googleMap
        if (mLocationPermissionsGranted) {
            if (!isEditAddress) {
                getDeviceLocation()
            } else {
                //mMap.scrollTo(10,500);
                SetsPin(LatLng(lat, lng))
            }
            if (ActivityCompat.checkSelfPermission(
                    requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            )
                mMap?.getUiSettings()?.setMyLocationButtonEnabled(false);
            mMap?.setOnCameraMoveListener(OnCameraMoveListener {
                mMap?.clear()
                //binding.imgLocationPinUp.visibility = View.VISIBLE
            })
            mMap?.setOnCameraIdleListener(OnCameraIdleListener {
                var latlng = mMap?.cameraPosition?.target!!
                if (latlng != null) {
                    lat = latlng.latitude
                    lng = latlng.longitude
                    LocationAddress().getAddressFromLocation(
                        lat,
                        lng,
                        requireActivity(),
                        GeoCodeHandler()
                    )
                }
            })
        }
    }


    internal inner class GeoCodeHandler : Handler() {
        override fun handleMessage(message: Message) {
            when (message.what) {
                1 -> {
                    val bundle = message.data
                    bundle.getString("address")?.let { updateAddressFields(it) }
                }
            }

        }
    }


    private fun updateAddressFields(locationAddress: String) {
        binding.edFullAddress.setText(locationAddress)
        binding.edSearch.setOnTypingModified(null)
        binding.edSearch.setText(locationAddress)
        binding.edSearch.setOnTypingModified(textWatcher)
        binding.edSearch.onFocusChangeListener = onFocusChangeListener
    }


    private fun getLocationPermission() {
        val permissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            if (ContextCompat.checkSelfPermission(
                    requireActivity(),
                    COURSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                mLocationPermissionsGranted = true
                initMap()
            } else {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                permissions,
                LOCATION_PERMISSION_REQUEST_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        mLocationPermissionsGranted = false
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.size > 0) {
                    var i = 0
                    while (i < grantResults.size) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false
                            return
                        }
                        i++
                    }
                    mLocationPermissionsGranted = true
                    //initialize our map
                    initMap()
                }
            }
        }
    }

    private fun initMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }


}