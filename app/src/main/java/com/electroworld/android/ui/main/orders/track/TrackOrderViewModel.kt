package com.electroworld.android.ui.main.orders.track

import androidx.lifecycle.ViewModel
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import javax.inject.Inject

class TrackOrderViewModel @Inject constructor(val mainApi: MainApi, val myPref : PreferencesManager) : ViewModel() {
}