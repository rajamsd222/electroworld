package com.electroworld.android.ui.main.fav

import android.app.Activity
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.electroworld.android.R
import com.electroworld.android.databinding.RowFavouritesBinding
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.ProductModel
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.utils.ScreenType
import com.electroworld.android.ui.main.products.VariantListAdapter
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.MyUtils
import java.util.ArrayList

class FavListAdapter(fragment: FavouritesFragment, val context: Activity, private val myPref: PreferencesManager) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items = arrayListOf<ProductModel>()
    var parentFragment: FavouritesFragment = fragment

    var bottomSheetDialog: BottomSheetDialog? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowFavouritesBinding =
            RowFavouritesBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(binding: RowFavouritesBinding) : RecyclerView.ViewHolder(binding.root) {
        private val binding: RowFavouritesBinding = binding
        fun bind(position: Int) {
            val model: ProductModel = items[position]
            binding.item = model
            binding.executePendingBindings()

            if (model.group_variants != null && model.group_variants.size > 1) {
                binding.tvGroupVariant.visibility = View.VISIBLE
                binding.tvVariantName.visibility = View.GONE
            } else  {
                if(model.variant1 != null && model.variant1!= null){
                    binding.tvVariantName.visibility = View.VISIBLE
                }else{
                    binding.tvVariantName.visibility = View.GONE
                }
                binding.tvGroupVariant.visibility = View.GONE
            }

            if(AppValidator.toStr(model.discount_label).length > 0){
                binding.tvOffer.visibility = View.VISIBLE
            }
            else{
                binding.tvOffer.visibility = View.GONE
            }
//
//            if (AppValidator.toStr(model.type).equals(Constants.KEY_TYPE_NON_VEG)) {
//                binding.ivType.setImageResource(R.mipmap.icon_non_veg)
//            } else if (AppValidator.toStr(model.type).equals(Constants.KEY_TYPE_VEG)) {
//                binding.ivType.setImageResource(R.mipmap.icon_veg)
//            } else {
//                binding.ivType.visibility = View.GONE
//            }
            val singleItemQuantity: Int =
                CartUtils.getSingleItemQuantity(myPref, AppValidator.toStr(model.warehouse_product_id))
            if (singleItemQuantity > 0) {
                binding.tvAdd.visibility = View.GONE
                binding.layCount.visibility = View.VISIBLE
            } else {
                binding.tvAdd.visibility = View.VISIBLE
                binding.layCount.visibility = View.GONE
            }

            binding.tvQty.text = singleItemQuantity.toString()
            val isAdded = model.is_favourite == 1
            binding.ivFav.setImageResource(if (isAdded) R.drawable.ic_favorite_color_primary_24dp else R.drawable.ic_favorite_border_black_24dp)

            if (AppValidator.toDouble(model.discounted_price) > 0) {
                binding.tvMRP.setVisibility(View.VISIBLE)
                binding.tvAmount.setText(MyUtils.formatCurrencyfromString(model.discounted_price))
                binding.tvMRP.setText(MyUtils.formatCurrencyfromString(model.price))
                binding.tvMRP.setPaintFlags(binding.tvMRP.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)
            } else {
                binding.tvMRP.setVisibility(View.GONE)
                binding.tvAmount.setText(MyUtils.formatCurrencyfromString(model.price))
            }
            if (AppValidator.toDouble(model.price) == AppValidator.toDouble(model.discounted_price)) {
                binding.tvMRP.setVisibility(View.GONE)
            }



            binding.tvGroupVariant.setOnClickListener(View.OnClickListener {
                showVariantDialog(model,binding.tvGroupVariant,position)
            })

            binding.ivFav.setOnClickListener {
                parentFragment.setAsFavouriteProduct(
                    position, items.get(
                        position
                    )
                )
            }

            binding.tvAdd.setOnClickListener {
                parentFragment.doAddToCart(
                    items[position],
                    true,
                    false,
                    1
                )
            }
            binding.tvIncrement.setOnClickListener {
                parentFragment.doAddToCart(
                    items[position], true, false, 1
                )
            }
            binding.tvDecrement.setOnClickListener {
                parentFragment.doAddToCart(
                    items[position], false, false, 1
                )
            }

            binding.parentView.setOnClickListener(View.OnClickListener {
                parentFragment.openDetailsScreen(model)
            })
        }

    }


    fun setItems(list: List<ProductModel>?) {
        if (list == null) {
            this.items.clear()
        } else {
            this.items = ArrayList(list)
        }
        notifyDataSetChanged()
    }

    fun clearAll() {
        this.items.clear()
        notifyDataSetChanged()
    }

    fun updateItem(pos: Int, item: ProductModel) {
        this.items.set(pos, item)
        notifyDataSetChanged()
    }

    fun showVariantDialog(model: ProductModel, tvGroupVariant: TextView, position: Int){
        val dialogView: View = context.getLayoutInflater().inflate(R.layout.dialog_variant, null)
        bottomSheetDialog = BottomSheetDialog(context, R.style.CustomBottomSheetDialog)
        bottomSheetDialog!!.setContentView(dialogView)
        var tvTitle = bottomSheetDialog!!.findViewById<TextView>(R.id.tvTitle)
        var recyclerView = bottomSheetDialog!!.findViewById<RecyclerView>(R.id.recycler_view)
        var adapter = VariantListAdapter(
            model.group_variants,
            parentFragment,
            context,
            bottomSheetDialog,
            tvGroupVariant,
            model,
            position,
            ScreenType.FAV
        )
        recyclerView?.adapter = adapter
        tvTitle!!.text = model.name_en/* + " - " + model.variant_en*/
        bottomSheetDialog!!.show()
    }
}
