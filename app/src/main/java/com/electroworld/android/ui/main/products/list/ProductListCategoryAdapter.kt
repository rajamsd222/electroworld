package com.electroworld.android.ui.main.products.list

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.databinding.RowItemProductCategoryBinding
import com.electroworld.android.model.CategoryModel
import java.util.*
import kotlin.collections.ArrayList

class ProductListCategoryAdapter(fragment: ProductListFragment,val context: Activity) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items = arrayListOf<CategoryModel>()
    var parentFragment: ProductListFragment = fragment

    inner class ViewHolder(binding: RowItemProductCategoryBinding) : RecyclerView.ViewHolder(binding.root) {
        private val binding: RowItemProductCategoryBinding = binding
        fun bind(position: Int) {
            val model: CategoryModel = items[position]
            binding.item = model
            binding.executePendingBindings()
            if(model.isSelected) binding.lineGreen.visibility = View.VISIBLE else binding.lineGreen.visibility = View.INVISIBLE
            binding.rowParent.setTag(position)
            binding.rowParent.setOnClickListener {
                val pos = it.getTag() as Int
                for (i in items.indices) {
                    items[i].isSelected = i == pos
                }
                parentFragment.updateSubCategoriesListData(items.get(pos),-1,)
                notifyDataSetChanged()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowItemProductCategoryBinding =
            RowItemProductCategoryBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(list: List<CategoryModel>?) {
        if (list == null) {
            this.items.clear()
        } else {
            this.items = ArrayList(list)
        }
        notifyDataSetChanged()
    }

    fun getItems(): ArrayList<CategoryModel> {
        return this.items
    }

    fun clearAll() {
        this.items.clear()
        notifyDataSetChanged()
    }
    fun removeItem(item: CategoryModel?) {
        items.remove(item)
        notifyDataSetChanged()
    }

}
