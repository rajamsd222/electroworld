package com.electroworld.android.ui.main.wallet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.bind.AppBinder
import com.electroworld.android.databinding.FragmentWalletBinding
import com.electroworld.android.databinding.RowTransactionListBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.TransactionListModel
import com.electroworld.android.ui.main.cashfree.CashFreePaymentFragment
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.PaymentType
import com.electroworld.android.utils.ScreenType
import com.electroworld.android.utils.Utils
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_title_cart.view.*
import javax.inject.Inject

class WalletFragment : DaggerFragment() {

    private lateinit var viewModel: WalletViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding : FragmentWalletBinding

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory
    @Inject
    lateinit var myPref: PreferencesManager
    private var mAdapter: GenericAdapter<TransactionListModel, RowTransactionListBinding>? = null
    private val disposable = CompositeDisposable()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_wallet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this,providersFactory).get(WalletViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentWalletBinding.bind(view)
        binding.ab.tvABTitle.text = "My Wallet"
        binding.cvAddMoney.setGone()
        AppBinder.formatCurrencyFromDouble(binding.tvWalletAmount,myPref.getWalletAmount())
        binding.ab.ivBack.setOnClickListener {
            if (binding.btnLoadMoney.visibility == View.VISIBLE) {
                mNavController.navigateUp()
            } else {
                binding.cvAddMoney.setGone()
                binding.btnLoadMoney.setVisible()
            }

        }

        initRecyclerView()
        makeTransactionListReq()
        binding.btnLoadMoney.setOnClickListener {
            binding.cvAddMoney.setVisible()
            binding.btnLoadMoney.setGone()
        }

        binding.tvAmount1.setOnClickListener {
            binding.edAmount.setText("200")
        }
        binding.tvAmount2.setOnClickListener {
            binding.edAmount.setText("500")
        }
        binding.tvAmount3.setOnClickListener {
            binding.edAmount.setText("1000")
        }

        binding.btnAddMoney.setOnClickListener {
            var amount = AppValidator.toDouble(binding.edAmount.text.toString().trim())
            if(amount>0){
                loadMoney(amount)
            }
            else{
                Utils.showWarning(activity,"Please enter valid amount")
            }
        }
    }

    private fun initRecyclerView() {
        mAdapter = object :
            GenericAdapter<TransactionListModel, RowTransactionListBinding>(R.layout.row_transaction_list) {
            override fun onBindData(
                model: TransactionListModel?,
                position: Int,
                dataBinding: RowTransactionListBinding?
            ) {
                dataBinding?.item = model
            }

            override fun onItemClick(model: TransactionListModel?, position: Int) {}
        }
        binding.recyclerView.apply {
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            adapter = mAdapter
        }
    }

    private fun makeTransactionListReq() {
        binding.tvEmptyMsg.setGone()
        disposable.add(
            viewModel.getTransactionList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<TransactionListModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<TransactionListModel>>) {
                        if (res.http_code == 200) {
                            mAdapter?.addItems(res.data)
                            if (mAdapter?.itemCount == 0) {
                                binding.cvTransactionList.setGone()
                                binding.tvEmptyMsg.setVisible()
                            } else {
                                binding.cvTransactionList.setVisible()
                                binding.tvEmptyMsg.setGone()
                            }
                        } else {
                            CustomDialog.show(requireActivity(),res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(),e.message,AlertType.SERVER_ERROR)
                    }
                })
        )
    }
    private fun loadMoney(amount : Double) {
        val cfTokenNeed = "1"
        viewModel.doAddMoneyRes(requireActivity(),amount,cfTokenNeed)?.
        observe(viewLifecycleOwner,{ res ->
                if (res != null) {
                    if (res.http_code == 200) {
                        openCashFreeScreen(res.data.cftoken,res.data.amount_total,res.data.id,"")
                    } else {
                        CustomDialog.show(requireActivity(),res.message, AlertType.ERROR)
                    }
                }
            })
    }

    private fun openCashFreeScreen(token : String,amount : String,orderId : String,id: String){
        val bundle = CashFreePaymentFragment.updateBundle(
            token,
            amount,
            orderId,
            id,
            PaymentType.DEFAULT,
            ScreenType.WALLET
        )
        mNavController.navigate(R.id.action_walletFragment_to_cashFreePaymentFragment, bundle)
    }
}