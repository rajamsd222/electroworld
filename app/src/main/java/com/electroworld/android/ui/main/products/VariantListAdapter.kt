package com.electroworld.android.ui.main.products

import android.app.Activity
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.databinding.ListItemVariantBinding
import com.electroworld.android.model.ProductModel
import com.electroworld.android.ui.main.fav.FavouritesFragment
import com.electroworld.android.ui.main.offers.OffersFragment
import com.electroworld.android.ui.main.products.detail.ProductDetailFragment
import com.electroworld.android.ui.main.products.list.ProductListFragment
import com.electroworld.android.ui.main.products.search.SearchFragment
import com.electroworld.android.ui.main.quickbuy.QuickBuyFragment
import com.electroworld.android.utils.ScreenType
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.util.*

class VariantListAdapter(
    items: List<GroupVariantsModel>,
    fragment: Fragment,
    val context: Activity,
    var bottomSheetDialog: BottomSheetDialog? = null,
    tvGroupVariant: TextView,
    model: ProductModel,
    listposition: Int,
    screenType: ScreenType
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items: List<GroupVariantsModel> = ArrayList<GroupVariantsModel>()
    var parentFragment: Fragment
    var tvGroupVariant: TextView
    var productModel: ProductModel
    var listposition = 0
    val list: MutableList<GroupVariantsModel> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: ListItemVariantBinding =
            ListItemVariantBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(binding: ListItemVariantBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val binding: ListItemVariantBinding = binding
        fun bind(position: Int) {
            val model: GroupVariantsModel = items[position]
            binding.item = model
            binding.executePendingBindings()

            binding.tvMRP.paintFlags = binding.tvMRP.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

            if (productModel.warehouse_product_id != null)
                if (productModel.warehouse_product_id == model.warehouse_product_id) {
                    binding.parentLayout.setBackgroundResource(R.drawable.variant_selected_bg)
                    binding.tvName.setTextColor(Color.WHITE)
                    binding.tvAmount.setTextColor(Color.WHITE)
                    binding.ivLeft.setImageResource(R.mipmap.circle_tick_icon)
                } else {
                    binding.parentLayout.setBackgroundResource(R.drawable.variant_default_bg)
                    binding.ivLeft.setImageResource(R.mipmap.dot_icon)
                }

            binding.parentLayout.setOnClickListener {
                //tvGroupVariant.text = model.variant_en
                bottomSheetDialog!!.dismiss()
                if (parentFragment is ProductListFragment) {
                    (parentFragment as ProductListFragment).getProductInfoByVariantId(
                        listposition, productModel, model.warehouse_product_id
                    )
                } else if (parentFragment is SearchFragment) {
                    (parentFragment as SearchFragment).getProductInfoByVariantId(
                        listposition, productModel, model.warehouse_product_id
                    )
                } else if (parentFragment is OffersFragment) {
                    (parentFragment as OffersFragment).getProductInfoByVariantId(
                        listposition, productModel, model.warehouse_product_id
                    )
                } else if (parentFragment is QuickBuyFragment) {
                    (parentFragment as QuickBuyFragment).getProductInfoByVariantId(
                        listposition, productModel, model.warehouse_product_id
                    )
                } else if (parentFragment is ProductDetailFragment) {
                    (parentFragment as ProductDetailFragment).getProductInfoByVariantId(
                        listposition, productModel, model.warehouse_product_id
                    )
                } else if (parentFragment is FavouritesFragment) {
                    (parentFragment as FavouritesFragment).getProductInfoByVariantId(
                        listposition, productModel, model.warehouse_product_id
                    )
                }
            }

        }

    }

    init {
        this.items = items
        parentFragment = fragment
        this.tvGroupVariant = tvGroupVariant
        this.productModel = model
        this.listposition = listposition
    }

}