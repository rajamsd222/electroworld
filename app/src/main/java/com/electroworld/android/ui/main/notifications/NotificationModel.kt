package com.electroworld.android.ui.main.notifications

data class NotificationModel(
    val title: String,
    val message: String,
    val created_at: String
)