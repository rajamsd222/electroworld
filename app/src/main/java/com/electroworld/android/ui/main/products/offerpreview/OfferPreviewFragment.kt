package com.electroworld.android.ui.main.products.offerpreview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.databinding.FragmentOfferPreviewBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.PlaceHolderDetailsApiModel
import com.electroworld.android.repository.PIMSRepository
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import javax.inject.Inject

class OfferPreviewFragment : DaggerFragment() {

    companion object {
        private const val ARG_PLACE_HOLDER_ID = "placeholder_id"
        fun getPlaceHolderBundles(id: String?, title: String?): Bundle {
            val bundle = Bundle()
            bundle.putString(ARG_PLACE_HOLDER_ID, id)
            return bundle
        }
    }

    private lateinit var viewModel: OfferPreviewViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentOfferPreviewBinding

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    @Inject
    lateinit var pims: PIMSRepository
    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_offer_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(OfferPreviewViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentOfferPreviewBinding.bind(view)
        binding.ab.tvABTitle.text = ""
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }

        var placeholder_id = arguments?.getString(ARG_PLACE_HOLDER_ID)
        if (placeholder_id != null) {
            getPlaceHolderDetails(placeholder_id)
        }

    }

    private fun getPlaceHolderDetails(placeHolderId: String) {
        disposable.add(
            pims.getPlaceHolderDetailsNew(placeHolderId, 1)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<PlaceHolderDetailsApiModel>>() {
                    override fun onSuccess(res: ApiResponse<PlaceHolderDetailsApiModel>) {
                        if (res.http_code == 200) {
                            res.data.placeholder.forEachIndexed { index, placeholder ->
                                if (index == 0) {
                                    binding.item = placeholder
                                    binding.tcDesc.text = HtmlCompat.fromHtml(
                                        placeholder.description,
                                        HtmlCompat.FROM_HTML_MODE_LEGACY
                                    );
                                }
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    override fun onDestroyView() {
        disposable.clear()
        super.onDestroyView()
    }
}