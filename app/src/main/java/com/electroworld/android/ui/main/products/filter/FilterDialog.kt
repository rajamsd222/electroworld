package com.electroworld.android.ui.main.products.filter

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.model.filter.FilterModel
import com.electroworld.android.model.filter.SortBy
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.ScreenType
import java.util.*
import kotlin.collections.ArrayList


class FilterDialog(
    private val context: MainActivity,
    private val okClickListener: FilterOKClickListener?,
    private val screenType: ScreenType
) {
    private val isCancelable = true
    var dialog: Dialog? = null
    var mutableList: MutableList<FilterModel> = ArrayList()
    var sortByMutableList: MutableList<SortBy> = ArrayList()
    var sortBy: String = ""

    fun setItems(mFiltersList: List<FilterModel>, mSortByList: List<SortBy>, sort_by: String) {
        if (mSortByList == null) {
            sortByMutableList = ArrayList()
        } else {
            sortByMutableList.addAll(mSortByList)
        }
        if (mFiltersList == null) {
            mutableList = ArrayList()
        } else {
            mutableList.addAll(mFiltersList)
        }
        sortBy = sort_by
    }

    fun showDialog() {
        //dialog = new Dialog(context, R.style.Theme_Dialog);
        dialog = Dialog(context, R.style.AppTheme)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(isCancelable)
        dialog!!.setCanceledOnTouchOutside(isCancelable)
        dialog!!.setContentView(R.layout.fragment_filter)
        Objects.requireNonNull(dialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val mDialogOk = dialog!!.findViewById<Button>(R.id.btnApply)
        val mDialogNo = dialog!!.findViewById<Button>(R.id.btnClearAll)
        val ivClose = dialog!!.findViewById<ImageView>(R.id.ivClose)
        val recyclerView: RecyclerView = dialog!!.findViewById(R.id.recycler_view)
        val filterlayout = dialog!!.findViewById<LinearLayout>(R.id.filterlayout)
        val sortByLayout = dialog!!.findViewById<LinearLayout>(R.id.sortByLayout)
        val tvFilterBy = dialog!!.findViewById<TextView>(R.id.tvFilterBy)
        val tvSortBy = dialog!!.findViewById<TextView>(R.id.tvSortBy)
        val rgSortBy = dialog!!.findViewById<RadioGroup>(R.id.rgSortBy)
        val mAdapter = FilterRecyclerAdapter(context)

        tvSortBy.visibility = if (screenType == ScreenType.PLACE_HOLDER) View.GONE else View.VISIBLE

        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = mAdapter
        mAdapter.setItems(mutableList)
        mAdapter.notifyDataSetChanged()

        for (item in sortByMutableList) {
            val radioButton = RadioButton(context)
            radioButton.text = item.title
            if (sortBy == item.title) {
                Constants.SORT_BY = item.key
                Constants.ORDER_BY = item.order_by
                Constants.SORT_BY_TITLE = item.title
            }
            radioButton.setOnClickListener {
                Constants.SORT_BY = item.key
                Constants.ORDER_BY = item.order_by
                Constants.SORT_BY_TITLE = item.title
                update(item.title, rgSortBy)
            }
            rgSortBy.addView(radioButton)
        }
        update(sortBy, rgSortBy)

        tvFilterBy.setOnClickListener {
            filterlayout.visibility = View.VISIBLE
            sortByLayout.visibility = View.GONE
            tvFilterBy.setBackgroundResource(R.drawable.filter_icon_bg)
            tvSortBy.setBackgroundResource(0)
        }
        tvSortBy.setOnClickListener {
            filterlayout.visibility = View.GONE
            sortByLayout.visibility = View.VISIBLE
            tvFilterBy.setBackgroundResource(0)
            tvSortBy.setBackgroundResource(R.drawable.filter_icon_bg)
        }
        mDialogOk.setOnClickListener { v: View? ->
            dialog!!.dismiss()
            okClickListener?.onOKClick(v, "apply")
        }
        mDialogNo.setOnClickListener { v: View? ->
            dialog!!.dismiss()
            okClickListener?.onOKClick(v, "clear")
        }
        ivClose.setOnClickListener { dialog!!.dismiss() }
        dialog!!.show()
    }

    fun update(title: String, group: RadioGroup) {
        for (i in 0 until group.childCount) {
            val view = group.getChildAt(i) as RadioButton
            view.isChecked = view.text == title
        }
    }

    fun dismissDialog() {
        if (dialog != null) dialog!!.dismiss()
    }

    interface FilterOKClickListener {
        fun onOKClick(view: View?, tag: String?)
    }
}