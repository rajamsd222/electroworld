package com.electroworld.android.ui.main.cart

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.electroworld.android.R
import com.electroworld.android.databinding.RowCartListBinding
import com.electroworld.android.ui.main.cart.models.CartSingleItem
import java.util.*

class CartListAdapter(fragment: CartListFragment, val context: Activity) :
    RecyclerSwipeAdapter<RecyclerView.ViewHolder>() {
    private var items = arrayListOf<CartSingleItem>()
    var parentFragment: CartListFragment = fragment

    inner class ViewHolder(private val binding: RowCartListBinding) : RecyclerView.ViewHolder(binding.root) {

        var swipeRevealLayout: SwipeLayout? = null
        fun bind(position: Int) {
            val model: CartSingleItem = items[position]
            binding.item = model
            swipeRevealLayout = binding.swipeRevealLayout
            binding.executePendingBindings()
            binding.tvQty.text = model.quantity.toString()
            binding.executePendingBindings()
            binding.tvIncrement.setOnClickListener {
                parentFragment.updateCart(
                    model,
                    true,
                    false,
                    1
                )
            }
            binding.tvDecrement.setOnClickListener {
                parentFragment.updateCart(
                    model,
                    false,
                    false,
                    1
                )
            }


            binding.deleteLayout.setOnClickListener { parentFragment.deleteCartItem(model) }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowCartListBinding =
            RowCartListBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)

        holder.swipeRevealLayout?.setShowMode(
            SwipeLayout.ShowMode.LayDown
        )
        holder.swipeRevealLayout?.addSwipeListener(
            object : SimpleSwipeListener() {
                override fun onOpen(layout: SwipeLayout) {
                    //YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.trash));
                }
            })
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipeRevealLayout
    }

    fun setItems(list: List<CartSingleItem>?) {
        if (list == null) {
            this.items.clear()
        } else {
            this.items = ArrayList(list)
        }
        notifyDataSetChanged()
    }

    fun clearAll() {
        this.items.clear()
        notifyDataSetChanged()
    }
    fun removeItem(item: CartSingleItem?) {
        items.remove(item)
        notifyDataSetChanged()
    }
}
