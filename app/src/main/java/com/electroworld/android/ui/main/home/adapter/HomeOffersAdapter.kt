package com.electroworld.android.ui.main.home.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.databinding.RowItemHomeOffersBinding
import com.electroworld.android.model.Placeholder
import com.electroworld.android.ui.main.products.list.ProductListFragment
import java.util.ArrayList

class HomeOffersAdapter (
    mNavController: NavController,
    posts: List<Placeholder>,
    fragment: Fragment,
    val context: Activity
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var posts: List<Placeholder> = ArrayList<Placeholder>()
    var parentFragment: Fragment
    var mNavController : NavController = mNavController
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowItemHomeOffersBinding =
            RowItemHomeOffersBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    inner class ViewHolder(binding: RowItemHomeOffersBinding) : RecyclerView.ViewHolder(binding.root) {
        private val binding: RowItemHomeOffersBinding = binding
        fun bind(position: Int) {
            val model: Placeholder = posts[position]
            binding.item = model
            binding.executePendingBindings()
            binding.parentView.setTag(model)
            binding.parentView.setOnClickListener {
                var tag = it.getTag() as Placeholder?
                /*val bundle = Bundle()
                bundle.putString("screenType", "home")
                bundle.putString("url", tag?.cms_page)
                bundle.putString("name", tag?.title)
                mNavController.navigate(R.id.action_navigation_home_to_webViewFragment, bundle)*/
                val bundle = ProductListFragment.getPlaceHolderBundles(tag?.id,tag?.title)
                mNavController.navigate(R.id.action_navigation_home_to_productListFragment, bundle)
            }

        }

    }

    init {
        this.posts = posts
        parentFragment = fragment
    }

}