package com.electroworld.android.ui.main.products

data class GroupVariantsModel (
    val id: String,
    val variant_en: String,
    val value_en: String,
    val variant1: String,
    val variant2: String,
    val variant_type1_name_en: String,
    val variant_type2_name_en: String,
    val group_id: String,
    val product_id: String,
    val warehouse_product_id: String,
    val price: String,
    val discounted_price: String
)