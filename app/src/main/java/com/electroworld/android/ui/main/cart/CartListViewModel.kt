package com.electroworld.android.ui.main.cart

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.model.*
import com.electroworld.android.repository.PIMSRepository
import com.electroworld.android.ui.UIHandler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CartListViewModel @Inject constructor(private val pimsRepository: PIMSRepository) : ViewModel() {

    private val disposable = CompositeDisposable()
    val apiError = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<String>()
    val uiHandlerLiveData = MutableLiveData<UIHandler>()

    val deliverySlotApi = MutableLiveData<DeliverySlotApiModel>()

    fun getDeliverySlots(){
        disposable.add(
            pimsRepository.getDeliverySlots()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<DeliverySlotApiModel>>() {
                    override fun onSuccess(res: ApiResponse<DeliverySlotApiModel>) {
                        loading.value = false
                        uiHandlerLiveData.value = UIHandler("","",false)
                        if(res.http_code==200){
                            deliverySlotApi.value = res.data
                        }
                        else{
                            apiError.value = res.message
                        }
                    }

                    override fun onError(e: Throwable) {
                        uiHandlerLiveData.value = UIHandler("","",false)
                        errorMessage.value = e.message
                        loading.value = false
                    }
                })
        )
    }

}