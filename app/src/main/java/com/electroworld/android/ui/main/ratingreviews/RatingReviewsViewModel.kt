package com.electroworld.android.ui.main.ratingreviews

import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.model.RatingReviewsListModel
import com.electroworld.android.repository.PIMSRepository
import io.reactivex.Single
import javax.inject.Inject

class RatingReviewsViewModel   @Inject constructor(private val repo : PIMSRepository) : ViewModel() {

    fun getRatingReviewsList(): Single<ApiResponse<List<RatingReviewsListModel>>> {
        return repo.getRatingReviewsList()
    }
}