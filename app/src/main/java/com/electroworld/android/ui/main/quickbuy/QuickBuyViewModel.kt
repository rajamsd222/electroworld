package com.electroworld.android.ui.main.quickbuy

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.HomeModel
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.SetFavModel
import com.electroworld.android.utils.AppUtils
import com.electroworld.android.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class QuickBuyViewModel @Inject constructor(val mainApi: MainApi,val myPref: PreferencesManager) : ViewModel() {

    private val disposable = CompositeDisposable()



    private fun getProductInfo(
        context: Context,
        warehouse_product_id: String
    ): MutableLiveData<ApiResponse<ProductModel>?>? {
        val res: MutableLiveData<ApiResponse<ProductModel>?> =
            MutableLiveData<ApiResponse<ProductModel>?>()
        val dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.getProductInfoByVariant(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            warehouse_product_id
        )
            .enqueue(object : Callback<ApiResponse<ProductModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<ProductModel>?>,
                    response: Response<ApiResponse<ProductModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponse<ProductModel>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun getProductInfoRes(
        context: Context,
        warehouse_product_id: String
    ): LiveData<ApiResponse<ProductModel>?>? {
        return getProductInfo(
            context,
            warehouse_product_id
        )
    }

    private val responseLiveDataFav = MutableLiveData<ApiResource<ApiResponse<SetFavModel>>>()

    fun setAsFav(
        warehouse_product_id: String,
        product_id: String,
        is_favourite: Int,
        group_id: String
    ){
        disposable.clear()
        disposable.add(mainApi.setFav(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            warehouse_product_id,
            product_id,
            is_favourite,
            group_id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveDataFav.setValue(
                    ApiResource.loading<ApiResponse<SetFavModel>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveDataFav.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveDataFav.setValue(
                    ApiResource.error<ApiResponse<SetFavModel>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getFavResponse(): MutableLiveData<ApiResource<ApiResponse<SetFavModel>>> {
        return responseLiveDataFav
    }

    fun getQuickBuyData(
        context: Context,
        page: String,
        pageNo: Int
    ): MutableLiveData<ApiResponse<List<HomeModel>>?>? {
        val res: MutableLiveData<ApiResponse<List<HomeModel>>?> =
            MutableLiveData<ApiResponse<List<HomeModel>>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.getHomeListCall(myPref.getPrefKeyAccessToken(), myPref.getPrefKeyCustomerId(),
            page, myPref.getPrefKeyStoreId(), myPref.getPrefKeyVendorId(),pageNo
        )
            .enqueue(object : Callback<ApiResponse<List<HomeModel>>?> {
                override fun onResponse(
                    call: Call<ApiResponse<List<HomeModel>>?>,
                    response: Response<ApiResponse<List<HomeModel>>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponse<List<HomeModel>>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun getQuickBuyDataRes(
        context: Context,
        page: String,
        pageNo: Int
    ): LiveData<ApiResponse<List<HomeModel>>?>? {
        return getQuickBuyData(
            context,
            page,
            pageNo
        )
    }
    override fun onCleared() {
        disposable.clear()
    }
}