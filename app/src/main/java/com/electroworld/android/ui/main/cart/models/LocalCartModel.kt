package com.electroworld.android.ui.main.cart.models

import com.electroworld.android.model.DeliverySlot
import com.electroworld.android.utils.AppValidator
import java.util.*

class LocalCartModel {
    var cartItemList: MutableList<CartSingleItem> = ArrayList()
        get() = if (field == null) ArrayList() else field
    var promocode = ""
        get() = AppValidator.toStr(field)
    var promocodeDiscountAmount = 0.0
    var promocodeDiscountPercentage = 0.0
    var isGiftEnabled = false
    var giftAmount = 0.0
    var giftFrom = ""
    var giftTo = ""
    var giftMessage = ""
    var deliveryCharge = 0.0
    var isWalletEnabled = false
    var isPickUpFromStoreEnabled = false
    var deliverySlot: DeliverySlot? = null
    var pickupSlot: DeliverySlot? = null
    var orderInstruction: String? = null
    var isBillingAddressEnabled = true
    var billing_address = ""
    var billing_phone = ""
    var billing_contact_person = ""
}