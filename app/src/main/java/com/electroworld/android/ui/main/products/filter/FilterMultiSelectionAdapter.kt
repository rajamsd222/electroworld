package com.electroworld.android.ui.main.products.filter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.databinding.RowCheckboxBinding
import com.electroworld.android.model.filter.FilterData

class FilterMultiSelectionAdapter(
    val context: Activity,
    private var list: List<FilterData>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(),Filterable {

    var items = ArrayList<FilterData>()

    init {
        items = list as ArrayList<FilterData>
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowCheckboxBinding =
            RowCheckboxBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(private val binding: RowCheckboxBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            val item: FilterData = items[position]
            binding.item = item
            binding.executePendingBindings()
            binding.cb.tag = position
            binding.cb.setOnClickListener {
                val pos = it.tag as Int
                items[pos].isSelected = (it as CheckBox).isChecked
               // notifyDataSetChanged()
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    items = list as ArrayList<FilterData>
                } else {
                    val resultList = ArrayList<FilterData>()
                    for (row in list) {
                        if (row.name.toLowerCase().contains(constraint.toString().toLowerCase())) {
                            resultList.add(row)
                        }
                    }
                    items = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = items
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                items = results?.values as ArrayList<FilterData>
                notifyDataSetChanged()
            }
        }
    }
}