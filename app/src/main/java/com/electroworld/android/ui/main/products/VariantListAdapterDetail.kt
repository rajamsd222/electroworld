package com.electroworld.android.ui.main.products

import android.app.Activity
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.databinding.ListItemVariantBinding
import com.electroworld.android.model.ProductModel
import com.electroworld.android.ui.main.products.detail.ProductDetailFragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.util.*

class VariantListAdapterDetail(
    items: List<GroupVariantsModel>,
    fragment: ProductDetailFragment,
    val context: Activity,
    var bottomSheetDialog: BottomSheetDialog? = null,
    model: ProductModel
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items: List<GroupVariantsModel> = ArrayList<GroupVariantsModel>()
    var parentFragment: ProductDetailFragment
    var productModel: ProductModel
    val list: MutableList<GroupVariantsModel> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: ListItemVariantBinding =
            ListItemVariantBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(binding: ListItemVariantBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val binding: ListItemVariantBinding = binding
        fun bind(position: Int) {
            val model: GroupVariantsModel = items[position]
            binding.item = model
            binding.executePendingBindings()

            binding.tvMRP.paintFlags = binding.tvMRP.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            if (productModel.warehouse_product_id != null)
                if (productModel.warehouse_product_id.equals(model.warehouse_product_id)) {
                    binding.parentLayout.setBackgroundResource(R.drawable.variant_selected_bg)
                    binding.tvName.setTextColor(Color.WHITE)
                    binding.tvAmount.setTextColor(Color.WHITE)
                    binding.ivLeft.setImageResource(R.mipmap.circle_tick_icon)
                } else {
                    binding.parentLayout.setBackgroundResource(R.drawable.variant_default_bg)
                    binding.ivLeft.setImageResource(R.mipmap.dot_icon)
                }
            binding.parentLayout.setTag(model.warehouse_product_id)
            binding.parentLayout.setOnClickListener(View.OnClickListener {
                bottomSheetDialog!!.dismiss()
                var warehouseId = it.getTag() as String
                parentFragment.updateVariants(warehouseId)
            })

        }

    }

    init {
        this.items = items
        parentFragment = fragment
        this.productModel = model
    }

}