package com.electroworld.android.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.electroworld.android.R
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.utils.AppUtils
import com.electroworld.android.utils.AppValidator
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import dagger.android.support.DaggerAppCompatActivity
import org.json.JSONObject
import javax.inject.Inject

class PaymentActivity : DaggerAppCompatActivity(), PaymentResultListener {

    @Inject
    lateinit var myPref: PreferencesManager
    var userPhone = ""
    var userEmail = ""
    var amount = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        Checkout.preload(getApplicationContext());
        userEmail = AppValidator.toStr(myPref.getEmail())
        userPhone = AppValidator.toStr(myPref.getMobileNumber())
        userEmail = if (userEmail.length == 0) "info@invenzolabs.com" else userEmail
        userPhone = if (userPhone.length < 10) "9876543210" else userPhone
        val bundle = intent.extras
        if (bundle != null) {
            amount = AppValidator.toStr(bundle.getString("amount"))
        }

        startPayment()
    }

    private fun startPayment() {
        /*
        *  You need to pass current activity in order to let Razorpay create CheckoutActivity
        * */
        val activity: Activity = this
        val co = Checkout()
        co.setKeyID(getString(R.string.razor_pay_key))
        try {
            val options = JSONObject()
            options.put("name", "FlipZee")
            options.put("description", "")
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("theme.color", "#56940f")
            options.put("currency", "INR");
            //options.put("order_id", "order_DBJOWzybf0sJbb");
            options.put("amount", AppUtils.getRazorPayAmount(amount))//pass amount in currency subunits

            val prefill = JSONObject()
            prefill.put("email", userEmail)
            prefill.put("contact", userPhone)

            options.put("prefill", prefill)
            co.open(activity, options)

        }catch (e: Exception){
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    override fun onPaymentSuccess(razorpayPaymentID: String?) {
        val intent = Intent()
        val bundle = Bundle()
        bundle.putString("transaction_status", "success")
        bundle.putString("transaction_id", razorpayPaymentID)
        bundle.putString("amount", amount)
        intent.putExtras(bundle)
        setResult(RESULT_OK, intent)
        finish()
    }

    override fun onPaymentError(code: Int, response: String?) {
        if(code == Checkout.NETWORK_ERROR){
            Toast.makeText(
                this@PaymentActivity,
                "There was a network error: $response",
                Toast.LENGTH_LONG
            ).show()
        }
        else  if(code == Checkout.INVALID_OPTIONS){
            Toast.makeText(
                this@PaymentActivity,
                "An issue with options passed: $response",
                Toast.LENGTH_LONG
            ).show()
        }
        else  if(code == Checkout.PAYMENT_CANCELED){
            val intent = Intent()
            val bundle = Bundle()
            bundle.putString("transaction_status", "cancelled")
            bundle.putString("transaction_id", "")
            bundle.putString("amount", amount)
            intent.putExtras(bundle)
            setResult(RESULT_OK, intent)
            finish()
            Toast.makeText(this@PaymentActivity, "User cancelled the payment.", Toast.LENGTH_LONG).show()
        }
        else  if(code == Checkout.TLS_ERROR){
            Toast.makeText(
                this@PaymentActivity,
                "The device does not support TLS v1.1 or TLS v1.2.",
                Toast.LENGTH_LONG
            ).show()
        }
        finish()
    }


}