package com.electroworld.android.ui.main.products.detail.image

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.viewpager.widget.PagerAdapter
import com.electroworld.android.R
import com.electroworld.android.customclass.TouchImageView
import java.util.*

class FullScreenImageViewAdapter internal constructor(
    private val activity: FullScreenImageViewActivity,
    private val imageList: ArrayList<String>
) :
    PagerAdapter() {
    override fun getCount(): Int {
        return imageList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater =
            (activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater)
        val viewLayout: View = inflater.inflate(R.layout.layout_fullscreen_image, container, false)
        val imgDisplay: TouchImageView = viewLayout.findViewById(R.id.imgDisplay)
        activity.setImage(imageList[position], imgDisplay)
        imgDisplay.setMaxZoom(4f)
        container.addView(viewLayout)
        return viewLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }
}
