package com.electroworld.android.ui.main.myoffersothers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.databinding.FragmentMyOffersOthersBinding
import com.electroworld.android.di.ViewModelProviderFactory
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import javax.inject.Inject

class MyOffersAndOthers : DaggerFragment() {

    private lateinit var viewModel: MyOffersAndOthersViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentMyOffersOthersBinding

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_offers_others, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(MyOffersAndOthersViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentMyOffersOthersBinding.bind(view)
        binding.ab.tvABTitle.text = resources.getString(R.string.profile_my_offer_and_others)
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }

    }

}