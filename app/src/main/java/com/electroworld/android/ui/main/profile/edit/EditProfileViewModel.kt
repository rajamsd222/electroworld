package com.electroworld.android.ui.main.profile.edit

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.UserModel
import com.electroworld.android.utils.AppUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import javax.inject.Inject

class EditProfileViewModel   @Inject constructor(val mainApi: MainApi,val myPref: PreferencesManager) : ViewModel() {


    fun updateProfile(
        context: Context?,
        uploadedFile: File?,
        token: String?,
        userId: String?,
        first_name: String?,
        last_name: String?,
        email_id: String?,
        gstnumber: String?,
        companyName: String?,
        vendorId: String?,
        language: String?
    ): MutableLiveData<ApiResponse<UserModel>?>? {
        val res: MutableLiveData<ApiResponse<UserModel>?> =
            MutableLiveData<ApiResponse<UserModel>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        var imageFile: MultipartBody.Part? = null
        if (uploadedFile != null) {
            if (uploadedFile.exists()) {
                val requestFile =
                    uploadedFile.asRequestBody("multipart/form-data".toMediaTypeOrNull())
                imageFile =
                    MultipartBody.Part.createFormData("profile_image", uploadedFile.name, requestFile)
            }
        }
        val token_ = token!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val UserId_ = userId!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val language_ = language!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val email_id_ = email_id!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val first_name_ = first_name!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val last_name_ = last_name!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val gstnumber = gstnumber!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val companyName_ = companyName!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val vendorId_ = vendorId!!.toRequestBody("text/plain".toMediaTypeOrNull())
        mainApi.updateProfile(token_, UserId_, first_name_,
            last_name_, email_id_,gstnumber,companyName_, vendorId_, language_, imageFile
        )
            .enqueue(object : Callback<ApiResponse<UserModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<UserModel>?>,
                    response: Response<ApiResponse<UserModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful()) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponse<UserModel>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun updateProfileRes(
        context: Context?,
        uploadedFile: File?,
        token: String?,
        userId: String?,
        first_name: String?,
        last_name: String?,
        email_id: String?,
        gstnumber: String?,
        companyName: String?,
        vendorId: String?,
        language: String?
    ): MutableLiveData<ApiResponse<UserModel>?>? {
        return updateProfile(
            context, uploadedFile, token, userId, first_name, last_name,
            email_id, gstnumber,companyName,vendorId, language
        )
    }
}