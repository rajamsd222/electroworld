package com.electroworld.android.ui.main.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.AlertViewSimple
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.bind.AppBinder
import com.electroworld.android.databinding.FragmentProfileBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.UserModel
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.Utils
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ProfileFragment : DaggerFragment() {

    private lateinit var viewModel: ProfileViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding : FragmentProfileBinding


    @Inject
    lateinit var providersFactory: ViewModelProviderFactory
    @Inject
    lateinit var myPref: PreferencesManager


    private fun makeLogoutReq() {
        viewModel.callLogoutInfo("1")
    }

    private fun consumeLogoutResponse(res: ApiResource<ApiResponse<ApiResponseDefault>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    resetAll()
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            Utils.showApiSuccess(activity, res.data.message)
                        } else {
                            //Utils.showApiError(activity, res.data.message)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    resetAll()
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(),res.message,AlertType.SERVER_ERROR)
                }
            }
        }
    }

    private fun resetAll(){
        val storeId: String = myPref.getPrefKeyStoreId()
        val storeAddress: String = myPref.getPrefKeyStoreAddress()
        val storeName: String = myPref.getPrefKeyStoreName()
        val homeAddressTop: String = myPref.getHomeAddressTop()
        myPref.clear()
        Constants.ADDRESS_TYPE = ""
        Constants.ADDRESS_ID = ""
        Constants.DELIVERY_TIME = ""
        Constants.PAYMENT_METHOD = ""
        Constants.BANK_TRANSFER_NUMBER = ""
        myPref.setPrefKeyStoreId(storeId)
        myPref.setPrefKeyStoreAddress(storeAddress)
        myPref.setPrefKeyStoreName(storeName)
        myPref.setHomeAddressTop(homeAddressTop)
        myPref.setPrefKeyLanguage("en")
        myPref.setPrefKeyVendorId(Constants.VENDOR_ID)
        mNavController.navigateUp()
    }

    private fun makeProfileInfoReq() {
        viewModel.callProfileInfo()
    }

    private fun consumeProfileResponse(res: ApiResource<ApiResponse<UserModel>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            //Utils.showApiSuccess(activity, res.data.message)
                            renderProfileResponse(res.data.data)
                        } else {
                            CustomDialog.show(requireActivity(),res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(),res.message,AlertType.SERVER_ERROR)
                }
            }
        }
    }
    private fun renderProfileResponse(data: UserModel) {
        myPref.setPrefKeyUserModel(data)
        updateProfileInfo()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mNavController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this, providersFactory).get(ProfileViewModel::class.java)
        binding = FragmentProfileBinding.bind(view)

        initViews()
        initOnClicks()
        viewModel.profileResponse().observe(viewLifecycleOwner) { res -> consumeProfileResponse(res) }
        viewModel.logoutResponse().observe(viewLifecycleOwner) { res -> consumeLogoutResponse(res) }
        makeProfileInfoReq()
        updateProfileInfo()
        binding.tvAddress.setText(Constants.ADDRESS_NAME)
    }

    private fun updateProfileInfo() {
        var model = myPref.getPrefKeyUserModel()
        if (model != null) {
            binding.item = model
            binding.tvName.setText(
                AppValidator.toStr(model.firstname) + " " + AppValidator.toStr(
                    model.lastname
                )
            )
            AppBinder.formatCurrencyFromDouble(binding.tvWalletAmount, myPref.getWalletAmount())
        }
    }
    private fun initViews() {

    }

    private fun initOnClicks() {
        binding.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }

        binding.tvMyOrders.setOnClickListener {
            mNavController.navigate(R.id.action_profileFragment_to_orderListFragment)
        }
        binding.tvWallet.setOnClickListener {
            mNavController.navigate(R.id.action_profileFragment_to_walletFragment)
        }

        binding.tvMyAddress.setOnClickListener {
            mNavController.navigate(R.id.action_profileFragment_to_addressListFragment)
        }

        binding.tvNotifications.setOnClickListener {
            mNavController.navigate(R.id.action_profileFragment_to_notificationFragment)
        }

        binding.tvMyFavorites.setOnClickListener {
            mNavController.navigate(R.id.action_profileFragment_to_favouritesFragment)
        }
        binding.tvPremiumAccess.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("screenType", "premium_access")
            mNavController.navigate(R.id.action_profileFragment_to_webViewFragment, bundle)
        }
        binding.tvRatingAndReviews.setOnClickListener {
            mNavController.navigate(R.id.action_profileFragment_to_ratingReviewsListFragment)
        }
        binding.tvContactUs.setOnClickListener {
            mNavController.navigate(R.id.action_profileFragment_to_contactusFragment)
        }

        binding.tvMyOffersAndOthers.setOnClickListener {
            mNavController.navigate(R.id.action_profileFragment_to_myOffersAndOthers)
        }
        binding.ivEdit.setOnClickListener {
            mNavController.navigate(R.id.action_profileFragment_to_editProfileFragment)
        }

        binding.tvLogout.setOnClickListener {
            showLogoutAlert()
        }
    }

    private fun showLogoutAlert() {
        val avcDialog = AlertViewSimple(requireActivity() ,object : AlertViewSimple.OKClickListener {
            override fun onOKClick(view: View?) {
                makeLogoutReq()
            }
        })
        avcDialog.setCancelable(true)
        avcDialog.setDialogTitle("Logout")
        avcDialog.setDialogMessage("Are you sure want to logout?")
        avcDialog.setPositiveText("Yes")
        avcDialog.setNegativeText("No")
        avcDialog.setShowNegativeButton(true)
        avcDialog.showDialog()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }
}