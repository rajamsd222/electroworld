package com.electroworld.android.ui.main.home.models

data class FoodReceipeModel(
    val image: String,
    val name: String,
)