package com.electroworld.android.ui.auth.login

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView.OnEditorActionListener
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.databinding.FragmentLoginBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.UpdatePriceModel
import com.electroworld.android.model.UserModel
import com.electroworld.android.ui.auth.AuthActivity
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.cart.models.CartSingleItem
import com.electroworld.android.ui.main.cart.models.LocalCartModel
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.SystemUtils
import com.electroworld.android.utils.UiUtils
import com.electroworld.android.utils.Utils
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject


class LoginFragment : DaggerFragment() {


    private val LOGIN_TYPE_NORMAL = "normal"
    private val LOGIN_TYPE_GOOGLE = "gmail"
    private val LOGIN_TYPE_FACEBOOK = "facebook"

    private val SCREEN_TYPE_LOGIN = "login"
    private val SCREEN_TYPE_SIGN_UP = "signup"


    lateinit var viewModel: LoginViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentLoginBinding

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    private var socialUserId = ""
    private var mobileNumber = ""

    private val disposable = CompositeDisposable()
    private fun getType(): String {
        if (binding.tvCnt1.text.toString().equals(getString(R.string.login_into_your_account))) {
            return SCREEN_TYPE_LOGIN
        } else if (binding.tvCnt1.text.toString()
                .equals(getString(R.string.welcome_to_adishwar_super_market))
        ) {
            return SCREEN_TYPE_SIGN_UP
        } else if (binding.tvCnt1.text.toString()
                .equals(getString(R.string.enter_the_mobile_number_to_complete_reg))
        ) {
            return SCREEN_TYPE_SIGN_UP
        }
        return SCREEN_TYPE_LOGIN
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                handleBackPressManually()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private fun handleBackPressManually() {
        if (getType().equals(SCREEN_TYPE_LOGIN)) {
            activity?.finish()
        } else if (getType().equals(SCREEN_TYPE_SIGN_UP)) {
            showLoginUi()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(
            mMessageReceiver,
            IntentFilter("social_login")
        )
        mNavController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this, providersFactory).get(LoginViewModel::class.java)
        binding = FragmentLoginBinding.bind(view)

        if (myPref.getFCMToken().isEmpty()) {
            viewModel.getFcmToken()
        }
        binding.textView.setPaintFlags(binding.textView.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        binding.tvDonthaveanAcc.setOnClickListener {
            showNextScreen()
        }
        binding.tvSignUp.setOnClickListener {
            showNextScreen()
        }
        binding.textView.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("screenType", "tnc")
            mNavController.navigate(R.id.action_loginFragment_to_webViewFragment2, bundle)
        }

        binding.edMobileNumber.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                binding.tvSubmit.performClick()
                return@OnEditorActionListener true
            }
            false
        })

        binding.ivBack.setOnClickListener {
            handleBackPressManually()
        }


        binding.googleLayout.setOnClickListener {
            (activity as AuthActivity).doGoogleSignIn()
        }
        binding.facebooklayout.setOnClickListener {
            (activity as AuthActivity).doFacebookLogin()
        }
        binding.tvSubmit.setOnClickListener {
            mobileNumber = binding.edMobileNumber.text.toString().trim()
            if (mobileNumber.equals("")) {
                UiUtils.showInputAlert(
                    activity,
                    "Please enter mobile number",
                    binding.edMobileNumber
                )
            } else {
                closeKeyboard()
                if (getType().equals(SCREEN_TYPE_LOGIN)) {
                    makeLoginReq(LOGIN_TYPE_NORMAL, "")
                } else if (getType().equals(SCREEN_TYPE_SIGN_UP)) {
                    makeSignUpReq(LOGIN_TYPE_NORMAL, "")
                }
            }
        }
        if (getType().equals(SCREEN_TYPE_LOGIN)) {
            showLoginUi()
        }

        observeLoginRes()
        observeSignUpRes()
    }

    private fun closeKeyboard() {
        try {
            val imm =
                requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(binding.edMobileNumber.windowToken, 0)
        } catch (e: Exception) {
            // TODO: handle exception
        }
    }

    private fun observeSignUpRes() {
        viewModel.mUserLogin.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.http_code == 200) {
                    makeLoginReq(LOGIN_TYPE_NORMAL, "")
                } else if (it.http_code == 201 || it.http_code == 202) {//202 - for gmail 201 - for facebook
                    showSocialSignUpUi()
                } else {
                    CustomDialog.show(requireActivity(), it.message, AlertType.ERROR)
                }
            }
        })
        viewModel.loading.observe(viewLifecycleOwner, Observer { isLoading ->
            isLoading?.let {
                binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
            }
        })
    }

    private fun observeLoginRes() {
        viewModel.mUserLogin.observe(viewLifecycleOwner, Observer {
            it?.let {
                renderLoginResponse(it)
            }
            viewModel.mUserLogin.removeObservers(viewLifecycleOwner)
        })
        viewModel.loading.observe(viewLifecycleOwner, Observer { isLoading ->
            isLoading?.let {
                binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
            }
        })
    }

    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val social_type = AppValidator.toStr(intent.getStringExtra("social_type"))
            val id = AppValidator.toStr(intent.getStringExtra("id"))
            if (social_type.equals("google")) {
                if (getType().equals(SCREEN_TYPE_LOGIN)) {
                    makeLoginReq(LOGIN_TYPE_GOOGLE, id)
                } else if (getType().equals(SCREEN_TYPE_SIGN_UP)) {
                    checkSocialIdExists(LOGIN_TYPE_GOOGLE, id)
                }

            } else if (social_type.equals("facebook")) {
                if (getType().equals(SCREEN_TYPE_LOGIN)) {
                    makeLoginReq(LOGIN_TYPE_FACEBOOK, id)
                } else if (getType().equals(SCREEN_TYPE_SIGN_UP)) {
                    checkSocialIdExists(LOGIN_TYPE_FACEBOOK, id)
                }

            }
            //(requireActivity() as MainActivity).currentAddressName
        }
    }

    private fun showLoginUi() {
        binding.lblWelcome.text = getString(R.string.welcome_back)
        binding.tvCnt1.text = getString(R.string.login_into_your_account)
        binding.tvSubmit.text = getString(R.string.submit)
        binding.tvDonthaveanAcc.text = getString(R.string.don_t_have_an_account)
        binding.tvSignUp.text = getString(R.string.signup)
        binding.lblGoogle.text = getString(R.string.sign_in_with_google)
        binding.lblFacebook.text = getString(R.string.log_in_with_facebook)
        binding.ivBack.setGone()
        binding.edMobileNumber.text.clear()
        resetSocialSignUpUi()
    }

    private fun showSignUpUi() {
        binding.lblWelcome.text = getString(R.string.title_signup)
        binding.tvCnt1.text = getString(R.string.welcome_to_adishwar_super_market)
        binding.tvSubmit.text = getString(R.string.submit)
        binding.tvDonthaveanAcc.text = getString(R.string.already_have_an_account)
        binding.tvSignUp.text = getString(R.string.login)
        binding.lblGoogle.text = getString(R.string.sign_up_with_google)
        binding.lblFacebook.text = getString(R.string.sign_up_with_facebook)
        binding.ivBack.setVisible()
        binding.edMobileNumber.text.clear()
        resetSocialSignUpUi()
    }

    private fun showNextScreen() {
        if (binding.tvSignUp.text.toString().equals(getString(R.string.login))) {
            showLoginUi()
        } else if (binding.tvSignUp.text.toString().equals(getString(R.string.signup))) {
            showSignUpUi()
        }
    }

    private fun makeSignUpReq(loginType: String, socialLoginId: String) {
        val device_id =
            Settings.Secure.getString(requireActivity().contentResolver, Settings.Secure.ANDROID_ID)
        val app_version: String = SystemUtils.getAppVersion(requireActivity())
        val device_name: String = SystemUtils.getDeviceName()
        val device_version = Build.VERSION.SDK_INT.toString()
        val email = (requireActivity() as AuthActivity).email
        val fName = (requireActivity() as AuthActivity).firstName
        val lName = (requireActivity() as AuthActivity).lastName
        viewModel.doSignUpRes(
            requireActivity(), loginType, socialLoginId,
            email, mobileNumber, fName, lName, device_id, device_name, device_version, app_version,
            myPref.getFCMToken(), myPref.getPrefKeyVendorId()
        )?.observe(viewLifecycleOwner, { res ->
            if (res != null) {
                if (res.http_code == 200) {
                    makeLoginReq(LOGIN_TYPE_NORMAL, "")
                } else if (res.http_code == 201 || res.http_code == 202) {//202 - for gmail 201 - for facebook
                    showSocialSignUpUi()
                } else {
                    CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                }
            }
        })
    }

    private fun showSocialSignUpUi() {
        binding.socialLogin.setInvisible()
        binding.groupSignUP.setInvisible()
        binding.groupTNC.setInvisible()
        binding.line.setInvisible()
        binding.tvCnt1.text = getString(R.string.enter_the_mobile_number_to_complete_reg)
    }

    private fun resetSocialSignUpUi() {
        binding.socialLogin.setVisible()
        binding.groupSignUP.setVisible()
        binding.groupTNC.setVisible()
        binding.line.setVisible()
    }

    private fun makeLoginReq(loginType: String, socialLoginId: String) {
        var userId = ""
        if (socialLoginId.isNotEmpty()) {
            userId = (requireActivity() as AuthActivity).email
        } else {
            userId = mobileNumber
        }
        socialUserId = socialLoginId
        //viewModel.makeLoginReq(requireActivity() as AuthActivity,userId,loginType,socialLoginId)
        disposable.add(
            viewModel.doLogin(requireActivity() as AuthActivity, userId, loginType, socialLoginId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<UserModel>>() {
                    override fun onSuccess(res: ApiResponse<UserModel>) {
                        renderLoginResponse(res)
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun renderLoginResponse(res: ApiResponse<UserModel>) {
        if (res.http_code == 200) {
            Utils.showApiSuccess(activity, res.message)
            if (socialUserId.isNotEmpty()) {
                viewModel.saveUserDetails(res.data, "otp")
                updatePriceList()
            } else {
                res.data.phone = mobileNumber
                callOTPScreen(res.data, "login")
            }
        } else if (res.http_code == 203) {
            Utils.showApiSuccess(activity, res.message)
            callOTPScreen(res.data, "login")
        } else {
            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
        }
    }

    private fun checkSocialIdExists(loginType: String, socialLoginId: String) {
        /*var sId = socialLoginId+(1..12).shuffled().first()
        (requireActivity() as AuthActivity).email = "mine"+(1..10).shuffled().first()+"@gmail.com"*/
        viewModel.doCheckSocialIdExistsRes(
            requireActivity() as AuthActivity,
            loginType,
            socialLoginId,
            (requireActivity() as AuthActivity).email
        )?.observe(viewLifecycleOwner, { res ->
            if (res != null) {
                if (res.http_code == 200) {
                    viewModel.saveUserDetails(res.data, "otp")
                    updatePriceList()
                    Utils.showApiSuccess(activity, res.message)
                } else if (res.http_code == 201 || res.http_code == 202) {//202 - for gmail 201 - for facebook
                    showSocialSignUpUi()
                    //Utils.showApiSuccess(activity, res.message)
                } else if (res.http_code == 203) {
                    Utils.showApiSuccess(activity, res.message)
                    callOTPScreen(res.data, "signup")
                } else {
                    CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                }
            }
        })
    }

    private fun callOTPScreen(data: UserModel, s: String) {
        val bundle = Bundle()
        bundle.putString("screen_name", s)
        bundle.putString("phone", data.phone)
        bundle.putString("otp", data.otp)
        bundle.putString("authentication_device_id", data.authentication_device_id)
        bundle.putString("authentication_id", data.authentication_id)
        mNavController.navigate(R.id.action_loginFragment_to_otpFragment, bundle)
    }


    private fun updatePriceList() {
        var cartListStr = ""
        val model: LocalCartModel? = myPref.getLocalCart()
        if (model != null) {
            val list: List<CartSingleItem> = model.cartItemList
            val listStr = ArrayList<String>()
            list.forEach {
                listStr.add(it.productModel.warehouse_product_id)
            }
            cartListStr = listStr.toString()
            viewModel.updateCartListPriceRes(
                requireActivity(),
                myPref.getPrefKeyAccessToken(),
                myPref.getPrefKeyCustomerId(),
                myPref.getPrefKeyVendorId(),
                cartListStr,
                myPref.getPrefKeyLanguage()
            )!!.observe(
                viewLifecycleOwner,
                { res ->
                    if (res != null) {
                        if (res.http_code == 200) {
                            if (res.data != null) {
                                if (res.data.isNotEmpty()) {
                                    for (info in res.data.get(0).group_variants) {
                                        CartUtils.updateCartDetails(myPref, info)
                                    }
                                }
                            }
                        }
                    }
                    //startActivity(Intent(activity, MainActivity::class.java))
                    requireActivity().finish()
                    //requireActivity().finish()
                })
        } else {
            requireActivity().finish()
        }
    }

    override fun onDestroyView() {
        disposable.clear()
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(mMessageReceiver)
        super.onDestroyView()
    }
}