package com.electroworld.android.ui.main.categories

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.model.CategoryModel
import com.electroworld.android.repository.PIMSRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CategoriesViewModel @Inject constructor(private val pimsRepository: PIMSRepository) :
    ViewModel() {

    private val disposable = CompositeDisposable()
    val errorMessage = MutableLiveData<String>()
    val apiError = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()
    val categoryList = MutableLiveData<List<CategoryModel>>()

/*

    init {
        doCategoryReq()
    }
*/


    fun doCategoryReq() {
        disposable.add(
            pimsRepository.getCategoryList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<CategoryModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<CategoryModel>>) {
                        loading.value = false
                        if(res.http_code==200){
                            categoryList.value = res.data
                        }
                        else{
                            apiError.value = res.message
                        }
                    }

                    override fun onError(e: Throwable) {
                        errorMessage.value = e.message
                        loading.value = false
                    }
                })
        )

    }

    override fun onCleared() {
        disposable.clear()
    }

}