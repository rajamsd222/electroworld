package com.electroworld.android.ui.main.products.list

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.customclass.LoadMoreNestedScrollView
import com.electroworld.android.databinding.FragmentProductListBinding
import com.electroworld.android.databinding.ListItemSortByBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.*
import com.electroworld.android.model.filter.FilterApiRes
import com.electroworld.android.model.filter.SortBy
import com.electroworld.android.repository.PIMSRepository
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.products.ProductListAdapter
import com.electroworld.android.ui.main.products.filter.FilterDialog
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.ScreenType
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_title_cart.view.*
import java.lang.reflect.Type
import javax.inject.Inject


class ProductListFragment : DaggerFragment() {

    companion object {
        private const val ARG_SCREEN_NAME = "screenName"
        private const val ARG_PLACE_HOLDER_ID = "placeholder_id"
        private const val ARG_NAME = "name"
        private const val ARG_CATEGORY_ID = "categoryId"
        private const val ARG_SUB_CATEGORY_ID = "subCategoryId"
        private const val ARG_BRAND_ID = "brand_id"
        private const val ARG_LIST = "list"
        fun getPlaceHolderBundles(id: String?, title: String?): Bundle {
            val bundle = Bundle()
            bundle.putString(ARG_PLACE_HOLDER_ID, id)
            bundle.putString(ARG_NAME, title)
            bundle.putSerializable(ARG_SCREEN_NAME, ScreenType.PLACE_HOLDER)
            return bundle
        }

        fun getCategoryBundles(
            categoryId: String,
            subCategoryId: String,
            categoryList: String?
        ): Bundle {
            val bundle = Bundle()
            bundle.putString(ARG_CATEGORY_ID, categoryId)
            bundle.putString(ARG_SUB_CATEGORY_ID, subCategoryId)
            bundle.putString(ARG_LIST, categoryList)
            bundle.putSerializable(ARG_SCREEN_NAME, ScreenType.CATEGORY)
            return bundle
        }

        fun getProductDetailBundles(id: String?, title: String?): Bundle {
            val bundle = Bundle()
            bundle.putString(ARG_BRAND_ID, id)
            bundle.putString(ARG_NAME, title)
            bundle.putSerializable(ARG_SCREEN_NAME, ScreenType.PRODUCT_DETAIL)
            return bundle
        }
    }

    private lateinit var viewModel: ProductListViewModel
    private lateinit var mNavController: NavController
    private var typedText = ""
    private lateinit var binding: FragmentProductListBinding

    var bottomSheetDialog: BottomSheetDialog? = null
    var mAdapter: ProductListAdapter? = null
    var mCategoriesAdapter: ProductListCategoryAdapter? = null
    var mSubCategoriesAdapter: ProductListSubCategoryAdapter? = null


    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    @Inject
    lateinit var pims: PIMSRepository
    private var page = 1
    private var sort_by = "most"
    private var sortTitle = "Most Popular"
    private var order_by = ""
    private var comesFrom = ""
    private var categoryId = ""
    private var subCategoryId = ""
    private var category_id = ""
    private var level = ""
    private var currentModel: ProductModel? = null
    private var currentPos = -1
    private var setfavValue = 0
    private var isLoadMore = true
    private var placeholder_id = ""
    private var brand_id = ""
    private var dynamicParams: HashMap<String, String> = HashMap()
    private val disposable = CompositeDisposable()
    private var toolbarTitle: String = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        page = 1
        viewModel = ViewModelProvider(this, providersFactory).get(ProductListViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentProductListBinding.bind(view)
        val name = arguments?.getString(ARG_NAME)
        if (name != null) {
            toolbarTitle = name
            binding.ab.tvABTitle.text = toolbarTitle
        }
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        binding.ab.ivSearch.setOnClickListener {
            mNavController.navigate(R.id.action_productListFragment_to_navigation_search)
        }

        binding.tvAll.setOnClickListener {
            subCategoryId = ""
            binding.tvAll.setBackgroundResource(R.drawable.green_circle_bg)
            val mModel = getSelectedGroupModel()
            if (mModel != null) {
                getProductListByCategoryId(mModel.id, mModel.name)
            }

            mSubCategoriesAdapter?.getItems()?.forEach {
                it.isSelected = false
            }
            mSubCategoriesAdapter?.notifyDataSetChanged()
        }
        binding.ivFilter.setOnClickListener {
            if ((activity as MainActivity).mFiltersList != null) {
                val avcDialog = FilterDialog(
                    requireActivity() as MainActivity,
                    object : FilterDialog.FilterOKClickListener {
                        override fun onOKClick(view: View?, tag: String?) {
                            if (tag.equals("apply")) {
                                dynamicParams.clear()
                                (activity as MainActivity).mFiltersList.forEach {
                                    var key = it.key
                                    val ids = ArrayList<String>()//Creating an empty arraylist
                                    it.filter_data.forEach { data ->
                                        if (data.isSelected) {
                                            ids.add(data.id.toString())
                                        }
                                    }
                                    if (ids.size > 0) {
                                        if (it.type.equals("single")) {
                                            dynamicParams[key] =
                                                TextUtils.join(",", ids)
                                        } else {
                                            dynamicParams[key] =
                                                "[" + TextUtils.join(",", ids) + "]"
                                        }

                                    }
                                }
                                if (dynamicParams.size > 0) {
                                    binding.tvFilterCount.setVisible()
                                    binding.tvFilterCount.text = "" + dynamicParams.size
                                }
                                sortTitle = Constants.SORT_BY_TITLE
                                page = 1
                                makeReq()

                            } else if (tag.equals("clear")) {
                                clearFilter()
                                makeReq()
                            }
                        }
                    }, ScreenType.CATEGORY
                )
                avcDialog.setItems(
                    (activity as MainActivity).mFiltersList,
                    (activity as MainActivity).mSortByList,
                    sortTitle
                )
                avcDialog.showDialog()
            }

        }
        binding.ab.iconCart.setOnClickListener {
            mNavController.navigate(R.id.action_productListFragment_to_cartListFragment)
        }
        initRecyclerViews()

        viewModel.getFiltersResponse().observe(viewLifecycleOwner) { res ->
            consumeFilterResponse(
                res
            )
        }
        if ((activity as MainActivity).mFiltersList.isEmpty() ||
            (activity as MainActivity).mSortByList.isEmpty()
        ) {
            viewModel.getFiltersList()
        } else {
            for (item in (activity as MainActivity).mSortByList) {
                if (item.default == "1") {
                    sortTitle = item.title
                    sort_by = item.key
                    order_by = item.order_by
                }
            }
        }


        binding.ivGridListToggle.setOnClickListener {
            toggleViews()
        }
        binding.ivGridListToggle1.setOnClickListener {
            toggleViews()
        }

        when (arguments?.getSerializable(ARG_SCREEN_NAME)) {
            ScreenType.CATEGORY -> {
                val list = arguments?.getString(ARG_LIST)
                if (categoryId == null || categoryId.isEmpty()) {
                    categoryId = arguments?.getString(ARG_CATEGORY_ID).toString()
                    category_id = categoryId
                }
                if (subCategoryId == null || subCategoryId.isEmpty()) {
                    subCategoryId = arguments?.getString(ARG_SUB_CATEGORY_ID).toString()
                }
                if (level != null && level == "2") {
                    categoryId = category_id
                }
                binding.layoutFilter.visibility = View.GONE
                findCategoryPosAndLoadData(list, categoryId, subCategoryId)
            }
            ScreenType.PLACE_HOLDER -> {
                placeholder_id = AppValidator.toStr(arguments?.getString(ARG_PLACE_HOLDER_ID))
                if (placeholder_id.isNotEmpty()) {
                    binding.rvCategories.visibility = View.GONE
                    binding.layoutSubCateogory.visibility = View.GONE
                    binding.layoutFilter.visibility = View.VISIBLE
                    getPlaceHolderDetails()
                }
            }
            ScreenType.PRODUCT_DETAIL -> {
                brand_id = AppValidator.toStr(arguments?.getString(ARG_BRAND_ID))
                if (brand_id.isNotEmpty()) {
                    brand_id = "[$brand_id]"
                    binding.layoutFilter.visibility = View.GONE
                    binding.rvCategories.visibility = View.GONE
                    binding.layoutSubCateogory.visibility = View.GONE
                    makeReq()
                }
            }
            else -> {
                doInitCategory(0, 0, null)
            }
        }
        viewModel.getFavResponse().observe(viewLifecycleOwner) { res -> consumeFavResponse(res) }
        viewModel.getCategoriesResponse().observe(viewLifecycleOwner) { res ->
            consumeCategoriesResponse(
                res
            )
        }

        binding.layoutSortBy.setOnClickListener {
            showSortByDialog()
        }


        binding.layoutFilterBy.setOnClickListener {
            if ((activity as MainActivity).mFiltersList != null) {
                val avcDialog = FilterDialog(
                    requireActivity() as MainActivity,
                    object : FilterDialog.FilterOKClickListener {
                        override fun onOKClick(view: View?, tag: String?) {
                            if (tag.equals("apply")) {
                                dynamicParams.clear()
                                (activity as MainActivity).mFiltersList.forEach {
                                    var key = it.key
                                    val ids = ArrayList<String>()//Creating an empty arraylist
                                    it.filter_data.forEach { data ->
                                        if (data.isSelected) {
                                            ids.add(data.id.toString())
                                        }
                                    }
                                    if (ids.size > 0) {
                                        if (it.type.equals("single")) {
                                            dynamicParams[key] =
                                                TextUtils.join(",", ids)
                                        } else {
                                            dynamicParams[key] =
                                                "[" + TextUtils.join(",", ids) + "]"
                                        }

                                    }
                                }
                                if (dynamicParams.size > 0) {
                                    binding.txtFilterBy.text =
                                        getString(R.string.filter_by) + " ( " + dynamicParams.size + " )"
                                } else {
                                    binding.txtFilterBy.text = getString(R.string.filter_by)
                                }
                                sortTitle = Constants.SORT_BY_TITLE
                                sort_by = Constants.SORT_BY
                                order_by = Constants.ORDER_BY
                                page = 1
                                getPlaceHolderDetails()

                            } else if (tag.equals("clear")) {
                                clearFilter()
                                binding.txtFilterBy.text = getString(R.string.filter_by)
                                getPlaceHolderDetails()
                            }
                        }
                    }, ScreenType.PLACE_HOLDER
                )
                avcDialog.setItems(
                    (activity as MainActivity).mFiltersList,
                    (activity as MainActivity).mSortByList,
                    sortTitle
                )
                avcDialog.showDialog()
            }
        }
    }

    public fun toggleViews() {

        if (mAdapter != null) {
            val isSwitched = mAdapter!!.toggleItemViewType()
            binding.recyclerView.layoutManager =
                if (isSwitched) LinearLayoutManager(requireContext()) else GridLayoutManager(
                    requireContext(),
                    2
                )
            mAdapter?.notifyDataSetChanged()
            binding.ivGridListToggle.setImageResource(
                if (isSwitched) R.mipmap.ic_stat_grid_view else R.mipmap.ic_stat_list
            )

            binding.ivGridListToggle1.setImageResource(
                if (isSwitched) R.mipmap.ic_stat_grid_view else R.mipmap.ic_stat_list
            )


            if(!isSwitched){
                val loadMoreNestedScrollView =
                    LoadMoreNestedScrollView(binding.recyclerView.layoutManager as GridLayoutManager)
                loadMoreNestedScrollView.setOnLoadMoreListener {
                    when (arguments?.getSerializable(ARG_SCREEN_NAME)) {
                        ScreenType.PLACE_HOLDER -> {
                            if (isLoadMore) {
                                page = page.plus(1)
                                getPlaceHolderDetails()
                            }
                        }
                        else -> {
                            if (isLoadMore) {
                                page = page.plus(1)
                                makeReq()
                            }
                        }
                    }

                }
                binding.nestedScrollView.setOnScrollChangeListener(loadMoreNestedScrollView)
            }

        }
    }


    fun showSortByDialog() {
        val dialogView: View =
            requireActivity().layoutInflater.inflate(R.layout.dialog_variant, null)
        bottomSheetDialog =
            BottomSheetDialog(requireActivity(), R.style.CustomBottomSheetDialog)
        bottomSheetDialog!!.setContentView(dialogView)
        val tvTitle = bottomSheetDialog!!.findViewById<TextView>(R.id.tvTitle)
        val AppTitle1 = bottomSheetDialog!!.findViewById<TextView>(R.id.AppTitle1)
        val recyclerView = bottomSheetDialog!!.findViewById<RecyclerView>(R.id.recycler_view)

        val adapter =
            object : GenericAdapter<SortBy, ListItemSortByBinding>(R.layout.list_item_sort_by) {
                override fun onBindData(
                    model: SortBy?,
                    position: Int,
                    dataBinding: ListItemSortByBinding?
                ) {
                    dataBinding!!.item = model
                    if (sortTitle == model!!.title) {
                        dataBinding.parentLayout.setBackgroundResource(R.drawable.variant_selected_bg)
                        dataBinding.tvName.setTextColor(Color.WHITE)
                        dataBinding.ivLeft.setImageResource(R.mipmap.circle_tick_icon)
                    } else {
                        dataBinding.parentLayout.setBackgroundResource(R.drawable.variant_default_bg)
                        dataBinding.ivLeft.setImageResource(R.mipmap.dot_icon)
                    }

                }

                override fun onItemClick(model: SortBy?, position: Int) {
                    sort_by = model!!.key
                    sortTitle = model.title
                    binding.tvSortBy.text = sortTitle
                    order_by = model.order_by
                    bottomSheetDialog!!.dismiss()
                    page = 1
                    getPlaceHolderDetails()
                }
            }
        recyclerView?.adapter = adapter
        adapter.items = (requireActivity() as MainActivity).mSortByList
        AppTitle1!!.setGone()
        tvTitle!!.text = getString(R.string.sort_by)
        bottomSheetDialog!!.show()
    }

    private fun clearFilter() {
        dynamicParams.clear()
        Constants.SORT_BY = ""
        (activity as MainActivity).mFiltersList.forEach {
            it.filter_data.forEach { data ->
                data.isSelected = false
            }
        }
        binding.tvFilterCount.setInvisible()
    }

    private fun consumeFilterResponse(res: ApiResource<ApiResponse<FilterApiRes>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            (activity as MainActivity).mFiltersList = res.data.data.filter_list
                            (activity as MainActivity).mSortByList = res.data.data.sort_by

                            for (item in (activity as MainActivity).mSortByList) {
                                if (item.default == "1") {
                                    sortTitle = item.title
                                    sort_by = item.key
                                    order_by = item.order_by
                                }
                            }


                        } else {
                            CustomDialog.show(requireActivity(), res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }

    private fun findCategoryPosAndLoadData(
        list: String?,
        categoryId: String?,
        subCategoryId: String?
    ) {
        if (list != null) {
            if (list.isNotEmpty()) {
                val listUserType: Type = object : TypeToken<List<CategoryModel?>?>() {}.type
                var mCategoriesList: List<CategoryModel> = Gson().fromJson(list, listUserType)
                val groupPos = getGroupPos(mCategoriesList, categoryId)
                val childPos = getChildPos(mCategoriesList.get(groupPos).subcategory, subCategoryId)
                doInitCategory(groupPos, childPos, list)
            }
        }
    }

    private fun getChildPos(list: List<Subcategory>?, id: String?): Int {
        if (list != null && list.isNotEmpty()) {
            for (i in list.indices) {
                if (list[i].id == id) {
                    return i
                }
            }
        }
        return -1
    }

    private fun getGroupPos(list: List<CategoryModel>?, categoryId: String?): Int {
        if (list != null && list.isNotEmpty()) {
            for (i in list.indices) {
                if (list[i].id == categoryId) {
                    return i
                }
            }
        }
        return -1
    }

    private fun getSelectedGroupModel(): CategoryModel? {
        mCategoriesAdapter?.getItems()?.forEach {
            if (it.isSelected) {
                return it
            }
        }
        return null
    }


    private fun doInitCategory(groupPos: Int, childPos: Int, list: String?) {
        binding.rvCategories.visibility = View.VISIBLE
        binding.layoutSubCateogory.visibility = View.VISIBLE
        if (list != null) {
            loadCategories(groupPos, childPos, list)
        } else {
            if ((activity as MainActivity).mCategoryList.isNotEmpty()) {
                var list = Gson().toJson((activity as MainActivity).mCategoryList)
                loadCategories(0, 0, list)
            } else {
                binding.rvCategories.visibility = View.GONE
                binding.layoutSubCateogory.visibility = View.GONE
                makeCategoriesReq()
            }
        }
    }


    private fun makeCategoriesReq() {
        viewModel.getCategoriesList()
    }

    private fun consumeCategoriesResponse(res: ApiResource<ApiResponse<List<CategoryModel>>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            (activity as MainActivity).mCategoryList = res.data.data
                            doInitCategory(0, 0, null)
                        } else {
                            CustomDialog.show(requireActivity(), res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }


    fun getProductInfoByVariantId(pos: Int, model: ProductModel, warehouseProductId: String) {
        viewModel.getProductInfoRes(
            requireActivity(),
            warehouseProductId
        )?.observe(viewLifecycleOwner, object : Observer<ApiResponse<ProductModel>?> {
            override fun onChanged(res: ApiResponse<ProductModel>?) {
                if (res != null) {
                    if (res.http_code === 200) {
                        if (res.data != null) {
                            val item: ProductModel? = res.data
                            if (item != null) {
                                item.group_variants = model.group_variants
                                mAdapter?.updateItem(pos, item)
                            }
                            mAdapter?.notifyDataSetChanged()
                        }
                    } else {
                        CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                    }
                }
            }
        })
    }

    private fun getPlaceHolderDetails() {
        disposable.add(
            pims.getPlaceHolderDetails(
                placeholder_id, page,
                sort_by,
                order_by,
                brand_id,
                dynamicParams
            )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<PlaceHolderDetailsApiModel>>() {
                    override fun onSuccess(res: ApiResponse<PlaceHolderDetailsApiModel>) {
                        if (res.http_code == 200) {
                            res.data.placeholder.forEachIndexed { index, placeholder ->
                                if (index == 0) {
                                    if (page == 1) {
                                        mAdapter?.clearAll()
                                    }
                                    mAdapter?.setItems(placeholder.products)
                                    isLoadMore = placeholder.products.isNotEmpty()

                                }
                                binding.ab.tvABTitle.text =
                                    toolbarTitle + " (" + res.data.product_count + " items)"
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                        updateAdapter()
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun makeReq() {
        updateSortByInputs()

        viewModel.getProductListObserverRes(
            requireActivity(),
            categoryId,
            level,
            page,
            typedText,
            sort_by,
            order_by,
            brand_id,
            dynamicParams
        )?.observe(viewLifecycleOwner,
            Observer<ApiResponse<ProductApiModel>?> { res ->
                if (res != null) {
                    if (res.http_code == 200) {
                        if (res.data != null) {
                            if (page == 1) {
                                mAdapter?.clearAll()
                            }
                            mAdapter?.setItems(res.data.products)
                            isLoadMore = res.data.products.isNotEmpty()

                            binding.ab.tvABTitle.text =
                                toolbarTitle + " (" + res.data.product_count + " items)"

                        }
                    } else {
                        CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                    }
                }
                updateAdapter()
            })

    }

    private fun updateSortByInputs() {

        sort_by = Constants.SORT_BY
        order_by = Constants.ORDER_BY
//
//        if (Constants.SORT_BY.equals(Constants.SORT_BY_TYPE_NAME)) {
//            sort_by = "name"
//            order_by = "ASC"
//        } else if (Constants.SORT_BY.equals(Constants.SORT_BY_TYPE_PRICE_HIGH_TO_LOW)) {
//            sort_by = "price"
//            order_by = "DESC"
//        } else if (Constants.SORT_BY.equals(Constants.SORT_BY_TYPE_PRICE_LOW_TO_HIGH)) {
//            sort_by = "price"
//            order_by = "ASC"
//        } else if (Constants.SORT_BY.equals(Constants.SORT_BY_TYPE_DISCOUNT_HIGH_TO_LOW)) {
//            sort_by = "discount"
//            order_by = "DESC"
//        } else if (Constants.SORT_BY.equals(Constants.SORT_BY_TYPE_DISCOUNT_LOW_TO_HIGH)) {
//            sort_by = "discount"
//            order_by = "ASC"
//        } else if (Constants.SORT_BY.equals(Constants.SORT_BY_TYPE_POPULARITY)) {
//            sort_by = "popularity"
//            order_by = "DESC"
//        } else if (Constants.SORT_BY.equals(Constants.SORT_BY_TYPE_RELEVANT)) {
//            sort_by = "relevant"
//            order_by = "DESC"
//        } else {
//            sort_by = "date"
//            order_by = "DESC"
//        }

    }

    private fun updateAdapter() {
        if (mAdapter?.itemCount == 0) {
            binding.tvEmptyMsg.visibility = View.VISIBLE
        } else {
            binding.tvEmptyMsg.visibility = View.GONE
        }
        mAdapter?.notifyDataSetChanged()
    }

    private fun consumeFavResponse(res: ApiResource<ApiResponse<SetFavModel>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            if (currentModel != null && currentPos != -1) {
                                currentModel!!.is_favourite = setfavValue
                                mAdapter?.updateItem(currentPos, currentModel!!)
                                (activity as MainActivity?)?.setFavouriteCount(res.data.data.total_fav_count)
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }


    fun setAsFavouriteProduct(pos: Int, item: ProductModel) {
        currentModel = item
        currentPos = pos
        setfavValue = if (item.is_favourite == 1) 0 else 1
        disposable.add(viewModel.makeFavReq(
            if (item.warehouse_product_id == null) "0" else item.warehouse_product_id,
            item.product_id,
            setfavValue,
            item.group_id
        )
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                binding.progressBar.visibility = View.VISIBLE
            }
            .doFinally {
                binding.progressBar.visibility = View.GONE
            }
            .subscribeWith(object : DisposableSingleObserver<ApiResponse<SetFavModel>>() {
                override fun onSuccess(res: ApiResponse<SetFavModel>) {
                    //  binding.progressBar.visibility = View.GONE
                    if (res.http_code == 200) {
                        if (currentModel != null && currentPos != -1) {
                            currentModel!!.is_favourite = setfavValue
                            mAdapter?.updateItem(currentPos, currentModel!!)
                            (activity as MainActivity?)?.setFavouriteCount(res.data.total_fav_count)
                        }
                    } else {
                        CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                    }
                }

                override fun onError(e: Throwable) {
                    CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                }
            })
        )
    }


    private fun getProductListByCategoryId(cId: String, cName: String) {
        binding.tvAll.setBackgroundResource(R.drawable.green_circle_bg)
        categoryId = cId
        category_id = categoryId
        level = "1"
        page = 1
        makeReq()
        toolbarTitle = cName
        binding.ab.tvABTitle.text = toolbarTitle
    }

    private fun loadCategories(groupPos: Int, childPos: Int, list: String) {
        if (list.isNotEmpty()) {
            val listUserType: Type = object : TypeToken<List<CategoryModel?>?>() {}.type
            var mCategoriesList: List<CategoryModel> = Gson().fromJson(list, listUserType)
            if (mCategoriesList.isNotEmpty()) {
                mCategoriesList.get(groupPos).isSelected = true
                mCategoriesAdapter?.setItems(mCategoriesList)
                Handler(Looper.getMainLooper()).postDelayed({
                    binding.rvCategories.smoothScrollToPosition(
                        groupPos
                    )
                }, 200)
                updateSubCategoriesListData(mCategoriesList.get(groupPos), childPos)
            } else {
                makeReq()
            }
        } else {
            makeReq()
        }
    }

    fun getListBySubcategoryId(subCatId: String, subCategoryName: String) {
        categoryId = subCatId
        subCategoryId = subCatId
        level = "2"
        page = 1
        makeReq()
        toolbarTitle = subCategoryName
        binding.ab.tvABTitle.text = toolbarTitle
        binding.tvAll.setBackgroundResource(0)
    }

    fun updateSubCategoriesListData(model: CategoryModel, childPos: Int) {
        clearFilter()
        if (model.subcategory.isNotEmpty()) {
            if (childPos == -1) {
                getProductListByCategoryId(model.id, model.name)
            } else {
                model.subcategory[childPos].isSelected = true
                Handler(Looper.getMainLooper()).postDelayed({
                    binding.rvSubCategories.smoothScrollToPosition(
                        childPos
                    )
                }, 200)
                getListBySubcategoryId(
                    model.subcategory[childPos].id,
                    model.subcategory[childPos].name
                )
            }
        } else {
            level = "1"
            categoryId = model.id
            page = 1
            makeReq()
        }
        mSubCategoriesAdapter?.setItems(model.subcategory)
        mSubCategoriesAdapter?.notifyDataSetChanged()
    }

    private fun initRecyclerViews() {
        //init category recyclerviews
        mCategoriesAdapter = ProductListCategoryAdapter(this, requireActivity())
        binding.rvCategories.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        binding.rvCategories.adapter = mCategoriesAdapter
        //init sub category recyclerviews
        mSubCategoriesAdapter = ProductListSubCategoryAdapter(
            this,
            requireActivity()
        )
        binding.rvSubCategories.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        binding.rvSubCategories.adapter = mSubCategoriesAdapter

        //Product list
        mAdapter = ProductListAdapter(
            this,
            activity as MainActivity,
            myPref
        )
        binding.recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = mAdapter

        val loadMoreNestedScrollView =
            LoadMoreNestedScrollView(binding.recyclerView.layoutManager as LinearLayoutManager)
        loadMoreNestedScrollView.setOnLoadMoreListener {
            when (arguments?.getSerializable(ARG_SCREEN_NAME)) {
                ScreenType.PLACE_HOLDER -> {
                    if (isLoadMore) {
                        page = page.plus(1)
                        getPlaceHolderDetails()
                    }
                }
                else -> {
                    if (isLoadMore) {
                        page = page.plus(1)
                        makeReq()
                    }
                }
            }

        }
        binding.nestedScrollView.setOnScrollChangeListener(loadMoreNestedScrollView)
    }


    fun openDetailsScreen(model: ProductModel) {
        val bundle = Bundle()
        bundle.putString("name", model.name_en)
        bundle.putString("group_id", model.group_id)
        bundle.putString("warehouse_product_id", model.warehouse_product_id)
        mNavController.navigate(R.id.action_productListFragment_to_productDetailFragment, bundle)
    }


    fun doAddToCart(model: ProductModel, isAdd: Boolean, isBulkUpdate: Boolean, selectedQty: Int) {
        CartUtils.addToCartProductList(
            requireActivity() as MainActivity,
            myPref,
            model,
            isAdd,
            isBulkUpdate,
            selectedQty
        )
        mAdapter?.notifyDataSetChanged()
        updateCartCountUI()
    }


    override fun onResume() {
        super.onResume()
        updateCartCountUI()
    }

    private fun updateCartCountUI() {
        binding.ab.tvCartCount.text = myPref.getCartCount().toString()
        when {
            myPref.getCartCount() > 0 -> {
                binding.ab.tvCartCount.visibility = View.VISIBLE
            }
            else -> {
                binding.ab.tvCartCount.visibility = View.GONE
            }
        }
    }
}