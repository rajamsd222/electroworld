package com.electroworld.android.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.VersionInfo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainActivityViewModel   @Inject constructor(val mainApi: MainApi, val myPref: PreferencesManager) : ViewModel() {

    private val disposable = CompositeDisposable()
    val userLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    val liveDataVersion = MutableLiveData<ApiResponse<VersionInfo>>()

    fun getVersionInfoReq() {
        loading.value = true
        disposable.add(
            mainApi.getVersionInfo(myPref.getPrefKeyVendorId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ApiResponse<VersionInfo>>() {
                    override fun onSuccess(t: ApiResponse<VersionInfo>) {
                        liveDataVersion.value = t
                        userLoadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        userLoadError.value = true
                        loading.value = false

                    }
                })
        )
    }

}