package com.electroworld.android.ui.main.categories

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.databinding.GroupRowBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.CategoryModel
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.products.list.ProductListFragment
import com.google.gson.Gson
import com.jakewharton.rxbinding.widget.RxTextView
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList


class CategoriesFragment : DaggerFragment() {

    private lateinit var viewModel: CategoriesViewModel

    private lateinit var mNavController: NavController

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    private var mAdapter: GenericAdapter<CategoryModel, GroupRowBinding>? = null
    private var localList = ArrayList<CategoryModel>()
    var mCategoryList: List<CategoryModel> = java.util.ArrayList()
    var fromOnCreate = true
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fromOnCreate = true
        mNavController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this, providersFactory).get(CategoriesViewModel::class.java)
        ab.tvABTitle.text = ""
        RxTextView.textChanges(edSearch)
            .debounce(500, TimeUnit.MILLISECONDS)
            .subscribe { textChanged: CharSequence? ->
                requireActivity().runOnUiThread {
                    if(!fromOnCreate && textChanged!!.isNotEmpty()){
                        filterData(edSearch.text.toString().trim())
                    }
                }
            }


//        edSearch.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//            }
//
//            override fun afterTextChanged(s: Editable?) {
//                filterData(s.toString().trim())
//            }
//
//        })


        ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        initRecyclerView()
        observeViewModel()
        viewModel.doCategoryReq()
    }

    private fun filterData(text: String) {
        mCategoryList = (activity as MainActivity).mCategoryList
        localList.clear()
        if (text.isEmpty()) {
            mAdapter?.setItems((activity as MainActivity).mCategoryList)
//            if(mCategoryList.isNotEmpty()){
//                mCategoryList.forEachIndexed { index, categoryModel ->
//                    categoryModel.subcategory.forEachIndexed { index, subcategory ->
//                        if (subcategory.name.toLowerCase(Locale.ROOT).contains(text)){
//                            subcategory.isSelected = false
//                            localList.add(categoryModel)
//                        }
//                    }
//                }
//            }
//            mAdapter?.setItems(localList)
//            updateAdapter()
        } else {

            if (mCategoryList.isNotEmpty()) {
                mCategoryList.forEachIndexed { index, categoryModel ->
                    if (categoryModel.name.toLowerCase(Locale.ROOT).contains(text)) {
                        localList.add(categoryModel)
                    } else {
                        categoryModel.subcategory.forEachIndexed { index, subcategory ->
                            if (subcategory.name.toLowerCase(Locale.ROOT).contains(text)) {
                                subcategory.isSelected = true
                                localList.add(categoryModel)
                            }
                        }
                    }
                }
            }
            mAdapter?.setItems(localList)
            updateAdapter()
        }

    }

    private fun observeViewModel() {
        viewModel.categoryList.observe(viewLifecycleOwner, Observer { list ->
            list?.let {
                fromOnCreate = false
                mCategoryList = it
                (activity as MainActivity).mCategoryList = it
                mAdapter?.setItems(it)
                updateAdapter()
            }
        })
        viewModel.errorMessage.observe(viewLifecycleOwner, { isError ->
            isError?.let {
                CustomDialog.show(requireActivity(), it, AlertType.SERVER_ERROR)
            }
        })
        viewModel.apiError.observe(viewLifecycleOwner, { isError ->
            isError?.let {
                CustomDialog.show(requireActivity(), it, AlertType.ERROR)
            }
        })
        viewModel.loading.observe(viewLifecycleOwner, { isLoading ->
            isLoading?.let {
                progressBar.visibility = if (it) View.VISIBLE else View.GONE
            }
        })
    }

    private fun updateAdapter() {
        tvEmptyMsg.visibility = if (mAdapter?.itemCount == 0) View.VISIBLE else View.GONE
        mAdapter?.notifyDataSetChanged()
    }

    private fun openProductListScreen(categoryId: String, subCategoryId: String) {
        val data = Gson().toJson((activity as MainActivity).mCategoryList)
        val bundle = ProductListFragment.getCategoryBundles(categoryId, subCategoryId, data)
        mNavController.navigate(R.id.action_navigation_categories_to_productListFragment, bundle)
    }

    private fun initRecyclerView() {
        mAdapter = object :
            GenericAdapter<CategoryModel, GroupRowBinding>(R.layout.group_row) {
            override fun onBindData(
                model: CategoryModel,
                position: Int,
                binding: GroupRowBinding
            ) {
                binding.item = model
                addSubCategoryList(model, binding.subCategoryLayout, binding.ivArrow)
                binding.subCategoryLayout.setTag(position)
                binding.ivArrow.setOnClickListener {
                    showHideUI(model, binding.ivArrow, binding.subCategoryLayout)
                }
                binding.parentView.setOnClickListener {
                    openProductListScreen(model.id, "")
                }
            }

            override fun onItemClick(model: CategoryModel?, position: Int) {}
        }
        recyclerView.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(
                    activity,
                    LinearLayoutManager.VERTICAL
                )
            )
        }

    }

    private fun showHideUI(
        model: CategoryModel,
        ivArrow: AppCompatImageView,
        subCategoryLayout: LinearLayout
    ) {
        if (model.subcategory.isNotEmpty()) {
            ivArrow.visibility = View.VISIBLE
            if (subCategoryLayout.visibility == View.VISIBLE) {
                subCategoryLayout.visibility = View.GONE
                ivArrow.setImageResource(R.mipmap.arrow_down)
            } else {
                subCategoryLayout.visibility = View.VISIBLE
                ivArrow.setImageResource(R.mipmap.arrow_up)
            }
        }
    }

    private fun addSubCategoryList(
        item: CategoryModel,
        subCategoryLayout: LinearLayout,
        ivArrow: AppCompatImageView
    ) {
        subCategoryLayout.removeAllViews()
        subCategoryLayout.setVisibility(View.GONE)
        ivArrow.setImageResource(R.mipmap.arrow_up)
        if (item.subcategory.isEmpty()) {
            ivArrow.visibility = View.INVISIBLE
        }
        item.subcategory.forEachIndexed { index, model ->
            val itemView = LayoutInflater.from(subCategoryLayout.context)
                .inflate(R.layout.child_row, subCategoryLayout, false)
            val name = itemView.findViewById<TextView>(R.id.name)
            val childParent = itemView.findViewById<ConstraintLayout>(R.id.childParent)
            name.text = model.name
            subCategoryLayout.addView(itemView)

            childParent.setOnClickListener {
                openProductListScreen(item.id, model.id)
            }

            if (model.isSelected) {
                subCategoryLayout.setVisibility(View.VISIBLE)
                ivArrow.setImageResource(R.mipmap.arrow_down)
            }
        }
    }

}