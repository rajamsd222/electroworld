package com.electroworld.android.ui.main.address.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.AddressListModel
import com.electroworld.android.model.StoreModel
import com.electroworld.android.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class AddressListViewModel @Inject constructor(val mainApi: MainApi,val myPref: PreferencesManager) : ViewModel() {


    private val disposable = CompositeDisposable()
    private val responseLiveData = MutableLiveData<ApiResource<ApiResponse<List<AddressListModel>>>>()

    private val responseLiveDataDelete = MutableLiveData<ApiResource<ApiResponse<ApiResponseDefault>>>()
    private val liveDataStoreInfo = MutableLiveData<ApiResponse<StoreModel>>()
    val errorMessage = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()

    fun getStoreId(
        latitude: Double,
        longitude: Double
    ){
        disposable.add(
            mainApi.getStoreInfoSingle(
                latitude,
                longitude,
                myPref.getPrefKeyVendorId()
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<StoreModel>>() {
                    override fun onSuccess(res: ApiResponse<StoreModel>) {
                        loading.value = false
                        liveDataStoreInfo.value = res
                    }

                    override fun onError(e: Throwable) {
                        errorMessage.value = e.message
                        loading.value = false
                    }
                })
        )
    }


    fun deleteAddress(
        address_type: String
    ){
        disposable.add(mainApi.deleteAddress(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            address_type,
            myPref.getPrefKeyVendorId())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveDataDelete.setValue(
                    ApiResource.loading<ApiResponse<ApiResponseDefault>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveDataDelete.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveDataDelete.setValue(
                    ApiResource.error<ApiResponse<ApiResponseDefault>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getDeleteApiResponse(): MutableLiveData<ApiResource<ApiResponse<ApiResponseDefault>>> {
        return responseLiveDataDelete
    }


    fun getList(){
        disposable.add(mainApi.addressList(
            Constants.HEADER_TOKEN,myPref.getPrefKeyAccessToken(),myPref.getPrefKeyCustomerId(),myPref.getPrefKeyVendorId(), myPref.getPrefKeyLanguage())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.setValue(
                    ApiResource.loading<ApiResponse<List<AddressListModel>>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveData.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveData.setValue(
                    ApiResource.error<ApiResponse<List<AddressListModel>>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getApiResponse(): MutableLiveData<ApiResource<ApiResponse<List<AddressListModel>>>> {
        return responseLiveData
    }

    override fun onCleared() {
        disposable.clear()
    }

}