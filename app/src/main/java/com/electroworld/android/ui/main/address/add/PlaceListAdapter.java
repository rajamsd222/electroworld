package com.electroworld.android.ui.main.address.add;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.electroworld.android.R;

import java.util.List;

public class PlaceListAdapter extends RecyclerView.Adapter<PlaceListAdapter.ViewHolder> {
    private static final String TAG = "HomeRAdapter";

    private Activity mContext;
    private List<PlaceModel> homeData = null;
    PlaceEventHandler handler;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView place_address;

        public ViewHolder(View v) {
            super(v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getAdapterPosition() + " clicked.");
                }
            });

            place_address = (TextView) v.findViewById(R.id.place_address);

        }

        public TextView getTextViewName() {
            return place_address;
        }

    }

    public PlaceListAdapter(Activity applicationContext, List<PlaceModel> venueData, PlaceEventHandler handler) {
        this.mContext = applicationContext;
        this.homeData = venueData;
        this.handler = handler;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.place_recycler_item_layout, viewGroup, false);

        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (homeData.get(position).getDescription() != null) {
            holder.place_address.setText(homeData.get(position).getDescription());
        }
        holder.place_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.handle(homeData.get(position).getpId(), homeData.get(position).getDescription());
            }
        });

    }


    @Override
    public int getItemCount() {
        return homeData.size();
    }


}
