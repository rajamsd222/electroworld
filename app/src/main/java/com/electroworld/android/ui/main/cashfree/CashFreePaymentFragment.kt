package com.electroworld.android.ui.main.cashfree

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import com.cashfree.pg.CFPaymentService.*
import com.electroworld.android.R
import com.electroworld.android.alert.AlertMakeOrderAsCOD
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.AlertViewSimple
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.databinding.FragmentCashFreePaymentBinding
import com.electroworld.android.databinding.RowItemCustomPaymentBinding
import com.electroworld.android.databinding.RowPaymentListCashFreeBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.*
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.PaymentType
import com.electroworld.android.utils.ScreenType
import com.electroworld.android.utils.ToastUtil
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_cash_free_payment.*
import kotlinx.android.synthetic.main.layout_card.view.*
import java.util.*
import javax.inject.Inject

class CashFreePaymentFragment : DaggerFragment() {

    private val TAG = "CashFreePaymentFragment"
    private val CODE_CARD = "card_cashfree"
    private val CODE_NET_BANKING = "net_cashfree"
    private val CODE_UPI = "upi_cashfree"
    private val CODE_WALLET = "wallet_cashfree"

    companion object {
        private const val ARG_SCREEN_TYPE = "screen_type"
        private const val ARG_PAYMENT_TYPE = "payment_type"
        private const val ARG_TOKEN = "token"
        private const val ARG_AMOUNT = "amount"
        private const val ARG_ORDER_ID = "order_id"
        private const val ARG_ORDER_ID_API = "id"
        fun updateBundle(
            token: String?,
            amount: String?,
            orderId: String?,
            id: String?,
            type: PaymentType,
            screenType: ScreenType
        ): Bundle {
            val bundle = Bundle()
            bundle.putString(ARG_TOKEN, token)
            bundle.putString(ARG_AMOUNT, amount)
            bundle.putString(ARG_ORDER_ID, orderId)
            bundle.putString(ARG_ORDER_ID_API, id)
            bundle.putSerializable(ARG_PAYMENT_TYPE, type)
            bundle.putSerializable(ARG_SCREEN_TYPE, screenType)
            return bundle
        }
    }

    val paymentParams: MutableMap<String, String> = HashMap()

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager
    private lateinit var viewModel: CashFreeViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentCashFreePaymentBinding
    private val disposable = CompositeDisposable()

    var APP_ID = "45152280660e1554f9418349325154"
    var APP_CURRENCY = "INR"
    var userName = ""
    var userPhone = ""
    var userEmail = ""
    var token = ""
    var amount = ""
    var orderId = ""
    var orderIdApi = ""
    var paymentCode = ""
    var selectedBankCode = ""
    var cardNumber = ""
    var cardExpiryMonth = ""
    var cardExpiryYear = ""
    var cardCVV = ""
    var paymentType = PaymentType.DEFAULT
    var screenType = ScreenType.UN_KNOWN
    var cashFreeMode = "TEST"
    var paymentList: MutableList<PaymentModel> = mutableListOf()
    var subList: MutableList<PaymentProviderItem> = mutableListOf()
    private var mAdapter: GenericAdapter<PaymentModel, RowPaymentListCashFreeBinding>? = null
    private var mSubAdapter: GenericAdapter<PaymentProviderItem, RowItemCustomPaymentBinding>? =
        null

    var paymentResModel: PaymentProvidersModel? = null
    var oneTimePopupShowed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                handleBackPressManually()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private fun handleBackPressManually() {
        if (mainLayout.visibility == View.GONE) {
            loadParentList()
        } else {
            if (!oneTimePopupShowed) {
                showCancelDialog()
            } else {
                mNavController.navigateUp()
            }

        }
    }

    private fun showCancelDialog() {
        val avcDialog =
            AlertViewSimple(requireActivity(), object : AlertViewSimple.OKClickListener {
                override fun onOKClick(view: View?) {
                    if (screenType == ScreenType.CHECKOUT) {
                        showPaymentFailedDialog("checkout", false)
                    } else if (screenType == ScreenType.ORDER_DETAIL) {
                        oneTimePopupShowed = true
                        showPaymentFailedDialog("retry", true)
                    } else {
                        mNavController.navigateUp()
                    }

                }
            })
        avcDialog.setCancelable(true)
        avcDialog.setDialogTitle("Alert")
        avcDialog.setDialogMessage("Are you sure want to exit?")
        avcDialog.setPositiveText("OK")
        avcDialog.setNegativeText(getString(R.string.btn_cancel))
        avcDialog.setShowNegativeButton(true)
        avcDialog.showDialog()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cash_free_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*val decorView = re.window.decorView // Hide the status bar.
        val uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        decorView.systemUiVisibility = uiOptions*/

        viewModel = ViewModelProvider(this, providersFactory).get(CashFreeViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentCashFreePaymentBinding.bind(view)
        //binding.ab.tvABTitle.text = resources.getString(R.string.title_payment)
        binding.ivClose.setOnClickListener {
            showCancelDialog()
        }
        binding.tvTitle.setOnClickListener {
            handleBackPressManually()
        }
        userName = AppValidator.toStr(myPref.getFullName())
        userEmail = AppValidator.toStr(myPref.getEmail())
        userPhone = AppValidator.toStr(myPref.getMobileNumber())
        userName = if (userName.isEmpty()) "User" else userName
        userEmail = if (userEmail.isEmpty()) "info@invenzolabs.com" else userEmail
        userPhone = if (userPhone.length < 10) "9876543210" else userPhone
        token = arguments?.getString(ARG_TOKEN).toString()
        amount = arguments?.getString(ARG_AMOUNT).toString()
        orderId = arguments?.getString(ARG_ORDER_ID).toString()
        orderIdApi = arguments?.getString(ARG_ORDER_ID_API).toString()
        paymentType = arguments?.getSerializable(ARG_PAYMENT_TYPE) as PaymentType
        screenType = arguments?.getSerializable(ARG_SCREEN_TYPE) as ScreenType
        //initDefaultList()
        initRecyclerView()
        initSubListRecyclerView()
        makeReq()
        binding.cardlayout.number.addTextChangedListener(CardNumberFormattingTextWatcher())
        binding.cardlayout.monthYear.addTextChangedListener(CardMonthYearFormattingTextWatcher())
        btnPay.text = getString(R.string.btn_pay) + "₹ " + amount
        tvAmount.text = "₹ " + amount
        username.setText(userEmail)
        phone.setText(userPhone)
        btnPay.setOnClickListener {
            if (paymentCode == CODE_CARD) {
                clearFieldsError()
                cardNumber = binding.cardlayout.number.text.toString().filter { it.isDigit() }
                var cardMonthYear =
                    binding.cardlayout.monthYear.text.toString().filter { it.isDigit() }
                if (cardMonthYear.length == 4) {
                    cardExpiryMonth = cardMonthYear.substring(0, 2)
                    cardExpiryYear = "20" + cardMonthYear.substring(2)
                } else {
                    cardExpiryMonth = ""
                    cardExpiryYear = ""
                }

                cardCVV = binding.cardlayout.cvv.text.toString().trim()
                if (validateCardFields(
                        cardNumber,
                        cardExpiryMonth,
                        cardExpiryYear,
                        cardCVV,
                    )
                ) {
                    initiatePayment(paymentCode)
                }
            } else if (paymentCode == CODE_UPI) {
                if (binding.upiEditText.text.toString().trim().isEmpty()) {
                    ToastUtil.show(activity, "Please enter your UPI ID")
                } else {
                    initiatePayment(paymentCode)
                }
            } else {
                mSubAdapter?.items?.forEach {
                    if (it.isSelect) {
                        selectedBankCode = it.provider_code
                    }
                }
                if (selectedBankCode.isEmpty()) {
                    ToastUtil.show(activity, "Please select any one option")
                } else {
                    initiatePayment(paymentCode)
                }
            }

        }
    }

    private fun validateCardFields(
        number: String,
        month: String,
        year: String,
        cvv: String
    ): Boolean {
        var error = false
        clearFieldsError()

        val errorColor = Color.rgb(204, 0, 51)
        if (!CardValidator.validateCardNumber(number)) {
            activity?.runOnUiThread(Runnable {
                binding.cardlayout.number.background.setColorFilter(
                    errorColor,
                    PorterDuff.Mode.SRC_ATOP
                )
            })
            error = true
        }
        if (!CardValidator.validateExpiryDate(month, year)) {
            activity?.runOnUiThread(Runnable {
                binding.cardlayout.monthYear.background.setColorFilter(
                    errorColor,
                    PorterDuff.Mode.SRC_ATOP
                )
            })
            error = true
        }
        if (cvv == "") {
            activity?.runOnUiThread {
                binding.cardlayout.cvv.background.setColorFilter(
                    errorColor,
                    PorterDuff.Mode.SRC_ATOP
                )
            }
            error = true
        }
        return !error
    }

    private fun clearFieldsError() {
        activity?.runOnUiThread {
            binding.cardlayout.cvv.background.clearColorFilter()
            binding.cardlayout.monthYear.background.clearColorFilter()
            binding.cardlayout.number.background.clearColorFilter()
        }
    }

    private fun initDefaultList() {
        paymentList.clear()
        paymentList.add(PaymentModel("8", "Card", CODE_CARD, false))
        paymentList.add(PaymentModel("10", "Net Banking", CODE_NET_BANKING, false))
        paymentList.add(PaymentModel("9", "UPI", CODE_UPI, false))
        paymentList.add(PaymentModel("11", "Wallet", CODE_WALLET, false))
    }

    private fun loadParentList() {
        mAdapter?.items = paymentList
        mainLayout.setVisible()
        rvSubList.setGone()
        cardlayout.setGone()
        tvTitle.text = ""
        btnPay.setGone()
        upilayout.setGone()
        tvTitle.setVisible()
    }

    private fun loadSublist() {
        mainLayout.setGone()
        rvSubList.setVisible()
        cardlayout.setGone()
        upilayout.setGone()
        tvTitle.setVisible()
    }

    private fun loadCardInfoUI() {
        binding.cardlayout.name.setText(userName)
        mainLayout.setGone()
        rvSubList.setGone()
        cardlayout.setVisible()
        upilayout.setGone()
        tvTitle.setVisible()
        binding.cardlayout.cvv.text.clear()
        binding.cardlayout.monthYear.text.clear()
        binding.cardlayout.number.text.clear()
    }

    private fun loadUPIUI() {
        mainLayout.setGone()
        rvSubList.setGone()
        cardlayout.setGone()
        upilayout.setVisible()
        tvTitle.setVisible()
        binding.upiEditText.text.clear()
    }

    private fun hideAll() {
        binding.cardlayout.name.text.clear()
        mainLayout.setGone()
        rvSubList.setGone()
        cardlayout.setGone()
        upilayout.setGone()
        btnPay.setGone()
        tvTitle.setGone()
    }

    private fun initSubListRecyclerView() {
        mSubAdapter = object :
            GenericAdapter<PaymentProviderItem, RowItemCustomPaymentBinding>(R.layout.row_item_custom_payment) {
            override fun onBindData(
                model: PaymentProviderItem,
                position: Int,
                dataBinding: RowItemCustomPaymentBinding
            ) {
                dataBinding.item = model
                dataBinding.rbPaymentItem.isChecked = model.isSelect
                dataBinding.rbPaymentItem.setOnClickListener {
                    btnPay.isEnabled = true
                    subList.forEachIndexed { index, model ->
                        model.isSelect = index == position
                    }
                    mSubAdapter?.notifyDataSetChanged()
                }
            }

            override fun onItemClick(model: PaymentProviderItem?, position: Int) {}
        }
        binding.rvSubList.apply {
            adapter = mSubAdapter
        }
    }

    private fun initRecyclerView() {
        mAdapter = object :
            GenericAdapter<PaymentModel, RowPaymentListCashFreeBinding>(R.layout.row_payment_list_cash_free) {
            override fun onBindData(
                model: PaymentModel,
                position: Int,
                dataBinding: RowPaymentListCashFreeBinding
            ) {
                dataBinding.item = model
                if (model.code == CODE_CARD) {
                    dataBinding.ivImage.setImageResource(R.mipmap.cf_card)
                } else if (model.code == CODE_NET_BANKING) {
                    dataBinding.ivImage.setImageResource(R.mipmap.cf_net_banking)
                } else if (model.code == CODE_UPI) {
                    dataBinding.ivImage.setImageResource(R.mipmap.cf_upi)
                } else if (model.code == CODE_WALLET) {
                    dataBinding.ivImage.setImageResource(R.mipmap.cf_wallet)
                }
                dataBinding.parentView.setOnClickListener {
                    paymentList.forEachIndexed { index, paymentModel ->
                        if (index == position) {
                            updateUI(paymentModel.code, paymentModel.name_en)
                        }
                        paymentModel.isSelect = index == position
                    }
                    mAdapter?.notifyDataSetChanged()
                }
            }

            override fun onItemClick(model: PaymentModel?, position: Int) {}
        }
        binding.rvPaymentList.apply {
            adapter = mAdapter
            addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        }
    }


    private fun updateUI(code: String, name: String) {
        btnPay.isEnabled = false
        btnPay.setVisible()
        paymentCode = code
        if (paymentCode == CODE_CARD) {
            btnPay.isEnabled = true
            loadCardInfoUI()
        } else if (paymentCode == CODE_UPI) {
            btnPay.isEnabled = true
            loadUPIUI()
        } else {
            subList.clear()
            if (paymentCode == CODE_NET_BANKING) {
                paymentResModel?.net_banking_provider?.forEach {
                    it.isSelect = false
                    subList.add(it)
                }
            } else if (paymentCode == CODE_WALLET) {
                paymentResModel?.wallet_provider?.forEach {
                    it.isSelect = false
                    subList.add(it)
                }
            }
            mSubAdapter?.items = subList
            loadSublist()
        }
        tvTitle.text = "< " + name
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            val bundle = data.extras
            if (bundle != null) {
                paymentParams.clear()
                for (key in bundle.keySet()) {
                    paymentParams.put(key, AppValidator.toStr(bundle.getString(key)))
                }
                Log.e("TAG - paymentRes", "" + paymentParams)
                if (getResponseValue("txStatus") == "CANCELLED") {
                    /*when (screenType) {
                        ScreenType.WALLET -> {
                            CustomDialog.show(requireActivity(), "Transaction Cancelled", AlertType.ERROR)
                        }
                        else -> {

                        }
                    }*/
                } else if (getResponseValue("txStatus") != "SUCCESS") {
                    /*when(screenType){
                        ScreenType.CHECKOUT -> {
                            //hideAll()
                            ToastUtil.show(requireActivity(), getResponseValue("txMsg"))
                            showPaymentFailedDialog()
                        }
                        else -> {
                            refreshUI()
                            CustomDialog.show(
                                requireActivity(),
                                getResponseValue("txMsg"),
                                AlertType.ERROR
                            )
                        }
                    }*/
                    refreshUI()
                    CustomDialog.show(
                        requireActivity(),
                        getResponseValue("txMsg"),
                        AlertType.ERROR
                    )
                } else {
                    when (screenType) {
                        ScreenType.CHECKOUT -> {
                            handleOrderUpdate()
                        }
                        ScreenType.ORDER_DETAIL -> {
                            handleOrderUpdate()
                        }
                        ScreenType.WALLET -> {
                            handleWalletUpdate()
                        }
                        else -> {
                            CustomDialog.show(
                                requireActivity(),
                                getResponseValue("txMsg"),
                                AlertType.SUCCESS
                            )
                        }
                    }
                }

            }
        }
    }

    private fun handleWalletUpdate() {
        hideAll()
        updateWalletAmount(
            orderId,
            getResponseValue("orderAmount"),
            getResponseValue("referenceId"),
            getResponseValue("txStatus"),
            getResponseValue("txMsg"),
            getResponseValue("txTime"),
            getResponseValue("signature"),
            getResponseValue("paymentMode")
        )
    }

    private fun handleOrderUpdate() {
        hideAll()
        updateOrder(
            orderId,
            getResponseValue("orderAmount"),
            getResponseValue("referenceId"),
            getResponseValue("txStatus"),
            getResponseValue("txMsg"),
            getResponseValue("txTime"),
            getResponseValue("signature"),
            getResponseValue("paymentMode")
        )
    }

    /* CustomDialog.showDialog(requireActivity(),
               getString(R.string.payment_failed_title),
               getString(R.string.payment_failed_message),
               getString(R.string.view_my_orders),
               AlertType.WARNING,
               object : CustomDialog.OKClickListener {
                   override fun onClick(view: View?) {
                       val intent = Intent(activity, MainActivity::class.java)
                       intent.putExtra("navigate_screen_name", "my_orders")
                       intent.putExtra("order_id", orderId)
                       startActivity(intent)
                       requireActivity().finish()
                   }
               })*/
    private fun showPaymentFailedDialog(type: String, iscancelable: Boolean) {
        AlertMakeOrderAsCOD.showDialog(requireActivity(), type, iscancelable,
            object : AlertMakeOrderAsCOD.OrderSwitchClickListener {
                override fun onClick(view: View?, tag: String) {
                    if (tag == "order_detail") {
                        val intent = Intent(activity, MainActivity::class.java)
                        intent.putExtra("navigate_screen_name", "my_orders")
                        intent.putExtra("order_id", orderIdApi)
                        startActivity(intent)
                        requireActivity().finish()
                    } else if (tag == "cod") {
                        makeOrderAsCOD(orderIdApi)
                    }
                }
            })
    }

    private fun getResponseValue(keyName: String): String {
        paymentParams.forEach {
            if (it.key == keyName) {
                return it.value
            }
        }
        return ""
    }

    private fun makeReq() {
        disposable.add(
            viewModel.getList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<PaymentProvidersModel>>() {
                    override fun onSuccess(res: ApiResponse<PaymentProvidersModel>) {
                        if (res.http_code == 200) {
                            paymentResModel = res.data
                            APP_ID = res.data.cashfree_app_id
                            cashFreeMode = res.data.cashfree_app_env
                            paymentList.clear()
                            res.data.oneline_payment_options.forEachIndexed { index, paymentModel ->
                                paymentList.add(paymentModel)
                            }
                            //paymentList.add(PaymentModel("8", "Card", CODE_CARD, false))
                            refreshUI()
                            if (token.isEmpty()) {
                                getTokenReq(orderId, amount)
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun getTokenReq(orderId: String?, amount: String?) {
        disposable.add(
            viewModel.getToken(orderId, amount)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<CashFreeTokenReponse>>() {
                    override fun onSuccess(res: ApiResponse<CashFreeTokenReponse>) {
                        if (res.http_code == 200) {
                            token = res.data.cftoken
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun openSecondScreen(code: String) {
        paymentList.forEachIndexed { index, paymentModel ->
            if (paymentModel.code == code) {
                updateUI(paymentModel.code, paymentModel.name_en)
            }
        }
    }

    private fun refreshUI() {
        when (paymentType) {
            PaymentType.NETBANKING -> {
                openSecondScreen(CODE_NET_BANKING)
            }
            PaymentType.WALLET -> {
                openSecondScreen(CODE_WALLET)
            }
            PaymentType.UPI -> {
                openSecondScreen(CODE_UPI)
            }
            PaymentType.CARD -> {
                openSecondScreen(CODE_CARD)
            }
            else -> {
                loadParentList()
            }
        }
    }

    private fun initiatePayment(paymentCode: String) {
        val postData: MutableMap<String, String> = HashMap()
        postData[PARAM_APP_ID] = APP_ID
        postData[PARAM_ORDER_ID] = orderId
        postData[PARAM_ORDER_CURRENCY] = APP_CURRENCY
        postData[PARAM_ORDER_AMOUNT] = amount
        postData[PARAM_ORDER_NOTE] = ""  //optional
        postData[PARAM_CUSTOMER_NAME] = userName  //optional
        postData[PARAM_CUSTOMER_PHONE] = userPhone
        postData[PARAM_CUSTOMER_EMAIL] = userEmail
        postData[PARAM_NOTIFY_URL] = "https://test.gocashfree.com/notify"  //optional
        postData[PARAM_PAYMENT_MODES] = ""  //optional

        if (paymentCode == CODE_NET_BANKING) {
            postData[PARAM_PAYMENT_OPTION] = "nb"
            postData[PARAM_BANK_CODE] = selectedBankCode
        } else if (paymentCode == CODE_WALLET) {
            postData[PARAM_PAYMENT_OPTION] = "wallet"
            postData[PARAM_BANK_CODE] = selectedBankCode
        } else if (paymentCode == CODE_UPI) {
            postData[PARAM_PAYMENT_OPTION] = "upi"
            postData[PARAM_UPI_VPA] = binding.upiEditText.text.toString().trim()
        } else if (paymentCode == CODE_CARD) {
            postData[PARAM_PAYMENT_OPTION] = "card"
            postData[PARAM_CARD_NUMBER] = cardNumber
            postData[PARAM_CARD_MM] = cardExpiryMonth
            postData[PARAM_CARD_YYYY] = cardExpiryYear
            postData[PARAM_CARD_HOLDER] = binding.cardlayout.name.text.toString().trim()
            postData[PARAM_CARD_CVV] = cardCVV

        }
        Log.e(TAG, "" + postData.toString())
        val cfPaymentService = getCFPaymentServiceInstance()
        cfPaymentService.setOrientation(0)
        cfPaymentService.doPayment(requireActivity(), postData, token, cashFreeMode)
    }


    private fun makeOrderAsCOD(order_id: String?) {
        disposable.add(
            viewModel.makeOrderAsCOD(order_id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<BaseResponse>() {
                    override fun onSuccess(res: BaseResponse) {
                        if (res.http_code == 200) {
                            res.message?.let {
                                when (screenType) {
                                    ScreenType.CHECKOUT -> {
                                        orderId = order_id!!
                                        showOrderSuccesDialog(it)
                                    }
                                    else -> {
                                        ToastUtil.show(
                                            requireActivity(),
                                            "Payment status updated successfully."
                                        )
                                        mNavController.navigateUp()
                                    }
                                }
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun updateOrder(
        order_id: String?,
        orderAmount: String?,
        referenceId: String?,
        txStatus: String?,
        txMsg: String?,
        txTime: String?,
        signature: String?,
        paymentMode: String?
    ) {
        disposable.add(
            viewModel.updateOrder(
                order_id,
                orderAmount,
                referenceId,
                txStatus,
                txMsg,
                txTime,
                signature,
                paymentMode
            )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<CashFreeCallBack>>() {
                    override fun onSuccess(res: ApiResponse<CashFreeCallBack>) {
                        if (res.http_code == 200) {
                            res.message.let {
                                when (screenType) {
                                    ScreenType.CHECKOUT -> {
                                        orderId = res.data.id
                                        showOrderSuccesDialog(it)
                                    }
                                    else -> {
                                        ToastUtil.show(
                                            requireActivity(),
                                            "Payment status updated successfully."
                                        )
                                        mNavController.navigateUp()
                                    }
                                }

                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun updateWalletAmount(
        orderId: String?,
        orderAmount: String?,
        referenceId: String?,
        txStatus: String?,
        txMsg: String?,
        txTime: String?,
        signature: String?,
        paymentMode: String?
    ) {
        disposable.add(
            viewModel.updateWalletAmount(
                orderId,
                orderAmount,
                referenceId,
                txStatus,
                txMsg,
                txTime,
                signature,
                paymentMode
            )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<AddMoneyToWalletModel>>() {
                    override fun onSuccess(res: ApiResponse<AddMoneyToWalletModel>) {
                        if (res.http_code == 200) {
                            res.message.let { ToastUtil.show(requireActivity(), it) }
                            myPref.setWalletAmount(res.data.wallet_balance)
                            mNavController.navigateUp()
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }


    private fun showOrderSuccesDialog(message: String) {
        CustomDialog.showCheckOutDialog(requireActivity(),
            getString(R.string.success),
            message,
            getString(R.string.view_my_orders),
            AlertType.SUCCESS,
            object : CustomDialog.OKClickListener {
                override fun onClick(view: View?) {
                    val intent = Intent(activity, MainActivity::class.java)
                    intent.putExtra("navigate_screen_name", "my_orders")
                    intent.putExtra("order_id", orderId)
                    startActivity(intent)
                    requireActivity().finish()
                }

                override fun onContinueShoppingClick(view: View?) {
                    val intent = Intent(activity, MainActivity::class.java)
                    startActivity(intent)
                    requireActivity().finish()
                }

            })
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }
}