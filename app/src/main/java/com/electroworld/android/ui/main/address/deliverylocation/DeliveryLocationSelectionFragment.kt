package com.electroworld.android.ui.main.address.deliverylocation

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResource
import com.electroworld.android.customclass.CustomTypingEditText.OnTypingModified
import com.electroworld.android.databinding.FragmentDeliveryLocationBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.address.add.PlaceEventHandler
import com.electroworld.android.ui.main.address.add.PlaceListAdapter
import com.electroworld.android.ui.main.address.add.PlaceModel
import com.electroworld.android.ui.main.address.add.locationutil.LocationAddress
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.Constants.COMES_FROM_DELIVERY_LOCATION
import com.electroworld.android.utils.UrlUtils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import javax.inject.Inject


class DeliveryLocationSelectionFragment : DaggerFragment() {

    private lateinit var viewModel: DeliveryLocationViewModel
    private lateinit var binding: FragmentDeliveryLocationBinding
    private lateinit var mNavController: NavController

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    var listener: PlaceEventHandler? = null

    private var mMap: GoogleMap? = null
    var lat: Double = Constants.LATITUDE
    var lng: Double = Constants.LONGITUDE

    var placeModelArrayList: MutableList<PlaceModel> = ArrayList()

    var storeId = ""
    var storeName = ""
    var storeAddress = ""

    var fromOnCreate = true;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_delivery_location, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fromOnCreate = true

        if ((requireActivity() as MainActivity).currentLatitude == 0.0 ||
            (requireActivity() as MainActivity).currentLongitutde == 0.0
        ) {
            (requireActivity() as MainActivity).currentLatitude =
                (requireActivity() as MainActivity).latitude
            (requireActivity() as MainActivity).currentLongitutde =
                (requireActivity() as MainActivity).longitude
        }

        lat = (requireActivity() as MainActivity).currentLatitude
        lng = (requireActivity() as MainActivity).currentLongitutde

        viewModel =
            ViewModelProvider(this, providersFactory).get(DeliveryLocationViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentDeliveryLocationBinding.bind(view)
        binding.ab.tvABTitle.text = getString(R.string.delivery_location)

        initMapViews()
        initListeners()

        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }

        binding.ivClose.setOnClickListener {
            binding.edSearch.setText("")
            binding.recyclerView.setGone()
        }
        viewModel.getResultByPlaceId().observe(viewLifecycleOwner) { res ->
            consumePlaceInfoByPlaceIdResponse(
                res
            )
        }

        binding.btnConfirmLocation.setOnClickListener {
            Constants.ADDRESS_ID = ""
            Constants.ADDRESS_NAME = binding.tvAddress.text.trim().toString()
            Constants.LATITUDE = lat
            Constants.LONGITUDE = lng
            COMES_FROM_DELIVERY_LOCATION = true
            myPref.setPrefKeyStoreId(storeId)
            myPref.setPrefKeyStoreAddress(storeAddress)
            myPref.setPrefKeyStoreName(storeName)
            myPref.setHomeAddressTop(Constants.ADDRESS_NAME)
            mNavController.navigateUp()
        }

        binding.cvCurrentLocation.setOnClickListener {
            Constants.ADDRESS_NAME = (requireActivity() as MainActivity).currentAddressName
            Constants.LATITUDE = (requireActivity() as MainActivity).currentLatitude
            Constants.LONGITUDE = (requireActivity() as MainActivity).currentLongitutde
            setMapPin(LatLng(Constants.LATITUDE, Constants.LONGITUDE))
        }
    }


    private fun gettingStoreInfo(lat: Double, lng: Double) {
        binding.cvConfirmLocation.setGone()
        viewModel.getStoreInfoResponse(requireActivity(), lat, lng)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer { res ->
                if (res != null) {
                    if (res.http_code == 200) {
                        if (res.data.id != null && res.data.id.length > 0) {
                            storeId = res.data.id
                            storeAddress = res.data.address
                            storeName = res.data.name_en
                        }
                        binding.cvConfirmLocation.setVisible()
                    } else {
                        storeId = ""
                        storeAddress = ""
                        storeName = ""
                        CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                    }
                }
            })
    }


    private fun consumePlaceInfoByPlaceIdResponse(res: ApiResource<JsonObject>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        val json = JSONObject(res.data.toString())
                        val result = json.getJSONObject("result")
                        val geometry = result.getJSONObject("geometry")
                        val location = geometry.getJSONObject("location")
                        val formatted_address = result.getString("formatted_address")
                        updateAddressFields(formatted_address)
                        binding.recyclerView.setGone()

                        val latlng = LatLng(
                            location.getString("lat").toDouble(),
                            location.getString("lng").toDouble()
                        )
                        setMapPin(latlng)
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }

    private fun initListeners() {
        listener = object : PlaceEventHandler {
            override fun handle(placeid: String?, address: String?) {
                try {
                    val string: String =
                        "https://maps.googleapis.com/maps/api/place/details/json?input=&placeid=" + placeid + "&key=" + getString(
                            R.string.place_search_key
                        )
                    viewModel.getInfoByPlaceId(string)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    var textWatcher =
        OnTypingModified { view, isTyping ->
            if (isTyping) {
                Log.i("TAG", "onIsTypingModified: User started typing.")
            } else {
                Log.i("TAG", "onIsTypingModified: User stopped typing")
                handleTextChanges()
            }
        }

    var onFocusChangeListener =
        OnFocusChangeListener { view, b ->
            if (b) {
                binding.edSearch.text?.let { binding.edSearch.setSelection(it.length) }
                binding.ivClose.setVisible()
            } else {
                binding.ivClose.setGone()
            }
        }

    private fun handleTextChanges() {
        try {
            requireActivity().runOnUiThread {
                if (TextUtils.isEmpty(binding.edSearch.text.toString())) {
                    binding.recyclerView.setGone()
                    binding.ivClose.setGone()
                } else {
                    binding.ivClose.setVisible()
                    val url = UrlUtils.formatPlaceSearchUrl(
                        requireActivity(),
                        binding.edSearch.text.toString()
                    )
                    viewModel.getAddressListRes(url)?.observe(
                        viewLifecycleOwner,
                        object : Observer<JsonObject> {
                            override fun onChanged(res: JsonObject) {
                                val json = JSONObject(res.toString())
                                placeModelArrayList.clear()
                                try {
                                    val jsonArray = json.getJSONArray("predictions")
                                    for (i in 0 until jsonArray.length()) {
                                        val placeModel = PlaceModel()
                                        placeModel.setDescription(
                                            jsonArray.getJSONObject(i).getString(
                                                "description"
                                            )
                                        )
                                        placeModel.setpId(
                                            jsonArray.getJSONObject(i).getString(
                                                "place_id"
                                            )
                                        )
                                        placeModelArrayList!!.add(placeModel)
                                    }
                                    if (placeModelArrayList!!.size > 0) {
                                        val placeListAdapter = PlaceListAdapter(
                                            activity,
                                            placeModelArrayList,
                                            listener
                                        )
                                        val mLayoutManager: RecyclerView.LayoutManager =
                                            LinearLayoutManager(
                                                activity
                                            )
                                        binding.recyclerView.addItemDecoration(
                                            DividerItemDecoration(
                                                activity,
                                                LinearLayoutManager.VERTICAL
                                            )
                                        )
                                        binding.recyclerView.layoutManager = mLayoutManager
                                        binding.recyclerView.adapter = placeListAdapter
                                        binding.recyclerView.visibility = View.VISIBLE
                                    }
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            }
                        })
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun initMapViews() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private val callback = OnMapReadyCallback { googleMap ->
        mMap = googleMap
        if ((activity as MainActivity).checkLocationPermissionGiven()) {
            setMapPin(LatLng(lat, lng))
            mMap?.uiSettings?.isMyLocationButtonEnabled = false;
            mMap?.setOnCameraIdleListener {
                var latlng = mMap?.cameraPosition?.target
                if (latlng != null) {
                    lat = latlng.latitude
                    lng = latlng.longitude
                    if (fromOnCreate) {
                        fromOnCreate = false
                    } else {
                        gettingStoreInfo(lat, lng)
                    }
                    LocationAddress().getAddressFromLocation(
                        lat,
                        lng,
                        requireActivity(),
                        GeoCodeHandler()
                    )
                }
            }

        }
    }

    private fun setMapPin(latLng: LatLng) {
        lat = latLng.latitude
        lng = latLng.longitude
        if (mMap != null) {
            mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
        }
    }

    internal inner class GeoCodeHandler : Handler() {
        override fun handleMessage(message: Message) {
            when (message.what) {
                1 -> {
                    val bundle = message.data
                    bundle.getString("address")?.let { updateAddressFields(it) }
                }
            }
        }
    }

    private fun updateAddressFields(it: String) {
        binding.edSearch.setOnTypingModified(null)
        binding.edSearch.setText(it)
        binding.edSearch.setOnTypingModified(textWatcher)
        binding.edSearch.onFocusChangeListener = onFocusChangeListener
        binding.tvAddress.text = it
    }


}