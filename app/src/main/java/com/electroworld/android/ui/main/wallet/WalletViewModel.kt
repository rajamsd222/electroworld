package com.electroworld.android.ui.main.wallet

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.AddMoneyToWalletModel
import com.electroworld.android.model.TransactionListModel
import com.electroworld.android.repository.OMSRepository
import com.electroworld.android.utils.AppUtils
import com.electroworld.android.utils.Constants
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class WalletViewModel  @Inject constructor(private val mainApi: MainApi,private val myPref: PreferencesManager,private val repo : OMSRepository) : ViewModel() {

    fun getTransactionList(): Single<ApiResponse<List<TransactionListModel>>> {
        return repo.makeTransactionListReq()
    }

    private fun doAddMoney(context: Context, money: Double, cfTokenNeed: String): MutableLiveData<ApiResponse<AddMoneyToWalletModel>?>? {
        val res: MutableLiveData<ApiResponse<AddMoneyToWalletModel>?> = MutableLiveData<ApiResponse<AddMoneyToWalletModel>?>()
        val dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.addMoneyToWallet(Constants.HEADER_TOKEN,myPref.getPrefKeyAccessToken(),myPref.getPrefKeyCustomerId(),myPref.getPrefKeyVendorId(),
            money,cfTokenNeed,"mobile")
            .enqueue(object : Callback<ApiResponse<AddMoneyToWalletModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<AddMoneyToWalletModel>?>,
                    response: Response<ApiResponse<AddMoneyToWalletModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }
                override fun onFailure(call: Call<ApiResponse<AddMoneyToWalletModel>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun doAddMoneyRes(context: Context, money: Double,cfTokenNeed : String): LiveData<ApiResponse<AddMoneyToWalletModel>?>? {
        return doAddMoney(context,money,cfTokenNeed)
    }

}