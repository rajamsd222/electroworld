package com.electroworld.android.ui.main

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import com.electroworld.android.BuildConfig
import com.electroworld.android.R
import com.electroworld.android.alert.AlertViewSimple
import com.electroworld.android.databinding.ActivityMainBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.CategoryModel
import com.electroworld.android.model.DeliverySlot
import com.electroworld.android.model.VersionDetails
import com.electroworld.android.model.filter.FilterModel
import com.electroworld.android.model.filter.SortBy
import com.electroworld.android.ui.auth.AuthActivity
import com.electroworld.android.ui.main.address.add.locationutil.GetAddressFromLatLng
import com.electroworld.android.ui.main.address.add.locationutil.LocationHelper
import com.electroworld.android.ui.main.home.HomeFragment
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.Constants.LOCATION_PERMISSION_REQUEST_CODE
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import dagger.android.support.DaggerAppCompatActivity
import java.util.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), ConnectionCallbacks,
    OnConnectionFailedListener, OnRequestPermissionsResultCallback, LocationListener {
    private lateinit var navController: NavController
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainActivityViewModel

    var loginAlertDialog: AlertViewSimple? = null
    var locationHelper: LocationHelper? = null
    private val TAG = "MainActivity"

    var warehouse_product_id: String = ""
    var group_id: String = ""
    var name: String = ""

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    var i = 0

    @Inject
    lateinit var myPref: PreferencesManager
    private val INTERVAL = 1000 * 60 // 5 min interval
        .toLong()
    private val FASTEST_INTERVAL = 1000 * 60 // 3 min interval
        .toLong()
    lateinit var mLocationClient: GoogleApiClient
    var mLocationRequest = LocationRequest()
    var isLocationFetched = false
    var latitude = 0.0
    var longitude = 0.0
    var address = ""
    var asyncTaskGetAddress: GetAddressFromLatLng? = null


    var mCategoryList: List<CategoryModel> = ArrayList()
    var mDeliverySlotList: List<DeliverySlot> = ArrayList()
    var mFiltersList: List<FilterModel> = ArrayList()
    var mSortByList: List<SortBy> = ArrayList()

    var navigate_screen_name = ""
    var order_id = ""
    var currentAddressName = ""
    var currentLatitude = 0.0
    var currentLongitutde = 0.0

    private val FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
    private val COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProviders.of(this, providersFactory)[MainActivityViewModel::class.java]
        locationHelper = LocationHelper(this)

        asyncTaskGetAddress = GetAddressFromLatLng(this)

        if (locationHelper!!.checkPlayServices()) {
            // Building the GoogleApi client
            locationHelper!!.buildGoogleApiClientNoAlert()
        }

        val bundle = intent.extras
        if (bundle != null) {
            navigate_screen_name = AppValidator.toStr(bundle.getString("navigate_screen_name"))
            order_id = AppValidator.toStr(bundle.getString("order_id"))
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            parseProductLink()
        }
        initLocationRequest()
        initLoginDialog()
        initObservers()
        navController = findNavController(R.id.nav_host_fragment)
        //binding.navView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { controller: NavController?, destination: NavDestination, arguments: Bundle? ->
            if (destination.id == R.id.mapsFragment
                || destination.id == R.id.productListFragment
                || destination.id == R.id.productDetailFragment
                || destination.id == R.id.cartListFragment
                || destination.id == R.id.editProfileFragment
                || destination.id == R.id.webViewFragment
                || destination.id == R.id.notificationFragment
                || destination.id == R.id.trackOrderFragment
                || destination.id == R.id.orderDetailFragment
                || destination.id == R.id.checkOutFragment
                || destination.id == R.id.myOffersAndOthers
                || destination.id == R.id.returnOrderFragment
                || destination.id == R.id.cashFreePaymentFragment
                || destination.id == R.id.deliveryLocationSelectionFragment
            ) {
                hideBottomBar(true)
            } else {
                hideBottomBar(false)
            }

            if (destination.id == R.id.navigation_categories) {
                updateBottomTab(1)
            } else if (destination.id == R.id.navigation_search) {
                updateBottomTab(2)
            } else if (destination.id == R.id.navigation_home) {
                updateBottomTab(3)
            } else if (destination.id == R.id.navigation_offers) {
                updateBottomTab(4)
            } else if (destination.id == R.id.navigation_quick_buy) {
                updateBottomTab(5)
            }

        }

        if (navigate_screen_name == "my_orders") {
            if (order_id.isNotEmpty()) {
                isLocationFetched = true
                val bundleA = Bundle()
                bundleA.putString("order_id", order_id)
                navController.navigate(R.id.orderDetailFragment, bundleA)
            }
        } else if (navigate_screen_name == "notification") {
            navController.navigate(R.id.notificationFragment)
        }
    }

    private fun initObservers() {
        observeAppInfo()
    }

    private fun observeAppInfo() {
        viewModel.liveDataVersion.observe(this, {
            it?.let {
                if (it.http_code == 200) {
                    it.data.let {
                        validateAppVersionRes(it.app_details)
                        val serverMaxQty = it.config.max_order_product_qty
                        val currentMaxQty = myPref.getMaxQuantityLimit()
                        if (currentMaxQty > serverMaxQty) {
                            myPref.clearLocalCart()
                            if (getCurrentFragment() != null) {
                                (getCurrentFragment() as HomeFragment).updateCartCountUI()
                            }
                        }
                        myPref.setMaxQuantityLimit(serverMaxQty)
                    }
                }
            }
        })

    }

    private fun validateAppVersionRes(result: VersionDetails) {
        val currentVersionCode: Int = getAppVersionCode(this)
        var playStoreVersionCode = AppValidator.toInt(result.android_app_current_version_code)
        //playStoreVersionCode = 3

        val forceUpdateVersionCode = AppValidator.toInt(result.android_force_update_version)
        val isForceupdate =
            forceUpdateVersionCode > 0 && forceUpdateVersionCode > currentVersionCode
        if (playStoreVersionCode > 0 && playStoreVersionCode > currentVersionCode) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.app_update_alert_title))
            builder.setMessage(getString(R.string.app_update_alert_message))
            builder.setCancelable(false)
            builder.setPositiveButton(getString(R.string.btn_update)) { dialogInterface, i ->
                val appPackageName: String = BuildConfig.APPLICATION_ID
                try {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=$appPackageName")
                        )
                    )
                } catch (anfe: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                        )
                    )
                }
            }
            if (!isForceupdate) {
                builder.setNegativeButton(getString(R.string.later)) { dialogInterface, i -> dialogInterface.dismiss() }
            }
            builder.show()
        }
    }

/*
    @RequiresApi(Build.VERSION_CODES.O)
    private fun parseProductLink() {
        val intent = intent
        val data = intent.data
        try {
            if (data != null) {
                val uri = Uri.parse(data.toString())
                val segments = uri.path?.split("/")
                val last = segments?.get(segments.size - 1)
                val secondToLast = segments?.get(segments.size - 2)

                Log.e("last : ", last.toString())
                Log.e("secondToLast : ", secondToLast.toString())

                if (last != null && last.isNotEmpty() && last.contains("-")) {
                    warehouse_product_id = last.split("-")[0]
                    name = last.split("-")[1]
                }
                if (secondToLast != null && secondToLast.isNotEmpty() && secondToLast.contains("-")) {
                    group_id = secondToLast.split("-")[0]
                }

            }

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }*/
    private fun parseProductLink() {
        val intent = intent
        val data = intent.data
        try {
            if (data != null) {
                val uri = Uri.parse(data.toString())
                val segments = uri.path?.split("/")
                val last = segments?.get(segments.size - 1)
                val secondToLast = segments?.get(segments.size - 2)

                Log.e("last : ", last.toString())
                Log.e("secondToLast : ", secondToLast.toString())

                if (last != null && last.isNotEmpty() && last.contains("-")) {
                    warehouse_product_id = last.split("-")[0]
                    name = last.split("-")[1]
                }
                if (secondToLast != null && secondToLast.isNotEmpty() && secondToLast.contains("-")) {
                    group_id = secondToLast.split("-")[0]
                }

            }

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
    private fun getLastSegmentFromUrl(url: String): String {
        return url.replaceFirst(".*/([^/?]+).*".toRegex(), "$1")
    }

    private fun initLoginDialog() {
        loginAlertDialog = AlertViewSimple(this, object : AlertViewSimple.OKClickListener {
            override fun onOKClick(view: View?) {
                this@MainActivity.startActivity(Intent(this@MainActivity, AuthActivity::class.java))
            }
        })
        loginAlertDialog!!.setCancelable(true)
        loginAlertDialog!!.setDialogTitle(getString(R.string.login_alert))
        loginAlertDialog!!.setDialogMessage(getString(R.string.login_alert_msg))
        loginAlertDialog!!.setPositiveText(getString(R.string.btn_login))
        loginAlertDialog!!.setNegativeText(getString(R.string.btn_cancel))
        loginAlertDialog!!.setShowNegativeButton(true)
    }


    /*  @Override
    public boolean onSupportNavigateUp() {
        //return super.onSupportNavigateUp();
        return navController.navigateUp();
    }*/
    /*
   @Override
    public void onBackPressed() {
       if (navController..size() > 1) {
           navController.popBackStack();
        } else {
            super.onBackPressed();
        }
    }*/


    fun setFavouriteCount(count: Int) {
        /*if (count > 0) {
            navView.getOrCreateBadge(R.id.navigation_fav).setNumber(count)
            navView.getOrCreateBadge(R.id.navigation_fav)
                .setBackgroundColor(resources.getColor(R.color.colorPrimary))
            navView.getOrCreateBadge(R.id.navigation_fav).setVisible(true)
        } else {
            navView.getOrCreateBadge(R.id.navigation_fav).setVisible(false)
        }
        myPref.setFavCount(count)*/
    }

    fun showLoginDialog(message: String?) {
        loginAlertDialog!!.setDialogMessage(message!!)
        loginAlertDialog!!.showDialog()
    }

    fun hideBottomBar(isHide: Boolean) {
        binding.bottomView.visibility = if (isHide) View.GONE else View.VISIBLE
        binding.iconCenter.visibility = if (isHide) View.GONE else View.VISIBLE
    }

    fun openDetailsScreen(warehouse_product_id: String, name: String) {
        val bundle = Bundle()
        bundle.putString("name", name)
        bundle.putString("group_id", "148")
        bundle.putString("warehouse_product_id", warehouse_product_id)
        navController.navigate(R.id.productDetailFragment, bundle)
    }

    fun navCatClick(v: View) {
        updateBottomTab(1)
        navController.navigate(R.id.navigation_categories)
    }

    fun navSearchClick(v: View) {
        updateBottomTab(2)
        navController.navigate(R.id.navigation_search)
    }

    fun navHomeClick(v: View) {
        updateBottomTab(3)
        navController.navigate(R.id.navigation_home)
    }

    fun navoffersClick(v: View) {
        updateBottomTab(4)
        navController.navigate(R.id.navigation_offers)
    }

    fun navQuickBuyClick(v: View) {
        updateBottomTab(5)
        navController.navigate(R.id.navigation_compare)
    }

    private fun updateBottomTab(pos: Int) {
        binding.text1.setTextColor(ContextCompat.getColor(this, R.color.bottom_tab_color_default))
        binding.text2.setTextColor(ContextCompat.getColor(this, R.color.bottom_tab_color_default))
        binding.text3.setTextColor(ContextCompat.getColor(this, R.color.bottom_tab_color_default))
        binding.text4.setTextColor(ContextCompat.getColor(this, R.color.bottom_tab_color_default))
        binding.text5.setTextColor(ContextCompat.getColor(this, R.color.bottom_tab_color_default))

        binding.lineGreen1.setInvisible()
        binding.lineGreen2.setInvisible()
        binding.lineGreen3.setInvisible()
        binding.lineGreen4.setInvisible()
        binding.lineGreen5.setInvisible()

        binding.icon1.setColorFilter(
            ContextCompat.getColor(
                this,
                R.color.bottom_tab_color_default
            )
        )
        binding.icon2.setColorFilter(
            ContextCompat.getColor(
                this,
                R.color.bottom_tab_color_default
            )
        )
        binding.icon3.setColorFilter(
            ContextCompat.getColor(
                this,
                R.color.bottom_tab_color_default
            )
        )
        binding.icon4.setColorFilter(
            ContextCompat.getColor(
                this,
                R.color.bottom_tab_color_default
            )
        )
        binding.icon5.setColorFilter(
            ContextCompat.getColor(
                this,
                R.color.bottom_tab_color_default
            )
        )


        if (pos == 1) {
            binding.text1.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.bottom_tab_color_selected
                )
            )
            binding.icon1.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.bottom_tab_color_selected
                )
            )
            binding.lineGreen1.setVisible()
        } else if (pos == 2) {
            binding.text2.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.bottom_tab_color_selected
                )
            )
            binding.icon2.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.bottom_tab_color_selected
                )
            )
            binding.lineGreen2.setVisible()
        } else if (pos == 3) {
            binding.text3.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.bottom_tab_color_selected
                )
            )
            binding.icon3.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.bottom_tab_color_selected
                )
            )
        } else if (pos == 4) {
            binding.text4.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.bottom_tab_color_selected
                )
            )
            binding.icon4.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.bottom_tab_color_selected
                )
            )
            binding.lineGreen4.setVisible()
        } else if (pos == 5) {
            binding.text5.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.bottom_tab_color_selected
                )
            )
            binding.icon5.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.bottom_tab_color_selected
                )
            )
            binding.lineGreen5.setVisible()
        }
    }

    override fun onResume() {
        super.onResume()
        locationHelper!!.checkPlayServices()
        //getDeviceLocation()
        refreshAddToCompareBadge()
    }

    public fun refreshAppInfo() {
//        if (Urls.BASE_URL == Urls.PRODUCTION_SERVER_URL) {
//             viewModel.getVersionInfoReq()
//        }
        viewModel.getVersionInfoReq()
    }

    private fun stopLocationUpdates() {
        Log.e(TAG, "service destroyed")
        if (mLocationClient.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient, this)
            mLocationClient.disconnect()
        }
    }

    public fun refreshAddToCompareBadge() {
        var count: Int = 0
        val model = myPref.getAddToCompareData();
        if (model != null) {
            count = model.cartItemList.size
        }
        binding.tvCartCount.text = "" + count
        binding.tvCartCount.visibility = if (count > 0) View.VISIBLE else View.GONE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (i <= 1) {
            i += 1;
            super.onActivityResult(requestCode, resultCode, data)
            locationHelper!!.onActivityResult(requestCode, resultCode, data)
            val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
            val childFragments = navHostFragment?.childFragmentManager?.fragments
            childFragments?.forEach { it.onActivityResult(requestCode, resultCode, data) }
            when (requestCode) {
                LocationHelper.REQUEST_CHECK_SETTINGS -> when (resultCode) {
                    RESULT_OK -> try {
                        //Constants.COMES_FROM_SPLASH = true
                        Thread.sleep(1000)
                        getDeviceLocation()
                    } catch (ignored: Exception) {
                    }
                    RESULT_CANCELED -> {
                    }
                    else -> {
                    }
                }
            }

            /*if (data != null) {
            val bundle = data.extras
            if (bundle != null){
                for (key in bundle.keySet()) {
                    if (bundle.getString(key) != null) {
                        Log.d("TAG", key + " : " + bundle.getString(key))
                        Log.e("TAG", key + " : " + bundle.getString(key))
                    }
                }
            }
        }*/
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        locationHelper!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults != null && grantResults.isNotEmpty()) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        return
                    }
                }
            }
        }
    }

    private fun initLocationRequest() {
        /*// Initialize Places.
        Places.initialize(getApplicationContext(), apiKey);

        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);*/
        mLocationClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API) /*.addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)*/
            .enableAutoManage(this, this)
            .build()
        mLocationRequest.interval = INTERVAL
        mLocationRequest.fastestInterval = FASTEST_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationClient.connect()
    }

    override fun onConnected(p0: Bundle?) {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.d(TAG, "== Error On onConnected() Permission not granted")
            //Permission not granted by user so cancel the further execution.
            return
        }
        if (mLocationClient.isConnected) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mLocationClient,
                mLocationRequest,
                this
            )
        }

        Log.d(TAG, "Connected to Google API")
    }

    fun requestLocationUpdate() {
        if (mLocationClient != null && mLocationClient.isConnected) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mLocationClient,
                mLocationRequest,
                this
            )
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        locationHelper!!.connectApiClient()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("Not yet implemented")
    }

    override fun onLocationChanged(location: Location?) {
        Log.e(TAG, "Location changed")
        if (location != null) {
            Log.e(
                TAG,
                "getDeviceLocation: " + location.latitude + "," + location.longitude
            )
            if (!isLocationFetched || latitude == 0.00 || longitude == 0.0) {
                isLocationFetched = true
                latitude = location.latitude
                longitude = location.longitude
                GetAddressFromLatLng(this, latitude, longitude).execute()
//                asyncTaskGetAddress!!.setLatLng(latitude, longitude)
//                asyncTaskGetAddress!!.execute()
            }
        }
    }

    fun getDeviceLocation() {
        try {
            locationHelper!!.checkGPSEnabled()
            val mLastLocation = locationHelper!!.location
            if (mLastLocation != null) {
                val mLatLng = LatLng(mLastLocation.latitude, mLastLocation.longitude)
                if (!isLocationFetched || latitude == 0.0 || longitude == 0.00) {
                    isLocationFetched = true
                    latitude = mLatLng.latitude
                    longitude = mLatLng.longitude
                    GetAddressFromLatLng(this, latitude, longitude).execute()
//                    asyncTaskGetAddress!!.setLatLng(latitude, longitude)
//                    asyncTaskGetAddress!!.execute()
                }
            } else {
                requestLocationUpdate()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", "" + e.message)
        }
    }

    public fun setDefaultLocation() { //Adishwar Electro world Head Office
        latitude = 12.983796
        longitude = 77.559443
        Constants.LATITUDE = latitude
        Constants.LONGITUDE = longitude

//        12.906343165085982
//        longitude = 77.5856825709343

        GetAddressFromLatLng(this, latitude, longitude).execute()
//        asyncTaskGetAddress!!.setLatLng(latitude, longitude)
//        asyncTaskGetAddress!!.execute()
    }

    override fun onDestroy() {
        stopLocationUpdates()
        super.onDestroy()
        //locationHelper.cl
    }


    fun getAppVersionCode(context: Context): Int {
        var appVersion = 0
        try {
            appVersion = context.packageManager.getPackageInfo(context.packageName, 0).versionCode
            return appVersion
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return 0
    }

    fun checkLocationPermissionGiven(): Boolean {
        val permissions = arrayOf(FINE_LOCATION, COURSE_LOCATION)
        if (ContextCompat.checkSelfPermission(
                this,
                FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    COURSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                return true
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        } else {
            ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE)
        }
        return false
    }

    public fun getCurrentFragment(): Fragment? {
        var fragment: Fragment? = null
        val navHostFragment: Fragment? =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        if (navHostFragment != null) {
            try {
                if (navHostFragment.childFragmentManager.fragments.size > 0) {
                    fragment = navHostFragment
                        .childFragmentManager
                        .fragments[0]
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
        return fragment
    }


}