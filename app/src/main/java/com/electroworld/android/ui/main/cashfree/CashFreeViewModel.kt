package com.electroworld.android.ui.main.cashfree

import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.*
import com.electroworld.android.repository.OMSRepository
import io.reactivex.Single
import javax.inject.Inject

class CashFreeViewModel @Inject constructor(
    private val mainApi: MainApi,
    val myPref: PreferencesManager,
    private val repoOMS: OMSRepository
) : ViewModel() {

    fun getToken(orderId: String?,amount: String?): Single<ApiResponse<CashFreeTokenReponse>> {
        return repoOMS.getCashFreeToken(orderId,amount)
    }
    fun getList(): Single<ApiResponse<PaymentProvidersModel>> {
        return repoOMS.getPaymentProvidersList()
    }


    fun makeOrderAsCOD(orderId: String?): Single<BaseResponse> {
        return repoOMS.makeOrderAsCOD(orderId)
    }


    fun updateOrder(
        orderId: String?, orderAmount: String?, referenceId: String?, txStatus: String?, txMsg: String?,
        txTime: String?, signature: String?, paymentMode: String?
    ): Single<ApiResponse<CashFreeCallBack>> {
        return repoOMS.updateOrder(orderId, orderAmount,referenceId, txStatus, txMsg, txTime, signature,paymentMode)
    }

    fun updateWalletAmount(
        orderId: String?, orderAmount: String?, referenceId: String?, txStatus: String?, txMsg: String?,
        txTime: String?, signature: String?, paymentMode: String?
    ): Single<ApiResponse<AddMoneyToWalletModel>> {
        return repoOMS.updateWalletAmount(orderId, orderAmount,referenceId, txStatus, txMsg, txTime, signature,paymentMode)
    }
}