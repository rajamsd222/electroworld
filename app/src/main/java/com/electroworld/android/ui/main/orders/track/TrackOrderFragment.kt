package com.electroworld.android.ui.main.orders.track

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.databinding.FragmentTrackOrderBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.model.OrderStatus
import com.electroworld.android.model.TimeLineModel
import com.electroworld.android.model.TimelineAttributes
import com.electroworld.android.model.orderdetail.OrderDetailModel
import com.electroworld.android.ui.main.orders.TimeLineAdapter
import com.github.vipulasri.timelineview.TimelineView
import com.electroworld.android.extentions.dpToPx
import com.electroworld.android.extentions.getColorCompat
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import javax.inject.Inject

class TrackOrderFragment : DaggerFragment() {

    private lateinit var viewModel: TrackOrderViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentTrackOrderBinding

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    private lateinit var mAttributes: TimelineAttributes
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_track_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(TrackOrderViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentTrackOrderBinding.bind(view)
        binding.ab.tvABTitle.text = getString(R.string.title_track_order)


        var model = arguments?.getSerializable("data") as OrderDetailModel?

        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        mAttributes = TimelineAttributes(
            markerSize = dpToPx(20f),
            markerColor = getColorCompat(R.color.colorPrimary),
            markerInCenter = true,
            markerLeftPadding = dpToPx(0f),
            markerTopPadding = dpToPx(0f),
            markerRightPadding = dpToPx(0f),
            markerBottomPadding = dpToPx(0f),
            linePadding = dpToPx(2f),
            startLineColor = getColorCompat(R.color.colorPrimary),
            endLineColor = getColorCompat(R.color.colorPrimary),
            lineStyle = TimelineView.LineStyle.NORMAL,
            lineWidth = dpToPx(2f),
            lineDashWidth = dpToPx(4f),
            lineDashGap = dpToPx(2f)
        )

        var status: OrderStatus
        val mDataList = ArrayList<TimeLineModel>()
        model?.order_status_history?.forEach {
            status = OrderStatus.INACTIVE
            if (model.order_status_id > it.status_id) {
                status = OrderStatus.COMPLETED
            } else if (model.order_status_id == it.status_id) {
                status =
                    if (model.order_status_id == 8) OrderStatus.COMPLETED else OrderStatus.ACTIVE
            }
            mDataList.add(TimeLineModel(it.status, it.created_at, status))
        }
        binding.rvTimeLine.apply {
            layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
            adapter = TimeLineAdapter(mDataList, mAttributes, "track")
        }

    }

}