package com.electroworld.android.ui.auth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.auth.AuthApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.UserModel
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.SystemUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class AuthViewModel  @Inject constructor(private val authApi: AuthApi, val myPref: PreferencesManager) : ViewModel() {

    private val disposable = CompositeDisposable()
    val userLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    val mUserLogin = MutableLiveData<ApiResponse<UserModel>>()

    fun makeLoginReq(
        context: AuthActivity,
        mobileNumber: String?,
        loginType: String?,
        socialLoginId: String?
    ) {
        loading.value = true
        disposable.add(
            authApi.doLoginNew(
                Constants.ROLE,
                Constants.USER_TYPE,
                mobileNumber,
                SystemUtils.getDeviceID(context),
                SystemUtils.getDeviceName(),
                SystemUtils.getDeviceVersion(),
                SystemUtils.getAppVersion(context),
                Constants.DEVICE_TYPE,
                myPref.getFCMToken(),
                loginType,
                socialLoginId,
                myPref.getPrefKeyVendorId(),
                myPref.getPrefKeyLanguage()
            )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ApiResponse<UserModel>>() {
                    override fun onSuccess(t: ApiResponse<UserModel>) {
                        mUserLogin.value = t
                        userLoadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        userLoadError.value = true
                        loading.value = false

                    }
                })
        )
    }

}