package com.electroworld.android.ui.auth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.annotation.NonNull
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.facebook.*
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.electroworld.android.R
import dagger.android.support.DaggerAppCompatActivity
import org.json.JSONException
import java.util.*


class AuthActivity : DaggerAppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    private var mGoogleSignInClient: GoogleSignInClient? = null
    private var loginManager: LoginManager? = null
    private var callbackManager: CallbackManager? = null
    private val RC_SIGN_IN = 1

    var firstName = ""
    var lastName = ""
    var email = ""
    var profileImage = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_auth)
        initGoogleSignIn()
        facebookLogin()
    }

    fun doGoogleSignIn(){
        val signInIntent: Intent? = mGoogleSignInClient?.getSignInIntent()
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    fun doFacebookLogin(){
        loginManager?.logInWithReadPermissions(
            this,
            Arrays.asList(
                "email",
                "public_profile"
            )
        );
    }

    fun initGoogleSignIn(){
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun loadFacebookUserProfile(currentAccessToken: AccessToken) {
        val request = GraphRequest.newMeRequest(currentAccessToken) { `object`, response ->
            if (`object` != null) {
                try {
                    //val name = `object`.getString("name")
                    firstName = `object`.getString("first_name")
                    lastName = `object`.getString("last_name")
                    email = `object`.getString("email")
                    val fbUserID = `object`.getString("id")
                    profileImage = "https://graph.facebook.com/"+fbUserID+"/picture?type=normal"
                    disconnectFromFacebook()
                    val intent = Intent("social_login")
                    intent.putExtra("social_type", "facebook")
                    intent.putExtra("id", fbUserID)
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
                } catch (e: JSONException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }
            }
        }
        val parameters = Bundle()
        parameters.putString("fields", "first_name,last_name,id, name, email")
        request.parameters = parameters
        request.executeAsync()
    }

    fun facebookLogin() {
        loginManager = LoginManager.getInstance()
        callbackManager = CallbackManager.Factory.create()
        loginManager?.registerCallback(
            callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    loadFacebookUserProfile(loginResult.accessToken)
                }

                override fun onCancel() {
                    Log.v("LoginScreen", "---onCancel")
                }

                override fun onError(error: FacebookException) {
                    // here write code when get error
                    Log.v(
                        "LoginScreen", "----onError: "
                                + error.message
                    )
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> =
                GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }
    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            // Signed in successfully, show authenticated UI.
            updateUI(account)
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.e("TAG", "signInResult:failed code=" + e.statusCode)
           // Utils.showToast(this, "signInResult:failed code=" + e.statusCode)
        }
    }

    private fun updateUI(acct: GoogleSignInAccount?) {
        if (acct != null) {
            firstName = acct.givenName.toString()
            lastName = acct.familyName.toString()
            email = acct.email.toString()
            val personId: String? = acct.id
            profileImage = acct.photoUrl.toString()
            signOut()
            revokeAccess()
            val intent = Intent("social_login")
            intent.putExtra("social_type", "google")
            intent.putExtra("id", personId)
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        }
    }

    private fun disconnectFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return  // already logged out
        }

        GraphRequest(
            AccessToken.getCurrentAccessToken(),
            "/me/permissions/",
            null,
            HttpMethod.DELETE
        ) { LoginManager.getInstance().logOut() }
            .executeAsync()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    private fun signOut() {
        mGoogleSignInClient!!.signOut()
            .addOnCompleteListener(this, object : OnCompleteListener<Void?> {
                override fun onComplete(@NonNull task: Task<Void?>) {
                    // ...
                }
            })
    }
    private fun revokeAccess() {
        mGoogleSignInClient!!.revokeAccess()
            .addOnCompleteListener(this) {
                // ...
            }
    }
}