package com.electroworld.android.ui.auth.login

import android.app.Dialog
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.auth.AuthApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.di.storage.PreferencesManager.*
import com.electroworld.android.di.storage.PreferencesManager.Companion.PREF_KEY_ACCESS_TOKEN
import com.electroworld.android.di.storage.PreferencesManager.Companion.PREF_KEY_AUTHENTICATION_ID
import com.electroworld.android.di.storage.PreferencesManager.Companion.PREF_KEY_CUSTOMER_ID
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.SendOTPModel
import com.electroworld.android.model.UpdatePriceModel
import com.electroworld.android.model.UserModel
import com.electroworld.android.ui.auth.AuthActivity
import com.electroworld.android.utils.AppUtils
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.SystemUtils
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class LoginViewModel @Inject constructor(
    private val authApi: AuthApi,
    val myPref: PreferencesManager
) : ViewModel() {

    private val disposable = CompositeDisposable()
    val userLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    val mUserLogin = MutableLiveData<ApiResponse<UserModel>>()

    private fun doSignUp(
        context: Context,
        loginType: String,
        socialLoginId: String,
        email: String,
        phone: String,
        firstname: String,
        lastname: String,
        device_id: String,
        device_name: String,
        device_version: String,
        app_version: String,
        fcm_token: String,
        vendor_id: String
    )
            : MutableLiveData<ApiResponse<SendOTPModel>?>? {
        val res: MutableLiveData<ApiResponse<SendOTPModel>?> =
            MutableLiveData<ApiResponse<SendOTPModel>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        authApi.doSignupUser(
            Constants.HEADER_TOKEN,
            Constants.ROLE,
            loginType,
            socialLoginId,
            email,
            Constants.COUNTRY_CODE,
            phone,
            "",
            firstname,
            lastname,
            Constants.DEVICE_TYPE,
            device_id,
            device_name,
            device_version,
            app_version,
            fcm_token,
            Constants.LATITUDE,
            Constants.LONGITUDE,
            "",
            vendor_id,
            ""
        ).enqueue(object : Callback<ApiResponse<SendOTPModel>?> {
            override fun onResponse(
                call: Call<ApiResponse<SendOTPModel>?>,
                response: Response<ApiResponse<SendOTPModel>?>
            ) {
                dialog.dismiss()
                if (response.isSuccessful) {
                    res.setValue(response.body())
                }
            }

            override fun onFailure(call: Call<ApiResponse<SendOTPModel>?>, t: Throwable) {
                dialog.dismiss()
                res.setValue(null)
            }
        })
        return res
    }

    fun doSignUpRes(
        context: Context,
        loginType: String,
        socialLoginId: String,
        email: String,
        phone: String,
        firstname: String,
        lastname: String,
        device_id: String,
        device_name: String,
        device_version: String,
        app_version: String,
        fcm_token: String,
        vendor_id: String
    ): LiveData<ApiResponse<SendOTPModel>?>? {
        return doSignUp(
            context,
            loginType,
            socialLoginId,
            email,
            phone,
            firstname,
            lastname,
            device_id,
            device_name,
            device_version,
            app_version,
            fcm_token,
            vendor_id
        )
    }

    private fun doCheckSocialIdExists(
        context: AuthActivity,
        loginType: String,
        socialLoginId: String,
        email: String
    )
            : MutableLiveData<ApiResponse<UserModel>?>? {
        val res: MutableLiveData<ApiResponse<UserModel>?> =
            MutableLiveData<ApiResponse<UserModel>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        authApi.doCheckSocialIdExists(
            Constants.HEADER_TOKEN,
            loginType,
            socialLoginId,
            myPref.getPrefKeyVendorId(),
            email,
            Constants.ROLE,
            Constants.DEVICE_TYPE,
            SystemUtils.getDeviceName(),
            SystemUtils.getAppVersion(context),
            myPref.getFCMToken(),
            Constants.LATITUDE,
            Constants.LONGITUDE,
            "",
            SystemUtils.getDeviceID(context),
            SystemUtils.getDeviceVersion()
        ).enqueue(object : Callback<ApiResponse<UserModel>?> {
            override fun onResponse(
                call: Call<ApiResponse<UserModel>?>,
                response: Response<ApiResponse<UserModel>?>
            ) {
                dialog.dismiss()
                if (response.isSuccessful) {
                    res.setValue(response.body())
                }
            }

            override fun onFailure(call: Call<ApiResponse<UserModel>?>, t: Throwable) {
                dialog.dismiss()
                res.setValue(null)
            }
        })
        return res
    }

    fun doCheckSocialIdExistsRes(
        context: AuthActivity,
        loginType: String,
        socialLoginId: String,
        email: String
    ): LiveData<ApiResponse<UserModel>?>? {
        return doCheckSocialIdExists(context, loginType, socialLoginId, email)
    }

    private fun doLogin(
        context: Context, mobileNumber: String, device_id: String, app_version: String,
        loginType: String, socialLoginId: String
    ): MutableLiveData<ApiResponse<UserModel>?>? {
        val res: MutableLiveData<ApiResponse<UserModel>?> =
            MutableLiveData<ApiResponse<UserModel>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        authApi.doLogin(
            Constants.HEADER_TOKEN,
            Constants.ROLE,
            Constants.USER_TYPE,
            mobileNumber,
            device_id,
            SystemUtils.getDeviceName(),
            Build.VERSION.SDK_INT.toString(),
            app_version,
            Constants.DEVICE_TYPE,
            myPref.getFCMToken(),
            loginType,
            socialLoginId,
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyLanguage()
        ).enqueue(object : Callback<ApiResponse<UserModel>?> {
            override fun onResponse(
                call: Call<ApiResponse<UserModel>?>,
                response: Response<ApiResponse<UserModel>?>
            ) {
                dialog.dismiss()
                if (response.isSuccessful) {
                    res.setValue(response.body())
                }
            }

            override fun onFailure(call: Call<ApiResponse<UserModel>?>, t: Throwable) {
                dialog.dismiss()
                res.setValue(null)
            }
        })
        return res
    }

    fun doLoginRes(
        context: Context, mobileNumber: String, device_id: String, app_version: String,
        loginType: String, socialLoginId: String
    ): LiveData<ApiResponse<UserModel>?>? {
        return doLogin(
            context,
            mobileNumber, device_id, app_version,
            loginType, socialLoginId
        )
    }

    private fun updateCartListPrice(
        context: Context,
        token: String,
        customerId: String,
        vendorId: String,
        productArray: String,
        language: String
    ): MutableLiveData<ApiResponse<List<UpdatePriceModel>>?>? {
        val res: MutableLiveData<ApiResponse<List<UpdatePriceModel>>?> =
            MutableLiveData<ApiResponse<List<UpdatePriceModel>>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        authApi.getUpdatedPriceList(
            Constants.HEADER_TOKEN,
            token,
            customerId,
            vendorId,
            productArray,
            language
        ).enqueue(object : Callback<ApiResponse<List<UpdatePriceModel>>?> {
            override fun onResponse(
                call: Call<ApiResponse<List<UpdatePriceModel>>?>,
                response: Response<ApiResponse<List<UpdatePriceModel>>?>
            ) {
                dialog.dismiss()
                if (response.isSuccessful) {
                    res.setValue(response.body())
                }
            }

            override fun onFailure(call: Call<ApiResponse<List<UpdatePriceModel>>?>, t: Throwable) {
                dialog.dismiss()
                res.setValue(null)
            }
        })
        return res
    }

    fun updateCartListPriceRes(
        context: Context,
        token: String,
        customerId: String,
        vendorId: String,
        productArray: String,
        language: String
    ): LiveData<ApiResponse<List<UpdatePriceModel>>?>? {
        return updateCartListPrice(
            context, token, customerId, vendorId, productArray, language
        )
    }


    fun makeLoginReq(
        context: AuthActivity,
        mobileNumber: String?,
        loginType: String?,
        socialLoginId: String?
    ) {
        loading.value = true
        disposable.add(
            authApi.doLoginNew(
                Constants.ROLE,
                Constants.USER_TYPE,
                mobileNumber,
                SystemUtils.getDeviceID(context),
                SystemUtils.getDeviceName(),
                SystemUtils.getDeviceVersion(),
                SystemUtils.getAppVersion(context),
                Constants.DEVICE_TYPE,
                myPref.getFCMToken(),
                loginType,
                socialLoginId,
                myPref.getPrefKeyVendorId(),
                myPref.getPrefKeyLanguage()
            )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ApiResponse<UserModel>>() {
                    override fun onSuccess(t: ApiResponse<UserModel>) {
                        mUserLogin.value = t
                        userLoadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        userLoadError.value = true
                        loading.value = false
                    }
                })
        )
    }

    fun doLogin(
        context: AuthActivity, mobileNumber: String?,
        loginType: String?,
        socialLoginId: String?
    ): Single<ApiResponse<UserModel>> {
        return authApi.doLoginNew(
            Constants.ROLE,
            Constants.USER_TYPE,
            mobileNumber,
            SystemUtils.getDeviceID(context),
            SystemUtils.getDeviceName(),
            SystemUtils.getDeviceVersion(),
            SystemUtils.getAppVersion(context),
            Constants.DEVICE_TYPE,
            myPref.getFCMToken(),
            loginType,
            socialLoginId,
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyLanguage()
        )
    }

    fun getFcmToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("LoginFragment", "GetInstanceId Failed", task.exception)
                return@OnCompleteListener
            }
            // Get new Instance ID token
            val fcmToken = task.result!!.token
            Log.e("LoginFragment", "FirebaseInstanceId token$fcmToken")
            if (fcmToken.length > 0) {
                myPref.saveFCM(fcmToken)
            }
        })
    }

    fun saveUserDetails(data: UserModel, s: String) {
        myPref.setStringValue(PREF_KEY_CUSTOMER_ID, data.CustomerId)
        myPref.setStringValue(PREF_KEY_ACCESS_TOKEN, data.token)
        myPref.setStringValue(PREF_KEY_AUTHENTICATION_ID, data.authentication_device_id)
        myPref.setPrefKeyUserModel(data)
    }
}