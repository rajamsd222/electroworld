package com.electroworld.android.ui.main.products.search

import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import co.lujun.androidtagview.TagView.OnTagClickListener
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.customclass.LoadMoreNestedScrollView
import com.electroworld.android.databinding.FragmentSearchBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.ProductApiModel
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.SearchHistoryModel
import com.electroworld.android.model.SetFavModel
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.products.ProductListAdapter
import com.jakewharton.rxbinding.widget.RxTextView
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.toolbar_title_cart.view.*
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class SearchFragment : DaggerFragment() {

    private lateinit var viewModel: SearchViewModel

    private lateinit var mNavController: NavController
    var mAdapter: ProductListAdapter? = null
    var typedText = ""
    private lateinit var binding : FragmentSearchBinding
    var page = 1
    private var isLoadMore = true
    var currentModel: ProductModel? = null
    var currentPos = -1
    var setfavValue = 0

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory
    @Inject
    lateinit var myPref: PreferencesManager

    val tagsList = ArrayList<String>()

    private fun makeReq() {
        viewModel.getList(
            "",
            "",
            page,
            typedText,
            android_device_id
        )
    }

    private fun consumeResponse(res: ApiResource<ApiResponse<ProductApiModel>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            if (page == 1) {
                                mAdapter?.clearAll()
                            }
                            mAdapter?.setItems(res.data.data.products)
                            isLoadMore = res.data.data.products.isNotEmpty()
                            updateAdapter()
                        } else {
                            CustomDialog.show(requireActivity(),res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(),res.message,AlertType.SERVER_ERROR)
                }
            }
        }
    }
    var android_device_id = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        android_device_id =
            Settings.Secure.getString(requireActivity().contentResolver, Settings.Secure.ANDROID_ID)
        mNavController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this, providersFactory).get(SearchViewModel::class.java)
        binding = FragmentSearchBinding.bind(view)
        binding.ab.tvABTitle.text = getString(R.string.title_search)
        binding.recyclerView.setGone()
        RxTextView.textChanges(binding.edSearch)
            .debounce(900, TimeUnit.MILLISECONDS)
            .subscribe { textChanged: CharSequence? ->
                try {
                    requireActivity().runOnUiThread {
                        typedText = binding.edSearch.text.toString().trim()
                        mAdapter?.clearAll()
                        mAdapter?.notifyDataSetChanged()
                        if (typedText.isNotEmpty() && typedText.length >= 2) {
                            page = 1
                            makeReq()
                        } else {
                            binding.tvEmptyMsg.setGone()
                            updateTagListAdapter()
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }


      /*  binding.edSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                callNewSearch()
                return@OnEditorActionListener true
            }
            false
        })
*/
        //binding.ab.abParent.setBackground(null)
        binding.ab.ivBack.setOnClickListener(View.OnClickListener {
            mNavController.navigateUp()
        })
        binding.ab.ivSearch.setGone()
        binding.ab.iconCart.setOnClickListener(View.OnClickListener {
            mNavController.navigate(R.id.action_navigation_search_to_cartListFragment)
        })
        initRecyclerView()
        viewModel.getApiResponse().observe(viewLifecycleOwner) { res -> consumeResponse(res) }
        viewModel.getFavResponse().observe(viewLifecycleOwner) { res -> consumeFavResponse(res) }

        //binding.taglayout.backgroundColor = Color.WHITE
        binding.taglayout.tagTextColor = resources.getColor(R.color.search_tag_color)
        binding.taglayout.tagBackgroundColor = Color.WHITE
        binding.taglayout.tagBorderColor = Color.GRAY

        getSearchHistory()

        binding.taglayout.setOnTagClickListener(object : OnTagClickListener {
            override fun onTagClick(position: Int, text: String) {
                binding.edSearch.setText(text)
                //callNewSearch()
            }

            override fun onTagLongClick(position: Int, text: String) {
                // ...
            }

            override fun onSelectedTagDrag(position: Int, text: String) {
                // ...
            }

            override fun onTagCrossClick(position: Int) {
                // ...
            }
        })
    }

    private fun updateAdapter(){
        binding.tvEmptyMsg.setGone()
        if(mAdapter?.itemCount == 0 ){
            binding.searchHistoryLayout.visibility = View.VISIBLE
            if(binding.edSearch.text.toString().isNotEmpty()){
                binding.tvEmptyMsg.setVisible()
                binding.searchHistoryLayout.visibility = View.GONE
            }else{
                binding.searchHistoryLayout.visibility = View.VISIBLE
            }
        }
        else{
            binding.recyclerView.setVisible()
            binding.searchHistoryLayout.visibility = View.GONE
        }

    }

    private fun initRecyclerView() {
        mAdapter = ProductListAdapter(
            this,
            requireActivity() as MainActivity, myPref
        )
        binding.recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = mAdapter
        val loadMoreNestedScrollView = LoadMoreNestedScrollView(binding.recyclerView.layoutManager as LinearLayoutManager)
        loadMoreNestedScrollView.setOnLoadMoreListener {
            if (isLoadMore) {
                page = page.plus(1)
                makeReq()
            }
        }
        binding.nestedScrollView.setOnScrollChangeListener(loadMoreNestedScrollView)
    }

    fun openDetailsScreen(model: ProductModel){
        val bundle = Bundle()
        bundle.putString("name", model.name_en)
        bundle.putString("group_id", model.group_id)
        bundle.putString("warehouse_product_id", model.warehouse_product_id)
        mNavController.navigate(R.id.action_navigation_search_to_productDetailFragment, bundle)
    }


    fun doAddToCart(model: ProductModel, isAdd: Boolean, isBulkUpdate: Boolean, selectedQty: Int) {
        CartUtils.addToCartProductList(requireActivity() as MainActivity,myPref, model, isAdd, isBulkUpdate, selectedQty)
        mAdapter?.notifyDataSetChanged()
        updateCartCountUI()
    }

    private fun getSearchHistory() {
        binding.searchHistoryLayout.setGone()
        viewModel.searchHistoryRes(
            requireActivity(),
            android_device_id
        )?.observe(viewLifecycleOwner, object : Observer<ApiResponse<List<SearchHistoryModel>>?> {
            override fun onChanged(res: ApiResponse<List<SearchHistoryModel>>?) {
                if (res != null) {
                    if (res.http_code == 200) {
                        if (res.data != null) {
                            tagsList.clear()
                            res.data.forEach {
                                tagsList.add(it.search_text)
                            }
                            binding.taglayout.tags = tagsList
                            updateTagListAdapter()
                        }
                    } else {
                        CustomDialog.show(requireActivity(),res.message,AlertType.ERROR)
                    }
                }
            }
        })
    }

    private fun updateTagListAdapter() {
        if(tagsList.size>0){
            binding.searchHistoryLayout.setVisible()
        }
        else{
            binding.searchHistoryLayout.setGone()
        }
    }

    fun getProductInfoByVariantId(pos: Int, model: ProductModel, warehouseProductId: String) {
        viewModel.getProductInfoRes(
            requireActivity(),
            warehouseProductId
        )?.observe(viewLifecycleOwner, object : Observer<ApiResponse<ProductModel>?> {
            override fun onChanged(res: ApiResponse<ProductModel>?) {
                if (res != null) {
                    if (res.http_code === 200) {
                        if (res.data != null) {
                            val item: ProductModel? = res.data
                            if (item != null) {
                                item.group_variants = model.group_variants
                                mAdapter?.updateItem(pos, item)
                            }
                            mAdapter?.notifyDataSetChanged()
                        }
                    } else {
                        CustomDialog.show(requireActivity(),res.message,AlertType.ERROR)
                    }
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        updateCartCountUI()
    }


    fun setAsFavouriteProduct(pos: Int, item: ProductModel) {
        currentModel = item
        currentPos = pos
        setfavValue = if (item.is_favourite == 1) 0 else 1
        viewModel.setAsFav(
            item.warehouse_product_id,
            item.product_id,
            setfavValue,
            item.group_id
        )
    }


    private fun consumeFavResponse(res: ApiResource<ApiResponse<SetFavModel>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            if (currentModel != null && currentPos != -1) {
                                currentModel!!.is_favourite = setfavValue
                                mAdapter?.updateItem(currentPos, currentModel!!)
                                (activity as MainActivity?)?.setFavouriteCount(res.data.data.total_fav_count)
                            }
                        } else {
                            CustomDialog.show(requireActivity(),res.data.message,AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(),res.message,AlertType.SERVER_ERROR)
                }
            }
        }
    }

    private fun updateCartCountUI() {
        binding.ab.tvCartCount.text = myPref.getCartCount().toString()
        when {
            myPref.getCartCount()>0 -> {
                binding.ab.tvCartCount.visibility = View.VISIBLE
            }
            else -> {
                binding.ab.tvCartCount.visibility = View.GONE
            }
        }
    }

}