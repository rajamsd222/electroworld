package com.electroworld.android.ui.main.orders.returnorder

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Paint
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.databinding.FragmentReturnOrderBinding
import com.electroworld.android.databinding.RowGridReturnImageBinding
import com.electroworld.android.databinding.RowOrderDetailItemsBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.ImagesModel
import com.electroworld.android.model.UserModel
import com.electroworld.android.model.orderlist.OrderListProductItem
import com.electroworld.android.ui.main.orders.detail.OrderDetailFragment
import com.electroworld.android.ui.main.orders.detail.OrderDetailFragment.Companion.ARG_DATA
import com.electroworld.android.utils.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import java.io.File
import java.lang.reflect.Type
import javax.inject.Inject


class ReturnOrderFragment : DaggerFragment() {

    private lateinit var viewModel: ReturnOrderViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentReturnOrderBinding

    var mAdapterOrderDetail: GenericAdapter<OrderListProductItem, RowOrderDetailItemsBinding>? =
        null
    private var orderId: String? = null
    var list: MutableList<OrderListProductItem>? = ArrayList()
    var listTemp: List<OrderListProductItem>? = ArrayList()
    val imageList: MutableList<ImagesModel> = ArrayList()
    var mGridAdapter: GenericAdapter<ImagesModel, RowGridReturnImageBinding>? = null
    var maxImageCount = 0

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    private val REQUEST_CAMERA = 0
    private val SELECT_FILE = 1
    private val ALL_PERMISSIONS_RESULT = 107
    val MY_PERMISSIONS_REQUEST_CAMERA = 321
    val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123
    val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1234
    private val permissionsToRequest: ArrayList<String>? = null
    private val permissionsRejected = ArrayList<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_return_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(ReturnOrderViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentReturnOrderBinding.bind(view)
        val listStr = arguments?.getString(ARG_DATA)
        orderId = arguments?.getString(OrderDetailFragment.ARG_ORDER_ID)
        binding.ab.tvABTitle.text = getString(R.string.return_order) + " #" + orderId
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        if (listStr?.isNotEmpty() == true) {
            val listUserType: Type = object : TypeToken<List<OrderListProductItem?>?>() {}.getType()
            listTemp = Gson().fromJson(listStr, listUserType)
            listTemp?.forEach {
                if (it.is_return_order == 0 && it.is_return_confirmed == 0) {
                    list?.add(it)
                }
            }
            maxImageCount = list?.size!!
        }
        initRecyclerView()
        initImageRecyclerView()
        binding.tvBrowse.setOnClickListener {
            doActionImageViewClick()
        }

        binding.tvReturnOrder.setOnClickListener {
            val comments = binding.edMessage.text.toString()
            if (comments.isEmpty()) {
                UiUtils.showInputAlert(
                    activity, getString(R.string.error_enter_comments),
                    binding.edMessage
                )
            } else {
                var pIds = getSelectedProductIds()
                pIds = pIds.substring(1, pIds.length - 1)
                if (pIds.isEmpty()) {
                    UiUtils.showInputAlert(
                        activity, getString(R.string.error_product_not_selected),
                        binding.edMessage
                    )
                } else {
                    makeReturnOrderReq(comments, pIds)
                }

            }

        }
    }

    private fun makeReturnOrderReq(
        comments: String,
        pIds: String
    ) {
        viewModel.makeReturnOrderRes(
            activity,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            orderId,
            pIds,
            comments,
            imageList.size.toString(),
            imageList
        )!!.observe(viewLifecycleOwner,
            fun(res: ApiResponse<UserModel>?) {
                if (res != null) {
                    if (res.http_code == 200) {
                        Utils.showApiSuccess(activity, res.message)
                        mNavController.popBackStack()
                    } else {
                        CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                    }
                }
            })
    }

    private fun getSelectedProductIds(): String {
        var ids: MutableList<String> = ArrayList()
        list?.forEachIndexed { index, orderListProductItem ->
            if (orderListProductItem.isSelect) {
                ids.add(orderListProductItem.id)
            }
        }
        return ids.toString()
    }

    private fun doActionImageViewClick() {
        if (Utils.checkPermission(activity)) {
            openAlertPicker()
        }
    }

    private fun initImageRecyclerView() {
        mGridAdapter = object :
            GenericAdapter<ImagesModel, RowGridReturnImageBinding>(R.layout.row_grid_return_image) {
            override fun onBindData(
                model: ImagesModel,
                position: Int,
                dataBinding: RowGridReturnImageBinding
            ) {
                dataBinding.item = model
                Glide.with(requireActivity())
                    .load(Uri.fromFile(File(model.image)))
                    .into(dataBinding.ivProduct)
                dataBinding.tvRemove.setOnClickListener {
                    mGridAdapter?.removeItem(model)
                    updateGridAdapter()
                }
            }

            override fun onItemClick(model: ImagesModel?, position: Int) {

            }

        }
        binding.recyclerViewGrid.apply {
            adapter = mGridAdapter
        }
        binding.recyclerViewGrid.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

    }

    private fun initRecyclerView() {
        mAdapterOrderDetail = object :
            GenericAdapter<OrderListProductItem, RowOrderDetailItemsBinding>(R.layout.row_order_detail_items) {
            override fun onBindData(
                model: OrderListProductItem,
                position: Int,
                dataBinding: RowOrderDetailItemsBinding
            ) {
                dataBinding.item = model
                dataBinding.tvMRP.paintFlags =
                    dataBinding.tvMRP.paintFlags.or(Paint.STRIKE_THRU_TEXT_FLAG)
                dataBinding.tvComments.setGone()
                dataBinding.ratingBar.setGone()
                dataBinding.tvAddRating.setGone()
                //dataBinding.chk.setVisible()
                dataBinding.chk.visibility =
                    if (model.quantity == 0) View.GONE else View.VISIBLE
                dataBinding.chk.isChecked = model.isSelect
                dataBinding.chk.setOnClickListener {
                    model.isSelect = dataBinding.chk.isChecked
                    // notifyDataSetChanged()
                }
            }

            override fun onItemClick(model: OrderListProductItem?, position: Int) {
                model?.isSelect = !model?.isSelect!!
                notifyDataSetChanged()
            }
        }
        binding.recyclerView.apply {
            adapter = mAdapterOrderDetail
        }
        mAdapterOrderDetail?.clearAll()
        mAdapterOrderDetail?.addItems(list)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var bm: Bitmap?
        var userImagePath = ""
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                val picUri = data?.data
                userImagePath = AppValidator.toStr(RealPathUtils.getImagePath(activity, data))
            } else if (requestCode == REQUEST_CAMERA) {
                if (data != null) {
                    bm = data.extras!!["data"] as Bitmap?
                    userImagePath = ImageUtils.saveImageFromBitmapDynamic(activity, bm)
                }
            }
            if (userImagePath.isNotEmpty()) {
                imageList.add(ImagesModel(userImagePath, "", ""))
            }
            updateGridAdapter()
        }
    }

    private fun updateGridAdapter() {
        mGridAdapter?.setItems(imageList)
        if (imageList.size >= maxImageCount) {
            binding.cvChooseImageLayout.setGone()
        } else {
            binding.cvChooseImageLayout.setVisible()
        }
    }


    @JvmName("checkPermission1")
    @TargetApi(Build.VERSION_CODES.M)
    fun onRequestPermissionsResult(
        requestCode: Int,
        grantResults: IntArray
    ) {
        when (requestCode) {
            ALL_PERMISSIONS_RESULT -> {
                for (perms in permissionsToRequest!!) {
                    if (hasPermission(perms)) {
                    } else {
                        permissionsRejected.add(perms)
                    }
                }
                if (permissionsRejected.size > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected[0])) {
                            showMessageOKCancel(
                                getString(R.string.These_permissions_are_mandatory_for_the_application_Please_allow_access)
                            ) { dialog: DialogInterface?, which: Int ->
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(
                                        permissionsRejected.toArray(
                                            arrayOfNulls(
                                                permissionsRejected.size
                                            )
                                        ), ALL_PERMISSIONS_RESULT
                                    )
                                }
                            }
                            return
                        }
                    }
                }
            }
            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doActionImageViewClick()
            }
        }
    }

    private fun hasPermission(permission: String): Boolean {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return ContextCompat.checkSelfPermission(
                    requireActivity(),
                    permission
                ) == PackageManager.PERMISSION_GRANTED
            }
        }
        return true
    }

    private fun canMakeSmores(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(requireActivity())
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", null)
            .create()
            .show()
    }

    private fun openAlertPicker() {
        val cameraresult =
            checkPermission(Manifest.permission.CAMERA, MY_PERMISSIONS_REQUEST_CAMERA)
        val storageresult = checkPermission(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
        )
        val writeStorage = checkPermission(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
        )
        if (cameraresult && storageresult && writeStorage) selectImage()
    }

    private fun selectImage() {
        val items = arrayOf<CharSequence>(
            resources.getString(R.string.title_take_photo),
            resources.getString(R.string.title_pick_from_gallery),
            resources.getString(R.string.title_cancel)
        )
        val builder = AlertDialog.Builder(
            requireActivity()
        )
        builder.setTitle(resources.getString(R.string.title_image_picker))
        builder.setItems(
            items
        ) { dialog: DialogInterface, item: Int ->
            if (item == 0) {
                cameraIntent()
            } else if (item == 1) {
                galleryIntent()
            } else if (item == 2) {
                dialog.dismiss()
            }
        }
        builder.show()
    }


    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT //
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE)
    }

    private fun cameraIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUEST_CAMERA)
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    fun checkPermission(permissionName: String, requestCode: Int): Boolean {
        val permission = ContextCompat.checkSelfPermission(requireActivity(), permissionName)
        val listPermissionsNeeded: MutableList<String> = ArrayList()
        if (permission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(permissionName)
        }
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(
                    (requireActivity() as Activity?)!!, listPermissionsNeeded
                        .toTypedArray(),
                    requestCode
                )
                false
            } else {
                true
            }
        } else {
            true
        }
    }
}