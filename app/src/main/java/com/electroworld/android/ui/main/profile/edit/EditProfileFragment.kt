package com.electroworld.android.ui.main.profile.edit

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.bind.AppBinder
import com.electroworld.android.databinding.FragmentEditProfileBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.UserModel
import com.electroworld.android.utils.*
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import java.io.File
import java.util.*
import javax.inject.Inject

class EditProfileFragment : DaggerFragment() {

    companion object {
        fun newInstance() = EditProfileFragment()
    }

    private lateinit var viewModel: EditProfileViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding : FragmentEditProfileBinding

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory
    @Inject
    lateinit var myPref: PreferencesManager
/*
    @Inject
    var requestManager: RequestManager? = null*/
    private var uploadedFile: File? = null

    private val REQUEST_CAMERA = 0
    private val SELECT_FILE= 1
    private val ALL_PERMISSIONS_RESULT = 107
    val MY_PERMISSIONS_REQUEST_CAMERA = 321
    val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123
    val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1234
    private val permissionsToRequest: ArrayList<String>? = null
    private val permissionsRejected = ArrayList<String>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mNavController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this, providersFactory).get(EditProfileViewModel::class.java)
        binding = FragmentEditProfileBinding.bind(view)
        updateProfileInfo(myPref.getPrefKeyUserModel())
        binding.ab.tvABTitle.text = getString(R.string.title_edit_profile)
        binding.ab.ivBack.setOnClickListener(View.OnClickListener {
            mNavController.navigateUp()
        })
        binding.editProfileImageView.setOnClickListener(View.OnClickListener {
            doActionImageViewClick()
        })
        binding.tvBrowse.setOnClickListener(View.OnClickListener {
            doActionImageViewClick()
        })
        binding.btnUpdate.setOnClickListener {
            val firstName = binding.edFirstName.text.toString().trim()
            val lastName = binding.edLastName.text.toString().trim()
            val email = binding.edEmail.text.toString().trim()
            val gstnumber = binding.edGSTNumber.text.toString().trim()
            val companyname = binding.edCompanyName.text.toString().trim()
            if (firstName.isEmpty()) {
                UiUtils.showInputAlert(
                    activity, getString(R.string.error_enter_first_name),
                    binding.edFirstName
                )
            } else if (lastName.isEmpty()) {
                UiUtils.showInputAlert(
                    activity, getString(R.string.error_enter_last_name),
                    binding.edLastName
                )
            } else if (email.isNotEmpty() && !AppUtils.isValidEmail(email)) {
                UiUtils.showInputAlert(
                    activity, getString(R.string.invalid_email_id),
                    binding.edEmail
                )
            } else {
                doUpdateProfile(
                    uploadedFile,
                    firstName,
                    lastName,
                    email,
                    gstnumber,
                    companyname
                )
            }

        }

    }

    private fun doUpdateProfile(
        uploadedFile: File?,
        first_name: String,
        last_name: String,
        email_id: String,
        gstnumber: String,
        companyname: String
    ) {
        viewModel.updateProfileRes(
            activity, uploadedFile, myPref.getPrefKeyAccessToken(), myPref.getPrefKeyCustomerId(), first_name, last_name,
            email_id,gstnumber,companyname, myPref.getPrefKeyVendorId(), myPref.getPrefKeyLanguage()
        )!!.observe(viewLifecycleOwner,
            fun(res: ApiResponse<UserModel>?) {
                if (res != null) {
                    if (res.http_code == 200) {
                        if (res.data != null) {
                            myPref.setPrefKeyUserModel(res.data)
                            Utils.showApiSuccess(activity, res.message)
                            updateProfileInfo(myPref.getPrefKeyUserModel())
                            mNavController.navigateUp()
                        }
                    } else {
                        CustomDialog.show(requireActivity(),res.message, AlertType.ERROR)
                    }
                }
            })
    }
    private fun updateProfileInfo(model: UserModel?) {
        if (model != null) {
            binding.item = model
            binding.edEmail.setText(model.email)
            binding.edGSTNumber.setText(myPref.getGSTNumber())
            binding.edCompanyName.setText(myPref.getCompanyName())
            AppBinder.setProfileImage(binding.editProfileImageView,model.profile_image)
            if(TextUtils.isEmpty(model.email)){
                binding.emailLayout.setVisible()
            }
            else{
                binding.emailLayout.setGone()
            }
            binding.executePendingBindings();
        }
    }


    private fun doActionImageViewClick() {
        if (Utils.checkPermission(activity)) {
            openAlertPicker()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var bm: Bitmap?
        var userImagePath = ""
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                val picUri = data?.data
                userImagePath = AppValidator.toStr(RealPathUtils.getImagePath(activity, data))
                binding.editProfileImageView.setImageURI(picUri)
            } else if (requestCode == REQUEST_CAMERA) {
                if (data != null) {
                    bm = data.extras!!["data"] as Bitmap?
                    userImagePath = ImageUtils.saveImageFromBitmap(activity, bm)
                    binding.editProfileImageView.setImageBitmap(bm)
                }
            }
            if (userImagePath.length > 0) {
                binding.tvPath.setText(userImagePath)
                uploadedFile = File(userImagePath)
            }
        }
    }


    @JvmName("checkPermission1")
    @TargetApi(Build.VERSION_CODES.M)
    fun onRequestPermissionsResult(
        requestCode: Int,
        grantResults: IntArray
    ) {
        when (requestCode) {
            ALL_PERMISSIONS_RESULT -> {
                for (perms in permissionsToRequest!!) {
                    if (hasPermission(perms)) {
                    } else {
                        permissionsRejected.add(perms)
                    }
                }
                if (permissionsRejected.size > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected[0])) {
                            showMessageOKCancel(
                                getString(R.string.These_permissions_are_mandatory_for_the_application_Please_allow_access)
                            ) { dialog: DialogInterface?, which: Int ->
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(
                                        permissionsRejected.toArray(
                                            arrayOfNulls(
                                                permissionsRejected.size
                                            )
                                        ), ALL_PERMISSIONS_RESULT
                                    )
                                }
                            }
                            return
                        }
                    }
                }
            }
            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doActionImageViewClick()
            }
        }
    }

    private fun hasPermission(permission: String): Boolean {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return ContextCompat.checkSelfPermission(
                    requireActivity(),
                    permission
                ) == PackageManager.PERMISSION_GRANTED
            }
        }
        return true
    }

    private fun canMakeSmores(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(requireActivity())
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", null)
            .create()
            .show()
    }

    private fun openAlertPicker() {
        val cameraresult =
            checkPermission(activity, Manifest.permission.CAMERA, MY_PERMISSIONS_REQUEST_CAMERA)
        val storageresult = checkPermission(
            activity,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
        )
        val writeStorage = checkPermission(
            activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
        )
        if (cameraresult && storageresult && writeStorage) selectImage()
    }

    private fun selectImage() {
        val items = arrayOf<CharSequence>(
            resources.getString(R.string.title_take_photo),
            resources.getString(R.string.title_pick_from_gallery),
            resources.getString(R.string.title_cancel)
        )
        val builder = AlertDialog.Builder(
            requireActivity()
        )
        builder.setTitle(resources.getString(R.string.title_image_picker))
        builder.setItems(
            items
        ) { dialog: DialogInterface, item: Int ->
            if (item == 0) {
                cameraIntent()
            } else if (item == 1) {
                galleryIntent()
            } else if (item == 2) {
                dialog.dismiss()
            }
        }
        builder.show()
    }


    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT //
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE)
    }

    private fun cameraIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUEST_CAMERA)
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    fun checkPermission(context: Context?, permissionName: String, requestCode: Int): Boolean {
        val permission = ContextCompat.checkSelfPermission(requireContext(), permissionName)
        val listPermissionsNeeded: MutableList<String> = ArrayList()
        if (permission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(permissionName)
        }
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(
                    (context as Activity?)!!, listPermissionsNeeded
                        .toTypedArray(),
                    requestCode
                )
                false
            } else {
                true
            }
        } else {
            true
        }
    }
}