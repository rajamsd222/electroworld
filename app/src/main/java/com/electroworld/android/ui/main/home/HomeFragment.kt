package com.electroworld.android.ui.main.home

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.electroworld.android.R
import com.electroworld.android.alert.AlertMakeOrderAsCOD
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.AlertViewSimple
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.databinding.FragmentHomeBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.*
import com.electroworld.android.ui.auth.AuthActivity
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartRepo
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.cashfree.CashFreePaymentFragment
import com.electroworld.android.ui.main.home.adapter.HomeOffersAdapter
import com.electroworld.android.ui.main.home.adapter.ImagesAdapter
import com.electroworld.android.ui.main.products.ProductsGridAdapter
import com.electroworld.android.ui.main.products.list.ProductListFragment
import com.electroworld.android.utils.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.make.dots.dotsindicator.DotsIndicator
import dagger.android.support.DaggerFragment
import java.util.*
import javax.inject.Inject


class HomeFragment : DaggerFragment(), ProductsGridAdapter.ProductInterface {

    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding
    private lateinit var mNavController: NavController
    var mGridAdapter: ProductsGridAdapter? = null

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    var comesFromOnCreate = false
    var isLocationFetched = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        comesFromOnCreate = true;
    }

    lateinit var bottomSheetDialog: BottomSheetDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    var width = 0
    var height = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        width = displayMetrics.widthPixels
        height = displayMetrics.heightPixels
        // RatingDialogFragment().show(requireActivity().getSupportFragmentManager(),"DialogFragment")
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(
            mMessageReceiver,
            IntentFilter("location_update")
        )
        mNavController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this, providersFactory).get(HomeViewModel::class.java)
        binding = FragmentHomeBinding.bind(view)


        binding.ab.tvLocation.setGone()
        binding.ab.iconLocation.setGone()
        binding.ab.tvLbl.setGone()

        binding.ab.tvLocation.setOnClickListener {
            openAddressScreen()
        }
        binding.ab.tvLbl.setOnClickListener {
            openAddressScreen()
        }
        binding.ab.ivLeftmenu.setOnClickListener {
            if (!myPref.isLogin()) {
                startActivity(Intent(requireActivity(), AuthActivity::class.java))
            } else {
                mNavController.navigate(R.id.action_navigation_home_to_profileFragment)
            }

        }

        binding.ab.iconCart.setOnClickListener {
            mNavController.navigate(R.id.action_navigation_home_to_cartListFragment)
        }
        binding.tvSearch.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("name", "")
            mNavController.navigate(R.id.action_navigation_home_to_navigation_search, bundle)
        }


        if ((requireActivity() as MainActivity).warehouse_product_id != null && (requireActivity() as MainActivity).warehouse_product_id.length > 0) {
            Handler(Looper.getMainLooper()).postDelayed({
                openDetailsScreen(
                    (requireActivity() as MainActivity).warehouse_product_id,
                    (requireActivity() as MainActivity).name,
                    (requireActivity() as MainActivity).group_id
                )
                (requireActivity() as MainActivity).warehouse_product_id = ""
            }, 500)
        }

        (requireActivity() as MainActivity).setDefaultLocation()


    }

    private fun showEditProfileDialog() {
        val avcDialog =
            AlertViewSimple(requireActivity(), object : AlertViewSimple.OKClickListener {
                override fun onOKClick(view: View?) {
                    mNavController.navigate(R.id.editProfileFragment)
                }
            })
        avcDialog.setCancelable(true)
        avcDialog.setDialogTitle("Alert")
        avcDialog.setDialogMessage("Please fill your profile information to continue")
        avcDialog.setPositiveText("Go to Edit Profile")
        avcDialog.setNegativeText(getString(R.string.btn_cancel))
        avcDialog.setShowNegativeButton(true)
        avcDialog.showDialog()
    }


    private fun showPaymentFailedDialog() {
        AlertMakeOrderAsCOD.showDialog(requireActivity(), "retry", false,
            object : AlertMakeOrderAsCOD.OrderSwitchClickListener {
                override fun onClick(view: View?, tag: String) {
                    if (tag == "order_detail") {
                        ToastUtil.show(activity, "order_detail")
                    } else if (tag == "cod") {
                        ToastUtil.show(activity, "cod")
                    }
                }
            })
    }

    private fun openCashFreeScreen(token: String, amount: String, orderId: String) {
        val bundle = CashFreePaymentFragment.updateBundle(
            token,
            amount,
            orderId,
            "",
            PaymentType.DEFAULT,
            ScreenType.HOME
        )
        mNavController.navigate(R.id.action_navigation_home_to_cashFreePaymentFragment, bundle)
    }

    private fun openAddressScreen() {
        mNavController.navigate(R.id.action_navigation_home_to_addressListFragment)
    }


    private fun showCarousel(
        contentLayout: LinearLayout,
        placeholders: List<Placeholder>,
        homeModel: HomeModel
    ) {
        val imagesList: ArrayList<ImagesModel> = ArrayList()
        placeholders.forEach {
            imagesList.add(ImagesModel(it.image, homeModel.title, it.id))
        }
        if (imagesList.size > 0) {
            val itemView = LayoutInflater.from(requireActivity())
                .inflate(R.layout.row_home_carousel, contentLayout, false)
            val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
            val imageViewPager = itemView.findViewById<ViewPager>(R.id.imageViewPager)
            val imageFrame = itemView.findViewById<RelativeLayout>(R.id.imageFrame);
            val dotsIndicator = itemView.findViewById<DotsIndicator>(R.id.dotsIndicator)
            val dotsFrame = itemView.findViewById<FrameLayout>(R.id.dotsFrame);
            val bannerFullView = itemView.findViewById<RelativeLayout>(R.id.bannerFullView)
            imageViewPager.adapter = ImagesAdapter(imagesList, mNavController, homeModel.type,ScreenType.HOME)
            dotsIndicator.setViewPager(imageViewPager)
            imageViewPager.adapter?.registerDataSetObserver(dotsIndicator.dataSetObserver)
            if (imagesList.size == 1) {
                dotsIndicator.visibility = View.GONE
            }
            tvTitle.setGone()
            tvTitle.setText(homeModel.title)
            var bannerHeight = (height * 0.21).toInt()
            if (homeModel.name == "EML1") {
                imageFrame.setBackgroundColor(Color.WHITE);
                val param = imageViewPager.layoutParams as ViewGroup.MarginLayoutParams
                param.setMargins(15, 15, 15, 15)
                imageViewPager.layoutParams = param
                bannerHeight = (height * 0.2).toInt()
            } else if (homeModel.name == "EML2") {
                dotsFrame.setGone()
                imageFrame.setBackgroundColor(Color.WHITE);
                val param = imageViewPager.layoutParams as ViewGroup.MarginLayoutParams
                param.setMargins(10, 10, 10, 10)
                imageViewPager.layoutParams = param
                bannerHeight = (height * 0.07).toInt()
            } else if (homeModel.name == "EML3") {
                dotsFrame.setBackgroundResource(0)
                imageFrame.setBackgroundColor(Color.WHITE);
                tvTitle.setVisible()
                bannerHeight = (height * 0.25).toInt()
            } else if (homeModel.name == "EML4") {
                dotsFrame.setBackgroundResource(0)
                bannerHeight = (height * 0.18).toInt()
            } else if (homeModel.name == "EML6") {
                dotsFrame.setBackgroundResource(0)
                bannerHeight = (height * 0.25).toInt()
            }
            val params = bannerFullView.layoutParams
            params.height = bannerHeight
            bannerFullView.layoutParams = params

            contentLayout.addView(itemView)
        }


    }

    private fun showOfferZone(
        contentLayout: LinearLayout,
        placeholders: List<Placeholder>,
        homeModel: HomeModel
    ) {
        val imagesList: ArrayList<ImagesModel> = ArrayList()
        placeholders.forEach {
            imagesList.add(ImagesModel(it.image, homeModel.title, it.id))
        }
        if (imagesList.size > 0) {
            val itemView = LayoutInflater.from(requireActivity())
                .inflate(R.layout.row_home_offer, contentLayout, false)
            val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
            val imageViewPager = itemView.findViewById<ViewPager>(R.id.imageViewPager)
            val imageFrame = itemView.findViewById<LinearLayout>(R.id.imageFrame);
            val dotsIndicator = itemView.findViewById<DotsIndicator>(R.id.dotsIndicator)
            val bannerFullView = itemView.findViewById<RelativeLayout>(R.id.bannerFullView)
            imageViewPager.adapter = ImagesAdapter(imagesList, mNavController, homeModel.type,ScreenType.HOME)
            dotsIndicator.setViewPager(imageViewPager)
            imageViewPager.adapter?.registerDataSetObserver(dotsIndicator.dataSetObserver)
            if (imagesList.size == 1) {
                dotsIndicator.visibility = View.GONE
            }
            tvTitle.setGone()
            if (homeModel.name == "EML3") {
                tvTitle.setText(homeModel.title)
                imageFrame.setBackgroundColor(Color.WHITE);
                tvTitle.setVisible()
            }
            var bannerHeight: Int = (height * 0.3).toInt()
            val params = bannerFullView.layoutParams
            params.height = bannerHeight
            bannerFullView.layoutParams = params

            contentLayout.addView(itemView)
        }


    }


    private fun showProducts(
        contentLayout: LinearLayout,
        placeholders: List<Placeholder>,
        homeModel: HomeModel
    ) {
        placeholders.forEach {
            val itemView = LayoutInflater.from(requireActivity())
                .inflate(R.layout.row_home_products, contentLayout, false)
            val parentView = itemView.findViewById<RelativeLayout>(R.id.parentView)
            val titlelayout = itemView.findViewById<RelativeLayout>(R.id.titlelayout)
            val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
            val ivRight = itemView.findViewById<ImageView>(R.id.ivRight)
            val tvViewMore = itemView.findViewById<TextView>(R.id.tvViewMore)
            val recyclerView = itemView.findViewById<RecyclerView>(R.id.recycler_view)
            tvTitle.setText(homeModel.title)
            tvViewMore.setTag(it)
            ivRight.setTag(it)
            if (homeModel.name.equals("EML5")) {
                mGridAdapter = ProductsGridAdapter(
                    this,
                    requireActivity() as MainActivity,
                    myPref,
                    ScreenType.CART,
                    this,
                    homeModel.name
                )
                if (TextUtils.isEmpty(homeModel.image)) {
                    ivRight.setGone()
                } else {
                    Glide.with(requireActivity())
                        .load(homeModel.image)
                        .into(ivRight)
                }
            } else {
                mGridAdapter = ProductsGridAdapter(
                    this,
                    requireActivity() as MainActivity,
                    myPref,
                    ScreenType.HOME,
                    this,
                    ""
                )
            }

            recyclerView.layoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            recyclerView.adapter = mGridAdapter
            mGridAdapter?.setItems(it.products)
            mGridAdapter?.notifyDataSetChanged()

            if (homeModel.name.equals("EML5")) {
                ivRight.setVisible()
                titlelayout.setGone()
                parentView.setBackgroundColor(Color.WHITE)

                val param = parentView.layoutParams as ViewGroup.MarginLayoutParams
                param.setMargins(0, 15, 0, 10)
                parentView.layoutParams = param

            } else {
                ivRight.setGone()
                titlelayout.setVisible()
            }
            if (it.products != null && it.products.size > 0) {
                contentLayout.addView(itemView)
            }

            ivRight.setOnClickListener {
                var tag = it.getTag() as Placeholder?
                val bundle = ProductListFragment.getPlaceHolderBundles(tag?.id, homeModel.title)
                mNavController.navigate(R.id.action_navigation_home_to_productListFragment, bundle)
            }
            tvViewMore.setOnClickListener {
                var tag = it.getTag() as Placeholder?
                val bundle = ProductListFragment.getPlaceHolderBundles(tag?.id, homeModel.title)
                mNavController.navigate(R.id.action_navigation_home_to_productListFragment, bundle)
            }
        }

    }

    private fun showCategoryView(
        contentLayout: LinearLayout,
        placeholders: List<Placeholder>,
        homeModel: HomeModel
    ) {
        val itemView = LayoutInflater.from(requireActivity())
            .inflate(R.layout.layer_home_category, contentLayout, false)
        val tvTitle = itemView.findViewById<TextView>(R.id.tvCategoryTitle)
        val recyclerView = itemView.findViewById<RecyclerView>(R.id.rvCategories)
        tvTitle.setText(homeModel.title)
        var mCategoriesAdapter = HomeCategoriesAdapter(
            mNavController,
            placeholders,
            this,
            requireActivity()
        )
        recyclerView.layoutManager = GridLayoutManager(activity, 3)
        recyclerView.adapter = mCategoriesAdapter

        if (placeholders != null && placeholders.size > 0) {
            contentLayout.addView(itemView)
        }
    }

    private fun show50PercentOfferView(
        contentLayout: LinearLayout,
        placeholders: List<Placeholder>,
        homeModel: HomeModel
    ) {
        val itemView = LayoutInflater.from(requireActivity())
            .inflate(R.layout.layer_home_offers, contentLayout, false)
        val iv1 = itemView.findViewById<ImageView>(R.id.iv1)
        val rvOffers = itemView.findViewById<RecyclerView>(R.id.rv)
        val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
        val tvViewMore = itemView.findViewById<TextView>(R.id.tvViewMore)


        tvTitle.text = homeModel.title
        tvViewMore.visibility = View.GONE

        tvViewMore.setOnClickListener {
            var tag = it.getTag() as Placeholder?
            val bundle = ProductListFragment.getPlaceHolderBundles(
                homeModel?.id.toString(),
                homeModel?.title
            )
            mNavController.navigate(R.id.action_navigation_home_to_productListFragment, bundle)
        }

        if (TextUtils.isEmpty(homeModel.image)) {
            iv1.setGone()
        } else {
            Glide.with(requireActivity())
                .load(homeModel.image)
                .into(iv1)
        }

        var mOffersAdapter = HomeOffersAdapter(
            mNavController,
            placeholders,
            this,
            requireActivity()
        )
        rvOffers.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        rvOffers.adapter = mOffersAdapter

        if (placeholders.isNotEmpty()) {
            contentLayout.addView(itemView)
        }

    }

    private fun showFoodRecipeView(
        contentLayout: LinearLayout,
        placeholders: List<Placeholder>,
        homeModel: HomeModel
    ) {
        val itemView = LayoutInflater.from(requireActivity())
            .inflate(R.layout.layer_home_foods, contentLayout, false)
        val tvTitle = itemView.findViewById<TextView>(R.id.tvFoodReceipesTitle)
        val recyclerView = itemView.findViewById<RecyclerView>(R.id.rvFoodReceipes)
        tvTitle.setText(homeModel.title)
        var mFoodRecipeAdapter = FoodRecipeAdapter(
            mNavController,
            placeholders,
            this,
            requireActivity()
        )
        recyclerView.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerView.adapter = mFoodRecipeAdapter

        if (placeholders != null && placeholders.size > 0) {
            contentLayout.addView(itemView)
        }

    }

    private fun showBlogsView(
        contentLayout: LinearLayout,
        placeholders: List<Placeholder>,
        homeModel: HomeModel
    ) {
        val itemView = LayoutInflater.from(requireActivity())
            .inflate(R.layout.layer_home_blogs, contentLayout, false)
        val tvTitle = itemView.findViewById<TextView>(R.id.tvblogTitle)
        val recyclerView = itemView.findViewById<RecyclerView>(R.id.recyclerView)
        tvTitle.setText(homeModel.title)
        var mFoodRecipeAdapter = FoodRecipeAdapter(
            mNavController,
            placeholders,
            this,
            requireActivity()
        )
        recyclerView.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerView.adapter = mFoodRecipeAdapter

        if (placeholders != null && placeholders.size > 0) {
            contentLayout.addView(itemView)
        }

    }

    private fun showBrandsView(
        contentLayout: LinearLayout,
        placeholders: List<Placeholder>,
        homeModel: HomeModel
    ) {
        val itemView = LayoutInflater.from(requireActivity())
            .inflate(R.layout.layer_home_brands, contentLayout, false)
        val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
        val recyclerView = itemView.findViewById<RecyclerView>(R.id.recyclerView)
        tvTitle.setText(homeModel.title)
        var mTopBrandsAdapter = TopBrandsAdapter(
            mNavController, placeholders, this, requireActivity()
        )
        recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = mTopBrandsAdapter

        if (placeholders != null && placeholders.size > 0) {
            contentLayout.addView(itemView)
        }

    }

    fun setAsFavouriteProduct(mAdapter: ProductsGridAdapter?, pos: Int, model: ProductModel) {
        val setfav = if (model.is_favourite == 1) 0 else 1
        viewModel.setFavProductRes(
            requireActivity(),
            model.warehouse_product_id,
            model.product_id,
            setfav,
            model.group_id
        )!!.observe(viewLifecycleOwner,
            androidx.lifecycle.Observer<ApiResponse<SetFavModel>?> { res ->
                if (res != null) {
                    if (res.http_code == 200) {
                        model.is_favourite = setfav
                        mAdapter?.updateItem(pos, model)
                        if (setfav == 1) {
                            Utils.showApiSuccess(activity, "Product added to favourites list")
                        } else if (setfav == 0) {
                            Utils.showApiSuccess(activity, "Product removed from favourites list")
                        }

                    } else {
                        CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                    }
                }
            })
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).refreshAppInfo()
        updateCartCountUI()
        updateAddress(Constants.ADDRESS_NAME)
        if (myPref.getPrefKeyStoreId().isNotEmpty()) {
            if (Constants.COMES_FROM_SPLASH) {
                Constants.COMES_FROM_SPLASH = false
                //showLocationSelectionDialog(true)
            }
            updateAddress(myPref.getHomeAddressTop())
            makeDashBoardListApiReq()
        } else {
            if (Constants.COMES_FROM_SPLASH) {
                Constants.COMES_FROM_SPLASH = false
                //showLocationSelectionDialog(false)
            }
        }

        if (myPref.isLogin() && myPref.getFirstName()?.length == 0) {
            Handler(Looper.getMainLooper()).postDelayed({
                showEditProfileDialog()
            }, 3000)
        }
    }

    public fun updateCartCountUI() {
        binding.ab.tvCartCount.text = myPref.getCartCount().toString()
        when {
            myPref.getCartCount() > 0 -> {
                binding.ab.tvCartCount.visibility = View.VISIBLE
            }
            else -> {
                binding.ab.tvCartCount.visibility = View.GONE
            }
        }
    }

    fun openDetailsScreen(model: ProductModel) {
        val bundle = Bundle()
        bundle.putString("name", model.name_en)
        bundle.putString("group_id", model.group_id)
        bundle.putString("warehouse_product_id", model.warehouse_product_id)
        mNavController.navigate(R.id.productDetailFragment, bundle)
    }

    fun openDetailsScreen(warehouse_product_id: String, name: String, group_id: String) {
        val bundle = Bundle()
        bundle.putString("name", name)
        bundle.putString("group_id", group_id)
        bundle.putString("warehouse_product_id", warehouse_product_id)
        mNavController.navigate(R.id.productDetailFragment, bundle)
    }

    fun doAddToCart(
        adapter: ProductsGridAdapter?,
        model: ProductModel,
        isAdd: Boolean,
        isBulkUpdate: Boolean,
        selectedQty: Int
    ) {
        CartUtils.addToCartProductList(
            requireActivity() as MainActivity,
            myPref,
            model,
            isAdd,
            isBulkUpdate,
            selectedQty
        )
        adapter?.notifyDataSetChanged()
        updateCartCountUI()
    }

    private fun showLocationSelectionDialog(b: Boolean) {
        val dialogView: View = getLayoutInflater().inflate(R.layout.dialog_pick_location, null)
        bottomSheetDialog =
            BottomSheetDialog(requireActivity(), R.style.CustomBottomSheetDialog)
        bottomSheetDialog.setContentView(dialogView)
        val btnLocation = bottomSheetDialog.findViewById<Button>(R.id.btnLocation)
        val btnAddAddress = bottomSheetDialog.findViewById<Button>(R.id.btnAddAddress)
        btnLocation!!.setOnClickListener {
            bottomSheetDialog.dismiss()
            loadDataByCurrentLocation()
        }
        btnAddAddress!!.setOnClickListener {
            bottomSheetDialog.dismiss()
            val bundle = Bundle()
            bundle.putString("screenName", "location_picker")
            mNavController.navigate(R.id.addressListFragment, bundle)
        }
        bottomSheetDialog.show()
    }


    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val isGotLocation = intent.getBooleanExtra(Constants.KEY_LAT_LNG_UPDATED, false)
            if (isGotLocation) {
                (requireActivity() as MainActivity).currentAddressName = Constants.ADDRESS_NAME
                (requireActivity() as MainActivity).currentLatitude = Constants.LATITUDE
                (requireActivity() as MainActivity).currentLongitutde = Constants.LONGITUDE
                if (myPref.getPrefKeyStoreId().isEmpty()) {
                    updateAddress(Constants.ADDRESS_NAME)
                    myPref.setHomeAddressTop(Constants.ADDRESS_NAME)
                    makeStoreInfoReq(Constants.LATITUDE, Constants.LONGITUDE)
                }
                isLocationFetched = true
            }
        }
    }

    private fun updateAddress(name: String) {
        binding.ab.tvLocation.text = name
    }

    private fun makeStoreInfoReq(latitude: Double, longitutde: Double) {
        viewModel.getStoreInfoResponse(
            requireActivity(), latitude,
            longitutde
        )?.observe(viewLifecycleOwner, { res ->
            if (res != null) {
                if (res.http_code == 200) {
                    validateStoreRes(res.data)
                } else if (res.http_code == 400) {
                    //validateStoreRes(res.data)
                    //CustomDialog.show(requireActivity(),"Currently we don't serve at your current location and we’ll soon start serving. In the meanwhile, experience FlipZeeemart!", AlertType.ERROR)
                    CustomDialog.showDialog(requireActivity(),
                        "",
                        res.message,
                        getString(R.string.ok),
                        AlertType.WARNING,
                        object : CustomDialog.OKClickListener {
                            override fun onClick(view: View?) {
                                loadDefaultAddressData()
                            }

                            override fun onContinueShoppingClick(view: View?) {
                                TODO("Not yet implemented")
                            }
                        }
                    )

                } else {
                    CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                }
            }
            updateCartCountUI()
        })
    }

    private fun loadDefaultAddressData() {
        var model = myPref.getDefaultAddress();
        if (model == null) {
            model = AddressListModel(
                "342",
                "Home",
                "",
                "",
                "",
                "",
                "# 700, 24th Main Road, 15th Cross Rd, near Nandini Hotel, 2nd Phase, J. P. Nagar, Bengaluru, Karnataka 560078, India",
                "",
                "",
                12.906343165085982,
                77.5856825709343
            )
        }
        if (model != null) {
            Constants.ADDRESS_NAME = model.address
            Constants.LATITUDE = model.latitude
            Constants.LONGITUDE = model.longitude
            updateAddress(Constants.ADDRESS_NAME)
            myPref.setHomeAddressTop(Constants.ADDRESS_NAME)
            makeStoreInfoReq(Constants.LATITUDE, Constants.LONGITUDE)
        }
    }

    private fun validateStoreRes(data: StoreModel) {
        if (data.id != null && data.id.isNotEmpty()) {
            myPref.setPrefKeyStoreId(data.id)
            myPref.setPrefKeyStoreAddress(data.address)
            myPref.setPrefKeyStoreName(data.name_en)
            makeDashBoardListApiReq()
        } else {
            resetHomeList()
            myPref.resetHomeStoreInfo()
        }
    }

    private fun resetHomeList() {
        Constants.ADDRESS_ID = ""
        binding.contentLayout.removeAllViews()
    }

    override fun onDestroyView() {
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(mMessageReceiver)
        super.onDestroyView()
    }


    private fun makeDashBoardListApiReq() {
        viewModel.getMobileLayerResponse(requireActivity(), "home", 1)
            ?.observe(viewLifecycleOwner, { res ->
                if (res != null) {
                    if (res.http_code == 200) {
                        parseHomeResponse(res.data)
                    } else {
                        //showLocationSelectionDialog(true)
                        CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                    }
                }
            })
    }

    private fun loadDataByCurrentLocation() {
        if (isLocationFetched) {
            Constants.ADDRESS_NAME = (requireActivity() as MainActivity).currentAddressName
            Constants.LATITUDE = (requireActivity() as MainActivity).currentLatitude
            Constants.LONGITUDE = (requireActivity() as MainActivity).currentLongitutde
            updateAddress(Constants.ADDRESS_NAME)
            myPref.setHomeAddressTop(Constants.ADDRESS_NAME)
            makeStoreInfoReq(Constants.LATITUDE, Constants.LONGITUDE)
        } else {
            (requireActivity() as MainActivity).isLocationFetched = false
            (requireActivity() as MainActivity).getDeviceLocation()
        }

    }

    private fun parseHomeResponse(list: List<HomeModel>) {
        binding.contentLayout.removeAllViews()
        list.forEach {

            when {
                it.name == "EML8" -> { // EML8 - CATEGORY
                    showCategoryView(binding.contentLayout, it.placeholders, it)
                }
                it.name == "EML11" -> { // EML11 Brands
                    showBrandsView(binding.contentLayout, it.placeholders, it)
                }
                it.type == "1" -> {// for banners,offer zone, other offers
                    showCarousel(binding.contentLayout, it.placeholders, it)
                }
                it.type == "4" -> {//bank offers
                    showCarousel(binding.contentLayout, it.placeholders, it)
                }
                it.type == "3" -> {// products, hot deal prducts
                    showProducts(binding.contentLayout, it.placeholders, it)
                }
            }


//            if (it.name == "EML1" || it.name == "EML2") {  // FML1 - BANNER
//                showCarousel(binding.contentLayout, it.placeholders, it)
//            } else if (it.name == "EML5" || it.name == "EML7" ||it.name == "EML9" || it.name =="EML10") { // FML2 - HOT DEALS , FML4 - TOP PICK PRODUCTS
//                showProducts(binding.contentLayout, it.placeholders, it)
//            } else if (it.name == "EML3" || it.name == "EML4" || it.name == "EML6") { // FML3 - OFFER ZONE , FML5 - FIRST ORDER OFFER
//                showOfferZone(binding.contentLayout, it.placeholders, it)
//            } else if (it.name == "FML6") { // FML6 - BLOGS
//                showBlogsView(binding.contentLayout, it.placeholders, it)
//            }


//            if (it.name.equals("FML7")) {
//                showCategoryView(binding.contentLayout, it.placeholders, it)
//            } else if (it.name.equals("FML9")) {
//                showFoodRecipeView(binding.contentLayout, it.placeholders, it)
//            } else if (it.name.equals("FML6")) {
//                showBlogsView(binding.contentLayout, it.placeholders, it)
//            } else if (it.name.equals("FML11")) {
//                showBrandsView(binding.contentLayout, it.placeholders, it)
//            } else if (it.name.equals("FML3")) {
//                show50PercentOfferView(binding.contentLayout, it.placeholders, it)
//            } else if (it.type.equals("1") || it.type.equals("2") || it.type.equals("4")) {
//                showCarousel(binding.contentLayout, it.placeholders, it)
//            } else if (it.type.equals("3")) {
//                showProducts(binding.contentLayout, it.placeholders, it)
//            }
        }
    }

    override fun addItem(productModel: ProductModel, quantity: Int) {
        val isAdded = CartRepo.addItemsToCart(productModel, quantity)
        Toast.makeText(
            requireActivity(),
            "" + isAdded + " ->  " + CartRepo.getCart().value?.size,
            Toast.LENGTH_SHORT
        ).show()
        CartRepo.getCart()
    }

    companion object {
        private const val TAG = "HomeFragment"
    }
}