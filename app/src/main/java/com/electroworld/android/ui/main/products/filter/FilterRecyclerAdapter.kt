package com.electroworld.android.ui.main.products.filter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.R
import com.electroworld.android.databinding.FilterGroupRowBinding
import com.electroworld.android.model.filter.FilterData
import com.electroworld.android.model.filter.FilterModel
import com.jakewharton.rxbinding.widget.RxTextView
import java.util.concurrent.TimeUnit


class FilterRecyclerAdapter(val context: Activity) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items = arrayListOf<FilterModel>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: FilterGroupRowBinding =
            FilterGroupRowBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(private val binding: FilterGroupRowBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bind(position: Int) {
            val item: FilterModel = items[position]
            binding.item = item
            binding.executePendingBindings()
            binding.subCategoryLayout.visibility = View.GONE
            if(item.key == "brand_filter"){
                binding.editSearchLayout.setVisible()
            }
            else{
                binding.editSearchLayout.setGone()
            }


            binding.ivClose.setOnClickListener {
                binding.edSearch.setText("")
                filterData(binding.edSearch.text.toString().trim())
            }

            binding.edSearch.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    binding.edSearch.text?.let { binding.edSearch.setSelection(it.length) }
                    binding.ivClose.setVisible()
                }
                else{
                    binding.ivClose.setGone()
                }
            }

            if(item.filter_data.isEmpty()){
                binding.ivArrow.visibility = View.INVISIBLE
            }

            if(item.type.equals("single")){
                val mAdapter = FilterSingleSelectionAdapter(context,item.filter_data)
                binding.rv.apply {
                    adapter = mAdapter
                }
            }
            else{
                val mAdapter = FilterMultiSelectionAdapter(context,item.filter_data)
                binding.rv.apply {
                    adapter = mAdapter
                }
                RxTextView.textChanges(binding.edSearch)
                    .debounce(500, TimeUnit.MILLISECONDS)
                    .subscribe { textChanged: CharSequence? ->
                        context.runOnUiThread {
                            //filterData(binding.edSearch.text.toString().trim())
                            mAdapter.filter.filter(binding.edSearch.text.toString().trim())
                        }
                    }
            }


            binding.subCategoryLayout.setTag(position)
            binding.parentView.setOnClickListener {
                if(item.filter_data.isNotEmpty()){
                    binding.ivArrow.visibility = View.VISIBLE
                    if(binding.subCategoryLayout.visibility == View.VISIBLE){
                        binding.subCategoryLayout.visibility = View.GONE
                        binding.ivArrow.setImageResource(R.mipmap.arrow_filter_right)
                    }
                    else{
                        binding.subCategoryLayout.visibility = View.VISIBLE
                        binding.ivArrow.setImageResource(R.mipmap.arrow_filter_down)
                    }
                }

            }

        }

    }
    private fun filterData(text: String) {

    }
    private fun addMultipleSelection(
        list: List<FilterData>,
        layout: LinearLayout,
        subCategoryLayout: LinearLayout
    ) {
        layout.removeAllViews()
        list.forEachIndexed { index, model ->
            val itemView =
                LayoutInflater.from(context).inflate(R.layout.row_checkbox, layout, false)
            val cb = itemView.findViewById<CheckBox>(R.id.cb)
            cb.setText(model.name)
            cb.isChecked = model.isSelected
            if(model.isSelected){
                subCategoryLayout.setVisible()
            }
            cb.setTag(index)
            layout.addView(cb)
            cb.setOnClickListener { v->
                var pos = v.getTag() as Int
                list.get(pos).isSelected = (v as CheckBox).isChecked
            }
        }
    }

    private fun addSingleSelection(
        list: List<FilterData>,
        rg: RadioGroup,
        subCategoryLayout: LinearLayout,
        groupCheckBoxLayout: LinearLayout
    ) {
        groupCheckBoxLayout.removeAllViews()
        val rg = RadioGroup(subCategoryLayout.context)
        list.forEachIndexed { index, model ->
            var padding = rg.context.resources.getDimension(R.dimen._10sdp)
            val rb = RadioButton(subCategoryLayout.context)
            var params = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.setMargins((padding*2).toInt(),0,0,0)
            rb.layoutParams = params
            rb.setPadding(0, padding.toInt(), padding.toInt(), padding.toInt())
            rb.setText(model.name)
            rb.setTag(index)
            rb.setTextAppearance(subCategoryLayout.context, R.style.CustomRadioButtonStyle)
            rb.isChecked = model.isSelected
            if(model.isSelected){
                subCategoryLayout.setVisible()
            }
            rg.addView(rb)
        }
        groupCheckBoxLayout.addView(rg)

        if(rg.childCount>0){
            rg.setOnCheckedChangeListener { group, checkedId ->
                val button = group.findViewById<RadioButton>(checkedId)
                var pos = button.getTag() as Int
                list.forEachIndexed { index, filterData ->
                    filterData.isSelected = pos == index
                }
            }
        }
    }

    fun setItems(list: List<FilterModel>?) {
        if (list == null) this.items.clear() else this.items = ArrayList(list)
        notifyDataSetChanged()
    }

    fun getItems(): ArrayList<FilterModel> {
        return this.items
    }

    fun clearAll() {
        this.items.clear()
        notifyDataSetChanged()
    }

}
