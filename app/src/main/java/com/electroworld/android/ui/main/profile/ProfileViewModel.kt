package com.electroworld.android.ui.main.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.UserModel
import com.electroworld.android.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProfileViewModel  @Inject constructor(val mainApi: MainApi,val myPref: PreferencesManager) : ViewModel() {


    private val disposable = CompositeDisposable()

    private val responseLiveData = MutableLiveData<ApiResource<ApiResponse<UserModel>>>()
    private val responseLiveDataLogout = MutableLiveData<ApiResource<ApiResponse<ApiResponseDefault>>>()


    fun callLogoutInfo(logoutFromAll : String){
        disposable.clear()
        disposable.add(mainApi.apiUserLogout(
            Constants.HEADER_TOKEN,myPref.getPrefKeyAccessToken(),myPref.getPrefKeyAuthenticationId(),logoutFromAll,myPref.getPrefKeyVendorId(), myPref.getPrefKeyLanguage())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveDataLogout.setValue(
                    ApiResource.loading<ApiResponse<ApiResponseDefault>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveDataLogout.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveDataLogout.setValue(
                    ApiResource.error<ApiResponse<ApiResponseDefault>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun logoutResponse(): MutableLiveData<ApiResource<ApiResponse<ApiResponseDefault>>> {
        return responseLiveDataLogout
    }

    fun callProfileInfo(){
        disposable.add(mainApi.getUserDetail(
            Constants.HEADER_TOKEN,myPref.getPrefKeyAccessToken(),myPref.getPrefKeyCustomerId(),myPref.getPrefKeyVendorId(), myPref.getPrefKeyLanguage())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.setValue(
                    ApiResource.loading<ApiResponse<UserModel>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveData.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveData.setValue(
                    ApiResource.error<ApiResponse<UserModel>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun profileResponse(): MutableLiveData<ApiResource<ApiResponse<UserModel>>> {
        return responseLiveData
    }

    override fun onCleared() {
        disposable.clear()
    }


}