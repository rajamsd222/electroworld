package com.electroworld.android.ui.auth.webview

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.api.Urls
import com.electroworld.android.databinding.FragmentWebviewBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.utils.AppValidator
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class WebViewFragment : DaggerFragment() {

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory
    @Inject
    lateinit var myPref: PreferencesManager

    private lateinit var navController: NavController
    private lateinit var binding: FragmentWebviewBinding

    var screenType = ""
    var loadUrl = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_webview, container, false
        )
        return binding.getRoot()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        screenType = AppValidator.toStr(arguments?.getString("screenType"))
        loadUrl = AppValidator.toStr(arguments?.getString("url"))
        if (screenType == "tnc") {
            loadUrl = Urls.URL_TNC
            binding.title.text = getString(R.string.terms_amp_conditions)
        } else if (screenType == "pp") {
            loadUrl = Urls.URL_PP
            binding.title.text = getString(R.string.privacy_policy)
        } else if (screenType == "premium_access") {
            loadUrl = Urls.PREMIUM_ACCESS
            binding.title.text = getString(R.string.profile_premium)
        } else if (screenType == "home") {
            val title = AppValidator.toStr(arguments?.getString("name"))
            binding.title.text = title
        }
        binding.ivBack.setOnClickListener { navController.navigateUp() }
        binding.webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
        binding.webView.settings.loadWithOverviewMode = true
        binding.webView.settings.useWideViewPort = true
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.displayZoomControls = false
        binding.webView.settings.builtInZoomControls = true
        binding.webView.settings.setSupportZoom(true)
        binding.webView.webViewClient = CustomWebClient()
        binding.webView.loadUrl(loadUrl)
    }

    private inner class CustomWebClient : WebViewClient() {
        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
            binding.itemProgressbar.visibility = View.VISIBLE
            super.onPageStarted(view, url, favicon)
        }

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            binding.itemProgressbar.visibility = View.VISIBLE
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            binding.itemProgressbar.visibility = View.GONE
        }
    }


    /*class CustomWebClient internal constructor(itemProgressbar: ProgressBar) : WebViewClient() {

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            val url: String = request?.url.toString();
            view?.loadUrl(url)
            return true
        }

        override fun shouldOverrideUrlLoading(webView: WebView, url: String): Boolean {
            webView.loadUrl(url)
            return true
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            itemProgressbar.visibility = View.VISIBLE
            super.onPageStarted(view, url, favicon)
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            itemProgressbar.visibility = View.GONE
            super.onPageFinished(view, url)
        }

    }*/

    companion object {
        private const val TAG = "WebViewFragment"
    }
}