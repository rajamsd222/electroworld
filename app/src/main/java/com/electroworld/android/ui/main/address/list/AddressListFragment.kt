package com.electroworld.android.ui.main.address.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.databinding.FragmentAddressListBinding
import com.electroworld.android.databinding.RowAddressListBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.AddressListModel
import com.electroworld.android.model.StoreModel
import com.electroworld.android.repository.PIMSRepository
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.Constants.COMES_FROM_DELIVERY_LOCATION
import com.electroworld.android.utils.Utils
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import java.util.*
import javax.inject.Inject

class AddressListFragment : DaggerFragment() {


    private lateinit var viewModel: AddressListViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentAddressListBinding
    private var mAdapter: GenericAdapter<AddressListModel, RowAddressListBinding>? = null
    var mList: List<AddressListModel> = ArrayList()

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    @Inject
    lateinit var pims: PIMSRepository
    private var screenName: String? = null
    private var deletedAddressId: String? = null
    private val disposable = CompositeDisposable()
    var fromOnCreate = true;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_address_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(AddressListViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentAddressListBinding.bind(view)
        screenName = AppValidator.toStr(arguments?.getString("screenName"))
        initBasicObservers()
        if (screenName.equals("cart")) {
            binding.btnUseCurrentLocation.setGone()
            binding.placeTextView.setGone()
            binding.abLayout.setBackgroundResource(R.mipmap.dashboard_top_bar)
            binding.bgTop.setBackgroundResource(0)
        }
        binding.ab.tvABTitle.text = getString(R.string.delivery_locations)
        if (COMES_FROM_DELIVERY_LOCATION) {
            COMES_FROM_DELIVERY_LOCATION = false
            mNavController.navigateUp()
        }
        initRecyclerView()
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        binding.btnAddLocation.setOnClickListener {
            if (!myPref.isLogin()) {
                (requireActivity() as MainActivity).showLoginDialog(requireActivity().getString(R.string.alert_login_address))
            } else {
                openMapScreen(null)
            }
        }
        binding.btnUseCurrentLocation.setOnClickListener {
            openDeliveryLocationScreen()
        }
        binding.placeTextView.setOnClickListener {
            openDeliveryLocationScreen()
        }
        viewModel.getApiResponse().observe(viewLifecycleOwner) { res -> consumeResponse(res) }
        viewModel.getDeleteApiResponse()
            .observe(viewLifecycleOwner) { res -> consumeDeleteResponse(res) }

    }

    private fun initBasicObservers() {
        viewModel.errorMessage.observe(viewLifecycleOwner, { isError ->
            isError?.let {
                CustomDialog.show(requireActivity(), it, AlertType.ERROR)
            }
        })
        viewModel.loading.observe(viewLifecycleOwner, { isLoading ->
            isLoading?.let {
                progressBar.visibility = if (it) View.VISIBLE else View.GONE
            }
        })
    }

    private fun openDeliveryLocationScreen() {
        val bundle = Bundle()
        bundle.putString("screenName", screenName)
        mNavController.navigate(
            R.id.action_addressListFragment_to_deliveryLocationSelectionFragment,
            bundle
        )
    }

    private fun consumeDeleteResponse(res: ApiResource<ApiResponse<ApiResponseDefault>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            Utils.showApiSuccess(activity, res.data.message)
                            checkDefaultAddressDeleted()
                            makeReq()
                        } else {
                            CustomDialog.show(requireActivity(), res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }

    private fun checkDefaultAddressDeleted() {
        var model = myPref.getDefaultAddress()
        if (model != null) {
            if (deletedAddressId.equals(model.address_id)) {
                Constants.ADDRESS_ID = ""
                Constants.ADDRESS_NAME = (requireActivity() as MainActivity).currentAddressName
                Constants.LATITUDE = (requireActivity() as MainActivity).currentLatitude
                Constants.LONGITUDE = (requireActivity() as MainActivity).currentLongitutde
                myPref.setDefaultAddress(null)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (myPref.getPrefKeyCustomerId().isNotEmpty()) {
            makeReq()
        } else {
            if (fromOnCreate) {
                fromOnCreate = false
                if (screenName.equals("location_picker")) {
                    binding.btnUseCurrentLocation.performClick()
                }
            }

            binding.addressLayout.setGone()
        }
    }

    private fun makeReq() {
        hideAddressList()
        viewModel.getList()
    }

    private fun hideAddressList() {
        binding.tvChooseAddress.setGone()
        binding.recyclerView.setGone()
        binding.btnAddLocation.setGone()
    }

    private fun showAddressList() {
        binding.tvChooseAddress.setVisible()
        binding.recyclerView.setVisible()
    }

    private fun consumeResponse(res: ApiResource<ApiResponse<List<AddressListModel>>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            mAdapter?.items = res.data.data
                            binding.addressLayout.setVisible()
                            if (mAdapter?.itemCount == 0) {
                                hideAddressList()
                            } else {
                                showAddressList()
                            }
                            if (mAdapter?.itemCount ?: 0 >= 3) {
                                binding.btnAddLocation.setGone()
                            } else {
                                binding.btnAddLocation.setVisible()
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }

    fun saveAddress(model: AddressListModel?) {
        if (model != null) {
            makeStoreInfoReq(model)
        }
    }

    private fun updateAddressDetails(model: AddressListModel) {
        Constants.ADDRESS_ID = model.address_id
        Constants.ADDRESS_NAME = model.address
        Constants.LATITUDE = model.latitude
        Constants.LONGITUDE = model.longitude
        myPref.setDefaultAddress(model)
        mNavController.navigateUp()
    }

    private fun makeStoreInfoReq(model: AddressListModel) {
        disposable.add(
            pims.getStoreInfo(model.latitude, model.longitude)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<StoreModel>>() {
                    override fun onSuccess(res: ApiResponse<StoreModel>) {
                        if (res.http_code == 200) {
                            /*if(screenName.equals("cart")){
                                myPref.setDeliveryAddressStoreId(res.data.id)
                                myPref.setStoreModel(res.data)
                            }
                            else{
                                myPref.setPrefKeyStoreId(res.data.id)
                                myPref.setPrefKeyStoreAddress(res.data.address)
                                myPref.setPrefKeyStoreName(res.data.name_en)
                                myPref.setHomeAddressTop(model.address)
                            }
                            if(myPref.checkStoreModelAdded().isEmpty()){
                                myPref.setStoreModel(res.data)
                                myPref.setDeliveryAddressStoreId(res.data.id)
                            }*/
                            myPref.setDeliveryAddressStoreId(res.data.id)
                            myPref.setStoreModel(res.data)
                            myPref.setPrefKeyStoreId(res.data.id)
                            myPref.setPrefKeyStoreAddress(res.data.address)
                            myPref.setPrefKeyStoreName(res.data.name_en)
                            myPref.setHomeAddressTop(model.address)
                            updateAddressDetails(model)
                        } else if (res.http_code == 400) {
                            if (screenName.equals("cart")) {

                            } else {
                                //myPref.resetHomeStoreInfo()
                                if (res.data != null && res.data.id != null && res.data.id.isNotEmpty()) {
                                    myPref.setPrefKeyStoreId(res.data.id)
                                    myPref.setPrefKeyStoreAddress(res.data.address)
                                    myPref.setPrefKeyStoreName(res.data.name_en)
                                    myPref.setHomeAddressTop(model.address)
                                }
                                mNavController.navigateUp()
                            }
                            //CustomDialog.show(requireActivity(),"Currently we don't serve at your current location and we’ll soon start serving. In the meanwhile, experience FlipZeeemart!", AlertType.ERROR)
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    fun openMapScreen(model: AddressListModel?) {
        val bundle = Bundle()
        bundle.putSerializable("model", model)
        mNavController.navigate(R.id.action_addressListFragment_to_mapsFragment, bundle)
    }

    fun deleteAddress(model: AddressListModel?) {
        deletedAddressId = model?.address_id
        viewModel.deleteAddress(model!!.address_type)
    }

    private fun initRecyclerView() {
        mAdapter = object :
            GenericAdapter<AddressListModel, RowAddressListBinding>(R.layout.row_address_list) {
            override fun onBindData(
                model: AddressListModel,
                position: Int,
                dataBinding: RowAddressListBinding
            ) {
                dataBinding.item = model
                dataBinding.tvLine1.setText(model.house_no)
                if (model.apartment_name.isNotEmpty()) {
                    dataBinding.tvLine1.setText(model.house_no + ", " + model.apartment_name)
                }
                if (model.street.isNotEmpty()) {
                    dataBinding.tvLine1.setText(model.house_no + ", " + model.apartment_name + ", " + model.street)
                }

//                if (screenName.equals("cart")) {
//                    if (myPref.getDeliveryAddressStoreId().equals(model.address_id)) {
//                        dataBinding.rb.isChecked = true
//                    }
//                } else {
//                    if (Constants.ADDRESS_ID.equals(model.address_id)) {
//                        dataBinding.rb.isChecked = true
//                    }
//                }

                if (Constants.ADDRESS_ID.equals(model.address_id)) {
                    dataBinding.rb.isChecked = true
                }

                dataBinding.rb.setOnClickListener {
                    saveAddress(model)
                }
                dataBinding.ivEdit.setOnClickListener {
                    openMapScreen(model)
                }
                dataBinding.ivDelete.setOnClickListener {
                    deleteAddress(model)
                }
            }

            override fun onItemClick(model: AddressListModel?, position: Int) {
                saveAddress(model)
            }
        }
        binding.recyclerView.apply {
            adapter = mAdapter
        }
    }
}