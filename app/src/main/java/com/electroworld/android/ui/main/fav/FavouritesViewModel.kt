package com.electroworld.android.ui.main.fav

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.SetFavModel
import com.electroworld.android.repository.PIMSRepository
import com.electroworld.android.utils.Constants
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FavouritesViewModel @Inject constructor(val mainApi: MainApi,val myPref: PreferencesManager,private val repoPIMS : PIMSRepository) : ViewModel() {
    private val disposable = CompositeDisposable()
    private val responseLiveData = MutableLiveData<ApiResource<ApiResponse<List<ProductModel>>>>()

    private val responseLiveDataFav = MutableLiveData<ApiResource<ApiResponse<SetFavModel>>>()



    fun makeproductInfoReq(warehouseProductId: String): Single<ApiResponse<ProductModel>> {
        return repoPIMS.getProductInfoReq(warehouseProductId)
    }


    fun setAsFav(
        warehouse_product_id: String,
        product_id: String,
        is_favourite: Int,
        group_id: String
    ){
        disposable.clear()
        disposable.add(mainApi.setFav(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            warehouse_product_id,
            product_id,
            is_favourite,
            group_id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveDataFav.setValue(
                    ApiResource.loading<ApiResponse<SetFavModel>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveDataFav.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveDataFav.setValue(
                    ApiResource.error<ApiResponse<SetFavModel>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getFavResponse(): MutableLiveData<ApiResource<ApiResponse<SetFavModel>>> {
        return responseLiveDataFav
    }

    fun getList(){
        disposable.clear()
        disposable.add(mainApi.getFavList(
            Constants.HEADER_TOKEN,myPref.getPrefKeyAccessToken(),myPref.getPrefKeyCustomerId(),myPref.getPrefKeyStoreId(),myPref.getPrefKeyVendorId())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.setValue(
                    ApiResource.loading<ApiResponse<List<ProductModel>>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveData.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveData.setValue(
                    ApiResource.error<ApiResponse<List<ProductModel>>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getApiResponse(): MutableLiveData<ApiResource<ApiResponse<List<ProductModel>>>> {
        return responseLiveData
    }

    override fun onCleared() {
        disposable.clear()
    }


}