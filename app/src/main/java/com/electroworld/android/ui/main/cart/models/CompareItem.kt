package com.electroworld.android.ui.main.cart.models

data class CompareItem(
    var productId: String,
    var group_id: String,
    var second_level_category_id: String,
    var warehouse_product_id: String,
    var vendor_id: String,
    var store_id: String
)