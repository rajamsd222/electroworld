package com.electroworld.android.ui.main.checkout

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.alert.ShowOrderSuccessDialog
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.bind.AppBinder
import com.electroworld.android.databinding.FragmentCheckOutBinding
import com.electroworld.android.databinding.RowPaymentListBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.*
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.cashfree.CashFreePaymentFragment
import com.electroworld.android.ui.main.orders.SummaryModel
import com.electroworld.android.utils.*
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_check_out.*
import kotlinx.android.synthetic.main.toolbar_title_cart.view.*
import javax.inject.Inject


class CheckOutFragment : DaggerFragment() {


    private var paymentList: List<PaymentModel> = ArrayList()
    private lateinit var viewModel: CheckOutViewModel
    private lateinit var binding: FragmentCheckOutBinding
    private lateinit var mNavController: NavController

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager
    var grandTotal = 0.0
    var promotion_product_total_price = 0.0
    var address_id = ""
    var paymentId = ""
    var noPaymentId = "14"
    var paymentCode = ""
    var transactionId = ""
    var razorPayCode = "razorpay"
    var orderId: String = ""
    val list: MutableList<SummaryModel> = ArrayList()
    private var mAdapter: GenericAdapter<PaymentModel, RowPaymentListBinding>? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_check_out, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mNavController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this, providersFactory).get(CheckOutViewModel::class.java)
        binding = FragmentCheckOutBinding.bind(view)
        binding.ab.tvABTitle.text = getString(R.string.checkout)
        address_id = Constants.ADDRESS_ID

        if (myPref.getCartArray().length() == 0) {
            ToastUtil.show(requireActivity(), "Your cart is empty.Please add items in to the cart.")
            myPref.clearLocalCart()
            mNavController.navigateUp()
            return
        }
        showHideParentLayout(false)
        binding.cvPromotionsLayout.setGone()
        binding.promotionItemLayout.setGone()
        binding.promoContentLayout.setGone()
        binding.cvPaymentOptions.visibility = View.GONE
        binding.tvStoreName.text = myPref.getPrefKeyStoreAddress()
        binding.tvDeliverySlotValue.text =
            getString(R.string.lbl_delivery_on) + myPref.getDeliverySlotText()
        initPaymentRecyclerView()
        calculateOrderSummary()
        initOnClicks()
        loadApi()
        initCommonObserver()
        initDeliverySlotObserver()

        viewModel.getShippingSlotApiResponse().observe(viewLifecycleOwner) { res ->
            consumeShippingSlotResponse(
                res
            )
        }
        binding.cbUseWallet.isChecked = myPref.checkWalletSelected()
        binding.checkUseGST.isChecked = myPref.getGSTEnabled()
        binding.cbPickUpFromStore.isChecked = myPref.isPickupFromStoreSelected()
        binding.edGSTNumber.setText(myPref.getGSTNumber())
        binding.edCompanyName.setText(myPref.getCompanyName())
        binding.tvpickupTime.text = myPref.getPickUpSlotText()
        if (myPref.getGSTEnabled()) {
            binding.gstContentLayout.setVisible()
        } else {
            binding.gstContentLayout.setGone()
        }
        updateWalletAmountView()
        if (myPref.isPickupFromStoreSelected()) {
            binding.layoutPickupFromStore.visibility = View.VISIBLE
            binding.layoutDelivery.visibility = View.GONE
        }
    }

    private fun loadDeliverySlots() {
        viewModel.getDeliverySlots()
    }

    private fun updateDeliverySlotText() {
        binding.tvDeliverySlotValue.text =
            getString(R.string.lbl_delivery_on) + myPref.getDeliverySlotText()

        binding.tvDeliverySlotValue.setVisible()
    }

    private fun initDeliverySlotObserver() {
        viewModel.deliverySlotApi.observe(viewLifecycleOwner, Observer { apiModel ->
            apiModel?.let {
                (activity as MainActivity).mDeliverySlotList = it.delivery_slot
                if (myPref.getDeliverySlot() == null) {
                    updateDefaultSlot()
                } else {
                    var checkAlreadyPresent = true
                    val deliverySlotFromTime = myPref.getFromTime()
                    (activity as MainActivity).mDeliverySlotList.forEach { deliverySlot ->
                        checkAlreadyPresent = deliverySlotFromTime == deliverySlot.from_time
                    }
                    if (!checkAlreadyPresent) {
                        updateDefaultSlot()
                    }
                }
                updateDeliverySlotText()
                it.pickup_slot.forEachIndexed { index, deliverySlot ->
                    if (index == 0) {
                        myPref.updatePickUpSlot(deliverySlot)
                    }
                }
            }
        })
    }

    private fun updateDefaultSlot() {
        if ((activity as MainActivity).mDeliverySlotList.isNotEmpty()) {
            myPref.updateDeliverySlot((activity as MainActivity).mDeliverySlotList.get(0))
        }
    }


    fun showHideParentLayout(boolean: Boolean) {
        binding.parentView.visibility = if (boolean) View.VISIBLE else View.GONE
        binding.bottomView.visibility = if (boolean) View.VISIBLE else View.GONE
    }

    private fun initPaymentRecyclerView() {
        mAdapter = object :
            GenericAdapter<PaymentModel, RowPaymentListBinding>(R.layout.row_payment_list) {
            override fun onBindData(
                model: PaymentModel,
                position: Int,
                dataBinding: RowPaymentListBinding
            ) {
                dataBinding.item = model
                dataBinding.rb.isChecked = model.isSelect
                if (Constants.PAYMENT_METHOD == model.code) {

                }
                dataBinding.rb.setOnClickListener {
                    Constants.PAYMENT_METHOD == model.code
                    paymentList.forEachIndexed { index, paymentModel ->
                        paymentModel.isSelect = index == position
                    }
                    mAdapter?.notifyDataSetChanged()
                }
            }

            override fun onItemClick(model: PaymentModel?, position: Int) {

            }
        }
        binding.rvPaymentList.apply {
            adapter = mAdapter
        }
    }

    private fun updateWalletAmountView() {
        cbUseWallet.setText("Wallet " + MyUtils.formatCurrencyfromDouble(myPref.getWalletAmount()) + "\nAdishwar Electro World Amount")
    }

    private fun applyWallet() {
        calculateOrderSummary()
        resetPaymentOptions()
    }

    private fun removeWallet() {
        calculateOrderSummary()
    }

    private fun resetPaymentOptions() {
        paymentList.forEach {
            it.isSelect = false
        }
        paymentId = ""
        paymentCode = ""
        mAdapter?.notifyDataSetChanged()
    }

    private fun calculateOrderSummary() {
        if (myPref.getPromoCode().isNotEmpty()) {
            showRemovePromocodeView(myPref.getPromoCode())
        }

        var list = CartUtils.getOrderSummary(
            requireActivity() as MainActivity,
            myPref,
            promotion_product_total_price
        )
        if (list.size > 0) {
            grandTotal = list.get(list.size - 1).value
            if (grandTotal > 0) {
                paymentId = "";
                binding.cvPaymentOptions.setVisible()
                binding.lblPaymentOptions.setVisible()
            } else {
                binding.cvPaymentOptions.setGone()
                binding.lblPaymentOptions.setGone()
                paymentId = noPaymentId;
            }
            //updateSummaryViews()
            grandTotal = MyUtils.formatCurrencyFromDoubleWithoutSymbol(grandTotal)
            binding.tvAmount.text = MyUtils.formatCurrencyfromDouble(grandTotal)
            AppBinder.setOrderSummary(binding.orderSummaryLayout, list)
        }

    }

    private fun initOnClicks() {
        binding.cbUseWallet.setOnClickListener {
            myPref.updateWalletSelection(binding.cbUseWallet.isChecked)
            val box = it as CheckBox
            if (box.isChecked) {
                applyWallet()
            } else {
                removeWallet()
            }
        }

        binding.tvContinueShopping.setOnClickListener {
            mNavController.navigate(R.id.action_checkOutFragment_to_homeFragment)
        }

        binding.applyCouponTitlelayout.setOnClickListener {
            if (binding.promoContentLayout.visibility == View.VISIBLE) {
                binding.promoContentLayout.setGone()
            } else {
                binding.promoContentLayout.setVisible()
                if (myPref.getPromoCode().length > 0) {
                    binding.removePromoCodeLayout.setVisible()
                } else {
                    binding.applyPromoCodeLayout.setVisible()
                }
            }
        }

        if (myPref.getDeliveryInstruction().isNotEmpty()) {
            binding.tvDelveryInsTitle.text = getString(R.string.edit_delivery_instruction)
        } else {
            binding.tvDelveryInsTitle.text = getString(R.string.delivery_instructions_title)
        }
        binding.edDeliveryInst.setText(myPref.getDeliveryInstruction())

        binding.btnAddDeliver.setOnClickListener {
            CartUtils.addDeliveryNotes(myPref, binding.edDeliveryInst.text.toString().trim())
            binding.deliveryContentLayout.setGone()
            if (myPref.getDeliveryInstruction().isNotEmpty()) {
                binding.tvDelveryInsTitle.text = getString(R.string.edit_delivery_instruction)
            } else {
                binding.tvDelveryInsTitle.text = getString(R.string.delivery_instructions_title)
            }
        }

        binding.DelveryInsTitlelayout.setOnClickListener {
            if (binding.deliveryContentLayout.visibility == View.VISIBLE) {
                binding.deliveryContentLayout.setGone()
            } else {
                binding.deliveryContentLayout.setVisible()
                if (myPref.getDeliveryInstruction().isNotEmpty()) {
                    binding.tvDelveryInsTitle.text = getString(R.string.edit_delivery_instruction)
                } else {
                    binding.tvDelveryInsTitle.text = getString(R.string.delivery_instructions_title)
                }
            }
        }

        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }

        binding.tvContinueToBuy.setOnClickListener {
            mNavController.navigateUp()
        }
        binding.btnPlaceOrder.setOnClickListener {
            validateInputs()
        }
        binding.btnApply.setOnClickListener {
            if (binding.edPromoCode.text.toString().trim().isEmpty()) {
                UiUtils.showInputAlert(
                    activity,
                    getString(R.string.please_enter_promo_code),
                    binding.edPromoCode
                )
            } else {
                checkpromocode(binding.edPromoCode.text.toString().trim())
            }
        }





        binding.cbPickUpFromStore.setOnClickListener {
            myPref.updateFlagPickUpFromStore(binding.cbPickUpFromStore.isChecked)
            if (binding.cbPickUpFromStore.isChecked) {
                binding.layoutPickupFromStore.visibility = View.VISIBLE
                binding.layoutDelivery.visibility = View.GONE
            } else {
                binding.layoutPickupFromStore.visibility = View.GONE
                binding.layoutDelivery.visibility = View.VISIBLE
            }
            calculateOrderSummary()
        }
        binding.checkUseGST.setOnClickListener {
            if (binding.checkUseGST.isChecked) {
                binding.gstContentLayout.setVisible()
            } else {
                binding.gstContentLayout.setGone()
            }

        }

        binding.btnRemove.setOnClickListener {
            binding.promoContentLayout.setGone()
            binding.removePromoCodeLayout.setGone()
            binding.edPromoCode.setText("")
            CartUtils.removePromoCode(myPref)
            calculateOrderSummary()
            // updateGrandTotal()
        }
    }

    private fun checkValidOrder() {
        var cartListStr = myPref.getCartArray()
        if (cartListStr.length() == 0 || myPref.getDeliveryAddressStoreId().isEmpty()) {
            UiUtils.showToast(requireActivity(), "Invalid cart items found")
            myPref.clearLocalCart()
            mNavController.navigateUp()
            return
        }
        binding.btnPlaceOrder.isEnabled = false
        viewModel.checkValidOrderRes(
            requireActivity(),
            address_id,
            myPref.getCartArray(),
            myPref.getDeliveryAddressStoreId()
        )
            ?.observe(
                viewLifecycleOwner,
                { res ->
                    if (res != null) {
                        when (res.http_code) {
                            200 -> {
                                makePlaceOrderAPIReq()
                            }
                            401 -> {
                                binding.btnPlaceOrder.isEnabled = true
                                val sbOutOfStock = StringBuilder()
                                res.data.out_of_stock_product.forEachIndexed { index, info ->
                                    if (index != 0) {
                                        sbOutOfStock.append("\n")
                                    }
                                    sbOutOfStock.append("" + (index + 1) + ". " + info.product_name)
                                }
                                val sbLocation = StringBuilder()
                                res.data.location_product.forEachIndexed { index, unAvailableProductInfo ->
                                    if (index != 0) {
                                        sbLocation.append("\n")
                                    }
                                    sbLocation.append("" + (index + 1) + ". " + unAvailableProductInfo.product_name)
                                }
                                CustomDialog.showCheckOutWarningDialog(requireActivity(),
                                    res.message,
                                    sbOutOfStock.toString(),
                                    sbLocation.toString(),


                                    object : CustomDialog.OKClickListener {
                                        override fun onClick(view: View?) {
                                            removeItemsAndPlaceOrder(res.data.location_product)
                                            removeOutOfStockItems(res.data.out_of_stock_product)
                                            binding.ab.ivBack.performClick()
                                        }

                                        override fun onContinueShoppingClick(view: View?) {
                                            TODO("Not yet implemented")
                                        }
                                    })

                            }
                            else -> {
                                binding.btnPlaceOrder.isEnabled = true
                                CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                            }
                        }
                    }
                })
    }

    private fun removeOutOfStockItems(outOfStockProduct: List<OutOfStockProductInfo>) {
        outOfStockProduct.forEach {
            CartUtils.deleteCartItem(myPref, it.warehouse_product_id)
        }
    }

    private fun removeItemsAndPlaceOrder(list: List<OutOfStockProductInfo>) {
        list.forEach {
            CartUtils.deleteCartItem(myPref, it.warehouse_product_id)
        }
    }

    private fun loadApi() {
        viewModel.makePaymentListReq().observe(viewLifecycleOwner, {
            handlePaymentListResponse(it)
        })

        viewModel.getShippingCharges(myPref.getSubTotal(), "add")
    }

    private fun initCommonObserver() {
        viewModel.apiError.observe(viewLifecycleOwner, { isError ->
            isError?.let {
                CustomDialog.show(requireActivity(), it, AlertType.ERROR)
            }
        })
        viewModel.loading.observe(viewLifecycleOwner, { isLoading ->
            isLoading?.let {
                progressBar.visibility = if (it) View.VISIBLE else View.GONE
            }
        })
    }

    private fun handlePaymentListResponse(it: List<PaymentModel>) {
        showHideParentLayout(true)
        paymentList = it
        mAdapter?.addItems(paymentList)
        if (it.isNotEmpty()) {
            binding.cvPaymentOptions.setVisible()
        } else binding.cvPaymentOptions.setGone()

        loadDeliverySlots()
    }

    private fun consumeShippingSlotResponse(res: ApiResource<ApiResponse<ShippingChargesResponse>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            myPref.updateDeliveryCharge(res.data.data.delivery_charge)
                            myPref.setWalletAmount(res.data.data.wallet_balance)
                            myPref.updateWalletSelection(myPref.getWalletAmount() > 0)
                            binding.cbUseWallet.isChecked = myPref.checkWalletSelected()
                            promotion_product_total_price =
                                res.data.data.promotion_product_total_price
                            updateWalletAmountView()
                            calculateOrderSummary()
                            if (myPref.checkWalletSelected()) {
                                applyWallet()
                            }
                            binding.focItem = res.data.data
                        } else {
                            CustomDialog.show(requireActivity(), res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }


    private fun checkpromocode(promocode: String) {
        viewModel.getPromocodeInfoRes(
            requireActivity(),
            promocode,
            myPref.getSubTotal()
        )?.observe(viewLifecycleOwner, object : Observer<ApiResponse<PromoCodeModel>?> {
            override fun onChanged(res: ApiResponse<PromoCodeModel>?) {
                if (res != null) {
                    if (res.http_code === 200) {
                        if (res.data != null) {
                            CartUtils.addPromocode(
                                myPref,
                                binding.edPromoCode.text.toString().trim().toLowerCase(),
                                res.data.discount_amount,
                                res.data.discount_percent
                            )
                            showRemovePromocodeView(binding.edPromoCode.text.toString().trim())
                            calculateOrderSummary()
                            //updateGrandTotal()
                        }
                    } else {
                        CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                    }
                    //updateAdapter()
                }
            }
        })
    }

    private fun showRemovePromocodeView(promo_code: String) {
        binding.promoContentLayout.setVisible()
        binding.removePromoCodeLayout.setVisible()
        binding.applyPromoCodeLayout.setGone()
        binding.tvPromocodeName.text = promo_code
    }

    private fun validateInputs() {

        if (address_id.isEmpty()) {
            Utils.showWarning(activity, getString(R.string.error_select_delivery_address))
            return
        }

        if (myPref.getFromTime().isEmpty()) {
            Utils.showWarning(activity, getString(R.string.error_chosse_delivery_date_time))
            return
        }

        if (binding.checkUseGST.isChecked && binding.edCompanyName.text.toString().trim()
                .isEmpty()
        ) {
            Utils.showWarning(activity, getString(R.string.error_enter_company_name))
            return
        }
        if (binding.checkUseGST.isChecked && binding.edGSTNumber.text.toString().trim().isEmpty()) {
            Utils.showWarning(activity, getString(R.string.error_enter_gst_number))
            return
        }
        myPref.setGSTEnabled(
            binding.checkUseGST.isChecked,
            binding.edGSTNumber.text.toString().trim(),
            binding.edCompanyName.text.toString().trim()
        )

        paymentList.forEach {
            if (it.isSelect) {
                paymentId = it.id
                paymentCode = it.code
            }
        }

        if (paymentId.isEmpty() && grandTotal > 0) {
            Utils.showWarning(activity, getString(R.string.error_choose_payment_method))
            return
        }
        checkValidOrder()
    }

    private fun makePlaceOrderAPIReq() {
        if (myPref.getCartArray().length() == 0 || myPref.getDeliveryAddressStoreId().isEmpty()) {
            UiUtils.showToast(requireActivity(), "Invalid cart items found")
            myPref.clearLocalCart()
            mNavController.navigateUp()
            return
        }
        var cfTokenNeed = ""
        if (paymentCode.equals("card_cashfree") || paymentCode.equals("upi_cashfree")
            || paymentCode.equals("net_cashfree") || paymentCode.equals("wallet_cashfree")
        ) {
            cfTokenNeed = "1"
        }
        viewModel.doPlaceOrderRes(
            requireActivity(),
            address_id,
            paymentId,
            grandTotal,
            cfTokenNeed
        ).observe(
            viewLifecycleOwner,
            object : Observer<ApiResponse<OrderPlaceModel>?> {
                override fun onChanged(res: ApiResponse<OrderPlaceModel>?) {
                    if (res != null) {
                        if (res.http_code == 200) {
                            binding.btnPlaceOrder.isEnabled = false
                            myPref.clearLocalCart()
                            orderId = res.data.id
                            myPref.setWalletAmount(res.data.customer_balance_wallet_amount)
                            if (paymentCode.isEmpty() || paymentCode.equals("cash_on_delivery")) {
                                showOrderSuccessDialog(res.message, orderId)
                            } else {
                                binding.btnPlaceOrder.isEnabled = true
                                var paymentType = PaymentType.DEFAULT
                                if (paymentCode.equals("card_cashfree")) {
                                    paymentType = PaymentType.CARD
                                } else if (paymentCode.equals("upi_cashfree")) {
                                    paymentType = PaymentType.UPI
                                } else if (paymentCode.equals("net_cashfree")) {
                                    paymentType = PaymentType.NETBANKING
                                } else if (paymentCode.equals("wallet_cashfree")) {
                                    paymentType = PaymentType.WALLET
                                }

                                var amountTotal = res.data.amount_total;
                                amountTotal = "" + (MyUtils.formatCurrencyFromDoubleWithoutSymbol(
                                    AppValidator.toDouble(amountTotal)
                                ))

                                openCashFreeScreen(
                                    res.data.cftoken,
                                    amountTotal,
                                    res.data.order_id,
                                    res.data.id,
                                    paymentType
                                )
                            }

                        } else {
                            binding.btnPlaceOrder.isEnabled = true
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }
                }
            })

    }

    private fun openCashFreeScreen(
        token: String,
        amount: String,
        orderId: String,
        id: String,
        actionType: PaymentType
    ) {
        val bundle = CashFreePaymentFragment.updateBundle(
            token,
            amount,
            orderId,
            id,
            actionType,
            ScreenType.CHECKOUT
        )
        mNavController.navigate(R.id.action_checkOutFragment_to_cashFreePaymentFragment, bundle)
    }

    private fun showOrderSuccessDialog(message: String, number: String) {
        ShowOrderSuccessDialog.show(requireActivity(), number, message,
            object : ShowOrderSuccessDialog.OKClickListener {
                override fun onClick(view: View?) {
                    val intent = Intent(activity, MainActivity::class.java)
                    intent.putExtra("navigate_screen_name", "my_orders")
                    intent.putExtra("order_id", orderId)
                    startActivity(intent)
                    requireActivity().finish()
                }

                override fun onContinueShoppingClick(view: View?) {
                    mNavController.navigate(R.id.action_checkOutFragment_to_homeFragment)
                }

            })
    }

}