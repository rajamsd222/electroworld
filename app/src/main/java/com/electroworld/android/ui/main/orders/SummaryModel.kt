package com.electroworld.android.ui.main.orders

data class SummaryModel (
    val name: String,
    val value: Double
)