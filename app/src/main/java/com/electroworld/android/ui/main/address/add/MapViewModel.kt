package com.electroworld.android.ui.main.address.add

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MapViewModel @Inject constructor(val mainApi: MainApi,val myPref: PreferencesManager) : ViewModel() {


    private val disposable = CompositeDisposable()
    private val responseLiveData = MutableLiveData<ApiResource<ApiResponse<ApiResponseDefault>>>()
    private val resPlaceSearch = MutableLiveData<ApiResource<ResponseBody>>()
    private val resGetPlaceInfoById = MutableLiveData<ApiResource<JsonObject>>()
    var res: MutableLiveData<ResponseBody> = MutableLiveData<ResponseBody>()


    fun getInfoByPlaceId(url: String){
        disposable.clear()
        disposable.add(mainApi.getPlaceInfoByUrl(url)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                resGetPlaceInfoById.setValue(
                    ApiResource.loading<JsonObject?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    resGetPlaceInfoById.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                resGetPlaceInfoById.setValue(
                    ApiResource.error<JsonObject?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getResultByPlaceId(): MutableLiveData<ApiResource<JsonObject>> {
        return resGetPlaceInfoById
    }

    private fun getAddressList(url: String?): MutableLiveData<JsonObject>? {
        val res: MutableLiveData<JsonObject> =
            MutableLiveData<JsonObject>()
        mainApi.doPlaceSearchByUrlNew(url)
            .enqueue(object : Callback<JsonObject> {
                override fun onResponse(
                    call: Call<JsonObject>,
                    response: Response<JsonObject>
                ) {
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    res.setValue(null)
                }
            })
        return res
    }
    fun getAddressListRes(url: String?): LiveData<JsonObject>? {
        return getAddressList(url)
    }

    fun searchPlaceResponse(): MutableLiveData<ApiResource<ResponseBody>> {
        return resPlaceSearch
    }

    fun getList(
        address_type: String,
        ID: String,
        house_no: String,
        apartment_name: String,
        street_details: String,
        landmark: String,
        mobile_number: String,
        contact_person: String,
        address: String,
        latitude: Double,
        longitude: Double
    ){
        disposable.add(mainApi.addAddress(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            address_type,
            ID,
            house_no,
            apartment_name,
            street_details,
            landmark,
            mobile_number,
            contact_person,
            address,
            latitude,
            longitude,
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyLanguage()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.setValue(
                    ApiResource.loading<ApiResponse<ApiResponseDefault>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveData.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveData.setValue(
                    ApiResource.error<ApiResponse<ApiResponseDefault>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getApiResponse(): MutableLiveData<ApiResource<ApiResponse<ApiResponseDefault>>> {
        return responseLiveData
    }

    override fun onCleared() {
        disposable.clear()
    }

}