package com.electroworld.android.ui.main.ratingreviews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.databinding.FragmentRatingReviewsListBinding
import com.electroworld.android.databinding.RowItemRatingReviewsListBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.RatingReviewsListModel
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_title_cart.view.*
import javax.inject.Inject

class RatingReviewsListFragment : DaggerFragment() {
    private lateinit var viewModel: RatingReviewsViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding : FragmentRatingReviewsListBinding

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory
    @Inject
    lateinit var myPref: PreferencesManager
    private val disposable = CompositeDisposable()
    private var mRatingListAdapter: GenericAdapter<RatingReviewsListModel, RowItemRatingReviewsListBinding>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_rating_reviews_list, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(RatingReviewsViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentRatingReviewsListBinding.bind(view)
        binding.ab.tvABTitle.text = getString(R.string.profile_rating)
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        initRatingRecyclerView()
        getRatingList()
    }

    private fun initRatingRecyclerView() {
        mRatingListAdapter = object :
            GenericAdapter<RatingReviewsListModel, RowItemRatingReviewsListBinding>(R.layout.row_item_rating_reviews_list) {
            override fun onBindData(
                model: RatingReviewsListModel,
                position: Int,
                dataBinding: RowItemRatingReviewsListBinding
            ) {
                dataBinding.item = model
            }

            override fun onItemClick(model: RatingReviewsListModel?, position: Int) {}
        }
        binding.recyclerView.apply {
            adapter = mRatingListAdapter
        }
    }

    private fun getRatingList() {
        binding.tvEmptyMsg.setGone()
        disposable.add(
            viewModel.getRatingReviewsList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<RatingReviewsListModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<RatingReviewsListModel>>) {
                        if (res.http_code == 200) {
                            mRatingListAdapter?.addItems(res.data)
                            if (mRatingListAdapter?.itemCount == 0) {
                                binding.tvEmptyMsg.setVisible()
                            } else {
                                binding.tvEmptyMsg.setGone()
                            }
                        } else {
                            CustomDialog.show(requireActivity(),res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(),e.message,AlertType.SERVER_ERROR)
                    }
                })
        )
    }
}