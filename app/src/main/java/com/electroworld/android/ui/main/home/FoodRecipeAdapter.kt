package com.electroworld.android.ui.main.home

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.databinding.RowItemFoodRecipeBinding
import com.electroworld.android.model.Placeholder
import java.util.*

class FoodRecipeAdapter(
    mNavController: NavController,
    posts: List<Placeholder>,
    fragment: Fragment,
    val context: Activity
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var posts: List<Placeholder> = ArrayList<Placeholder>()
    var parentFragment: Fragment
    var mNavController : NavController = mNavController
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowItemFoodRecipeBinding =
            RowItemFoodRecipeBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    inner class ViewHolder(binding: RowItemFoodRecipeBinding) : RecyclerView.ViewHolder(binding.root) {
        private val binding: RowItemFoodRecipeBinding = binding
        fun bind(position: Int) {
            val model: Placeholder = posts[position]
            binding.item = model
            binding.executePendingBindings()
            binding.parentView.setTag(model)
            binding.parentView.setOnClickListener {
                var tag = it.getTag() as Placeholder?
                val bundle = Bundle()
                bundle.putString("screenType", "home")
                bundle.putString("url", tag?.cms_page)
                bundle.putString("name", tag?.title)
                mNavController.navigate(R.id.action_navigation_home_to_webViewFragment, bundle)
            }

        }

    }

    init {
        this.posts = posts
        parentFragment = fragment
    }

}
