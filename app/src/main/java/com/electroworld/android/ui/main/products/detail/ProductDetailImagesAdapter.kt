package com.electroworld.android.ui.main.products.detail

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.electroworld.android.R
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.utils.Utils
import kotlinx.android.synthetic.main.image_item.view.imageView
import kotlinx.android.synthetic.main.image_item_product_detail.view.*
import java.util.*
import kotlin.collections.ArrayList


class ProductDetailImagesAdapter(
    val list: ArrayList<String>,
    productDetailFragment: ProductDetailFragment
) : PagerAdapter() {


    private var parentFragment: ProductDetailFragment

    init {
        this.parentFragment = productDetailFragment
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object` as View

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val ctx = container.context
        val view = (ctx as MainActivity).layoutInflater.inflate(
            R.layout.image_item_product_detail,
            container,
            false
        )
        var extension: String = list[position].substring(list[position].lastIndexOf("."))
        extension = extension.toLowerCase()

        val isVideoUrl = extension.toLowerCase() == ".mp4" || extension == ".3gp"
                || list[position].contains("www.youtube.com")


        view.iv_thumbnail.visibility = if (isVideoUrl) View.VISIBLE else View.GONE

        view.iv_thumbnail.alpha = if (isVideoUrl) 0.5f else 1f

        if (list[position].contains("www.youtube.com")) {
            Glide.with(ctx)
                .load(Utils.getThumbnailUrl(list[position]))
                .centerInside()
                .into(view.imageView)
        } else {
            Glide.with(ctx)
                .load(list[position])
                .centerInside()
                .into(view.imageView)
        }


        container.addView(view)
        container.tag = "View$position"

        view.imageView.setOnClickListener {
            if (isVideoUrl) {
                parentFragment.callVideoPlayerActivity("Video", list[position])
            } else {
                val tempList: ArrayList<String> = ArrayList()
                for (strUrl in list) {
                    val ext: String = strUrl.substring(strUrl.lastIndexOf("."))
                    extension = ext.toLowerCase(Locale.getDefault())
                    val isVideo = ext == ".mp4" || ext == ".3gp"
                            || strUrl.contains("www.youtube.com")
                    if (!isVideo) {
                        tempList.add(strUrl)
                    }
                }
                parentFragment.callFullImageActivity(position, tempList)
            }
        }
        return view
    }

    override fun getCount(): Int = list.size

    override fun getItemPosition(`object`: Any): Int = super.getItemPosition(`object`)

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


}