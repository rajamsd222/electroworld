package com.electroworld.android.ui.main.products.compare

import com.electroworld.android.model.ProductModel

data class ProductCompareModel(
    val data: List<ProductModel>
    )