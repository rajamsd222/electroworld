package com.electroworld.android.ui.main.contactus

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.alert.AlertViewSimple
import com.electroworld.android.databinding.FragmentContactUsBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import javax.inject.Inject


class ContactusFragment : DaggerFragment() {

    private lateinit var viewModel: ContactUsViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding : FragmentContactUsBinding


    @Inject
    lateinit var providersFactory: ViewModelProviderFactory
    @Inject
    lateinit var myPref: PreferencesManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_contact_us, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(ContactUsViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentContactUsBinding.bind(view)
        binding.ab.tvABTitle.text = resources.getString(R.string.profile_contact_us)
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }

        binding.tvNumber.setOnClickListener {
            checkPermission()
        }

        binding.tvEmail.setOnClickListener {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", getString(R.string.contact_email), null
                )
            )
            startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }
    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CALL_PHONE
            )
            != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.CALL_PHONE
                )) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42
                )
            }
        } else {
            // Permission has already been granted
            callPhone()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                callPhone()
            } else {
                // permission denied, boo! Disable the
                // functionality
            }
            return
        }
    }

    private fun callPhone(){
        val avcDialog = AlertViewSimple(
            requireActivity(),
            object : AlertViewSimple.OKClickListener {
                override fun onOKClick(view: View?) {
                    val value = "91" + resources.getString(R.string.contact_number)
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$value"))
                    startActivity(intent)
                }
            })
        avcDialog.setCancelable(true)
        avcDialog.setDialogMessage("Are you sure want to make a call?")
        avcDialog.setPositiveText("Yes")
        avcDialog.setNegativeText("No")
        avcDialog.setShowNegativeButton(true)
        avcDialog.showDialog()
    }
}