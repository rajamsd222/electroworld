package com.electroworld.android.ui.main.orders.detail

import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.DefaultData
import com.electroworld.android.model.ReOrderModel
import com.electroworld.android.model.orderdetail.OrderDetailModel
import com.electroworld.android.repository.OMSRepository
import com.electroworld.android.repository.PIMSRepository
import io.reactivex.Single
import javax.inject.Inject

class OrderDetailViewModel @Inject constructor(val mainApi: MainApi,val myPref : PreferencesManager,
                                               private val repoPIMS : PIMSRepository,private val repoOMS : OMSRepository
) : ViewModel() {

    fun makeOrderDetailReq(order_id: String): Single<ApiResponse<OrderDetailModel>> {
        return repoOMS.makeOrderDetailReq(order_id)
    }

    fun makeOrderFavouriteReq(order_id: String): Single<ApiResponse<DefaultData>> {
        return repoOMS.doFavouriteOrder(order_id)
    }

    fun makeOrderCancelReq(order_id: String): Single<ApiResponse<DefaultData>> {
        return repoOMS.doCancelOrder(order_id)
    }

    fun updateProductRatingReq(
        rating: String,
        rating_descriptions: String,
        product_id: String
    ): Single<ApiResponse<ApiResponseDefault>> {
        return repoPIMS.updateProductRatingReq(
            rating,
            rating_descriptions,
            "",
            "1",
            product_id
        )
    }

    fun makeReOrderReq(order_id: String): Single<ApiResponse<ReOrderModel>> {
        return repoOMS.doReOrder(order_id)
    }

}