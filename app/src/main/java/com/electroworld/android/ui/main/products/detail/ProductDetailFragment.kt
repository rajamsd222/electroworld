package com.electroworld.android.ui.main.products.detail

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.databinding.FragmentProductDetailBinding
import com.electroworld.android.databinding.ListItemVariantNewuiBinding
import com.electroworld.android.databinding.ListItemVariantUpdatedUiBinding
import com.electroworld.android.databinding.RowItemProductRatingBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.*
import com.electroworld.android.repository.PIMSRepository
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.products.GroupVariantsModel
import com.electroworld.android.ui.main.products.ProductsGridAdapter
import com.electroworld.android.ui.main.products.VariantListAdapterDetail
import com.electroworld.android.ui.main.products.compare.SpecificationsModel
import com.electroworld.android.ui.main.products.detail.image.FullScreenImageViewActivity
import com.electroworld.android.ui.main.products.detail.image.VideoPlayerActivity
import com.electroworld.android.ui.main.products.list.ProductListFragment
import com.electroworld.android.utils.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_title_cart.view.*
import javax.inject.Inject

class ProductDetailFragment : DaggerFragment(), ProductsGridAdapter.ProductInterface {

    private lateinit var viewModel: ProductDetailViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentProductDetailBinding
    var mSimilarItemsAdapter: ProductsGridAdapter? = null
    var mGridAdapter: ProductsGridAdapter? = null

    var groupId = ""
    var warehouse_product_id = ""
    var currentModel: ProductModel? = null

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager
    var groupList: List<ProductModel> = ArrayList()
    var variantsList = arrayListOf<GroupVariantsModel>()

    @Inject
    lateinit var pims: PIMSRepository

    var bottomSheetDialog: BottomSheetDialog? = null
    val list: MutableList<GroupVariantsModel> = java.util.ArrayList()

    private val disposable = CompositeDisposable()
    private var mRatingListAdapter: GenericAdapter<ProductRatingModel, RowItemProductRatingBinding>? =
        null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_detail, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, providersFactory).get(ProductDetailViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentProductDetailBinding.bind(view)
        groupId = arguments?.getString("group_id")!!
        warehouse_product_id = arguments?.getString("warehouse_product_id")!!

        initRatingRecyclerView()
        initFrequentlyBroughtRecyclerView()
        initRecyclerView()
        binding.ab.tvABTitle.text = "Product Detail"
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }

        binding.tvBrandName.setOnClickListener {
            val bundle = ProductListFragment.getProductDetailBundles(
                currentModel?.brand_id,
                currentModel?.brand_name
            )
            mNavController.navigate(
                R.id.action_productDetailFragment_to_productListFragment,
                bundle
            )
        }

        binding.ab.ivSearch.setOnClickListener {
            mNavController.navigate(R.id.action_productDetailFragment_to_navigation_search)
        }
        binding.ab.iconCart.setOnClickListener {
            mNavController.navigate(R.id.action_productDetailFragment_to_cartListFragment)
        }
        binding.shareLayout.setOnClickListener {
            //var shareText = AppUtils.getShareTextForProduct(currentModel)
            val shareText = currentModel?.share_link
            val groupId = currentModel?.group_id
            AppUtils.openShareIntent(requireActivity(), binding.tempImage, shareText, groupId)
        }

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Description"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Specifications"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Ratings"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Long Description"))

        getProductDetails()
        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> {
                        showProductInfoView()
                    }
                    1 -> {
                        showDescriptionView()
                    }
                    2 -> {
                        binding.infoLayout.setGone()
                        binding.dynamicSpec.setGone()
                        binding.tvLongDesc.setGone()
                        getRatingList()
                    }
                    3 -> {
                        binding.infoLayout.setGone()
                        binding.dynamicSpec.setGone()
                        binding.tvEmptyMsg.setGone()
                        binding.rvRatingsList.setGone()
                        binding.tvLongDesc.setVisible()
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })


    }

    private fun showDescriptionView() {
        //binding.tcDesc.text = Html.fromHtml(AppValidator.toStr(currentModel?.description_en))
        binding.infoLayout.setGone()
        binding.dynamicSpec.setVisible()
        binding.rvRatingsList.setGone()
        binding.tvEmptyMsg.setGone()
        binding.tvLongDesc.setGone()
    }

    private fun showProductInfoView() {
        //binding.tcDesc.text = Html.fromHtml(AppValidator.toStr(currentModel?.information_en))
        binding.infoLayout.setVisible()
        binding.dynamicSpec.setGone()
        binding.rvRatingsList.setGone()
        binding.tvEmptyMsg.setGone()
        binding.tvLongDesc.setGone()
    }

    private fun initFrequentlyBroughtRecyclerView() {
        mGridAdapter = ProductsGridAdapter(
            this,
            requireActivity() as MainActivity,
            myPref,
            ScreenType.PRODUCT_DETAIL_FREQUENTLY_BROUGHT,
            this,
            ""
        )
        binding.recyclerViewFrequently.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerViewFrequently.adapter = mGridAdapter
    }

    private fun loadFrequentlyProductsList() {
        disposable.add(
            pims.getFrequentlyProductsList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<ProductModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<ProductModel>>) {
                        if (res.http_code == 200) {
                            updateFreqBroughtsList(res.data)
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun updateFreqBroughtsList(data: List<ProductModel>) {
        mGridAdapter?.clearAll()
        mGridAdapter?.setItems(data)
        mGridAdapter?.notifyDataSetChanged()
        if (mGridAdapter?.itemCount == 0) {
            binding.layoutFreqProducts.setGone()
        } else {
            binding.layoutFreqProducts.setVisible()
        }
    }

    private fun initRatingRecyclerView() {
        mRatingListAdapter = object :
            GenericAdapter<ProductRatingModel, RowItemProductRatingBinding>(R.layout.row_item_product_rating) {
            override fun onBindData(
                model: ProductRatingModel?,
                position: Int,
                dataBinding: RowItemProductRatingBinding?
            ) {
                dataBinding?.item = model
            }

            override fun onItemClick(model: ProductRatingModel?, position: Int) {}
        }
        binding.rvRatingsList.apply {
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            adapter = mRatingListAdapter
        }
    }

    private fun getRatingList() {
        //binding.tvEmptyMsg.setGone()
        val pId = currentModel?.product_id ?: return
        disposable.add(
            pims.getProductReviewsList(pId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<ProductRatingModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<ProductRatingModel>>) {
                        if (res.http_code == 200) {
                            mRatingListAdapter?.clearAll()
                            if (res.data != null)
                                mRatingListAdapter?.addItems(res.data)
                            binding.rvRatingsList.setVisible()
                            if (mRatingListAdapter?.itemCount == 0) {
                                binding.tvEmptyMsg.setVisible()
                            } else {
                                binding.tvEmptyMsg.setGone()
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun getProductDetails() {
        binding.nestedScrollView.setGone()
        binding.bottomLayout.setGone()
        disposable.add(
            pims.getProductDetailsNew(warehouse_product_id, groupId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<ProductDetailModel>>() {
                    override fun onSuccess(res: ApiResponse<ProductDetailModel>) {
                        binding.nestedScrollView.setVisible()
                        binding.bottomLayout.setVisible()
                        if (res.http_code == 200) {
                            loadVariants(
                                res.data.product_variant,
                                res.data.products.warehouse_product_id
                            )
                            updateViews(res.data.products)

                            makeSimilarProductReq()
                            callOffersListAPi()
                            loadFrequentlyProductsList()
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun makeSimilarProductReq() {
        disposable.add(
            pims.getSimilarProducts(groupId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<ProductModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<ProductModel>>) {
                        if (res.http_code == 200) {
                            updateSimilarProductsList(res.data)
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun updateSimilarProductsList(data: List<ProductModel>) {
        mSimilarItemsAdapter?.clearAll()
        mSimilarItemsAdapter?.setItems(data)
        mSimilarItemsAdapter?.notifyDataSetChanged()
        if (mSimilarItemsAdapter?.itemCount == 0) {
            binding.layoutSimilarProducts.setGone()
        } else {
            binding.layoutSimilarProducts.setVisible()
        }
    }

    private fun callOffersListAPi() {
        disposable.add(
            pims.getMobileAdBlocks("product")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<HomeModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<HomeModel>>) {
                        if (res.http_code == 200) {
                            binding.offersListLayout.removeAllViews()
                            res.data.forEach {
                                if (it.type.equals("4")) {
                                    binding.cvOfferslist.visibility = View.VISIBLE
                                    val offersList = ArrayList<String>()
                                    it.placeholders.forEach {
                                        offersList.add(it.title)
                                    }
                                    addOffersListLayout(binding.offersListLayout, offersList)
                                }
                            }

                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun getSelectedVariantModel(warehouseProductId: String): ProductModel? {
        groupList.forEach {
            if (it.warehouse_product_id == warehouseProductId) {
                return it
            }
        }
        return null
    }

    private fun updateViews(model: ProductModel?) {
        binding.item = model
        if (model != null) {


            currentModel = model
            binding.tabLayout.getTabAt(0)?.select()
            showProductInfoView()
            binding.tvStoreName.text = myPref.getPrefKeyStoreName()
            val imagesList: ArrayList<String> = ArrayList()
            //imagesList.add("https://www.youtube.com/watch?v=021K-QZY0zw&ab_channel=Mr.Spoiler")
            //imagesList.add("https://www.youtube.com/watch?v=smvIlcQIqI0&ab_channel=GSMDome")
            currentModel?.images?.forEach {
                imagesList.add(it.image)
            }
            binding.imageViewPager.adapter = ProductDetailImagesAdapter(imagesList, this)
            binding.dotsIndicator.setViewPager(binding.imageViewPager)
            binding.imageViewPager.adapter?.registerDataSetObserver(binding.dotsIndicator.dataSetObserver)

            if (imagesList.size <= 1) {
                binding.dotsIndicator.visibility = View.GONE
            } else {
                binding.dotsIndicator.visibility = View.VISIBLE
            }


            binding.tvDelivery.text = model.first_delivery_slot_text
            updateProductDetailFavUI(currentModel?.is_favourite)
            updateProductDetailAddToCartUi(currentModel?.warehouse_product_id)
            binding.favLayout.setOnClickListener {
                if (!myPref.isLogin()) {
                    (activity as MainActivity).showLoginDialog(activity?.getString(R.string.login_alert_for_fav))
                } else {
                    currentModel?.let { it1 -> setAsFavouriteProduct(it1) }
                }
            }

            runDynamicSpecifications(
                binding.dynamicSpecificationLayout,
                model.specification
            )


            binding.tvAdd.setOnClickListener {
                doAddToCart(
                    true,
                    false,
                    1
                )
            }
            binding.tvIncrement.setOnClickListener {
                doAddToCart(
                    true, false, 1
                )
            }
            binding.tvDecrement.setOnClickListener {
                doAddToCart(
                    false, false, 1
                )
            }

            // loadVariants(model.warehouse_product_id)


            binding.tvBuyNow.visibility = if (model.available_qty == 0) View.GONE else View.VISIBLE

            binding.tvBuyNow.setOnClickListener {

                if (!myPref.isLogin()) {
                    (requireActivity() as MainActivity).showLoginDialog(
                        requireActivity().getString(
                            R.string.alert_login_checkout
                        )
                    )
                } else {
                    currentModel?.let {
                        val isAdded = CartUtils.addToCartProductList(
                            requireActivity() as MainActivity, myPref,
                            it, true, false, 1
                        )
                        if (isAdded) {
                            val deliveryAddressStoreId = myPref.checkStoreModelAdded()
                            if (deliveryAddressStoreId.isNotEmpty()) {
                                val modelAd = myPref.getDefaultAddress()
                                if (modelAd != null) {
                                    mNavController.navigate(R.id.action_productDetailFragment_to_checkOutFragment)
                                } else {
                                    Utils.showWarning(
                                        requireActivity(),
                                        getString(R.string.buy_now_hint)
                                    )
                                    mNavController.navigate(R.id.action_productDetailFragment_to_cartListFragment)
                                }
                            } else {
                                Utils.showWarning(
                                    requireActivity(),
                                    getString(R.string.buy_now_hint)
                                )
                                mNavController.navigate(R.id.action_productDetailFragment_to_cartListFragment)
                            }
                        }
                    }
                }


            }

            binding.tvGroupVariant.setOnClickListener {
                currentModel?.let { it1 -> showVariantDialog(it1) }
            }

            refresh(model.warehouse_product_id)
            binding.compareLayout.setOnClickListener {
                val isAdded = CartUtils.addToCompare(
                    null,
                    requireActivity() as MainActivity,
                    myPref,
                    model.product_id,
                    model.group_id,
                    model.second_level_category_id,
                    if (model.warehouse_product_id == null) "" else model.warehouse_product_id
                )
                refresh(model.warehouse_product_id)
            }


            binding.executePendingBindings()
        }
    }

    fun runDynamicSpecifications(
        dynamicSpecificationLayout: LinearLayout,
        specification: List<SpecificationsModel>
    ) {
        dynamicSpecificationLayout.removeAllViews()
        for (item in specification) {
            val child = layoutInflater.inflate(
                R.layout.row_item_specification,
                dynamicSpecificationLayout,
                false
            )
            val title = child.findViewById<TextView>(R.id.tv_key)
            val value = child.findViewById<TextView>(R.id.tv_value)
            title.text = SystemUtils.capitalize(item.specification_name)
            value.text = SystemUtils.capitalize(item.name_en)
            dynamicSpecificationLayout.addView(child)
        }
    }

    fun refresh(warehouseProductId: String) {

        val isExists = CartUtils.isExists(
            myPref,
            warehouseProductId
        )


        binding.ivCompare.setColorFilter(
            ContextCompat.getColor(
                requireActivity(),
                if (isExists) R.color.bottom_tab_color_selected else R.color.bottom_tab_color_default
            )
        )

        binding.compareTxt.text = if (isExists)
            "Remove from compare" else "Add to compare"

        binding.compareTxt.setTextColor(
            if (isExists) resources.getColor(R.color.bottom_tab_color_selected) else
                resources.getColor(R.color.bottom_tab_color_default)
        )
    }

    private fun loadVariants(
        variantList: List<VariantModel>,
        variantItemId: String
    ) {

        val titleAdapter: GenericAdapter<VariantModel, ListItemVariantUpdatedUiBinding> =
            object :
                GenericAdapter<VariantModel, ListItemVariantUpdatedUiBinding>(R.layout.list_item_variant_updated_ui) {
                override fun onBindData(
                    variantModel: VariantModel,
                    position: Int,
                    dataBinding: ListItemVariantUpdatedUiBinding
                ) {
                    dataBinding.item = variantModel

                    val viewPool = RecyclerView.RecycledViewPool()

                    val variantAdapter: GenericAdapter<VariantItem, ListItemVariantNewuiBinding> =
                        object :
                            GenericAdapter<VariantItem, ListItemVariantNewuiBinding>(
                                R.layout.list_item_variant_newui
                            ) {
                            override fun onBindData(
                                model: VariantItem,
                                position: Int,
                                childBinding: ListItemVariantNewuiBinding
                            ) {
                                childBinding.variantItem = model

                                childBinding.tvName.text = model.value

                                childBinding.parentLayout.visibility = if (model.value == null ||
                                    model.value.isEmpty()
                                ) View.GONE else View.VISIBLE


                                childBinding.parentLayout.setBackgroundResource(
                                    if (variantItemId == model.id) R.drawable.variant_selected_bg_new else
                                        R.drawable.variant_default_bg
                                )
                                childBinding.executePendingBindings()
                            }

                            override fun onItemClick(model: VariantItem?, position: Int) {
                                // call get variant api to get the product details ---
                                warehouse_product_id = model!!.id
                                groupId = model.group_id
                                getProductDetails()

                            }
                        }


                    val childLayoutManager = LinearLayoutManager(
                        dataBinding.recyclerViewVariantItem.context,
                        LinearLayoutManager.HORIZONTAL,
                        false
                    )
                    //childLayoutManager.initialPrefetchItemCount = model.variant_data.size

                    dataBinding.recyclerViewVariantItem.apply {
                        layoutManager = childLayoutManager
                        adapter = variantAdapter
                        setRecycledViewPool(viewPool)
                    }
                    variantAdapter.items = variantModel.variant_data

                    dataBinding.executePendingBindings()


                }

                override fun onItemClick(model: VariantModel?, position: Int) {
                }
            }

        val manager = LinearLayoutManager(activity)
        binding.layoutVariant.apply {
            layoutManager = manager
            adapter = titleAdapter
        }
        titleAdapter.items = variantList

    }

    private fun showVariantDialog(model: ProductModel) {
        val dialogView: View =
            requireActivity().layoutInflater.inflate(R.layout.dialog_variant, null)
        bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.CustomBottomSheetDialog)
        bottomSheetDialog!!.setContentView(dialogView)
        var tvTitle = bottomSheetDialog!!.findViewById<TextView>(R.id.tvTitle)
        var recyclerView = bottomSheetDialog!!.findViewById<RecyclerView>(R.id.recycler_view)
        var adapter = VariantListAdapterDetail(
            variantsList,
            this,
            requireActivity(),
            bottomSheetDialog,
            model
        )
        recyclerView?.adapter = adapter
        tvTitle!!.text = model.name_en  /*+ " - " + model.variant_en*/
        bottomSheetDialog!!.show()
    }

    fun updateVariants(warehouseProductId: String) {
        /*if(getSelectedVariantModel(warehouseProductId)!=null){
            updateViews(getSelectedVariantModel(warehouseProductId))
        }*/
        getProductInfoByVariantId(-1, currentModel, warehouseProductId)
    }

    fun getProductInfoByVariantId(
        pos: Int,
        model: ProductModel?,
        warehouseProductId: String
    ) {
        disposable.add(
            pims.getProductInfoReq(warehouseProductId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<ProductModel>>() {
                    override fun onSuccess(res: ApiResponse<ProductModel>) {
                        if (res.http_code == 200) {
                            if (res.data != null) {
                                val item: ProductModel? = res.data
                                if (item != null) {
                                    if (pos == -1) {
                                        //item.group_variants = model.group_variants
                                        updateViews(item)
                                    } else {
                                        item.group_variants = model?.group_variants!!
                                        mSimilarItemsAdapter?.updateItem(pos, item)
                                        mSimilarItemsAdapter?.notifyDataSetChanged()
                                    }
                                }

                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun updateProductDetailAddToCartUi(warehouse_product_id: String?) {
        val singleItemQuantity: Int =
            CartUtils.getSingleItemQuantity(
                myPref,
                AppValidator.toStr(warehouse_product_id)
            )
        if (singleItemQuantity > 0) {
            binding.tvAdd.visibility = View.GONE
            binding.layCount.visibility = View.VISIBLE
        } else {
            binding.tvAdd.visibility = View.VISIBLE
            binding.layCount.visibility = View.GONE
        }

        binding.tvBuyNow.visibility = if (singleItemQuantity > 0) View.GONE else View.VISIBLE

        binding.tvQty.text = singleItemQuantity.toString()
    }

    fun doAddToCart(isAdd: Boolean, isBulkUpdate: Boolean, selectedQty: Int) {
        currentModel?.let {
            CartUtils.addToCartProductList(
                requireActivity() as MainActivity, myPref,
                it, isAdd, isBulkUpdate, selectedQty
            )
        }
        mSimilarItemsAdapter?.notifyDataSetChanged()
        mGridAdapter?.notifyDataSetChanged()
        updateProductDetailAddToCartUi(currentModel?.warehouse_product_id)
        updateCartCountUI()
    }

    fun doAddToCart(model: ProductModel, isAdd: Boolean, isBulkUpdate: Boolean, selectedQty: Int) {
        CartUtils.addToCartProductList(
            requireActivity() as MainActivity,
            myPref,
            model,
            isAdd,
            isBulkUpdate,
            selectedQty
        )
        mSimilarItemsAdapter?.notifyDataSetChanged()
        mGridAdapter?.notifyDataSetChanged()
        updateProductDetailAddToCartUi(currentModel?.warehouse_product_id)
        updateCartCountUI()
    }

    fun setAsFavouriteProduct(model: ProductModel) {
        val setfavValue = if (model.is_favourite == 1) 0 else 1
        disposable.add(
            pims.doFavReq(model, setfavValue)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<SetFavModel>>() {
                    override fun onSuccess(res: ApiResponse<SetFavModel>) {
                        if (res.http_code == 200) {
                            updateAllFavouriteItems(res.data)
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun updateAllFavouriteItems(data: SetFavModel) {
        (activity as MainActivity?)?.setFavouriteCount(data.total_fav_count)

        //product detail ui update
        if (currentModel?.warehouse_product_id == data.warehouse_product_id &&
            currentModel?.product_id == data.product_id &&
            currentModel?.group_id == data.group_id
        ) {
            currentModel?.is_favourite = data.is_favourite
            updateProductDetailFavUI(currentModel?.is_favourite)
        }
        //to update similar products fav icon
        mSimilarItemsAdapter?.getItems()?.forEachIndexed { index, model ->
            if (model.warehouse_product_id == data.warehouse_product_id &&
                model.product_id == data.product_id &&
                model.group_id == data.group_id
            ) {
                model.is_favourite = data.is_favourite
                mSimilarItemsAdapter?.updateItem(index, model)
            }
        }
        //to update frequently products fav icon
        mGridAdapter?.getItems()?.forEachIndexed { index, model ->
            if (model.warehouse_product_id == data.warehouse_product_id &&
                model.product_id == data.product_id &&
                model.group_id == data.group_id
            ) {
                model.is_favourite = data.is_favourite
                mGridAdapter?.updateItem(index, model)
            }
        }

        if (data.is_favourite == 1) {
            Utils.showApiSuccess(activity, "Product added to favourites list")
        } else if (data.is_favourite == 0) {
            Utils.showApiSuccess(activity, "Product removed from favourites list")
        }
    }

    private fun updateProductDetailFavUI(isFavourite: Int?) {
        binding.ivFav.setImageResource(if (isFavourite == 1) R.drawable.ic_favorite_color_primary_24dp else R.drawable.ic_favorite_border_black_24dp)
        binding.tvFavLbl.text = if (isFavourite == 1) activity?.resources?.getString(
            R.string.remove_my_list
        ) else activity?.resources?.getString(R.string.save_my_list)
    }

    fun initRecyclerView() {
        mSimilarItemsAdapter = ProductsGridAdapter(
            this,
            requireActivity() as MainActivity,
            myPref,
            ScreenType.PRODUCT_DETAIL_SIMILAR_PRODUCT,
            this,
            ""
        )
        binding.recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView.adapter = mSimilarItemsAdapter
    }

    private fun addOffersListLayout(parentLayout: LinearLayout, offersList: ArrayList<String>) {
        if (offersList.size > 0) {
            parentLayout.removeAllViews()
            for (str in offersList) {
                val itemView = LayoutInflater.from(requireActivity())
                    .inflate(R.layout.row_item_offer_list, parentLayout, false)
                val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
                tvTitle.text = str
                parentLayout.addView(itemView)
            }
        }
    }

    fun openDetailsScreen(model: ProductModel) {
        val bundle = Bundle()
        bundle.putString("name", model.name_en)
        bundle.putString("group_id", model.group_id)
        bundle.putString("warehouse_product_id", model.warehouse_product_id)
        mNavController.navigate(R.id.productDetailFragment, bundle)
    }

    override fun onResume() {
        super.onResume()
        updateCartCountUI()
    }

    private fun updateCartCountUI() {
        binding.ab.tvCartCount.text = myPref.getCartCount().toString()
        when {
            myPref.getCartCount() > 0 -> {
                binding.ab.tvCartCount.visibility = View.VISIBLE
            }
            else -> {
                binding.ab.tvCartCount.visibility = View.GONE
            }
        }
    }

    fun callFullImageActivity(position: Int, list: ArrayList<String>) {
        val i = Intent(requireActivity(), FullScreenImageViewActivity::class.java)
        i.putExtra("imageList", list)
        i.putExtra("position", position)
        startActivity(i)
    }

    fun callVideoPlayerActivity(title: String, url: String) {
        val i = Intent(requireActivity(), VideoPlayerActivity::class.java)
        i.putExtra("title", title)
        i.putExtra("url", url)
        startActivity(i)
    }

    override fun onDestroyView() {
        disposable.clear()
        super.onDestroyView()
    }

    override fun addItem(productModel: ProductModel, quantity: Int) {

    }
}