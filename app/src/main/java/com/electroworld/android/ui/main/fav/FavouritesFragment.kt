package com.electroworld.android.ui.main.fav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setInvisible
import com.electroworld.android.extentions.setVisible
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.customclass.VerticalSpacingItemDecoration
import com.electroworld.android.databinding.FragmentFavouritesBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.SetFavModel
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartUtils
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_title_cart.view.*
import javax.inject.Inject

class FavouritesFragment : DaggerFragment() {


    private lateinit var viewModel: FavouritesViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding : FragmentFavouritesBinding
    var mAdapter: FavListAdapter? = null

    private var mLayoutManager: LinearLayoutManager? = null
    @Inject
    lateinit var providersFactory: ViewModelProviderFactory
    @Inject
    lateinit var myPref: PreferencesManager
    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favourites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, providersFactory).get(FavouritesViewModel::class.java)
        mNavController = Navigation.findNavController(view)
        binding = FragmentFavouritesBinding.bind(view)
        binding.ab.tvABTitle.text = "My Favourites"
        binding.ab.ivBack.setOnClickListener(View.OnClickListener {
            mNavController.navigateUp()
        })
        binding.ab.ivSearch.setGone()
        binding.ab.iconCart.setOnClickListener(View.OnClickListener {
            mNavController.navigate(R.id.action_favouritesFragment_to_cartListFragment)
        })
        initRecyclerView()
        makeReq()
        viewModel.getApiResponse().observe(viewLifecycleOwner) { res -> consumeResponse(res) }
        viewModel.getFavResponse().observe(viewLifecycleOwner) { res -> consumeFavResponse(res) }
    }


    override fun onResume() {
        super.onResume()
        updateCartCountUI()
    }

    private fun updateCartCountUI() {
        binding.ab.tvCartCount.text = myPref.getCartCount().toString()
        when {
            myPref.getCartCount()>0 -> {
                binding.ab.tvCartCount.visibility = View.VISIBLE
            }
            else -> {
                binding.ab.tvCartCount.visibility = View.GONE
            }
        }
    }

    private fun consumeResponse(res: ApiResource<ApiResponse<List<ProductModel>>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            mAdapter?.setItems(res.data.data)
                            mAdapter?.notifyDataSetChanged()
                            if (mAdapter?.itemCount == 0) {
                                binding.tvEmptyMsg.setVisible()
                            } else {
                                binding.tvEmptyMsg.setGone()
                            }
                        } else {
                            CustomDialog.show(requireActivity(),res.data.message, AlertType.ERROR)
                        }
                    } else {
                        binding.tvEmptyMsg.setVisible()
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.tvEmptyMsg.setVisible()
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(),res.message,AlertType.SERVER_ERROR)
                }
            }
        }
    }

    private fun makeReq() {
        binding.tvEmptyMsg.setGone()
        viewModel.getList()
    }
    private fun initRecyclerView() {
        mAdapter = FavListAdapter(this, requireActivity(),myPref)
        mLayoutManager = GridLayoutManager(activity, 3)
        binding.recyclerView.layoutManager = mLayoutManager

        val itemDecoration = VerticalSpacingItemDecoration(10)
        binding.recyclerView.addItemDecoration(itemDecoration)

        binding.recyclerView.adapter = mAdapter
    }

    fun setAsFavouriteProduct(pos: Int, item: ProductModel) {
        viewModel.setAsFav(
            item.warehouse_product_id,
            item.product_id,
            0,
            item.group_id
        )

    }

    private fun consumeFavResponse(res: ApiResource<ApiResponse<SetFavModel>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            makeReq()
                        } else {
                            CustomDialog.show(requireActivity(),res.data.message,AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(),res.message,AlertType.SERVER_ERROR)
                }
            }
        }
    }
    fun doAddToCart(model: ProductModel, isAdd: Boolean, isBulkUpdate: Boolean, selectedQty: Int) {
        CartUtils.addToCartProductList(requireActivity() as MainActivity,myPref, model, isAdd, isBulkUpdate, selectedQty)
        mAdapter?.notifyDataSetChanged()
        updateCartCountUI()
    }

    fun openDetailsScreen(model: ProductModel){
        val bundle = Bundle()
        bundle.putString("name", model.name_en)
        bundle.putString("group_id", model.group_id)
        bundle.putString("warehouse_product_id", model.warehouse_product_id)
        mNavController.navigate(R.id.action_favouritesFragment_to_productDetailFragment, bundle)
    }

    fun getProductInfoByVariantId(pos: Int, model: ProductModel, warehouseProductId: String) {

        disposable.add(
            viewModel.makeproductInfoReq(warehouseProductId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<ProductModel>>() {
                    override fun onSuccess(res: ApiResponse<ProductModel>) {
                        if (res.http_code === 200) {
                            if (res.data != null) {
                                val item: ProductModel? = res.data
                                if (item != null) {
                                    item.group_variants = model.group_variants
                                    mAdapter?.updateItem(pos, item)
                                }
                                mAdapter?.notifyDataSetChanged()
                            }
                        } else {
                            CustomDialog.show(requireActivity(),res.message,AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(),e.message,AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    override fun onDestroyView() {
        disposable.clear()
        super.onDestroyView()
    }
}