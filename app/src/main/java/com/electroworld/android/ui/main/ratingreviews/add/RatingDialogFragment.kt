package com.electroworld.android.ui.main.ratingreviews.add

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import com.electroworld.android.R
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.RatingReviewsListModel
import com.electroworld.android.repository.PIMSRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RatingDialogFragment : DialogFragment() {

    @Inject
    lateinit var myPref: PreferencesManager

    lateinit var repo: PIMSRepository

    private val disposable = CompositeDisposable()
    companion object{

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(
            R.layout.alert_rating, container,
            false
        )
        if (dialog != null && dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        }
        repo.getRatingReviewsList()
        // getDialog().setTitle("About DialogFragment");

        // ((TextView)rootView.findViewById(R.id.about_fragment)).setText("As a DialogFragment is also a Fragment that displays a dialog window, floating on top of its activity's window. This fragment contains a Dialog object, which it displays as appropriate based on the fragment's state.");
        return rootView
    }

    private fun getRatingList() {
        disposable.add(
            repo.getRatingReviewsList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                   // binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                   // binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<RatingReviewsListModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<RatingReviewsListModel>>) {
                        if (res.http_code == 200) {

                        } else {
                            CustomDialog.show(requireActivity(),res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(),e.message,AlertType.SERVER_ERROR)
                    }
                })
        )
    }
}