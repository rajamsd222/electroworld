package com.electroworld.android.ui.main.products

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.electroworld.android.R
import com.electroworld.android.databinding.RowItemProductGridNewBinding
import com.electroworld.android.databinding.RowItemProductListBinding
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.ProductModel
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.offers.OffersFragment
import com.electroworld.android.ui.main.products.list.ProductListFragment
import com.electroworld.android.ui.main.products.search.SearchFragment
import com.electroworld.android.ui.main.quickbuy.QuickBuyFragment
import com.electroworld.android.utils.ScreenType
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.util.*

class ProductListAdapter(
    fragment: Fragment,
    val context: MainActivity, private val myPref: PreferencesManager
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items = arrayListOf<ProductModel>()
    var parentFragment: Fragment = fragment
    var bottomSheetDialog: BottomSheetDialog? = null
    val list: MutableList<GroupVariantsModel> = ArrayList()
    var isListView: Boolean = true
    val LIST_ITEM: Int = 0
    val GRID_ITEM: Int = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        if (viewType == LIST_ITEM) {
            val itemBinding: RowItemProductListBinding =
                RowItemProductListBinding.inflate(layoutInflater, parent, false)
            return ViewHolder(itemBinding)
        } else {
            val itemBinding: RowItemProductGridNewBinding =
                RowItemProductGridNewBinding.inflate(layoutInflater, parent, false)
            return ViewHolderGrid(itemBinding)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (isListView) {
            (holder as ViewHolder).bind(position)
        } else {
            (holder as ViewHolderGrid).bind(position)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    public fun toggleItemViewType(): Boolean {
        isListView = !isListView;
        return isListView;
    }

    override fun getItemViewType(position: Int): Int {
        if (isListView) {
            return LIST_ITEM
        } else {
            return GRID_ITEM
        }
    }

    inner class ViewHolder(binding: RowItemProductListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val binding: RowItemProductListBinding = binding
        fun bind(position: Int) {
            val model: ProductModel = items[position]
            binding.item = model
            binding.executePendingBindings()

            if (model.group_variants != null && model.group_variants.size > 1) {
                binding.tvGroupVariant.visibility = View.VISIBLE
                binding.tvVariantName.visibility = View.GONE
            } else {
                if (model.variant1 != null && model.variant1 != null) {
                    binding.tvVariantName.visibility = View.VISIBLE
                } else {
                    binding.tvVariantName.visibility = View.GONE
                }
                binding.tvGroupVariant.visibility = View.GONE
            }
            binding.ivCompare.setColorFilter(
                ContextCompat.getColor(
                    context,
                    if (CartUtils.isExists(
                            myPref,
                            if (model.warehouse_product_id == null) "0" else model.warehouse_product_id
                        )
                    ) R.color.bottom_tab_color_selected else R.color.bottom_tab_color_default
                )
            )

            binding.compareLayout.setOnClickListener {
                val isAdded = CartUtils.addToCompare(
                    this@ProductListAdapter,
                    context,
                    myPref,
                    model.product_id,
                    model.group_id,
                    model.second_level_category_id,
                    if (model.warehouse_product_id == null) "0" else model.warehouse_product_id
                )
                notifyDataSetChanged()
            }


            binding.ivFav.setOnClickListener {
                if (!myPref.isLogin()) {
                    context.showLoginDialog(context.getString(R.string.login_alert_for_fav))
                } else {
                    binding.ivFav.isEnabled = false
                    if (parentFragment is ProductListFragment) {
                        (parentFragment as ProductListFragment).setAsFavouriteProduct(
                            position, items.get(
                                position
                            )
                        )
                    } else if (parentFragment is SearchFragment) {
                        (parentFragment as SearchFragment).setAsFavouriteProduct(
                            position, items.get(
                                position
                            )
                        )
                    } else if (parentFragment is OffersFragment) {
                        (parentFragment as OffersFragment).setAsFavouriteProduct(
                            position, items.get(
                                position
                            )
                        )
                    } else if (parentFragment is QuickBuyFragment) {
                        (parentFragment as QuickBuyFragment).setAsFavouriteProduct(
                            position, items.get(
                                position
                            )
                        )
                    }
                    Handler(Looper.getMainLooper()).postDelayed({
                        binding.ivFav.isEnabled = true
                    }, 1500)

                }
            }

            /* binding.tvAdd.setOnClickListener {
                 if (parentFragment is ProductListFragment) {
                     (parentFragment as ProductListFragment).doAddToCart(
                         items[position],
                         true,
                         false,
                         1
                     )
                 } else if (parentFragment is SearchFragment) {
                     (parentFragment as SearchFragment).doAddToCart(
                         items[position],
                         true,
                         false,
                         1
                     )
                 } else if (parentFragment is OffersFragment) {
                     (parentFragment as OffersFragment).doAddToCart(
                         items[position],
                         true,
                         false,
                         1
                     )
                 } else if (parentFragment is QuickBuyFragment) {
                     (parentFragment as QuickBuyFragment).doAddToCart(
                         items[position],
                         true,
                         false,
                         1
                     )
                 }
             }
             binding.tvIncrement.setOnClickListener {
                 if (parentFragment is ProductListFragment) {
                     (parentFragment as ProductListFragment).doAddToCart(
                         items[position], true, false, 1
                     )
                 } else if (parentFragment is SearchFragment) {
                     (parentFragment as SearchFragment).doAddToCart(
                         items[position], true, false, 1
                     )
                 } else if (parentFragment is OffersFragment) {
                     (parentFragment as OffersFragment).doAddToCart(
                         items[position], true, false, 1
                     )
                 } else if (parentFragment is QuickBuyFragment) {
                     (parentFragment as QuickBuyFragment).doAddToCart(
                         items[position], true, false, 1
                     )
                 }
             }
             binding.tvDecrement.setOnClickListener {
                 if (parentFragment is ProductListFragment) {
                     (parentFragment as ProductListFragment).doAddToCart(
                         items[position], false, false, 1
                     )
                 } else if (parentFragment is SearchFragment) {
                     (parentFragment as SearchFragment).doAddToCart(
                         items[position], false, false, 1
                     )
                 } else if (parentFragment is OffersFragment) {
                     (parentFragment as OffersFragment).doAddToCart(
                         items[position], false, false, 1
                     )
                 } else if (parentFragment is QuickBuyFragment) {
                     (parentFragment as QuickBuyFragment).doAddToCart(
                         items[position], false, false, 1
                     )
                 }
             }
 */

            binding.tvGroupVariant.setOnClickListener {
                showVariantDialog(model, binding.tvGroupVariant, position)
            }
            binding.parentView.setOnClickListener {
                if (parentFragment is ProductListFragment) {
                    (parentFragment as ProductListFragment).openDetailsScreen(model)
                } else if (parentFragment is SearchFragment) {
                    (parentFragment as SearchFragment).openDetailsScreen(model)
                } else if (parentFragment is OffersFragment) {
                    (parentFragment as OffersFragment).openDetailsScreen(model)
                } else if (parentFragment is QuickBuyFragment) {
                    (parentFragment as QuickBuyFragment).openDetailsScreen(model)
                }
            }
        }

    }


    inner class ViewHolderGrid(binding: RowItemProductGridNewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val binding: RowItemProductGridNewBinding = binding
        fun bind(position: Int) {
            val model: ProductModel = items[position]
            binding.item = model
            binding.executePendingBindings()

            if (model.group_variants != null && model.group_variants.size > 1) {
                binding.tvGroupVariant.visibility = View.VISIBLE
                binding.tvVariantName.visibility = View.GONE
            } else {
                if (model.variant1 != null && model.variant1 != null) {
                    binding.tvVariantName.visibility = View.VISIBLE
                } else {
                    binding.tvVariantName.visibility = View.GONE
                }
                binding.tvGroupVariant.visibility = View.GONE
            }
            binding.ivCompare.setColorFilter(
                ContextCompat.getColor(
                    context,
                    if (CartUtils.isExists(
                            myPref,
                            if (model.warehouse_product_id == null) "0" else model.warehouse_product_id
                        )
                    ) R.color.bottom_tab_color_selected else R.color.bottom_tab_color_default
                )
            )

            binding.compareLayout.setOnClickListener {
                val isAdded = CartUtils.addToCompare(
                    this@ProductListAdapter,
                    context,
                    myPref,
                    model.product_id,
                    model.group_id,
                    model.second_level_category_id,
                    if (model.warehouse_product_id == null) "0" else model.warehouse_product_id
                )
                notifyDataSetChanged()
            }


            binding.ivFav.setOnClickListener {
                if (!myPref.isLogin()) {
                    context.showLoginDialog(context.getString(R.string.login_alert_for_fav))
                } else {
                    binding.ivFav.isEnabled = false
                    if (parentFragment is ProductListFragment) {
                        (parentFragment as ProductListFragment).setAsFavouriteProduct(
                            position, items.get(
                                position
                            )
                        )
                    } else if (parentFragment is SearchFragment) {
                        (parentFragment as SearchFragment).setAsFavouriteProduct(
                            position, items.get(
                                position
                            )
                        )
                    } else if (parentFragment is OffersFragment) {
                        (parentFragment as OffersFragment).setAsFavouriteProduct(
                            position, items.get(
                                position
                            )
                        )
                    } else if (parentFragment is QuickBuyFragment) {
                        (parentFragment as QuickBuyFragment).setAsFavouriteProduct(
                            position, items.get(
                                position
                            )
                        )
                    }
                    Handler(Looper.getMainLooper()).postDelayed({
                        binding.ivFav.isEnabled = true
                    }, 1500)

                }
            }

            /* binding.tvAdd.setOnClickListener {
                 if (parentFragment is ProductListFragment) {
                     (parentFragment as ProductListFragment).doAddToCart(
                         items[position],
                         true,
                         false,
                         1
                     )
                 } else if (parentFragment is SearchFragment) {
                     (parentFragment as SearchFragment).doAddToCart(
                         items[position],
                         true,
                         false,
                         1
                     )
                 } else if (parentFragment is OffersFragment) {
                     (parentFragment as OffersFragment).doAddToCart(
                         items[position],
                         true,
                         false,
                         1
                     )
                 } else if (parentFragment is QuickBuyFragment) {
                     (parentFragment as QuickBuyFragment).doAddToCart(
                         items[position],
                         true,
                         false,
                         1
                     )
                 }
             }
             binding.tvIncrement.setOnClickListener {
                 if (parentFragment is ProductListFragment) {
                     (parentFragment as ProductListFragment).doAddToCart(
                         items[position], true, false, 1
                     )
                 } else if (parentFragment is SearchFragment) {
                     (parentFragment as SearchFragment).doAddToCart(
                         items[position], true, false, 1
                     )
                 } else if (parentFragment is OffersFragment) {
                     (parentFragment as OffersFragment).doAddToCart(
                         items[position], true, false, 1
                     )
                 } else if (parentFragment is QuickBuyFragment) {
                     (parentFragment as QuickBuyFragment).doAddToCart(
                         items[position], true, false, 1
                     )
                 }
             }
             binding.tvDecrement.setOnClickListener {
                 if (parentFragment is ProductListFragment) {
                     (parentFragment as ProductListFragment).doAddToCart(
                         items[position], false, false, 1
                     )
                 } else if (parentFragment is SearchFragment) {
                     (parentFragment as SearchFragment).doAddToCart(
                         items[position], false, false, 1
                     )
                 } else if (parentFragment is OffersFragment) {
                     (parentFragment as OffersFragment).doAddToCart(
                         items[position], false, false, 1
                     )
                 } else if (parentFragment is QuickBuyFragment) {
                     (parentFragment as QuickBuyFragment).doAddToCart(
                         items[position], false, false, 1
                     )
                 }
             }
 */

            binding.tvGroupVariant.setOnClickListener {
                showVariantDialog(model, binding.tvGroupVariant, position)
            }
            binding.parentView.setOnClickListener {
                if (parentFragment is ProductListFragment) {
                    (parentFragment as ProductListFragment).openDetailsScreen(model)
                } else if (parentFragment is SearchFragment) {
                    (parentFragment as SearchFragment).openDetailsScreen(model)
                } else if (parentFragment is OffersFragment) {
                    (parentFragment as OffersFragment).openDetailsScreen(model)
                } else if (parentFragment is QuickBuyFragment) {
                    (parentFragment as QuickBuyFragment).openDetailsScreen(model)
                }
            }

            if (model.discount_label.isNotEmpty()) {
                binding.tvOffer.setVisible()
                binding.tvOfferRight.setGone()
            } else {
                binding.tvOffer.setGone()
                binding.tvOfferRight.setGone()
            }

        }

    }

    fun setItems(list: List<ProductModel>?) {
        if (list == null) {
            this.items.clear()
        } else {
            this.items.addAll(list)
        }
        notifyDataSetChanged()
    }

    fun clearAll() {
        this.items.clear()
        notifyDataSetChanged()
    }

    fun updateItem(pos: Int, item: ProductModel) {
        try {
            if (items != null)
                this.items[pos] = item
            notifyDataSetChanged()
        } catch (e: Exception) {
        }
    }

    fun removeItem(item: ProductModel?) {
        items.remove(item)
        notifyDataSetChanged()
    }

    fun showVariantDialog(model: ProductModel, tvGroupVariant: TextView, position: Int) {
        val dialogView: View = context.layoutInflater.inflate(R.layout.dialog_variant, null)
        bottomSheetDialog = BottomSheetDialog(context, R.style.CustomBottomSheetDialog)
        bottomSheetDialog!!.setContentView(dialogView)
        var tvTitle = bottomSheetDialog!!.findViewById<TextView>(R.id.tvTitle)
        var recyclerView = bottomSheetDialog!!.findViewById<RecyclerView>(R.id.recycler_view)
        var adapter = VariantListAdapter(
            model.group_variants,
            parentFragment,
            context,
            bottomSheetDialog,
            tvGroupVariant,
            model,
            position,
            ScreenType.PRODUCT_LIST
        )
        recyclerView?.adapter = adapter
        tvTitle!!.text = model.name_en /*+ " - " + model.variant_en*/
        bottomSheetDialog!!.show()
    }
}
