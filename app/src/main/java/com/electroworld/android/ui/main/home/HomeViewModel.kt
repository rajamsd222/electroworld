package com.electroworld.android.ui.main.home

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.HomeModel
import com.electroworld.android.model.SetFavModel
import com.electroworld.android.model.StoreModel
import com.electroworld.android.repository.ProductRepositoryImpl
import com.electroworld.android.utils.AppUtils
import com.electroworld.android.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class HomeViewModel @Inject constructor(val mainApi: MainApi,val myPref: PreferencesManager,
                                        private val productRepositoryImpl: ProductRepositoryImpl) : ViewModel() {


    private val disposable = CompositeDisposable()



    fun getSlots(){
        return productRepositoryImpl.getDeliverySlots()
    }

    private fun getStoreInfo(
        context: Context,
        latitude: Double,
        longitude: Double
    ): MutableLiveData<ApiResponse<StoreModel>?>? {
        val res: MutableLiveData<ApiResponse<StoreModel>?> =
            MutableLiveData<ApiResponse<StoreModel>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.getStoreInfo(
            Constants.HEADER_TOKEN,
            latitude,
            longitude,
            myPref.getPrefKeyVendorId()
        ).enqueue(object : Callback<ApiResponse<StoreModel>?> {
            override fun onResponse(
                call: Call<ApiResponse<StoreModel>?>,
                response: Response<ApiResponse<StoreModel>?>
            ) {
                dialog.dismiss()
                if (response.isSuccessful) {
                    res.setValue(response.body())
                }
            }

            override fun onFailure(call: Call<ApiResponse<StoreModel>?>, t: Throwable) {
                dialog.dismiss()
                res.setValue(null)
            }
        })
        return res
    }

    fun getStoreInfoResponse(
        context: Context,
        latitude: Double,
        longitude: Double
    ): LiveData<ApiResponse<StoreModel>?>? {
        return getStoreInfo(
            context, latitude, longitude
        )
    }

    private fun getMobileLayer(
        context: Context,
        page: String,
        pageNo: Int
    ): MutableLiveData<ApiResponse<List<HomeModel>>?>? {
        val res: MutableLiveData<ApiResponse<List<HomeModel>>?> =
            MutableLiveData<ApiResponse<List<HomeModel>>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.getHomeListCall(myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            page,
            myPref.getPrefKeyStoreId(),
            myPref.getPrefKeyVendorId(),
            pageNo)
            .enqueue(object : Callback<ApiResponse<List<HomeModel>>?> {
            override fun onResponse(
                call: Call<ApiResponse<List<HomeModel>>?>,
                response: Response<ApiResponse<List<HomeModel>>?>
            ) {
                dialog.dismiss()
                if (response.isSuccessful) {
                    res.setValue(response.body())
                }
            }

            override fun onFailure(call: Call<ApiResponse<List<HomeModel>>?>, t: Throwable) {
                dialog.dismiss()
                res.setValue(null)
            }
        })
        return res
    }

    fun getMobileLayerResponse(
        context: Context,
        page: String,
        pageNo: Int
    ): LiveData<ApiResponse<List<HomeModel>>?>? {
        return getMobileLayer(
            context,  page,pageNo
        )
    }


    private fun setFavProduct(
        context : Context,
        warehouse_product_id: String,
        product_id: String,
        is_favourite: Int,
        group_id: String
    ): MutableLiveData<ApiResponse<SetFavModel>?>? {
        val res = MutableLiveData<ApiResponse<SetFavModel>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.setFavGrid(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            warehouse_product_id,
            product_id,
            is_favourite,
            group_id
        ).enqueue(object : Callback<ApiResponse<SetFavModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<SetFavModel>?>,
                    response: Response<ApiResponse<SetFavModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponse<SetFavModel>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun setFavProductRes(
        context : Context,
        warehouse_product_id: String,
        product_id: String,
        is_favourite: Int,
        group_id: String
    ): LiveData<ApiResponse<SetFavModel>?>? {
        return setFavProduct(
            context,
            warehouse_product_id,
            product_id,
            is_favourite,
            group_id
        )
    }

    override fun onCleared() {
        disposable.clear()
    }

}