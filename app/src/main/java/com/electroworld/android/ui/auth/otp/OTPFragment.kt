package com.electroworld.android.ui.auth.otp

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.api.Urls
import com.electroworld.android.databinding.FragmentOtpBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.UpdatePriceModel
import com.electroworld.android.model.UserModel
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.ui.main.cart.models.CartSingleItem
import com.electroworld.android.ui.main.cart.models.LocalCartModel
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.UiUtils
import com.electroworld.android.utils.Utils
import dagger.android.support.DaggerFragment
import java.util.*
import javax.inject.Inject


class OTPFragment : DaggerFragment() {

    private lateinit var viewModel: OTPViewModel
    private lateinit var mNavController: NavController
    private lateinit var binding: FragmentOtpBinding


    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager
    private var screen_name = ""
    private var mobileNumber = ""
    private var otp = ""
    private var authentication_device_id = ""
    private var authentication_id = ""


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mNavController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this, providersFactory).get(OTPViewModel::class.java)
        binding = FragmentOtpBinding.bind(view)
        initViews()
        initOnClicks()
        viewModel.otpResponse().observe(viewLifecycleOwner) { res -> consumeOTPResponse(res) }
        binding.ivBack.setOnClickListener(View.OnClickListener {
            mNavController.navigateUp()
        })

        binding.tvResendOTP.setPaintFlags(binding.tvResendOTP.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        screen_name = requireArguments().getString("screen_name")!!
        mobileNumber = requireArguments().getString("phone")!!
        otp = requireArguments().getString("otp")!!
        authentication_device_id = requireArguments().getString("authentication_device_id")!!
        authentication_id = requireArguments().getString("authentication_id")!!
        if (Urls.BASE_URL == Urls.STAGING_SERVER_URL) {
            binding.edOTP.setText(otp)
        }
        binding.edMobileNumber.setText(mobileNumber)

        if (screen_name.equals("signup")) {
            binding.tvCnt1.setText(getString(R.string.signup_with_your_mobile_number))
        }

        binding.tvResendOTP.setOnClickListener(View.OnClickListener {
            callResendOTP()
        })

        binding.edOTP.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                binding.btnSubmit.performClick()
                return@OnEditorActionListener true
            }
            false
        })
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_otp, container, false)
    }


    private fun makeVerifyOtpReq(otp: String, type: String) {
        viewModel.callOTP(
            Constants.HEADER_TOKEN, "1", authentication_device_id, authentication_id, otp,
            type, myPref.getPrefKeyVendorId(), myPref.getPrefKeyLanguage()
        )
    }

    private fun consumeOTPResponse(res: ApiResource<ApiResponse<UserModel>>?) {
        if (res != null) {
            when (res.status) {
                ApiResource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                ApiResource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (res.data != null) {
                        if (res.data.http_code == 200) {
                            Utils.showApiSuccess(activity, res.data.message)
                            renderOTPResponse(res.data.data)
                        } else {
                            CustomDialog.show(requireActivity(), res.data.message, AlertType.ERROR)
                        }
                    }
                }
                ApiResource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    CustomDialog.show(requireActivity(), res.message, AlertType.SERVER_ERROR)
                }
            }
        }
    }

    private fun renderOTPResponse(model: UserModel) {
        myPref.setStringValue(PreferencesManager.PREF_KEY_CUSTOMER_ID, model.CustomerId)
        myPref.setStringValue(PreferencesManager.PREF_KEY_ACCESS_TOKEN, model.token)
        myPref.setStringValue(
            PreferencesManager.PREF_KEY_AUTHENTICATION_ID,
            model.authentication_device_id
        )
        myPref.setPrefKeyUserModel(model)
        updatePriceList()
    }


    private fun updatePriceList() {
        var cartListStr = ""
        val model: LocalCartModel? = myPref.getLocalCart()
        if (model != null) {
            val list: MutableList<CartSingleItem> = model.cartItemList
            val listStr = ArrayList<String>()
            list.forEach {
                listStr.add(it.productModel.warehouse_product_id)
            }
            cartListStr = listStr.toString()
            viewModel.updateCartListPriceRes(
                requireActivity(),
                myPref.getPrefKeyAccessToken(),
                myPref.getPrefKeyCustomerId(),
                myPref.getPrefKeyVendorId(),
                cartListStr,
                myPref.getPrefKeyLanguage()
            )!!.observe(
                viewLifecycleOwner,
                { res ->
                    if (res != null) {
                        if (res.http_code == 200) {
                            if (res.data != null && res.data.isNotEmpty()) {
                                for (info in res.data.get(0).group_variants) {
                                    CartUtils.updateCartDetails(myPref, info)
                                }
                            }
                        }
                    }
                    //Constants.COMES_FROM_SPLASH = true
                    //startActivity(Intent(activity, MainActivity::class.java))
                    requireActivity().finish()
                    //requireActivity().finish()
                })
        } else {
            requireActivity().finish()
        }
    }

    private fun initOnClicks() {
        binding.btnSubmit.setOnClickListener {
            var otpText = binding.edOTP.text.toString().trim()
            if (otpText.equals("")) {
                UiUtils.showInputAlert(
                    activity,
                    "Please enter otp value",
                    binding.edOTP
                )
            } else {
                makeVerifyOtpReq(binding.edOTP.text.toString().trim(), "login")
            }
        }
    }


    private fun initViews() {
    }


    private fun callResendOTP() {
        reSetOtpUI()
        viewModel.resendOTPRes(
            requireActivity(),
            authentication_device_id,
            authentication_id,
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyLanguage()
        )?.observe(viewLifecycleOwner, Observer<ApiResponseDefault?> { res ->
            if (res != null) {
                if (res.getHttp_code() == 200) {

                }
                Utils.showApiError(activity, res.getMessage())
            }
        })
    }

    private fun reSetOtpUI() {
        binding.edOTP.setText("")
    }
}