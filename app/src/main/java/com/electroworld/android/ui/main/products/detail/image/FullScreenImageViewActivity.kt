package com.electroworld.android.ui.main.products.detail.image

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.request.RequestOptions
import com.electroworld.android.R
import com.electroworld.android.databinding.ActivityFullScreenImageViewBinding
import com.electroworld.android.utils.AppValidator
import dagger.android.support.DaggerAppCompatActivity
import java.util.*


class FullScreenImageViewActivity : DaggerAppCompatActivity(){


    var fullScreenImageViewAdapter: FullScreenImageViewAdapter? = null
    private val optionsNormalCC =
        RequestOptions() //  .signature(new ObjectKey(String.valueOf(System.currentTimeMillis())))
            .priority(Priority.IMMEDIATE) //  .diskCacheStrategy(DiskCacheStrategy.NONE)
            // .skipMemoryCache(true)
            .centerInside()
    private lateinit var binding : ActivityFullScreenImageViewBinding

    var imageList = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_image_view)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_full_screen_image_view)


        imageList = intent.getSerializableExtra("imageList") as ArrayList<String>
        val position = intent.getIntExtra("position", 0)
        loadImages()
        fullScreenImageViewAdapter = FullScreenImageViewAdapter(this, imageList)
        binding.pager.setAdapter(fullScreenImageViewAdapter)
        binding.pager.setCurrentItem(position)
        if (position != 0) layoutBGChange(position)
        fullScreenImageViewAdapter?.notifyDataSetChanged()

        binding.pager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(arg0: Int) {}
            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}
            override fun onPageSelected(currentPage: Int) {
                layoutBGChange(currentPage)
                val view: View = binding.dynamicImageLayout.getChildAt(currentPage)
                binding.horizontalScroll.smoothScrollTo(view.left, view.top)
            }
        })

        binding.backIconImageView.setOnClickListener {
            finish()
        }
    }


    private fun loadImages() {
        if (binding.dynamicImageLayout.getChildCount() > 0) binding.dynamicImageLayout.removeAllViews()
        for (i in imageList.indices) {
            val v: View = LayoutInflater.from(this)
                .inflate(
                    R.layout.dynamic_image_array_layout_full_screen,
                    binding.dynamicImageLayout,
                    false
                )
            val parent = v.findViewById<FrameLayout>(R.id.frame_lay)
            val imageView = v.findViewById<ImageView>(R.id.image_view_product_array)
            if (i == 0) {
                parent.setBackgroundResource(R.drawable.btn_corner_un_selected)
            } else {
                parent.setBackgroundResource(0)
            }
            setImage(imageList.get(i), imageView)
            v.setOnClickListener(clickListener)
            v.tag = i
            binding.dynamicImageLayout.addView(v)
        }
    }

    fun setImage(imgURL: String?, imageView: ImageView?) {
        if (AppValidator.toStr(imgURL).length === 0) {
            //imageView.setImageResource(R.mipmap.no_image);
            return
        }
        Glide.with(this)
            .load(imgURL)
            .apply(optionsNormalCC)
            .thumbnail(0.1f)
            .into(imageView!!)
    }
    var clickListener = View.OnClickListener { v ->
        val position = v.tag as Int
        binding.pager.setCurrentItem(position)
    }

    private fun layoutBGChange(position: Int) {
        for (i in 0 until binding.dynamicImageLayout.getChildCount()) {
            if (i == position) {
                binding.dynamicImageLayout.getChildAt(i)
                    .setBackgroundResource(R.drawable.btn_corner_un_selected)
            } else {
                binding.dynamicImageLayout.getChildAt(i)
                    .setBackgroundResource(0)
            }
        }
    }
}