package com.electroworld.android.ui.main.cart

import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.AlertViewCustomDialog
import com.electroworld.android.alert.AlertViewSimple
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.databinding.FragmentCartListBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.*
import com.electroworld.android.repository.PIMSRepository
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.models.CartSingleItem
import com.electroworld.android.ui.main.cart.models.LocalCartModel
import com.electroworld.android.ui.main.home.adapter.ImagesAdapter
import com.electroworld.android.ui.main.products.ProductsGridAdapter
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.MyUtils
import com.electroworld.android.utils.ScreenType
import com.electroworld.android.utils.Utils
import com.make.dots.dotsindicator.DotsIndicator
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.android.synthetic.main.toolbar_title_back.view.*
import java.util.*
import javax.inject.Inject

class CartListFragment : DaggerFragment(), ProductsGridAdapter.ProductInterface {

    private lateinit var viewModel: CartListViewModel
    private lateinit var binding: FragmentCartListBinding
    private lateinit var mNavController: NavController

    var mGridAdapter: ProductsGridAdapter? = null
    var adapter: CartListAdapter? = null

    private val disposable = CompositeDisposable()

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager

    var width = 0
    var height = 0

    @Inject
    lateinit var pims: PIMSRepository
    var subTotal = 0.0
    var deliverySlot: DeliverySlot? = null
    var gift_to_others_amount = 0.0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cart_list, container, false)
    }

    private fun showEditProfileDialog() {
        val avcDialog =
            AlertViewSimple(requireActivity(), object : AlertViewSimple.OKClickListener {
                override fun onOKClick(view: View?) {
                    mNavController.navigate(R.id.action_cartListFragment_to_editProfileFragment)
                }
            })
        avcDialog.setCancelable(true)
        avcDialog.setDialogTitle("Alert")
        avcDialog.setDialogMessage("Please fill your profile information to continue check out")
        avcDialog.setPositiveText("Go to Edit Profile")
        avcDialog.setNegativeText(getString(R.string.btn_cancel))
        avcDialog.setShowNegativeButton(true)
        avcDialog.showDialog()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        width = displayMetrics.widthPixels
        height = displayMetrics.heightPixels

        mNavController = Navigation.findNavController(view)
        viewModel = ViewModelProvider(this, providersFactory).get(CartListViewModel::class.java)
        binding = FragmentCartListBinding.bind(view)
        initBasicObservers()
        initDeliverySlotObserver()
        initCartList()
        hideAll()



        binding.ab.tvABTitle.text = getString(R.string.title_cart)
        updateDeliverySlotText()

        binding.tvClearAll.setOnClickListener {
            val avcDialog =
                AlertViewSimple(requireActivity(), object : AlertViewSimple.OKClickListener {
                    override fun onOKClick(view: View?) {
                        myPref.clearLocalCart()
                        adapter!!.clearAll()
                        updateAdapter()
                        hideAll()
                    }
                })
            avcDialog.setCancelable(true)
            avcDialog.setDialogTitle("Cart Clear")
            avcDialog.setDialogMessage("Are you sure want to clear?")
            avcDialog.setPositiveText("Yes")
            avcDialog.setNegativeText("No")
            avcDialog.setShowNegativeButton(true)
            avcDialog.showDialog()
            /*val intent = Intent(activity, MainActivity::class.java)
            intent.putExtra("navigate_screen_name", "my_orders")
            intent.putExtra("order_id", "825")
            startActivity(intent)
            requireActivity().finish()*/
        }

        binding.tvContinueShopping.setOnClickListener {
            mNavController.navigate(R.id.action_cartListFragment_to_homeFragment)
        }

        binding.btnCheckOut.setOnClickListener {
            myPref.updateGiftMessages(
                binding.edFrom.text.toString().trim(),
                binding.edTo.text.toString().trim(),
                binding.edMessage.text.toString().trim()
            )

            val isGiftChecked = binding.cbGift.isChecked
            val isBillingChecked = binding.tvBillingAddressCheckbox.isChecked

            myPref.updateBillingAddressInfo(
                isBillingChecked,
                binding.edAddress.text.toString().trim(),
                binding.edPhone.text.toString().trim(),
                binding.edContactPerson.text.toString().trim()
            )


            val deliveryText = binding.tvDeliverySlot.text
            if (!myPref.isLogin()) {
                (requireActivity() as MainActivity).showLoginDialog(requireActivity().getString(R.string.alert_login_checkout))
            } else {
                if (deliveryText.isEmpty()) {
                    Utils.showWarning(
                        activity,
                        getString(R.string.error_select_delivery_slot)
                    )
                } else if (myPref.getFirstName()?.length == 0) {
                    showEditProfileDialog()
                } else if (Constants.ADDRESS_ID.isEmpty()) {
                    Utils.showWarning(
                        activity,
                        getString(R.string.error_select_delivery_address)
                    )
                } else if (!isBillingChecked && binding.edContactPerson.text?.length == 0) {
                    Utils.showWarning(
                        activity,
                        getString(R.string.error_contact_person)
                    )
                } else if (!isBillingChecked && binding.edPhone.text?.length == 0) {
                    Utils.showWarning(
                        activity,
                        getString(R.string.error_phone)
                    )
                } else if (!isBillingChecked && binding.edPhone.text?.length != 10) {
                    Utils.showWarning(
                        activity,
                        getString(R.string.error_valid_phone)
                    )
                } else if (!isBillingChecked && binding.edAddress.text?.length == 0) {
                    Utils.showWarning(
                        activity,
                        getString(R.string.error_enter_address)
                    )
                } else if (isGiftChecked && binding.edFrom.text?.length == 0) {
                    Utils.showWarning(
                        activity,
                        getString(R.string.error_gift_from_name)
                    )
                } else if (isGiftChecked && binding.edTo.text?.length == 0) {
                    Utils.showWarning(
                        activity,
                        getString(R.string.error_gift_to_name)
                    )
                } else if (isGiftChecked && binding.edMessage.text?.length == 0) {
                    Utils.showWarning(
                        activity,
                        getString(R.string.error_gift_msg)
                    )
                } else {
                    mNavController.navigate(R.id.action_cartListFragment_to_checkOutFragment)
                }
            }

        }
        binding.ab.ivBack.setOnClickListener {
            mNavController.navigateUp()
        }
        binding.tvDeliverySlot.setOnClickListener {
            openDeliverySlot()
        }
        binding.tvChangeSlot.setOnClickListener {
            openDeliverySlot()
        }
        binding.cbGift.setOnClickListener {
            myPref.updateGiftInfo(binding.cbGift.isChecked, gift_to_others_amount)
            myPref.updateGiftMessages(
                binding.edFrom.text.toString().trim(),
                binding.edTo.text.toString().trim(),
                binding.edMessage.text.toString().trim()
            )
            if (binding.cbGift.isChecked) {
                binding.giftContentLayout.setVisible()
            } else {
                binding.giftContentLayout.setGone()
            }
        }
        binding.tvAddDeliveryAddress.setOnClickListener {
            if (!myPref.isLogin()) {
                (requireActivity() as MainActivity).showLoginDialog(requireActivity().getString(R.string.alert_login_checkout))
            } else {
                openAddressListScreen()
            }

        }
        binding.tvChangeAddress.setOnClickListener {
            openAddressListScreen()
        }
        binding.tvChangeAddress.setPaintFlags(binding.tvChangeAddress.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        binding.tvChangeSlot.setPaintFlags(binding.tvChangeSlot.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

        binding.cbGift.isChecked = myPref.checkGiftSelected()
        if (myPref.checkGiftSelected()) {
            binding.giftContentLayout.setVisible()
        }
        if (TextUtils.isEmpty(myPref.getGiftFromMessage())) {
            binding.edFrom.setText(myPref.getFullName())
        } else {
            binding.edFrom.setText(myPref.getGiftFromMessage())
        }
        binding.edTo.setText(myPref.getGiftToMessage())
        binding.edMessage.setText(myPref.getGiftMessage())

        binding.tvBillingAddressCheckbox.isChecked = myPref.getBillingChecked()

        binding.billingAddressContent.visibility =
            if (myPref.getBillingChecked()) View.GONE else View.VISIBLE


        binding.edAddress.setText(myPref.getBillingAddress())
        binding.edContactPerson.setText(myPref.getBillingContactName())
        binding.edPhone.setText(myPref.getBillingPhone())

        binding.tvBillingAddressCheckbox.setOnCheckedChangeListener { buttonView_, isChecked_ ->
            binding.billingAddressContent.visibility =
                if (isChecked_) View.GONE else View.VISIBLE
            myPref.updateBilling(isChecked_)
        }

    }

    private fun openAddressListScreen() {
        val bundle = Bundle()
        bundle.putString("screenName", "cart")
        mNavController.navigate(R.id.action_cartListFragment_to_addressListFragment, bundle)
    }

    private fun initDeliverySlotObserver() {
        viewModel.deliverySlotApi.observe(viewLifecycleOwner, Observer { apiModel ->
            apiModel?.let {
                (activity as MainActivity).mDeliverySlotList = it.delivery_slot
                if (myPref.getDeliverySlot() == null) {
                    updateDefaultSlot()
                } else {
                    var checkAlreadyPresent = true
                    val deliverySlotFromTime = myPref.getFromTime()
                    (activity as MainActivity).mDeliverySlotList.forEach { deliverySlot ->
                        checkAlreadyPresent = deliverySlotFromTime == deliverySlot.from_time
                    }
                    if (!checkAlreadyPresent) {
                        updateDefaultSlot()
                    }
                }
                updateDeliverySlotText()
                it.pickup_slot.forEachIndexed { index, deliverySlot ->
                    if (index == 0) {
                        myPref.updatePickUpSlot(deliverySlot)
                    }
                }
                if (it.gift_to_others == 1) {
                    binding.layoutGift.visibility = View.VISIBLE
                    binding.giftToOthersValue.text = it.gift_to_others_message
                    gift_to_others_amount = it.gift_to_others_amount
                    myPref.updateGiftInfo(binding.cbGift.isChecked, gift_to_others_amount)
                }
            }
        })
    }

    private fun updateDefaultSlot() {
        if ((activity as MainActivity).mDeliverySlotList.isNotEmpty()) {
            myPref.updateDeliverySlot((activity as MainActivity).mDeliverySlotList.get(0))
        }
    }


    private fun initBasicObservers() {
        viewModel.errorMessage.observe(viewLifecycleOwner, { isError ->
            isError?.let {
                CustomDialog.show(requireActivity(), it, AlertType.SERVER_ERROR)
            }
        })
        viewModel.apiError.observe(viewLifecycleOwner, { isError ->
            isError?.let {
                CustomDialog.show(requireActivity(), it, AlertType.ERROR)
            }
        })
        viewModel.loading.observe(viewLifecycleOwner, { isLoading ->
            isLoading?.let {
                progressBar.visibility = if (it) View.VISIBLE else View.GONE
            }
        })
    }

    private fun openDeliverySlot() {
        if ((activity as MainActivity).mDeliverySlotList.isNotEmpty()) {
            showDeliverySlotAlert()
        } else {
            loadDeliverySlots()
        }
    }

    private fun loadDeliverySlots() {
        viewModel.getDeliverySlots()
    }

    private fun hideAll() {
        binding.tvClearAll.setGone()
        binding.bottomView.setGone()
        binding.cvDeliverySlot.setGone()
        binding.deliveryAddressInfo.setGone()
        binding.cvDeliveryAddress.setGone()
        binding.cartListLayout.setGone()
        binding.layoutGift.setGone()
        binding.contentLayout.setGone()
        binding.giftContentLayout.setGone()
        binding.cvBillingAddress.setGone()
    }

    private fun updateDeliverySlotText() {
        binding.cvDeliverySlot.setVisible()
        binding.tvDeliverySlot.text =
            getString(R.string.lbl_delivery_on) + myPref.getDeliverySlotText()
    }

    private fun showProducts(contentLayout: LinearLayout, placeholders: List<Placeholder>) {
        placeholders.forEach {
            val itemView = LayoutInflater.from(requireActivity())
                .inflate(R.layout.row_cart_products, contentLayout, false)
            val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
            val titlelayout = itemView.findViewById<RelativeLayout>(R.id.titlelayout)
            val recyclerView = itemView.findViewById<RecyclerView>(R.id.recycler_view)
            tvTitle.text = it.title
            mGridAdapter = ProductsGridAdapter(
                this, requireActivity() as MainActivity, myPref,
                ScreenType.CART,
                this,
                ""
            )
            recyclerView.layoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            recyclerView.adapter = mGridAdapter
            mGridAdapter?.setItems(it.products)
            mGridAdapter?.notifyDataSetChanged()

            if (it.products.isNotEmpty()) {
                contentLayout.addView(itemView)
            }

            titlelayout.setOnClickListener {
                if (recyclerView.visibility == View.VISIBLE) {
                    recyclerView.setGone()
                } else {
                    recyclerView.setVisible()
                }
            }
        }

    }


    private fun showCarousel(
        contentLayout: LinearLayout,
        placeholders: List<Placeholder>,
        homeModel: HomeModel
    ) {
        val imagesList: ArrayList<ImagesModel> = ArrayList()
        placeholders.forEach {
            imagesList.add(ImagesModel(it.image, homeModel.title, it.id))
        }
        if (imagesList.size > 0) {
            val itemView = LayoutInflater.from(requireActivity())
                .inflate(R.layout.row_home_carousel, contentLayout, false)
            val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
            val imageViewPager = itemView.findViewById<ViewPager>(R.id.imageViewPager)
            val imageFrame = itemView.findViewById<RelativeLayout>(R.id.imageFrame);
            val dotsIndicator = itemView.findViewById<DotsIndicator>(R.id.dotsIndicator)
            val dotsFrame = itemView.findViewById<FrameLayout>(R.id.dotsFrame);
            val bannerFullView = itemView.findViewById<RelativeLayout>(R.id.bannerFullView)
            imageViewPager.adapter = ImagesAdapter(
                imagesList, mNavController, homeModel.type, ScreenType.CART
            )
            dotsIndicator.setViewPager(imageViewPager)
            imageViewPager.adapter?.registerDataSetObserver(dotsIndicator.dataSetObserver)
            if (imagesList.size == 1) {
                dotsIndicator.visibility = View.GONE
            }
            tvTitle.setGone()
            tvTitle.text = ""

            dotsFrame.setGone()
            imageFrame.setBackgroundColor(Color.WHITE);
            val param = imageViewPager.layoutParams as ViewGroup.MarginLayoutParams
            param.setMargins(10, 10, 10, 10)
            imageViewPager.layoutParams = param
            val bannerHeight = (height * 0.15).toInt()

            val params = bannerFullView.layoutParams
            params.height = bannerHeight
            bannerFullView.layoutParams = params

            contentLayout.addView(itemView)
        }


    }

    private fun showDeliverySlotAlert() {
        if ((activity as MainActivity).mDeliverySlotList.isNotEmpty()) {
            val avcDialog = AlertViewCustomDialog(
                requireActivity(),
                object : AlertViewCustomDialog.ItemClickListener {
                    override fun onItemClick(view: View?, position: Int) {
                        deliverySlot = (activity as MainActivity).mDeliverySlotList.get(position)
                        myPref.updateDeliverySlot(deliverySlot)
                        binding.tvDeliverySlot.text =
                            getString(R.string.lbl_delivery_on) + deliverySlot?.slot
                    }
                })
            avcDialog.setCancelable(true)
            avcDialog.setDialogTitle("Choose delivery slot")
            avcDialog.values = (activity as MainActivity).mDeliverySlotList
            avcDialog.showDialog()
        }
    }

    private fun loadApis() {
        disposable.add(
            pims.getMobileAdBlocks("cart")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<HomeModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<HomeModel>>) {
                        if (res.http_code == 200) {
                            binding.contentLayout.visibility = View.VISIBLE
                            binding.contentLayout.removeAllViews()
                            res.data.forEach {
                                if (it.name == "EML14") {
                                    showCarousel(binding.contentLayout, it.placeholders, it)
                                } else if (it.type == "3") {
                                    showProducts(binding.contentLayout, it.placeholders)
                                }
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
        loadDeliverySlots()
    }

    fun updateCart(
        item: CartSingleItem,
        isAdd: Boolean,
        isBulkUpdate: Boolean,
        selectedQty: Int
    ) {

        val isSuccess = CartUtils.updateCartQuantity(
            requireActivity() as MainActivity,
            myPref,
            item.productModel.warehouse_product_id,
            isAdd,
            isBulkUpdate,
            selectedQty
        )
        if (isSuccess) {
            if (isBulkUpdate) {
                item.quantity = selectedQty
            } else {
                if (isAdd) {
                    item.quantity = item.quantity + 1
                } else {
                    if (item.quantity == 1) {
                        adapter?.removeItem(item)
                    } else {
                        item.quantity = item.quantity - 1
                    }
                }
            }
            adapter?.notifyDataSetChanged()
            updateBottomInfo()
        }

    }


    fun deleteCartItem(item: CartSingleItem) {
        CartUtils.deleteCartItem(myPref, item.productModel.warehouse_product_id)
        adapter?.removeItem(item)
        updateBottomInfo()
    }

    override fun onResume() {
        super.onResume()
        getCartItems()
        loadSummary()
        updateAddressInfo()
    }

    private fun updateAddressInfo() {
        binding.deliveryAddressInfo.visibility = View.GONE
        binding.tvAddDeliveryAddress.visibility = View.VISIBLE
        val deliveryAddressStoreId = myPref.checkStoreModelAdded()
        if (deliveryAddressStoreId.isNotEmpty()) {
            var model = myPref.getDefaultAddress()
            if (model != null) {
                binding.deliveryAddressInfo.visibility = View.VISIBLE
                binding.tvAddDeliveryAddress.visibility = View.GONE
                Constants.ADDRESS_ID = model.address_id
                Constants.ADDRESS_NAME = model.address
                Constants.LATITUDE = model.latitude
                Constants.LONGITUDE = model.longitude

                binding.tvAddress.setText(model.house_no)
                if (model.apartment_name.isNotEmpty()) {
                    binding.tvAddress.setText(model.house_no + ", " + model.apartment_name)
                }
                if (model.street.isNotEmpty()) {
                    binding.tvAddress.setText(model.house_no + ", " + model.apartment_name + ", " + model.street)
                }
                binding.tvFullAddress.setText(model.address)
                var row1 = model.contact_person
                if (row1.isNotEmpty()) {
                    row1 = "$row1 - "
                }
                row1 += model.address_type
                binding.tvContactPerson.text = row1
                binding.tvlandmark.text =
                    "LandMark : " + model.landmark + " Mobile : " + model.mobile_number
                //binding.tvPhone.text = "Mobile : " + model.mobile_number
                binding.tvPhone.setGone()
            }
        }

    }

    private fun getCartItems() {
        val model: LocalCartModel? = myPref.getLocalCart()
        if (model != null) {
            CartUtils.removePromoCode(myPref)
            adapter?.setItems(model.cartItemList)
            subTotal = CartUtils.getSubTotal(myPref)
            binding.tvAmount.text = MyUtils.formatCurrencyfromDouble(subTotal)
            if (adapter?.itemCount == 0) {
                myPref.clearLocalCart()
            } else {
                binding.cvDeliveryAddress.visibility = View.VISIBLE
                binding.cartListLayout.setVisible()
                loadApis()
            }

        } else {
            adapter?.clearAll()
        }
        updateAdapter()

        mGridAdapter?.notifyDataSetChanged()
    }

    private fun updateBottomInfo() {
        loadSummary()
        updateAdapter()
    }

    private fun updateAdapter() {
        if (adapter?.itemCount == 0) {
            binding.tvEmptyMsg.setVisibility(View.VISIBLE)
            hideAll()
        } else {
            binding.bottomView.setVisibility(View.VISIBLE)
            binding.tvClearAll.setVisibility(View.VISIBLE)
            binding.tvEmptyMsg.setVisibility(View.GONE)
            binding.tvCartCount.setText("" + adapter?.itemCount + " items")
            binding.cvBillingAddress.setVisible()
        }
        mGridAdapter?.notifyDataSetChanged()
        //binding.badgeCount.setText(java.lang.String.valueOf(adapter.getItemCount()))
    }

    private fun loadSummary() {
        val model = myPref.getLocalCart()
        if (model != null) {
            subTotal = CartUtils.getSubTotal(myPref)
            binding.tvAmount.setText(MyUtils.formatCurrencyfromDouble(subTotal))
        }
    }

    private fun initCartList() {
        adapter = CartListAdapter(this, requireActivity())
        binding.recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireActivity(),
                LinearLayoutManager.VERTICAL
            )
        )
    }

    fun setAsFavouriteProduct(mAdapter: ProductsGridAdapter?, pos: Int, model: ProductModel) {
        val setfav = if (model.is_favourite == 1) 0 else 1
        disposable.add(
            pims.doFavReq(model, setfav)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<SetFavModel>>() {
                    override fun onSuccess(res: ApiResponse<SetFavModel>) {
                        if (res.http_code == 200) {
                            model.is_favourite = setfav
                            mAdapter?.updateItem(pos, model)
                            if (setfav == 1) {
                                Utils.showApiSuccess(activity, "Product added to favourites list")
                            } else if (setfav == 0) {
                                Utils.showApiSuccess(
                                    activity,
                                    "Product removed from favourites list"
                                )
                            }
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    fun doAddToCart(model: ProductModel, isAdd: Boolean, isBulkUpdate: Boolean, selectedQty: Int) {
        CartUtils.addToCartProductList(
            requireActivity() as MainActivity,
            myPref,
            model,
            isAdd,
            isBulkUpdate,
            selectedQty
        )
        mGridAdapter?.notifyDataSetChanged()
        getCartItems()
    }

    fun openDetailsScreen(model: ProductModel) {
        val bundle = Bundle()
        bundle.putString("name", model.name_en)
        bundle.putString("group_id", model.group_id)
        bundle.putString("warehouse_product_id", model.warehouse_product_id)
        mNavController.navigate(R.id.productDetailFragment, bundle)
    }

    override fun onDestroyView() {
        disposable.clear()
        super.onDestroyView()
    }

    override fun addItem(productModel: ProductModel, quantity: Int) {

    }
}