package com.electroworld.android.ui.auth.otp

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.api.auth.AuthApi
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.UpdatePriceModel
import com.electroworld.android.model.UserModel
import com.electroworld.android.utils.AppUtils
import com.electroworld.android.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class OTPViewModel @Inject constructor(val authApi: AuthApi) : ViewModel() {



    private val disposable = CompositeDisposable()

    private val responseLiveData = MutableLiveData<ApiResource<ApiResponse<UserModel>>>()


    private fun resendOTP(
        context: Context,
        authentication_device_id: String,
        authentication_id: String,
        vendorId: String,
        language: String
    ): MutableLiveData<ApiResponseDefault?>? {
        val res: MutableLiveData<ApiResponseDefault?> = MutableLiveData<ApiResponseDefault?>()
        val dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        authApi.resendOTP(
            Constants.HEADER_TOKEN,
            authentication_device_id,
            authentication_id,
            vendorId,
            language
        ).enqueue(object : Callback<ApiResponseDefault?> {
                override fun onResponse(
                    call: Call<ApiResponseDefault?>,
                    response: Response<ApiResponseDefault?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful()) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponseDefault?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun resendOTPRes(
        context: Context,
        authentication_device_id: String,
        authentication_id: String,
        vendorId: String,
        language: String
    ): LiveData<ApiResponseDefault?>? {
        return resendOTP(
            context,
            authentication_device_id,
            authentication_id,
            vendorId,
            language
        )
    }

    private fun updateCartListPrice(
        context: Context,
        token: String,
        customerId: String,
        vendorId: String,
        productArray: String,
        language: String
    ): MutableLiveData<ApiResponse<List<UpdatePriceModel>>?>? {
        val res: MutableLiveData<ApiResponse<List<UpdatePriceModel>>?> =
            MutableLiveData<ApiResponse<List<UpdatePriceModel>>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        authApi.getUpdatedPriceList(
            Constants.HEADER_TOKEN,
            token,
            customerId,
            vendorId,
            productArray,
            language
        )?.enqueue(object : Callback<ApiResponse<List<UpdatePriceModel>>?> {
            override fun onResponse(
                call: Call<ApiResponse<List<UpdatePriceModel>>?>,
                response: Response<ApiResponse<List<UpdatePriceModel>>?>
            ) {
                dialog.dismiss()
                if (response.isSuccessful) {
                    res.setValue(response.body())
                }
            }

            override fun onFailure(call: Call<ApiResponse<List<UpdatePriceModel>>?>, t: Throwable) {
                dialog.dismiss()
                res.setValue(null)
            }
        })
        return res
    }

    fun updateCartListPriceRes(
        context: Context,
        token: String,
        customerId: String,
        vendorId: String,
        productArray: String,
        language: String
    ): LiveData<ApiResponse<List<UpdatePriceModel>>?>? {
        return updateCartListPrice(
            context, token, customerId, vendorId, productArray, language
        )
    }

    fun callOTP(
        token: String, role: String, authentication_device_id: String, authentication_id: String,
        otp: String, type: String, vendorId: String, language: String
    ){
        disposable.add(authApi.doOTP(
            token,
            role,
            authentication_device_id,
            authentication_id,
            otp,
            type,
            vendorId,
            language
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.setValue(
                    ApiResource.loading<ApiResponse<UserModel>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveData.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveData.setValue(
                    ApiResource.error<ApiResponse<UserModel>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun otpResponse(): MutableLiveData<ApiResource<ApiResponse<UserModel>>> {
        return responseLiveData
    }
}