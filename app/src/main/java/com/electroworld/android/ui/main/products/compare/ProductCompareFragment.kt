package com.electroworld.android.ui.main.products.compare

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.electroworld.android.R
import com.electroworld.android.alert.AlertType
import com.electroworld.android.alert.AlertViewSimple
import com.electroworld.android.alert.CustomDialog
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.base.GenericAdapter
import com.electroworld.android.databinding.FragmentProductCompareBinding
import com.electroworld.android.databinding.RowItemProductCompareGridBinding
import com.electroworld.android.di.ViewModelProviderFactory
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.extentions.setGone
import com.electroworld.android.extentions.setVisible
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.SetFavModel
import com.electroworld.android.repository.PIMSRepository
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.cart.CartUtils
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.SystemUtils
import com.electroworld.android.utils.Utils
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_title_cart.view.*
import org.json.JSONArray
import org.json.JSONObject
import javax.inject.Inject

class ProductCompareFragment : DaggerFragment() {

    private lateinit var mAdapter: GenericAdapter<ProductModel, RowItemProductCompareGridBinding>

    private lateinit var viewModel: ProductCompareViewModel

    @Inject
    lateinit var providersFactory: ViewModelProviderFactory

    @Inject
    lateinit var myPref: PreferencesManager
    lateinit var mNavController: NavController

    @Inject
    lateinit var pims: PIMSRepository
    private var page = 1
    private val disposable = CompositeDisposable()
    private lateinit var binding: FragmentProductCompareBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductCompareBinding.inflate(layoutInflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        page = 1
        viewModel =
            ViewModelProvider(this, providersFactory).get(ProductCompareViewModel::class.java)
        mNavController = Navigation.findNavController(view)


        init()

        initRecyclerView()

    }

    fun init() {
        binding.ab.ivBack.setOnClickListener { mNavController.popBackStack() }
        binding.ab.tvABTitle.text = getString(R.string.title_compare)
        binding.ab.ivSearch.visibility = View.GONE


        binding.ab.tvClearAll.visibility =
            if (myPref.getCompareProductCount() > 0) View.VISIBLE else View.GONE

        binding.ab.tvClearAll.setOnClickListener {
            val avcDialog =
                AlertViewSimple(requireActivity(), object : AlertViewSimple.OKClickListener {
                    override fun onOKClick(view: View?) {
                        myPref.clearCompareProducts()
                        mAdapter.clearAll()
                        binding.tvEmptyMsg.visibility = View.VISIBLE
                        binding.ab.tvClearAll.visibility = View.GONE
                        (requireActivity() as MainActivity).refreshAddToCompareBadge()
                    }
                })
            avcDialog.setCancelable(true)
            avcDialog.setDialogTitle("Clear")
            avcDialog.setDialogMessage("Are you sure want to clear?")
            avcDialog.setPositiveText("Yes")
            avcDialog.setNegativeText("No")
            avcDialog.setShowNegativeButton(true)
            avcDialog.showDialog()
        }

        binding.ab.iconCart.setOnClickListener {
            mNavController.navigate(R.id.cartListFragment)
        }
    }

    fun initRecyclerView() {
        mAdapter = object : GenericAdapter<ProductModel, RowItemProductCompareGridBinding>
            (R.layout.row_item_product_compare_grid) {
            override fun onBindData(
                model: ProductModel,
                position: Int,
                dataBinding: RowItemProductCompareGridBinding?
            ) {
                dataBinding!!.item = model



                dataBinding.ivProduct.setOnClickListener {
                    openDetailsScreen(model)
                }

                val singleItemQuantity: Int =
                    CartUtils.getSingleItemQuantity(
                        myPref,
                        AppValidator.toStr(model.warehouse_product_id)
                    )

                dataBinding.btnAddToCart.visibility =
                    if (singleItemQuantity == 0 && model.available_qty > 0) View.VISIBLE else View.GONE


                dataBinding.layCount.visibility =
                    if (singleItemQuantity == 0) View.GONE else View.VISIBLE

                dataBinding.tvQty.text = singleItemQuantity.toString()


                runDynamicSpecifications(
                    dataBinding.dynamicSpecificationLayout,
                    model.specification
                )

                dataBinding.imgClear.setOnClickListener {
                    CartUtils.deleteCompareItem(
                        requireActivity() as MainActivity,
                        myPref,
                        model.warehouse_product_id
                    )
                    mAdapter.items.removeAt(position)
                    if (mAdapter.items.size == 0) {
                        binding.ab.tvClearAll.visibility = View.GONE
                        binding.tvEmptyMsg.visibility = View.VISIBLE
                    }
                    notifyDataSetChanged()
                }

                dataBinding.btnAddToCart.setOnClickListener {
                    doAddToCart(model, isAdd = true, isBulkUpdate = false, selectedQty = 1)
                    updateProductDetailAddToCartUi(model.warehouse_product_id, dataBinding)
                    notifyDataSetChanged()
                }

                dataBinding.tvIncrement.setOnClickListener {
                    doAddToCart(
                        model, true, isBulkUpdate = false, selectedQty = 1
                    )
                    updateProductDetailAddToCartUi(model.warehouse_product_id, dataBinding)
                    notifyDataSetChanged()
                }
                dataBinding.tvDecrement.setOnClickListener {
                    doAddToCart(
                        model, false, isBulkUpdate = false, selectedQty = 1
                    )
                    updateProductDetailAddToCartUi(model.warehouse_product_id, dataBinding)
                    notifyDataSetChanged()
                }
                dataBinding.favLayout.setOnClickListener {
                    if (!myPref.isLogin()) {
                        (requireActivity() as MainActivity).showLoginDialog(
                            requireActivity().getString(
                                R.string.login_alert_for_fav
                            )
                        )
                    } else {
                        setAsFavouriteProduct(model, dataBinding)
                    }
                }

                dataBinding.executePendingBindings()
            }

            override fun onItemClick(model: ProductModel?, position: Int) {

            }

        }


        binding.recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        binding.recyclerView.apply {
            adapter = mAdapter
        }
        getProductCompare()

    }


    fun setAsFavouriteProduct(model: ProductModel, rowBiding: RowItemProductCompareGridBinding?) {
        val setfavValue = if (model.is_favourite == 1) 0 else 1
        disposable.add(
            pims.doFavReq(model, setfavValue)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<SetFavModel>>() {
                    override fun onSuccess(res: ApiResponse<SetFavModel>) {
                        if (res.http_code == 200) {
                            updateAllFavouriteItems(model, res.data, rowBiding)
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
        )
    }

    private fun updateAllFavouriteItems(
        model: ProductModel, data: SetFavModel, rowBiding: RowItemProductCompareGridBinding?
    ) {
        (activity as MainActivity?)?.setFavouriteCount(data.total_fav_count)

        //product detail ui update
        if (model.warehouse_product_id == data.warehouse_product_id &&
            model.product_id == data.product_id &&
            model.group_id == data.group_id
        ) {
            model.is_favourite = data.is_favourite
            updateProductDetailFavUI(model.is_favourite, rowBiding)
        }

        if (data.is_favourite == 1) {
            Utils.showApiSuccess(activity, "Product added to favourites list")
        } else if (data.is_favourite == 0) {
            Utils.showApiSuccess(activity, "Product removed from favourites list")
        }
    }

    private fun updateProductDetailFavUI(
        isFavourite: Int?,
        rowBiding: RowItemProductCompareGridBinding?
    ) {
        rowBiding!!.ivFav.setImageResource(if (isFavourite == 1) R.drawable.ic_favorite_color_primary_24dp else R.drawable.ic_favorite_border_black_24dp)
        rowBiding!!.tvFavLbl.setText(
            if (isFavourite == 1) activity?.resources?.getString(
                R.string.remove_my_list
            ) else activity?.resources?.getString(R.string.save_my_list)
        )
    }


    fun runDynamicSpecifications(
        dynamicSpecificationLayout: LinearLayout,
        specification: List<SpecificationsModel>
    ) {
        dynamicSpecificationLayout.removeAllViews()
        for (item in specification) {
            val child = layoutInflater.inflate(
                R.layout.row_item_specification,
                dynamicSpecificationLayout,
                false
            )
            val title = child.findViewById<TextView>(R.id.tv_key)
            val value = child.findViewById<TextView>(R.id.tv_value)
            title.text = SystemUtils.capitalize(item.specification_name)
            value.text = SystemUtils.capitalize(item.name_en)
            dynamicSpecificationLayout.addView(child)
        }
    }

    fun doAddToCart(model: ProductModel, isAdd: Boolean, isBulkUpdate: Boolean, selectedQty: Int) {
        CartUtils.addToCartProductList(
            requireActivity() as MainActivity,
            myPref, model, isAdd, isBulkUpdate, selectedQty
        )
        updateCartCountUI()
    }

    private fun updateProductDetailAddToCartUi(
        warehouse_product_id: String,
        dataBinding: RowItemProductCompareGridBinding
    ) {
        val singleItemQuantity: Int =
            CartUtils.getSingleItemQuantity(
                myPref,
                AppValidator.toStr(warehouse_product_id)
            )
        if (singleItemQuantity > 0) {
            dataBinding.btnAddToCart.visibility = View.GONE
            dataBinding.layCount.visibility = View.VISIBLE
        } else {
            dataBinding.btnAddToCart.visibility = View.VISIBLE
            dataBinding.layCount.visibility = View.GONE
        }
        dataBinding.tvQty.text = singleItemQuantity.toString()
    }


    fun getProductCompare() {


        val array = JSONArray()

        val model = myPref.getAddToCompareData()

        if (model != null) {
            binding.tvEmptyMsg.setGone()

            if (model.cartItemList.isEmpty()) {
                binding.tvEmptyMsg.setVisible()
                return
            }

            val ar = JSONArray();
            var b = StringBuilder()
            var pos: Int = 0
            for (item in model.cartItemList) {
                b.append(item.warehouse_product_id).append(",")
                ar.put(pos, item.warehouse_product_id)
                pos += 1
                val json = JSONObject()
                json.put("group_id", item.group_id)
                json.put("product_id", item.productId)
                json.put("warehouse_product_id", item.warehouse_product_id)
                array.put(json)
            }
            Log.e("StringBuilder : ", ar.toString())

            disposable.add(viewModel.getProductCompare(ar)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    binding.progressBar.visibility = View.VISIBLE
                }
                .doFinally {
                    binding.progressBar.visibility = View.GONE
                }
                .subscribeWith(object :
                    DisposableSingleObserver<ApiResponse<List<ProductModel>>>() {
                    override fun onSuccess(res: ApiResponse<List<ProductModel>>) {
                        if (res.http_code == 200) {
                            mAdapter.items = res.data
                        } else {
                            CustomDialog.show(requireActivity(), res.message, AlertType.ERROR)
                        }
                    }

                    override fun onError(e: Throwable) {
                        CustomDialog.show(requireActivity(), e.message, AlertType.SERVER_ERROR)
                    }
                })
            )

        } else {
            binding.tvEmptyMsg.setVisible()
        }


    }


    fun openDetailsScreen(model: ProductModel) {
        val bundle = Bundle()
        bundle.putString("name", model.name_en)
        bundle.putString("group_id", model.group_id)
        bundle.putString("warehouse_product_id", model.warehouse_product_id)
        mNavController.navigate(R.id.productDetailFragment, bundle)
    }


    override fun onResume() {
        super.onResume()
        updateCartCountUI()
    }

    private fun updateCartCountUI() {
        binding.ab.tvCartCount.text = myPref.getCartCount().toString()
        when {
            myPref.getCartCount() > 0 -> {
                binding.ab.tvCartCount.visibility = View.VISIBLE
            }
            else -> {
                binding.ab.tvCartCount.visibility = View.GONE
            }
        }
    }
}