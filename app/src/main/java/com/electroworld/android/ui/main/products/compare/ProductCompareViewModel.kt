package com.electroworld.android.ui.main.products.compare

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.electroworld.android.api.ApiResource
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.*
import com.electroworld.android.model.filter.FilterApiRes
import com.electroworld.android.repository.PIMSRepository
import com.electroworld.android.utils.AppUtils
import com.electroworld.android.utils.Constants
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class ProductCompareViewModel @Inject constructor(private val mainApi: MainApi, private val myPref: PreferencesManager,
                                                  private val repo : PIMSRepository) : ViewModel() {


    private val disposable = CompositeDisposable()
    private val responseLiveData = MutableLiveData<ApiResource<ApiResponse<List<ProductModel>>>>()
    private val responseLiveDataFav = MutableLiveData<ApiResource<ApiResponse<SetFavModel>>>()

    private val responseLiveDataCategories = MutableLiveData<ApiResource<ApiResponse<List<CategoryModel>>>>()

    private val responseLiveDataFilter = MutableLiveData<ApiResource<ApiResponse<FilterApiRes>>>()

    private val responseLiveDataProductCompare = MutableLiveData<ApiResource<ApiResponse<ApiResponseDefault>>>()

    fun getProductCompare(products: JSONArray): Single<ApiResponse<List<ProductModel>>> {
        return repo.getProductCompare(products)
    }



    fun getFiltersList() {
        disposable.add(mainApi.getFiltersList(
            Constants.HEADER_TOKEN, myPref.getPrefKeyVendorId(),myPref.getPrefKeyLanguage()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveDataFilter.setValue(
                    ApiResource.loading<ApiResponse<FilterApiRes>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveDataFilter.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveDataFilter.setValue(
                    ApiResource.error<ApiResponse<FilterApiRes>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getFiltersResponse(): MutableLiveData<ApiResource<ApiResponse<FilterApiRes>>> {
        return responseLiveDataFilter
    }


    fun getProductCompareResponse(): MutableLiveData<ApiResource<ApiResponse<ApiResponseDefault>>> {
        return responseLiveDataProductCompare
    }


    private fun getProductInfo(
        context: Context,
        warehouse_product_id: String
    ): MutableLiveData<ApiResponse<ProductModel>?>? {
        val res: MutableLiveData<ApiResponse<ProductModel>?> =
            MutableLiveData<ApiResponse<ProductModel>?>()
        val dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.getProductInfoByVariant(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            warehouse_product_id
        )
            .enqueue(object : Callback<ApiResponse<ProductModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<ProductModel>?>,
                    response: Response<ApiResponse<ProductModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponse<ProductModel>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun getProductInfoRes(
        context: Context,
        warehouse_product_id: String
    ): LiveData<ApiResponse<ProductModel>?>? {
        return getProductInfo(
            context,
            warehouse_product_id
        )
    }

    fun getCategoriesList() {
        disposable.add(mainApi.getCategoryList(
            Constants.HEADER_TOKEN, myPref.getPrefKeyVendorId()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveDataCategories.setValue(
                    ApiResource.loading<ApiResponse<List<CategoryModel>>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveDataCategories.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveDataCategories.setValue(
                    ApiResource.error<ApiResponse<List<CategoryModel>>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getCategoriesResponse(): MutableLiveData<ApiResource<ApiResponse<List<CategoryModel>>>> {
        return responseLiveDataCategories
    }

    private fun getProductListInfo(
        context: Context,
        category_id: String,
        level: String,
        page: Int,
        search_product: String,
        sort_by: String,
        order_by: String,
        brand_id: String,
        dynamicParams: HashMap<String, String>
    ): MutableLiveData<ApiResponse<ProductApiModel>?>? {
        val res: MutableLiveData<ApiResponse<ProductApiModel>?> =
            MutableLiveData<ApiResponse<ProductApiModel>?>()
        val dialog: Dialog = AppUtils.showCustomDialog(context)
        dialog.show()
        mainApi.getProductListNormal(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyStoreId(),
            category_id,
            level,
            page,
            search_product,
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyLanguage(),
            sort_by,
            order_by,
            brand_id,
            dynamicParams
        )
            .enqueue(object : Callback<ApiResponse<ProductApiModel>?> {
                override fun onResponse(
                    call: Call<ApiResponse<ProductApiModel>?>,
                    response: Response<ApiResponse<ProductApiModel>?>
                ) {
                    dialog.dismiss()
                    if (response.isSuccessful) {
                        res.setValue(response.body())
                    }
                }

                override fun onFailure(call: Call<ApiResponse<ProductApiModel>?>, t: Throwable) {
                    dialog.dismiss()
                    res.setValue(null)
                }
            })
        return res
    }

    fun getProductListObserverRes(
        context: Context,
        category_id: String,
        level: String,
        page: Int,
        search_product: String,
        sort_by: String,
        order_by: String,
        brand_id: String,
        dynamicParams: HashMap<String, String>
    ): LiveData<ApiResponse<ProductApiModel>?>? {
        return getProductListInfo(
            context,
            category_id,
            level,
            page,
            search_product,
            sort_by,
            order_by,
            brand_id,
            dynamicParams
        )
    }

    fun setAsFav(
        warehouse_product_id: String,
        product_id: String,
        is_favourite: Int,
        group_id: String
    ){
        disposable.clear()
        disposable.add(mainApi.setFav(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            warehouse_product_id,
            product_id,
            is_favourite,
            group_id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveDataFav.setValue(
                    ApiResource.loading<ApiResponse<SetFavModel>?>(null)
                )
            }
            .subscribe(
                { userModelApiResponse ->
                    responseLiveDataFav.setValue(
                        ApiResource.success(
                            userModelApiResponse
                        )
                    )
                }
            ) { throwable ->
                responseLiveDataFav.setValue(
                    ApiResource.error<ApiResponse<SetFavModel>?>(
                        throwable.message!!,
                        null
                    )
                )
            })
    }


    fun getFavResponse(): MutableLiveData<ApiResource<ApiResponse<SetFavModel>>> {
        return responseLiveDataFav
    }

    override fun onCleared() {
        disposable.clear()
    }

}