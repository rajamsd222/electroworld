package com.electroworld.android

import retrofit2.http.Multipart
import retrofit2.http.POST
import com.electroworld.android.api.Urls
import okhttp3.RequestBody
import com.electroworld.android.model.UserModel
import retrofit2.Call
import retrofit2.http.Part
import retrofit2.http.PartMap

interface TestInterface {
    @Multipart
    @POST(Urls.UPDATE_PROFILE)
    fun sendUpdateProfileRequest(
        @Part("profile_img") img_file: RequestBody?,
        @PartMap params: Map<String?, RequestBody?>?
    ): Call<UserModel?>?
}