package com.electroworld.android.repository

import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.*
import com.electroworld.android.utils.Constants
import io.reactivex.Single
import org.json.JSONArray
import javax.inject.Inject

class PIMSRepository @Inject constructor(val mainApi: MainApi, val myPref: PreferencesManager) {


    fun getPlaceHolderDetails(
        placeholderId: String,
        page: Int,
        sort_by: String,
        order_by: String,
        brand_id: String,
        dynamicParams: HashMap<String, String>
    ): Single<ApiResponse<PlaceHolderDetailsApiModel>> {
        return mainApi.getPlaceHolderDetailsNew(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyStoreId(),
            placeholderId,
            page,
            myPref.getPrefKeyVendorId(),
            sort_by,
            order_by,
            brand_id,
            dynamicParams
        )
    }


    fun getPlaceHolderDetailsNew(
        placeholderId: String,
        page: Int
    ): Single<ApiResponse<PlaceHolderDetailsApiModel>> {
        return mainApi.getPlaceHolderDetails(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyStoreId(),
            placeholderId,
            page,
            myPref.getPrefKeyVendorId()
        )
    }

    fun getProductInfoReq(warehouse_product_id: String): Single<ApiResponse<ProductModel>> {
        return mainApi.getProductInfoByVariantNew(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            warehouse_product_id
        )
    }

    fun getSimilarProducts(group_id: String): Single<ApiResponse<List<ProductModel>>> {
        return mainApi.getSimilarProductList(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyStoreId(),
            group_id,
            myPref.getPrefKeyVendorId()
        )
    }

    fun getProductDetails(warehouse_product_id: String?): Single<ApiResponse<ProductDetailModel>> {
        return mainApi.getProductDetails(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyStoreId(),
            warehouse_product_id,
            myPref.getPrefKeyVendorId()
        )
    }

    fun getProductDetailsNew(
        warehouse_product_id: String?,
        group_id: String?
    ): Single<ApiResponse<ProductDetailModel>> {
        return mainApi.getProductDetailsNew(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyStoreId(),
            warehouse_product_id,
            group_id,
            myPref.getPrefKeyVendorId()
        )
    }

    fun getFrequentlyProductsList(): Single<ApiResponse<List<ProductModel>>> {
        return mainApi.getFrequentlyProductList(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyStoreId()
        )
    }

    fun getDeliverySlots(): Single<ApiResponse<DeliverySlotApiModel>> {
        return mainApi.getDeliveryslotTimings(
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyLanguage()
        )
    }

    fun getStoreInfo(latitude: Double, longitude: Double): Single<ApiResponse<StoreModel>> {
        return mainApi.getStoreInfoSingle(
            latitude,
            longitude,
            myPref.getPrefKeyVendorId()
        )
    }

    fun getMobileAdBlocks(page: String): Single<ApiResponse<List<HomeModel>>> {
        return mainApi.getHomeList(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyStoreId(),
            myPref.getPrefKeyVendorId(),
            page
        )
    }

    fun doFavReq(model: ProductModel, isAddToFav: Int): Single<ApiResponse<SetFavModel>> {
        return mainApi.doFav(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            model.warehouse_product_id,
            model.product_id,
            model.group_id,
            isAddToFav
        )
    }

    fun getCategoryList(): Single<ApiResponse<List<CategoryModel>>> {
        return mainApi.getCategoryList(myPref.getPrefKeyVendorId())
    }


    fun updateProductRatingReq(
        rating: String,
        rating_descriptions: String,
        rating_title: String,
        rated_in: String,
        product_id: String
    ): Single<ApiResponse<ApiResponseDefault>> {
        return mainApi.updateRating(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            rating,
            rating_descriptions,
            rating_title,
            rated_in,
            product_id,
            myPref.getPrefKeyVendorId()
        )
    }

    fun getProductReviewsList(product_id: String): Single<ApiResponse<List<ProductRatingModel>>> {
        return mainApi.getProductReviewsList(
            product_id,
            myPref.getPrefKeyVendorId()
        )
    }

    fun getRatingReviewsList(): Single<ApiResponse<List<RatingReviewsListModel>>> {
        return mainApi.getRatingReviewsList(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId()
        )
    }


    fun makeFavReq(
        warehouse_product_id: String,
        product_id: String,
        is_favourite: Int,
        group_id: String
    ): Single<ApiResponse<SetFavModel>> {
        return mainApi.setFavourite(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            warehouse_product_id,
            product_id,
            is_favourite,
            group_id
        )
    }


    fun getProductCompare(
        products: JSONArray
    ): Single<ApiResponse<List<ProductModel>>> {
        return mainApi.getProductCompare(
            Constants.HEADER_TOKEN,
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyStoreId(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            products
        )
    }

}