package com.electroworld.android.repository

import androidx.lifecycle.MutableLiveData
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.DeliverySlotResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProductRepositoryImpl @Inject constructor(
    private val mainApi: MainApi,
    private val myPref: PreferencesManager
) {

    private val disposable = CompositeDisposable()
    val apiError = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()
    val deliverySlotApi = MutableLiveData<DeliverySlotResponse>()

    fun getDeliverySlots() {
        disposable.add(
            mainApi.getDeliveryslotTimingsTest(
                myPref.getPrefKeyVendorId(),
                myPref.getPrefKeyLanguage()
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<DeliverySlotResponse>() {
                    override fun onSuccess(res: DeliverySlotResponse) {
                        loading.value = false
                        if(res.http_code==200){
                            deliverySlotApi.value = res
                        }
                        else{
                            apiError.value = res.message
                        }
                    }

                    override fun onError(e: Throwable) {
                        apiError.value = e.message
                        loading.value = false
                    }
                })
        )
    }
}