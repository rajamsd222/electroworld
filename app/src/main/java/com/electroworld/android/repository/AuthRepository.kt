package com.electroworld.android.repository

import androidx.lifecycle.MutableLiveData
import com.electroworld.android.api.auth.AuthApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.DeliverySlotResponse
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val authApi: AuthApi,
    private val myPref: PreferencesManager
){

    private val disposable = CompositeDisposable()
    val apiError = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()
    val deliverySlotApi = MutableLiveData<DeliverySlotResponse>()

}