package com.electroworld.android.repository

import androidx.lifecycle.MutableLiveData
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.main.MainApi
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.model.*
import com.electroworld.android.model.orderdetail.OrderDetailModel
import com.electroworld.android.model.orderlist.OrderListModel
import com.electroworld.android.ui.main.notifications.NotificationModel
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.json.JSONArray
import javax.inject.Inject

class OMSRepository @Inject constructor(val mainApi: MainApi, val myPref: PreferencesManager) {



    private val disposable = CompositeDisposable()
    val apiError = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()
    val _liveDataPaymentList = MutableLiveData<List<PaymentModel>>()


    fun makeOrderAsCOD(orderId: String?): Single<BaseResponse> {
        return mainApi.makeOrderAsCOD(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            orderId
        )
    }

    fun getDeliverySlots(): Single<ApiResponse<DeliverySlotApiModel>> {
        return mainApi.getDeliveryslotTimings(
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyLanguage()
        )
    }

/*
    fun getPaymentsList() {
        disposable.add(
            mainApi.getPayments(Constants.HEADER_TOKEN, myPref.getPrefKeyAccessToken(), myPref.getPrefKeyVendorId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<PaymentModeResponse>() {
                    override fun onSuccess(res: PaymentModeResponse) {
                        loading.value = false
                        if(res.http_code==200){
                            _liveDataPaymentList.value = res.data
                        }
                        else{
                            apiError.value = res.message
                        }
                    }

                    override fun onError(e: Throwable) {
                        apiError.value = e.message
                        loading.value = false
                    }
                })
        )
    }*/

    fun getCashFreeToken(orderId: String?,amount: String?): Single<ApiResponse<CashFreeTokenReponse>> {
        return mainApi.getCashFreeToken(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            orderId,
            amount
        )
    }
    fun updateWalletAmount(orderId: String?,orderAmount: String?,referenceId: String?,txStatus: String?,txMsg: String?,
                    txTime: String?,signature: String?,paymentMode: String?): Single<ApiResponse<AddMoneyToWalletModel>> {
        return mainApi.doUpdateWalletAmount(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            orderId,
            orderAmount,
            referenceId,
            txStatus,
            txMsg,
            txTime,
            signature,
            paymentMode
        )
    }
    fun updateOrder(orderId: String?,orderAmount: String?,referenceId: String?,txStatus: String?,txMsg: String?,
                    txTime: String?,signature: String?,paymentMode: String?): Single<ApiResponse<CashFreeCallBack>> {
        return mainApi.doOrderUpdate(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            orderId,
            orderAmount,
            referenceId,
            txStatus,
            txMsg,
            txTime,
            signature,
            paymentMode
        )
    }

    fun getPaymentProvidersList(): Single<ApiResponse<PaymentProvidersModel>> {
        return mainApi.getPaymentProvidersList(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId()
        )
    }

    fun getOrderList(page: Int, status: Int, is_return: Int): Single<ApiResponse<OrderListModel>> {
        return mainApi.orderlist(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            page,
            status,
            is_return
        )
    }


    fun doReOrder(order_id: String?): Single<ApiResponse<ReOrderModel>> {
        return mainApi.doReOrderApi(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            order_id
        )
    }
    fun doReturnOrder(order_id: String?,products : JSONArray): Single<ApiResponse<DefaultData>> {
        return mainApi.returnOrder(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            order_id,
            products
        )
    }


    fun makeOrderDetailReq(order_id: String?): Single<ApiResponse<OrderDetailModel>> {
        return mainApi.orderDetail(
            myPref.getPrefKeyAccessToken(),
            order_id,
            myPref.getPrefKeyVendorId()
        )
    }
    fun doFavouriteOrder(order_id: String?): Single<ApiResponse<DefaultData>> {
        return mainApi.doOrderFavApi(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            order_id,
            myPref.getPrefKeyLanguage()
        )
    }
    fun doCancelOrder(order_id: String?): Single<ApiResponse<DefaultData>> {
        return mainApi.cancelOrder(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            order_id
        )
    }

    fun makeNotificationReq(): Single<ApiResponse<List<NotificationModel>>> {
        return mainApi.getNotificationList(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId(),
            myPref.getPrefKeyLanguage()
        )
    }

    fun makeTransactionListReq(): Single<ApiResponse<List<TransactionListModel>>> {
        return mainApi.getTransactionList(
            myPref.getPrefKeyAccessToken(),
            myPref.getPrefKeyCustomerId(),
            myPref.getPrefKeyVendorId()
        )
    }


}