package com.electroworld.android.utils

object Constants {
    const val HEADER_TOKEN =
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjIxMiwiaWF0IjoxNTc0MTUwMTYzLCJleHAiOjE2MDU2ODYxNjN9.0b7PxovIcFiWayJduIklNs_0lidod2EkD0FcYEKaF5Q"
    const val USER_TYPE = "1"
    const val ROLE = "1"
    const val COUNTRY_CODE = "+91"
    const val DEVICE_TYPE = "android"
    const val VENDOR_ID = "3"
    var COMES_FROM_SPLASH = true
    const val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123
    const val LOCATION_PERMISSION_REQUEST_CODE = 1001
    const val DEFAULT_MAX_QTY = 10

    @JvmField
    var LATITUDE = 0.0

    @JvmField
    var LONGITUDE = 0.0
    var ADDRESS_ID = ""
    var ADDRESS_TYPE = ""

    @JvmField
    var ADDRESS_NAME = ""
    const val KEY_LAT_LNG_UPDATED = "key_lat_lng"
    var COMES_FROM_DELIVERY_LOCATION = false
    var DELIVERY_TIME = ""
    var PAYMENT_METHOD = ""
    var BANK_TRANSFER_NUMBER = ""
    var SORT_BY_TYPE_NAME = "name"
    var SORT_BY_TYPE_PRICE_LOW_TO_HIGH = "price_low_to_high"
    var SORT_BY_TYPE_PRICE_HIGH_TO_LOW = "price_high_to_low"
    var SORT_BY_TYPE_LATEST = "date"
    var SORT_BY_TYPE_POPULARITY = "popularity"
    var SORT_BY_TYPE_RELEVANT = "relevant"
    var SORT_BY_TYPE_DISCOUNT_HIGH_TO_LOW = "discount_high_to_low"
    var SORT_BY_TYPE_DISCOUNT_LOW_TO_HIGH = "discount_low_to_high"
    var SORT_BY = SORT_BY_TYPE_LATEST
    var ORDER_BY = ""
    var SORT_BY_TITLE = ""
    const val KEY_TYPE_VEG = "Veg"
    const val KEY_TYPE_NON_VEG = "Non-Veg"
    var DELIVERY_SLOT_TEXT = ""
}