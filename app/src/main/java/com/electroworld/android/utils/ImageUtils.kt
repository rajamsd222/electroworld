package com.electroworld.android.utils

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.FileProvider
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

object ImageUtils {
    @JvmStatic
    fun getLocalBitmapUri(activity: Activity, imageView: ImageView): Uri? {
        val drawable = imageView.drawable
        var bmp: Bitmap? = null
        bmp = if (drawable is BitmapDrawable) {
            (imageView.drawable as BitmapDrawable).bitmap
        } else {
            return null
        }
        var bmpUri: Uri? = null
        try {
            val file = File(
                activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "share_image_" + System.currentTimeMillis() + ".png"
            )
            val out = FileOutputStream(file)
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out)
            out.close()
            bmpUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                getURI(activity, file)
            } else {
                Uri.fromFile(file)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri
    }

    fun getURI(mCtx: Context, file: File?): Uri {
        return FileProvider.getUriForFile(mCtx, mCtx.packageName + ".fileprovider", file!!)
    }

    fun saveImageFromBitmap(context: Activity?, showedImgae: Bitmap?): String {
        return if (showedImgae != null) {
            //String root = Environment.getExternalStorageDirectory().toString();
            val myDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            )
            myDir.mkdirs()
            val fname = "image1.jpg"
            val file = File(myDir, fname)
            if (file.exists()) file.delete()
            try {
                val out = FileOutputStream(file)
                showedImgae.compress(Bitmap.CompressFormat.JPEG, 100, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(context, "" + e.message, Toast.LENGTH_LONG).show()
            }
            file.absolutePath
        } else {
            ""
        }
    }

    fun saveImageFromBitmapDynamic(context: Activity?, showedImgae: Bitmap?): String {
        return if (showedImgae != null) {
            //String root = Environment.getExternalStorageDirectory().toString();
            val myDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            )
            myDir.mkdirs()
            val fname = (System.currentTimeMillis() / 1000).toString() + ".jpg"
            val file = File(myDir, fname)
            if (file.exists()) file.delete()
            try {
                val out = FileOutputStream(file)
                showedImgae.compress(Bitmap.CompressFormat.JPEG, 100, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(context, "" + e.message, Toast.LENGTH_LONG).show()
            }
            file.absolutePath
        } else {
            ""
        }
    }
}