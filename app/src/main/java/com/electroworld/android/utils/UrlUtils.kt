package com.electroworld.android.utils

import android.content.Context
import com.electroworld.android.R

object UrlUtils {


    fun formatPlaceSearchUrl(context: Context, text: String): String {
        return "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + text + "&key=" + context.getString(
            R.string.place_search_key
        )
    }

}