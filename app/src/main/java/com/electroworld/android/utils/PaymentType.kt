package com.electroworld.android.utils

import java.io.Serializable

enum class PaymentType : Serializable {
    CARD, UPI, NETBANKING, WALLET, DEFAULT
}