package com.electroworld.android.utils

import android.text.TextUtils

object AppValidator {
    @JvmStatic
    fun toStr(str: Any?): String {
        var result = ""
        if (str == null) return result
        return if (str is String) {
            result = str
            result = if (TextUtils.isEmpty(result)) "" else if (result.equals(
                    "null",
                    ignoreCase = true
                )
            ) "" else result.replace("^\\s+".toRegex(), "")
            result
        } else {
            result
        }
    }


    fun toInt(str: String?): Int {
        if (str != null && toStr(str).isNotEmpty()) {
            return Integer.valueOf(str)
        }
        return 0
    }

    fun toFloat(str: String?): Float {
        return if (toStr(str).isNotEmpty()) java.lang.Float.valueOf(str!!) else 0F
    }

    @JvmStatic
    fun toDouble(str: String?): Double {
        var ss = toStr(str).replace(",".toRegex(), "")
        //ss = ss.replace("-".toRegex(), "")
        return if (ss.length > 0) java.lang.Double.valueOf(ss) else 0.0
    }

    @JvmStatic
    fun toDoubleNew(str: String?): Double {
        val ss = toStr(str).replace(",".toRegex(), "")
        return if (ss.length > 0) java.lang.Double.valueOf(ss) else 0.0
    }
}