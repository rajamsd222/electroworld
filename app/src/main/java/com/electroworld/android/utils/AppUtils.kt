package com.electroworld.android.utils

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.util.Patterns
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.electroworld.android.R
import com.electroworld.android.utils.AppValidator.toDouble
import com.electroworld.android.utils.ImageUtils.getLocalBitmapUri
import java.text.DecimalFormat

object AppUtils {
    /**
     * This is method for check the Internet connection
     */
    fun isOnline(mContext: Context): Boolean {
        val connec = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connec != null) {
            val result = connec.activeNetworkInfo
            return result != null && result.isConnectedOrConnecting
        }
        return false
    }

    /**
     * This method returns the custom progress dialog
     *
     * @param context
     * @return
     */
    fun showCustomDialog(context: Context?): Dialog {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.progress_dialog)
        return dialog
    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return if (target == null) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    fun getRazorPayAmount(amount: String?): String {
        var razorAmount = ""
        val payAmount = toDouble(amount)
        razorAmount = DecimalFormat("######0.00").format(payAmount).replace("\\.".toRegex(), "")
        return razorAmount
    }

    fun openShareIntent(activity: Activity, pImageView: ImageView?, strUrl: String?, groupID: String?) {
        var bmpUri: Uri? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionCheck = ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                bmpUri = getLocalBitmapUri(activity, pImageView!!)
            } else {
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    1
                )
            }
        } else {
            bmpUri = getLocalBitmapUri(activity, pImageView!!)
        }
        if (bmpUri != null) {
            // Construct a ShareIntent with link to image
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            //shareIntent.setPackage("com.whatsapp");
            shareIntent.putExtra(Intent.EXTRA_TEXT, strUrl)
            //shareIntent.putExtra(Intent.EXTRA_SUBJECT, strUrl); // NB:
            shareIntent.type = "image/*"
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri)
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            activity.startActivity(Intent.createChooser(shareIntent, "Share Image"))
        } else {
            Toast.makeText(activity, "Sharing Failed !!", Toast.LENGTH_SHORT).show()
        }
    }
}