package com.electroworld.android.utils

import android.content.Context
import android.widget.EditText
import android.widget.Toast

object UiUtils {


    fun showToast(context: Context?, message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showInputAlert(context: Context?, message: String?, editText: EditText) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        editText.requestFocus()
        editText.setSelection(editText.text.length)
    }

}