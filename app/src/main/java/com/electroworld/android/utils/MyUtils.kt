package com.electroworld.android.utils

import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object MyUtils {

    fun formatCurrencyfromDouble(amt: Double): String {
        val currencySymbol = "₹ "
        var amount = amt
        if (amt < 0) {
            amount = amt + 2 * (-1 * amt) //change negative to positive
        }
        val decimalFormat = "##,##,##0.00"
        var amountStr = DecimalFormat(decimalFormat).format(amount)
        amountStr = currencySymbol + amountStr
        amountStr = if (amt < 0) "- $amountStr" else amountStr
        amountStr = amountStr/*.replace(".00", "")*/
        return amountStr
    }

    fun formatCurrencyFromDoubleWithoutSymbol(amt: Double): Double {
        //val currencySymbol = "₹ "
        var amount = amt
        if (amt < 0) {
            amount = amt + 2 * (-1 * amt) //change negative to positive
        }
        val decimalFormat = "##,##,##0.00"
        var amountStr = DecimalFormat(decimalFormat).format(amount)
       // amountStr = currencySymbol + amountStr
        amountStr = if (amt < 0) "-$amountStr" else amountStr
        amountStr = amountStr/*.replace(".00", "")*/
        return AppValidator.toDouble(amountStr)
    }

    fun formatCurrencyfromString(amt: String?): String {
        return formatCurrencyfromDouble(AppValidator.toDouble(amt))
    }

    fun formatDates(dateSelected: String?, fromFormat: String?, toFormat: String): String? {
        val currentDate = ""
        val inputFormat = SimpleDateFormat(fromFormat, Locale.ENGLISH)
        val outputFormat = SimpleDateFormat(toFormat, Locale.ENGLISH)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(dateSelected)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }
}