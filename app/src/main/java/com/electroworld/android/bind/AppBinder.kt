package com.electroworld.android.bind

import android.graphics.Paint
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.electroworld.android.R
import com.electroworld.android.databinding.RowOrderSummaryBinding
import com.electroworld.android.databinding.RowPromotionItemBinding
import com.electroworld.android.extentions.getColorCompat
import com.electroworld.android.model.AddressListModel
import com.electroworld.android.model.FOCModel
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.orderlist.OrderListProductItem
import com.electroworld.android.ui.main.orders.SummaryModel
import com.electroworld.android.ui.main.products.GroupVariantsModel
import com.electroworld.android.utils.AppValidator
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.MyUtils
import java.text.DecimalFormat

object AppBinder {
    @BindingAdapter("setOrderSummary")
    fun setOrderSummary(layout: LinearLayout, data: List<SummaryModel>?) {
        layout.removeAllViews()
        val inflater = LayoutInflater.from(layout.context)
        if (data != null) {
            for (i in data.indices) {
                val model = data[i]
                layout.visibility = View.VISIBLE
                val binding: RowOrderSummaryBinding = DataBindingUtil.inflate(
                    inflater,
                    R.layout.row_order_summary, layout, true
                )
                binding.item = model

                if (model.name == layout.context.getString(R.string.os_lbl_delivery_charges)) {
                    if (model.value == 0.0) {
                        binding.tvRight.text = layout.context.getString(R.string.free)
                        binding.tvRight.setTextColor(
                            ContextCompat.getColor(
                                layout.context,
                                R.color.order_summary_text_color_free
                            )
                        )
                    } else {
                        amountOrderSummary(binding.tvRight, model.value)
                    }
                } else {
                    amountOrderSummary(binding.tvRight, model.value)
                }
                if (model.value < 0) {
                    binding.tvRight.setTextColor(
                        ContextCompat.getColor(
                            layout.context,
                            R.color.order_summary_text_color_negative
                        )
                    )
                }
                if (i == data.size - 1) {
                    binding.tvRight.setTextColor(
                        ContextCompat.getColor(
                            layout.context,
                            R.color.order_summary_text_color_total
                        )
                    )
                    binding.tvLeft.setTextColor(
                        ContextCompat.getColor(
                            layout.context,
                            R.color.order_summary_text_color_total
                        )
                    )
                    //binding.tvRight.setTypeface(binding.tvLeft.getTypeface(), Typeface.BOLD);
                    // binding.tvLeft.setTypeface(binding.tvLeft.typeface, Typeface.BOLD)
                    binding.line.visibility = View.VISIBLE
                    binding.tvLeft.setTextSize(
                        TypedValue.COMPLEX_UNIT_PX,
                        layout.context.getResources().getDimension(R.dimen._12sdp)
                    );
                    binding.tvRight.setTextSize(
                        TypedValue.COMPLEX_UNIT_PX,
                        layout.context.getResources().getDimension(R.dimen._13sdp)
                    );
                } else {
                    binding.line.visibility = View.GONE
                }
            }
        }
    }

    @JvmStatic
    @BindingAdapter("amountOrderSummary")
    fun amountOrderSummary(textView: TextView, amt: Double) {
        textView.text = "₹ " + DecimalFormat("##,##,##0.00").format(amt)/*.replace(".00", "")*/
    }

    @JvmStatic
    @BindingAdapter("setPromotionItem")
    fun setPromotionItem(layout: LinearLayout, data: List<FOCModel?>?) {
        layout.removeAllViews()
        val inflater = LayoutInflater.from(layout.context)
        if (data != null) {
            for (model in data) {
                layout.visibility = View.VISIBLE
                val binding: RowPromotionItemBinding = DataBindingUtil.inflate(
                    inflater,
                    R.layout.row_promotion_item, layout, true
                )
                binding.item = model
            }
        }
    }

    @JvmStatic
    @BindingAdapter("setAddressText")
    fun setAddressText(textView: TextView, data: AddressListModel?) {
        if (data != null) {
            textView.text = data.house_no
            if (data.apartment_name != null && data.apartment_name.length > 0) {
                textView.text = data.house_no + ", " + data.apartment_name
            }
            if (data.street != null && data.street.length > 0) {
                textView.text = data.house_no + ", " + data.apartment_name + ", " + data.street
            }
        }
    }

    @JvmStatic
    @BindingAdapter("setMRPPriceOrderDetail")
    fun setMRPPriceOrderDetail(textView: TextView, model: OrderListProductItem?) {
        if (model != null) {
            if (AppValidator.toDouble(model.price) == AppValidator.toDouble(model.discounted_price)) {
                textView.visibility = View.GONE
            } else {
                textView.visibility = View.VISIBLE
                textView.paintFlags = textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                textView.text = "MRP " + "₹ " + DecimalFormat("##,##,##0.00")
                    .format(AppValidator.toDouble(model.price))/*.replace(".00", "")*/
            }
        }
    }

    @JvmStatic
    @BindingAdapter("setMRPPrice")
    fun setMRPPrice(textView: TextView, model: ProductModel?) {
        if (model != null) {
            if (AppValidator.toDouble(model.price) == AppValidator.toDouble(model.discounted_price)) {
                textView.visibility = View.GONE
            } else {
                textView.visibility = View.VISIBLE
                textView.paintFlags = textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                textView.text = "MRP " + "₹ " + DecimalFormat("##,##,##0.00")
                    .format(AppValidator.toDouble(model.price))/*.replace(".00", "")*/
            }
        }
    }

    @JvmStatic
    @BindingAdapter("setMRPPriceVariant")
    fun setMRPPriceVariant(textView: TextView, model: GroupVariantsModel?) {
        if (model != null) {
            if (AppValidator.toDouble(model.price) == AppValidator.toDouble(model.discounted_price)) {
                textView.visibility = View.GONE
            } else {
                textView.visibility = View.VISIBLE
                textView.paintFlags = textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                textView.text =
                    "₹ " + DecimalFormat("##,##,##0.00").format(AppValidator.toDouble(model.price))
                /*.replace(".00", "")*/
            }
        }
    }

    @JvmStatic
    @BindingAdapter("setDiscountedPrice")
    fun setDiscountedPrice(textView: TextView, discounted_price: String?) {
        if (AppValidator.toDouble(discounted_price) < 0) {
            textView.text = "₹ " + DecimalFormat("##,##,##0.00")
                .format(AppValidator.toDouble("0"))
        } else {
            textView.text = "₹ " + DecimalFormat("##,##,##0.00")
                .format(AppValidator.toDouble(discounted_price))
        }

    }

    @JvmStatic
    @BindingAdapter("setFoodTypeImage")
    fun setFoodTypeImage(imageView: ImageView, type: String?) {
        if (AppValidator.toStr(type) == Constants.KEY_TYPE_NON_VEG) {
            imageView.visibility = View.VISIBLE
            imageView.setImageResource(R.mipmap.icon_non_veg)
        } else if (AppValidator.toStr(type) == Constants.KEY_TYPE_VEG) {
            imageView.visibility = View.VISIBLE
            imageView.setImageResource(R.mipmap.icon_veg)
        } else {
            imageView.visibility = View.GONE
        }
        imageView.visibility = View.GONE
    }

    @JvmStatic
    @BindingAdapter("setFavImage")
    fun setFavImage(imageView: ImageView, is_favourite: Int) {
        if (is_favourite == 1) {
            imageView.setImageResource(R.drawable.ic_favorite_color_primary_24dp)
        } else {
            imageView.setImageResource(R.drawable.ic_favorite_border_black_24dp)
        }
    }

    @JvmStatic
    @BindingAdapter("amountRound")
    fun formatCurrencyFromString(textView: TextView, amt: String?) {
        if (AppValidator.toDouble(amt) < 0) {
            textView.text = "₹ " + DecimalFormat("##,##,##0.00").format(AppValidator.toDouble("0"))
        } else {
            textView.text = "₹ " + DecimalFormat("##,##,##0.00").format(AppValidator.toDouble(amt))
        }
    }

    @JvmStatic
    @BindingAdapter("setVariantValue")
    fun setVariantValue(textView: TextView, model: ProductModel ) {
       var text = ""
        if(AppValidator.toStr(model.variant1).isNotEmpty()){
            text = AppValidator.toStr(model.variant1)
        }
        if(AppValidator.toStr(model.variant2).isNotEmpty()){
            if(text.isEmpty()){
                text = AppValidator.toStr(model.variant2)
            }else {
                text = text + " - " + AppValidator.toStr(model.variant2)
            }
        }
        textView.text = text
        //textView.visibility = if (text.isEmpty()) View.GONE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("setVariantValue")
    fun setVariantValue(textView: TextView, model: GroupVariantsModel ) {
        var text = ""
        if(AppValidator.toStr(model.variant1).isNotEmpty()){
            text = AppValidator.toStr(model.variant1)
        }
        if(AppValidator.toStr(model.variant2).isNotEmpty()){
            if(text.isEmpty()){
                text = AppValidator.toStr(model.variant2)
            }else {
                text = text + " - " + AppValidator.toStr(model.variant2)
            }
        }
        textView.text = text
        //textView.visibility = if (text.isEmpty()) View.GONE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("setVariantValue")
    fun setVariantValue(textView: TextView, model: OrderListProductItem ) {
        var text = ""
        if(AppValidator.toStr(model.variant1).isNotEmpty()){
            text = AppValidator.toStr(model.variant1)
        }
        if(AppValidator.toStr(model.variant2).isNotEmpty()){
            if(text.isEmpty()){
                text = AppValidator.toStr(model.variant2)
            }else {
                text = text + " - " + AppValidator.toStr(model.variant2)
            }
        }
        textView.text = text
        //textView.visibility = if (text.isEmpty()) View.GONE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("setVariantValue")
    fun setVariantValue(textView: TextView, model: FOCModel ) {
        var text = ""
        if(AppValidator.toStr(model.variant1).isNotEmpty()){
            text = AppValidator.toStr(model.variant1)
        }
        if(AppValidator.toStr(model.variant2).isNotEmpty()){
            if(text.isEmpty()){
                text = AppValidator.toStr(model.variant2)
            }else {
                text = text + " - " + AppValidator.toStr(model.variant2)
            }
        }
        textView.text = text
        //textView.visibility = if (text.isEmpty()) View.GONE else View.VISIBLE
    }



    @BindingAdapter("amountRoundFromDouble")
    fun formatCurrencyFromDouble(textView: TextView, amt: Double) {
        textView.text = "₹ " + DecimalFormat("##,##,##0.00").format(amt)/*.replace(".00", "")*/
    }

    @JvmStatic
    @BindingAdapter("setOrderDate")
    fun setOrderDate(textView: TextView, date: String?) {
        if (date != null) {
            textView.text = "Purchased On - " + MyUtils.formatDates(
                date.substring(
                    0,
                    10
                ), "yyyy-MM-dd", "dd-MM-yyyy"
            )
        }
    }

    /*9 - Returned
10 - Cancelled
12 - Partially Returned
14 - Return Requested
15 - Return Rejected*/
    @JvmStatic
    @BindingAdapter("setOrderStatusColor")
    fun setOrderStatusColor(textView: TextView, statusId: Int) {
        if (statusId == 9 || statusId == 12 || statusId == 14 || statusId == 15) {
            textView.setTextColor(getColorCompat(R.color.os_return_color))
        } else if (statusId == 10) {
            textView.setTextColor(getColorCompat(R.color.os_return_color))
        } else {
            if (statusId >= 2) {
                textView.setTextColor(getColorCompat(R.color.os_accept_color))
                if (statusId >= 5) {
                    textView.setTextColor(getColorCompat(R.color.os_dispatched_color))
                    if (statusId >= 7) {
                        textView.setTextColor(getColorCompat(R.color.os_out_for_delivery_color))
                        if (statusId >= 7) {
                            textView.setTextColor(getColorCompat(R.color.os_delivered_color))
                        }
                    }
                }
            }
        }


        /*
        else{
            textView.setTextColor(textView.getResources().getColor(R.color.os_out_for_delivery_color));
        }*/
    }

    @JvmStatic
    @BindingAdapter("profileImage")
    fun setProfileImage(view: ImageView, url: String?) {
        if (!TextUtils.isEmpty(url)) {
            Glide.with(view.context)
                .load(url)
                .circleCrop()
                .placeholder(R.mipmap.avata_profile)
                .into(view)
        } else {
            view.setImageResource(R.mipmap.avata_profile)
        }
    }

    @JvmStatic
    @BindingAdapter("circleImage")
    fun setAppCircleImage(view: ImageView, url: String?) {
        Glide.with(view.context)
            .load(url)
            .circleCrop()
            .into(view)
    }

    @JvmStatic
    @BindingAdapter("glideFitXY")
    fun setglideFitXY(view: ImageView, url: String?) {
        val cornerRadius = 2
        val px =
            Math.round(cornerRadius * (view.context.resources.displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
        Glide.with(view.context)
            .load(url)
            .transform(RoundedCorners(px))
            .into(view)
    }

    @BindingAdapter("bannerImage")
    fun setbannerImage(view: ImageView, url: String?) {
        val cornerRadius = 2
        val px =
            Math.round(cornerRadius * (view.context.resources.displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
        Glide.with(view.context)
            .load(url)
            .centerCrop()
            .transform(RoundedCorners(px))
            .into(view)
    }

    @BindingAdapter("glideCropCenter")
    fun setImageAsCropCenter(view: ImageView, url: String?) {
        val cornerRadius = 3
        val px =
            Math.round(cornerRadius * (view.context.resources.displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
        Glide.with(view.context)
            .load(url)
            .centerCrop()
            .transform(RoundedCorners(px))
            .into(view)
    }

    @JvmStatic
    @BindingAdapter("glideCenterInside")
    fun setImageAsCenterInside(view: ImageView, url: String?) {
        val cornerRadius = 3
        val px =
            Math.round(cornerRadius * (view.context.resources.displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
        Glide.with(view.context)
            .load(url)
            .centerInside()
            .transform(RoundedCorners(px))
            .into(view)
    }

    @JvmStatic
    @BindingAdapter("centerInsideNoPadding")
    fun setGlideCenterInsideNoPadding(view: ImageView, url: String?) {
        Glide.with(view.context)
            .load(url)
            .centerInside()
            .into(view)
    }
}