package com.electroworld.android.fcm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.electroworld.android.R
import com.electroworld.android.di.storage.PreferencesManager
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.utils.AppValidator.toStr
import dagger.android.AndroidInjection
import java.util.*
import javax.inject.Inject

class MyFirebaseMessagingService : FirebaseMessagingService() {
    @JvmField
    @Inject
    var myPref: PreferencesManager? = null
    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onNewToken(token: String) {
        Log.e(TAG, "Refreshed token: $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.from)
        if (remoteMessage.data.size > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.data)
            sendNotification(remoteMessage)
        }
    }

    private fun sendNotification(remoteMessage: RemoteMessage) {
        val title = getTitle(remoteMessage)
        val message = getMessage(remoteMessage)
        val order_id = getOrderId(remoteMessage)
        val intent = Intent(this, MainActivity::class.java)
        if (order_id.length > 0) {
            intent.putExtra("navigate_screen_name", "my_orders")
            intent.putExtra("order_id", order_id)
        } else {
            //intent.putExtra("navigate_screen_name", "notification")
            intent.putExtra("navigate_screen_name", "home")
        }
        val logoIcon = R.drawable.ic_stat_ic_notification
        val requestID = System.currentTimeMillis().toInt()
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, requestID, intent,
            PendingIntent.FLAG_ONE_SHOT
        )
        val notificationBuilder = NotificationCompat.Builder(this, title)
            .setSmallIcon(logoIcon)
            .setContentTitle(title)
            .setContentText(message)
            .setAutoCancel(true)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .setBigContentTitle(title)
                    .bigText(message)
            )
            .setContentIntent(pendingIntent)
        notificationBuilder.setDefaults(Notification.DEFAULT_SOUND)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        createChannels(notificationManager)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            postNotification(this, "my_channel_01", title, message, pendingIntent, logoIcon)
        } else {
            assert(notificationManager != null)
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
        }
    }

    private fun postNotification(
        context: Context,
        channelId: String,
        strtitle: String,
        message: String,
        pendingIntent: PendingIntent,
        logoIcon: Int
    ) {
        val mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        var notificationBuilder: NotificationCompat.Builder? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder = NotificationCompat.Builder(context, channelId)
                .setSmallIcon(logoIcon)
                .setContentTitle(strtitle)
                .setContentText(message)
                .setAutoCancel(true)
                .setStyle(
                    NotificationCompat.BigTextStyle()
                        .setBigContentTitle(strtitle)
                        .bigText(message)
                )
                .setContentIntent(pendingIntent)
        }
        val notificationId = Random().nextInt()
        mNotificationManager.notify(notificationId, notificationBuilder!!.build())
    }

    fun createChannels(notificationManager: NotificationManager?) {
        // create sound channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val androidChannel = NotificationChannel(
                "my_channel_01",
                "my_channel_name_01", NotificationManager.IMPORTANCE_HIGH
            )
            // Sets whether notifications posted to this channel should display notification lights
            androidChannel.enableLights(true)
            // Sets whether notification posted to this channel should vibrate.
            androidChannel.enableVibration(true)
            // Sets the notification light color for notifications posted to this channel
            androidChannel.lightColor = Color.GREEN
            notificationManager!!.createNotificationChannel(androidChannel)
        }
    }

    private fun getTitle(remoteMessage: RemoteMessage): String {
        return if (myPref!!.isEnglishLanguage()) {
            toStr(remoteMessage.data["title"])
        } else {
            toStr(remoteMessage.data["title_bs"])
        }
    }

    private fun getMessage(remoteMessage: RemoteMessage): String {
        return if (myPref!!.isEnglishLanguage()) {
            toStr(remoteMessage.data["message"])
        } else {
            toStr(remoteMessage.data["message_bs"])
        }
    }

    private fun getOrderId(remoteMessage: RemoteMessage): String {
        return toStr(remoteMessage.data["order_id"])
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }
}