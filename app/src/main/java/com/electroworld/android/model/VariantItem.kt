package com.electroworld.android.model

data class VariantItem(
    var id: String,
    var group_id: String,
    var value: String,
    var selected: Int,
    var price: String
)
