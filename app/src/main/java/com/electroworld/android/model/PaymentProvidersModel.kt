package com.electroworld.android.model

data class PaymentProvidersModel(
    val cashfree_app_id: String,
    val cashfree_app_env: String,
    val oneline_payment_options: List<PaymentModel>,
    val net_banking_provider: List<PaymentProviderItem>,
    val wallet_provider: List<PaymentProviderItem>
)

data class PaymentProviderItem(
    val provider_name: String,
    val provider_code: String,
    var isSelect: Boolean = false
)