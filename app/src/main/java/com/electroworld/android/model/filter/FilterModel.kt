package com.electroworld.android.model.filter

data class FilterModel(
    val filter_data: List<FilterData>,
    val key: String,
    val title: String,
    val type: String
)