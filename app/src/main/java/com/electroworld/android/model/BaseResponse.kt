package com.electroworld.android.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseResponse {

    @SerializedName("http_code")
    @Expose
    val http_code = 0

    @SerializedName("message")
    @Expose
    val message: String? = null

}