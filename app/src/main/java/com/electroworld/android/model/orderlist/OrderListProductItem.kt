package com.electroworld.android.model.orderlist

import java.io.Serializable

data  class OrderListProductItem (
    val id: String,
    val product_id: String,
    val discounted_price: String,
    val image1: String,
    val group_name_en: String,
    val name_en: String,
    val price: String,
    val quantity: Int,
    val is_return_order: Int,
    val is_return_confirmed: Int,
    val is_partially_canceled: Int,
    val is_partially_canceled_label: String,
    val subtotal: String,
    val rating: String,
    val rating_descriptions: String,
    val rating_created_at: String,
    val variant_en: String,
    val variant1: String,
    val variant2: String,
    val variant_type1_name_en: String,
    val variant_type2_name_en: String,
    var isSelect : Boolean = false
) : Serializable