package com.electroworld.android.model

data class UserModel(
    val email: String,
    val firstname: String,
    val lastname: String,
    val IsActive: String,
    val IsVerified: String,
    val CustomerId: String,
    var phone: String,
    val profile_image: String,
    val role: String,
    val Type: String,
    val token: String,
    val otp: String,
    val gst_number: String?,
    val gst_invoice_name : String?,
    val wallet_balance: Double,
    val authentication_id: String,
    val authentication_device_id: String
)