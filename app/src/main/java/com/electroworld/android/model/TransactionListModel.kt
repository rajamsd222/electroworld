package com.electroworld.android.model

data class TransactionListModel (
    val amount_paid: String,
    val transaction_type: String,
    val transaction_description: String,
    val created_at: String
)