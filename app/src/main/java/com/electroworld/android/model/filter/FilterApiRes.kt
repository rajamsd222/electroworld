package com.electroworld.android.model.filter

data class FilterApiRes(
    val filter_list: List<FilterModel>,
    val sort_by: List<SortBy>
)