package com.electroworld.android.model

data class OutOfStockProductInfo (
    val available_qty: Int,
    val image1: String,
    val out_of_stock: Int,
    val product_id: String,
    val product_name: String,
    val warehouse_id: Int,
    val warehouse_product_id: String
)