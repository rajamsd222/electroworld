package com.electroworld.android.model

data class ImagesModel(
    val image: String,
    val title: String,
    val id: String
)