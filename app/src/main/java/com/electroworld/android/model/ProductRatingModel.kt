package com.electroworld.android.model

data class ProductRatingModel(
    val user_name : String,
    val user_image : String,
    val rating_descriptions : String,
    val rating : Float,
    val created_at : String,
)