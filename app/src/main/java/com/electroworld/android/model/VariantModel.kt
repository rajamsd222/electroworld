package com.electroworld.android.model

data class VariantModel(
    var variant_id: String,
    var variant_title: String,
    var variant_data: List<VariantItem>
)
