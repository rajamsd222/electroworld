package com.electroworld.android.model

data class OrderUpdate(
    val product_id: String
)