package com.electroworld.android.model

data class DeliverySlot(
    val slot: String,
    val from_time: String,
    val to_time: String
)