package com.electroworld.android.model

data class UpdatePriceModel(
    var group_variants: List<ProductModel>
)