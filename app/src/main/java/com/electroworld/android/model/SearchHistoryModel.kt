package com.electroworld.android.model

data class SearchHistoryModel (
    val id: String,
    val search_text: String
)