package com.electroworld.android.model

data class ReOrderModel(
    val products: List<ProductModel>?
)