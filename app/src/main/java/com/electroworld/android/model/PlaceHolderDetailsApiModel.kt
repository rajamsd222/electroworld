package com.electroworld.android.model

data class PlaceHolderDetailsApiModel(
    val placeholder: List<Placeholder>,
    val product_count: String
)