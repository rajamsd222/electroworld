package com.electroworld.android.model

import java.io.Serializable

data class AddressListModel(
    val address_id: String,
    val address_type: String,
    val house_no: String,
    val apartment_name: String,
    val mobile_number: String,
    val landmark: String,
    val address: String,
    val street: String,
    val contact_person: String,
    val latitude: Double,
    val longitude: Double
): Serializable