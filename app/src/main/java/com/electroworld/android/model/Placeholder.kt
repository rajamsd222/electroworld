package com.electroworld.android.model

data class Placeholder(
    val description: String,
    val id: String,
    val image: String,
    val name: String,
    val products: List<ProductModel>,
    val sort_order: Int,
    val status: String,
    val title: String,
    val type: String,
    val url: String,
    val cms_page: String
)