package com.electroworld.android.model

data class PromoCodeModel(
    val discount_percent: Double,
    val discount_amount: Double,
    val minimum_order_value: String
)