package com.electroworld.android.model

data class ProductDetailModel(
    var product_variant: List<VariantModel>,
    var products: ProductModel
)