package com.electroworld.android.model

import java.io.Serializable

data class Subcategory(
    val id: String,
    var isSelected : Boolean,
    val name: String
): Serializable