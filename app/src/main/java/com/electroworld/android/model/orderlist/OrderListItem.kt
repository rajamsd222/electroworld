package com.electroworld.android.model.orderlist


data class OrderListItem(
    val order_date: String,
    val created_at: String,
    val customer_first_name: String,
    val customer_id: Int,
    val customer_last_name: String,
    val delivery_note: Any,
    val delivery_time: Any,
    val delivery_time_type: String,
    val grand_total: String,
    val is_favorite: String,
    val is_rated: String,
    val item_count: Int,
    val items: List<OrderListProductItem>?,
    val orderId: String,
    val order_status_history: List<OrderStatusHistoryItem>?,
    val products_subtotal: String,
    val shop_name_bs: Any,
    val shop_name_en: String,
    val status: String,
    val status_id: Int,
    val vendor_address: String
)