package com.electroworld.android.model.orderlist

import java.io.Serializable

data class OrderStatusHistoryItem (
    val status_id: Int,
    val status: String,
    val created_at: String
) : Serializable