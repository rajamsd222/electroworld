package com.electroworld.android.model

data class RatingReviewsListModel(
    val user_name: String,
    val rating_title: String,
    val rating_descriptions: String,
    val rating: Float,
    val name_en: String,
    val image1: String,
    val group_name_en: String,
    val variant_en: String,
    val created_at: String
)