package com.electroworld.android.model

data class CashFreeCallBack(
    val id: String,
    val status_id: String,
    val warehouse_id: String,
    val grand_total: String,
    val wallet_amount: String,
    val shop_name_en: String,
)