package com.electroworld.android.model.filter

data class SortBy(
    val title: String,
    val key: String,
    val default: String,
    val order_by: String
)