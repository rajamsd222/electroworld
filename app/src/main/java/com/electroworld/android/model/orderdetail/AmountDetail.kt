package com.electroworld.android.model.orderdetail

data class AmountDetail(
    val color: String,
    val label: String,
    val label_bs: String,
    val value: String
)