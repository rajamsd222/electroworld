package com.electroworld.android.model

data class OrderPlaceModel(
    val order_id : String,
    val id : String,
    val amount_total : String,
    val currency : String,
    val customer_email : String,
    val customer_contact : String,
    val customer_name : String,
    val cftoken : String,
    val customer_balance_wallet_amount : Double
)