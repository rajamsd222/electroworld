package com.electroworld.android.model

data class CashFreeTokenReponse(
    val cftoken: String
)