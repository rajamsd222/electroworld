package com.electroworld.android.model

data class CheckValidOrderModel(
    val location_product: List<OutOfStockProductInfo>,
    val out_of_stock_product: List<OutOfStockProductInfo>
)