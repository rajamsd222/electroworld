package com.electroworld.android.model

import java.io.Serializable

data class CategoryModel(
    val id: String,
    val image: String,
    val name: String,
    val description: String,
    var isSelected : Boolean,
    val subcategory: List<Subcategory>
) : Serializable
