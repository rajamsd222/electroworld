package com.electroworld.android.model


data class DeliverySlotResponse(
    val delivery_slot: List<ItemDeliverySlot>,
    val gift_to_others_amount: Double,
    val gift_to_others: Int,
    val gift_to_others_message: String,
    val pickup_slot: List<ItemDeliverySlot>,
): BaseResponse()

data class ItemDeliverySlot(
    val slot: String,
    val from_time: String,
    val to_time: String
)

data class ServiceResponse(
    val data: DeliverySlotResponse? = null,
    val http_code: Int = 0,
    val message: String,
    val isSuccess: Boolean = true
)