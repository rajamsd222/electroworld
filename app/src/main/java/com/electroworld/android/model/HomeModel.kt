package com.electroworld.android.model

data class HomeModel(
    val description: String,
    val id: Int,
    val image: String,
    val name: String,
    val placeholders: List<Placeholder>,
    val platform: String,
    val status: String,
    val title: String,
    val type: String,
    val url_key: String
)