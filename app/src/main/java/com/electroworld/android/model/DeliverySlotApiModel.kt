package com.electroworld.android.model

data class DeliverySlotApiModel(
    val delivery_slot: List<DeliverySlot>,
    val gift_to_others_amount: Double,
    val gift_to_others: Int,
    val gift_to_others_message: String,
    val pickup_slot: List<DeliverySlot>,
)