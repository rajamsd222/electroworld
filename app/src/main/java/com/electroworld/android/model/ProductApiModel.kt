package com.electroworld.android.model

data class ProductApiModel(
    val products: List<ProductModel>,
    val product_count: String
)