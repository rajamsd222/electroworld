
package com.electroworld.android.model

data class ProductItemModelCheckout(
    val product_id: String,
    val warehouse_product_id: String,
    val product_name: String,
    val image: String,
)