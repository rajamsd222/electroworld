package com.electroworld.android.model

data class PaymentModeResponse(
    val data: List<PaymentModel>,
) : BaseResponse()

data class PaymentModel(
    val id: String,
    val name_en: String,
    val code: String,
    var isSelect: Boolean = false
)