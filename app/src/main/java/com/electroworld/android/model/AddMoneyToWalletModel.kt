package com.electroworld.android.model

data class AddMoneyToWalletModel(
    val wallet_balance: Double,
    val cftoken: String,
    val id: String,
    val amount_total: String,
    val customer_name: String,
    val customer_email: String,
    val customer_contact: String
)