package com.electroworld.android.model

data class ShippingChargesResponse(
    val delivery_charge : Double,
    val invoice_promotion_product: List<FOCModel>,
    val invoice_promotion_product_message: String,
    val wallet_balance: Double,
    val promotion_product_total_price: Double,
    val store_info: StoreModel
)

data class StoreModel(
    val id : String,
    val name_en : String,
    val address : String,
    val pickup_time : String
)

data class FOCModel(
    val amount_from: String,
    val amount_to: String,
    val free_quantity: Int,
    val price: String,
    val id: Int,
    val image1: String,
    val image: String,
    val main_category_id: Int,
    val main_category_name: String,
    val name_bs: Any,
    val name_en: String,
    val product_id: Int,
    val promotion_type: String,
    val quantity: Int,
    val type: String,
    val variant_bs: Any,
    val variant1: String,
    val variant2: String,
    val variant_en: String
)