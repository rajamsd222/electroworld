package com.electroworld.android.model

data class VersionInfo(
    val app_details: VersionDetails,
    val config: AppConfig
)

data class VersionDetails (
    val android_app_current_version_code: String,
    val android_force_update_version: String
)
data class AppConfig (
    val max_order_product_qty: Int
)