package com.electroworld.android.model.filter

data class FilterData(
    val description: String,
    val id: Int,
    val image: String,
    val label: String,
    val level: Int,
    val name: String,
    var isSelected: Boolean,
    val name_bs: Any,
    val sort_order: Int,
    val status: String
)