package com.electroworld.android.model

data class SendOTPModel(
    val otp: String,
    val authentication_id: String,
    val authentication_device_id: String
)