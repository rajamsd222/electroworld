package com.electroworld.android.model

data class SetFavModel(
    val total_fav_count: Int,
    val warehouse_product_id: String,
    val product_id: String,
    val group_id: String,
    val is_favourite: Int
)