package com.electroworld.android.model.orderlist

data class OrderListModel(
    val order_count: Int,
    val order_list: List<OrderListItem>,
    val order_total_count: Int
)