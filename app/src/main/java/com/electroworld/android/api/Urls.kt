package com.electroworld.android.api

object Urls {


    const val STAGING_SERVER_URL = "http://stage-api.adishwaremart.com/"

    const val TEST_SERVER_URL = "http://asm-d-api.izoleap.com/"
    // const val PRODUCTION_SERVER_URL = "http://143.110.241.103/"
    const val PRODUCTION_SERVER_URL = "https://api.adishwaremart.com/"

    const val BASE_URL = STAGING_SERVER_URL


   /* const val URL_TNC = "http://asm-d-customer.izoleap.com/mobile/cms/terms-conditions"
    const val URL_PP = "http://asm-d-customer.izoleap.com/mobile/cms/privacy-policy"
    const val PREMIUM_ACCESS = "http://asm-d-customer.izoleap.com/mobile/cms/premium-access"*/

    const val URL_TNC                       = "http://stage-web.adishwaremart.com/cms/terms-conditions"
    const val URL_PP                        = "http://stage-web.adishwaremart.com/cms/privacy-policy"
    const val PREMIUM_ACCESS                = "http://stage-web.adishwaremart.com/cms/premium-access"

    /*Auth*/
    const val LOGIN                         = "customer/crm/v-1-0-0/login"
    const val RESEND_OTP                    = "customer/crm/v-1-0-0/resend-otp"
    const val VERIFY_OTP                    = "customer/crm/v-1-0-0/verify-otp"
    const val SIGN_UP                       = "customer/crm/v-1-0-0/signup"
    const val CHECK_SOCIAL_ID_EXISTS        = "customer/crm/v-1-0-0/check-social-id"

    /*Main*/
    /*CRM*/
    const val UPDATE_PROFILE                = "customer/crm/v-1-0-0/update-profile"
    const val LOGOUT                        = "customer/crm/v-1-0-0/logout"
    const val CUSTOMER_DETAIL               = "customer/crm/v-1-0-0/customer-detail"
    const val ADDRESS_LIST                  = "customer/crm/v-1-0-0/address/list"
    const val ADDRESS_ADD                   = "customer/crm/v-1-0-0/address/add"
    const val ADDRESS_DELETE                = "customer/crm/v-1-0-0/address/delete"
    const val GET_DELIVERY_SLOT             = "customer/crm/v-1-0-0/delivery-slot"
    const val LOAD_MONEY                    = "customer/crm/v-1-0-0/wallet/add-money-to-wallet"
    const val GET_VERSION_UPDATE            = "customer/crm/v-1-0-0/vendor/app-version"
    const val TRANSACTION_LIST              = "customer/crm/v-1-0-0/wallet/get-transactions"

    /*PIMS*/
    const val HOME_LIST                     = "customer/pims/v-1-0-1/ad-block/mobile-ad-blocks"
    const val CATEGORY_LIST                 = "customer/pims/v-1-0-1/product/category-list"
    const val PRODUCT_LIST                  = "customer/pims/v-1-0-1/product/list"
    const val SIMILAR_PRODUCT_LIST          = "customer/pims/v-1-0-1/product/related-products"
    const val FREQUENTLY_PRODUCT_LIST       = "customer/pims/v-1-0-1/product/most-selling-products"
    const val FAVOURITE_UPDATE              = "customer/pims/v-1-0-1/product/update-favourite"
    const val PRODUCT_DETAIL                = "customer/pims/v-1-0-1/product/details"
    const val PRODUCT_DETAIL_NEW            = "customer/pims/v-1-0-1/product/electro-product-details"
    const val FAVOURITE_LIST                = "customer/pims/v-1-0-1/product/favourite-product-list"
    const val GET_PRODUCT_INFO_BY_VARIANT   = "customer/pims/v-1-0-1/product/variant-details"
    const val GET_FILTERS_LIST              = "customer/pims/v-1-0-1/product/filter-list"
    const val GET_PLACE_HOLDER_DETAILS      = "customer/pims/v-1-0-1/placeholder/details"
    const val SEARCH_HISTORY                = "customer/pims/v-1-0-1/product/search-history"
    const val UPDATE_RATING                 = "customer/pims/v-1-0-1/review_and_ratings/store"
    const val RATING_REVIEWS_LIST           = "customer/pims/v-1-0-1/review_and_ratings/list"
    const val PRODUCT_REVIEWS_LIST          = "customer/pims/v-1-0-1/review_and_ratings/single-product-reviews"
    const val COMPARE_PRODUCT_DETAIL        = "electro/customer/pims/v-1-0-1/compare/details"

    /*OMS*/
    const val NOTIFICATION_LIST             = "customer/oms/v-1-0-0/notification-list"
    const val GET_PAYMENT_LIST              = "customer/oms/v-1-0-0/payment-list"
    const val PLACE_ORDER                   = "customer/oms/v-1-0-0/order/place-order"
    const val GET_SHIPPING_CHARGES          = "customer/oms/v-1-0-0/shipping-charges/charge"
    const val GET_ORDER_LIST                = "customer/oms/v-1-0-1/order/list"
    const val GET_ORDER_DETAIL              = "customer/oms/v-1-0-1/order/detail"
    const val GET_PROMO_CODE_INFO           = "customer/crm/v-1-0-0/promo-code/details-by-code"
    const val RE_ORDER_API                  = "customer/oms/v-1-0-0/order/reorder"
    const val ORDER_FAV_API                 = "customer/oms/v-1-0-0/order/add-to-wishlist"
    const val ORDER_CANCEL                  = "customer/oms/v-1-0-0/order/update-cancel-order-status"
    const val ORDER_RETURN                  = "customer/oms/v-1-0-0/order/update-return-order-status"
    const val ORDER_SWITCH_TO_COD           = "customer/oms/v-1-0-0/order/cod-payment"
    const val ORDER_UPDATE                  = "customer/oms/v-1-0-0/order/cashfree-callback"
    const val UPDATE_WALLET_AMOUNT          = "customer/crm/v-1-0-0/wallet/cashfree-callback-wallet"
    const val CASH_FREE_TOKEN               = "customer/oms/v-1-0-0/order/generate-cashfree-token"
    const val PAYMENT_PROVIDERS_LIST        = "customer/oms/v-1-0-0/order/payment-providers"

    /*WAREHOUSE*/
    const val GET_UPDATED_PRICE_LIST        = "customer/warehouse/v-1-0-0/warehouse-product/get-price-based-on-customer-type"
    const val CHECK_VALID_PRODUCT_ADDED     = "customer/warehouse/v-1-0-0/warehouse-product/check"
    const val GET_STORE_INFO                = "customer/warehouse/v-1-0-0/township/update-location"
}