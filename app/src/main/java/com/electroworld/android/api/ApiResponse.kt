package com.electroworld.android.api

import com.electroworld.android.utils.AppValidator

data class ApiResponse<T> (
    val http_code: Int = 0,
    var data: T,
    val message: String
){
    fun get(): String = AppValidator.toStr(message)
}