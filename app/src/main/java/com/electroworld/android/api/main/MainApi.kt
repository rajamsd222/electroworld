package com.electroworld.android.api.main

import com.electroworld.android.api.ApiConstants.PARAM_ADDRESS
import com.electroworld.android.api.ApiConstants.PARAM_ADDRESS_ID
import com.electroworld.android.api.ApiConstants.PARAM_ADDRESS_TYPE
import com.electroworld.android.api.ApiConstants.PARAM_ANDROID_DEVICE_ID
import com.electroworld.android.api.ApiConstants.PARAM_APARTMENT_NAME
import com.electroworld.android.api.ApiConstants.PARAM_AUTHENTICATION_DEVICE_ID
import com.electroworld.android.api.ApiConstants.PARAM_BILLING_ADDRESS
import com.electroworld.android.api.ApiConstants.PARAM_BILLING_CONTACT_NAME
import com.electroworld.android.api.ApiConstants.PARAM_BILLING_MOBILE_NO
import com.electroworld.android.api.ApiConstants.PARAM_BRAND_FILTER
import com.electroworld.android.api.ApiConstants.PARAM_CATEGORY_ID
import com.electroworld.android.api.ApiConstants.PARAM_CF_TOKEN
import com.electroworld.android.api.ApiConstants.PARAM_CHK_BILLING_ADDRESS
import com.electroworld.android.api.ApiConstants.PARAM_COMPARE_PRODUCTS
import com.electroworld.android.api.ApiConstants.PARAM_CONTACT_PERSON
import com.electroworld.android.api.ApiConstants.PARAM_CUSTOMER_ID
import com.electroworld.android.api.ApiConstants.PARAM_DELIVERY_CHARGE
import com.electroworld.android.api.ApiConstants.PARAM_DELIVERY_NOTES
import com.electroworld.android.api.ApiConstants.PARAM_DELIVERY_SLOT_FROM_TIME
import com.electroworld.android.api.ApiConstants.PARAM_DELIVERY_SLOT_TO_TIME
import com.electroworld.android.api.ApiConstants.PARAM_DELIVERY_TIME_TYPE
import com.electroworld.android.api.ApiConstants.PARAM_EMAIL_ID
import com.electroworld.android.api.ApiConstants.PARAM_FIRSTNAME
import com.electroworld.android.api.ApiConstants.PARAM_GIFT_FROM
import com.electroworld.android.api.ApiConstants.PARAM_GIFT_TO
import com.electroworld.android.api.ApiConstants.PARAM_GIFT_TO_OTHERS
import com.electroworld.android.api.ApiConstants.PARAM_GIFT_TO_OTHERS_AMOUNT
import com.electroworld.android.api.ApiConstants.PARAM_GIFT_TO_OTHERS_TEXT
import com.electroworld.android.api.ApiConstants.PARAM_GRAND_TOTAL
import com.electroworld.android.api.ApiConstants.PARAM_GROUP_ID
import com.electroworld.android.api.ApiConstants.PARAM_GST_INVOICE
import com.electroworld.android.api.ApiConstants.PARAM_GST_INVOICE_NAME
import com.electroworld.android.api.ApiConstants.PARAM_GST_INVOICE_NO
import com.electroworld.android.api.ApiConstants.PARAM_GST_NUMBER
import com.electroworld.android.api.ApiConstants.PARAM_HOUSE_NO
import com.electroworld.android.api.ApiConstants.PARAM_ID
import com.electroworld.android.api.ApiConstants.PARAM_IOS_DEVICE_ID
import com.electroworld.android.api.ApiConstants.PARAM_IS_FAVOURITE
import com.electroworld.android.api.ApiConstants.PARAM_IS_RETURN
import com.electroworld.android.api.ApiConstants.PARAM_IS_WALLET
import com.electroworld.android.api.ApiConstants.PARAM_LANDMARK
import com.electroworld.android.api.ApiConstants.PARAM_LANGUAGE
import com.electroworld.android.api.ApiConstants.PARAM_LASTNAME
import com.electroworld.android.api.ApiConstants.PARAM_LATITUDE
import com.electroworld.android.api.ApiConstants.PARAM_LEVEL
import com.electroworld.android.api.ApiConstants.PARAM_LOGOUT_FROM_ALL
import com.electroworld.android.api.ApiConstants.PARAM_LONGITUDE
import com.electroworld.android.api.ApiConstants.PARAM_MOBILE_NUMBER
import com.electroworld.android.api.ApiConstants.PARAM_MONEY
import com.electroworld.android.api.ApiConstants.PARAM_ORDER_BY
import com.electroworld.android.api.ApiConstants.PARAM_ORDER_ID
import com.electroworld.android.api.ApiConstants.PARAM_ORDER_PRODUCT_IDS
import com.electroworld.android.api.ApiConstants.PARAM_ORDER_TYPE
import com.electroworld.android.api.ApiConstants.PARAM_PAGE
import com.electroworld.android.api.ApiConstants.PARAM_PAYMENTMODE
import com.electroworld.android.api.ApiConstants.PARAM_PICK_UP_FROM_STORE
import com.electroworld.android.api.ApiConstants.PARAM_PLACEHOLDER_ID
import com.electroworld.android.api.ApiConstants.PARAM_PRODUCTS
import com.electroworld.android.api.ApiConstants.PARAM_PRODUCT_ID
import com.electroworld.android.api.ApiConstants.PARAM_PROMO_CODE
import com.electroworld.android.api.ApiConstants.PARAM_RATED_IN
import com.electroworld.android.api.ApiConstants.PARAM_RATING
import com.electroworld.android.api.ApiConstants.PARAM_RATING_DESC
import com.electroworld.android.api.ApiConstants.PARAM_RATING_TITLE
import com.electroworld.android.api.ApiConstants.PARAM_SEARCH_PRODUCT
import com.electroworld.android.api.ApiConstants.PARAM_SORT_BY
import com.electroworld.android.api.ApiConstants.PARAM_STATUS
import com.electroworld.android.api.ApiConstants.PARAM_STORE_ID
import com.electroworld.android.api.ApiConstants.PARAM_STREET
import com.electroworld.android.api.ApiConstants.PARAM_SUBTOTAL
import com.electroworld.android.api.ApiConstants.PARAM_TOKEN
import com.electroworld.android.api.ApiConstants.PARAM_TRANSACTION_ON
import com.electroworld.android.api.ApiConstants.PARAM_VENDOR_ID
import com.electroworld.android.api.ApiConstants.PARAM_WAREHOUSE_ID
import com.electroworld.android.api.ApiConstants.PARAM_WAREHOUSE_PRODUCT_ID
import com.electroworld.android.api.ApiConstants.PARAM_WAREHOUSE_PRODUCT_ID_
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.api.Urls
import com.electroworld.android.model.*
import com.electroworld.android.model.filter.FilterApiRes
import com.electroworld.android.model.orderdetail.OrderDetailModel
import com.electroworld.android.model.orderlist.OrderListModel
import com.electroworld.android.ui.main.notifications.NotificationModel
import com.google.gson.JsonObject
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.http.*

interface MainApi {

    @FormUrlEncoded
    @POST(Urls.PAYMENT_PROVIDERS_LIST)
    fun getPaymentProvidersList(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<PaymentProvidersModel>>

    @Multipart
    @POST(Urls.ORDER_RETURN)
    fun makeReturnOrder(
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>?
    ): Call<ApiResponse<UserModel>>


    @Multipart
    @POST(Urls.ORDER_RETURN)
    fun makeReturnOrderNew(
        @Part(PARAM_TOKEN) token: RequestBody?,
        @Part(PARAM_CUSTOMER_ID) customer_id: RequestBody?,
        @Part(PARAM_VENDOR_ID) vendor_id: RequestBody?,
        @Part("order_id") order_id: RequestBody?,
        @Part("order_product_ids") order_product_ids: RequestBody?,
        @Part("comments") comments: RequestBody?,
        @Part("image_count") image_count: RequestBody?,
        @Part images: List<MultipartBody.Part?>
    ): Call<ApiResponse<UserModel>>


    @Multipart
    @POST(Urls.UPDATE_PROFILE)
    fun updateProfile(
        @Part(PARAM_TOKEN) token: RequestBody?,
        @Part(PARAM_CUSTOMER_ID) customer_id: RequestBody?,
        @Part(PARAM_FIRSTNAME) first_name: RequestBody?,
        @Part(PARAM_LASTNAME) last_name: RequestBody?,
        @Part(PARAM_EMAIL_ID) email_id: RequestBody?,
        @Part(PARAM_GST_NUMBER) gstnumber: RequestBody?,
        @Part(PARAM_GST_INVOICE_NAME) companyName: RequestBody?,
        @Part(PARAM_VENDOR_ID) vendor_id: RequestBody?,
        @Part(PARAM_LANGUAGE) language: RequestBody?,
        @Part profileImage: MultipartBody.Part?
    ): Call<ApiResponse<UserModel>>


    @FormUrlEncoded
    @POST(Urls.ORDER_RETURN)
    fun returnOrder(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_ORDER_ID) order_id: String?,
        @Field(PARAM_ORDER_PRODUCT_IDS) ids: JSONArray?
    ): Single<ApiResponse<DefaultData>>


    @FormUrlEncoded
    @POST(Urls.PRODUCT_REVIEWS_LIST)
    fun getProductReviewsList(
        @Field(PARAM_PRODUCT_ID) product_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<List<ProductRatingModel>>>

    @FormUrlEncoded
    @POST(Urls.RATING_REVIEWS_LIST)
    fun getRatingReviewsList(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<List<RatingReviewsListModel>>>


    @FormUrlEncoded
    @POST(Urls.UPDATE_RATING)
    fun updateRating(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_RATING) rating: String?,
        @Field(PARAM_RATING_DESC) rating_descriptions: String?,
        @Field(PARAM_RATING_TITLE) rating_title: String?,
        @Field(PARAM_RATED_IN) rated_in: String,
        @Field(PARAM_PRODUCT_ID) product_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<ApiResponseDefault>>


    @FormUrlEncoded
    @POST(Urls.UPDATE_WALLET_AMOUNT)
    fun doUpdateWalletAmount(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field("orderId") order_id: String?,
        @Field("orderAmount") orderAmount: String?,
        @Field("reference_number_external") referenceId: String?,
        @Field("txStatus") txStatus: String?,
        @Field("txMsg") txMsg: String?,
        @Field("txTime") txTime: String?,
        @Field("signature_external") signature: String?,
        @Field("paymentMode") paymentMode: String?,
    ): Single<ApiResponse<AddMoneyToWalletModel>>


    @FormUrlEncoded
    @POST(Urls.CASH_FREE_TOKEN)
    fun getCashFreeToken(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field("orderId") order_id: String?,
        @Field("amount_payable") amount_payable: String?,
    ): Single<ApiResponse<CashFreeTokenReponse>>

    @FormUrlEncoded
    @POST(Urls.ORDER_SWITCH_TO_COD)
    fun makeOrderAsCOD(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field("order_id") order_id: String?
    ): Single<BaseResponse>

    @FormUrlEncoded
    @POST(Urls.ORDER_UPDATE)
    fun doOrderUpdate(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field("orderId") order_id: String?,
        @Field("orderAmount") orderAmount: String?,
        @Field("referenceId") referenceId: String?,
        @Field("txStatus") txStatus: String?,
        @Field("txMsg") txMsg: String?,
        @Field("txTime") txTime: String?,
        @Field("signature") signature: String?,
        @Field("paymentMode") paymentMode: String?,
    ): Single<ApiResponse<CashFreeCallBack>>

    @FormUrlEncoded
    @POST(Urls.ORDER_CANCEL)
    fun cancelOrder(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_ORDER_ID) order_id: String?
    ): Single<ApiResponse<DefaultData>>

    @FormUrlEncoded
    @POST(Urls.GET_VERSION_UPDATE)
    fun getVersionInfo(
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<VersionInfo>>

    @FormUrlEncoded
    @POST(Urls.ORDER_CANCEL)
    fun doOrderCancelApi(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_ORDER_ID) order_id: String?
    ): Call<ApiResponse<DefaultData>>

    @FormUrlEncoded
    @POST(Urls.LOAD_MONEY)
    fun addMoneyToWallet(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_MONEY) money: Double?,
        @Field(PARAM_CF_TOKEN) cfTokenNeed: String?,
        @Field(PARAM_TRANSACTION_ON) transaction_on: String?
    ): Call<ApiResponse<AddMoneyToWalletModel>>

    @FormUrlEncoded
    @POST(Urls.ORDER_FAV_API)
    fun doOrderFavApi(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_ORDER_ID) order_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Single<ApiResponse<DefaultData>>

    @FormUrlEncoded
    @POST(Urls.RE_ORDER_API)
    fun doReOrderApi(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_ORDER_ID) order_id: String?
    ): Single<ApiResponse<ReOrderModel>>

    @FormUrlEncoded
    @POST(Urls.GET_FILTERS_LIST)
    fun getFiltersList(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Observable<ApiResponse<FilterApiRes>>

    @FormUrlEncoded
    @POST(Urls.GET_PRODUCT_INFO_BY_VARIANT)
    fun getProductInfoByVariant(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_WAREHOUSE_PRODUCT_ID) warehouse_product_id: String?
    ): Call<ApiResponse<ProductModel>>


    @FormUrlEncoded
    @POST(Urls.GET_PRODUCT_INFO_BY_VARIANT)
    fun getProductInfoByVariantNew(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_WAREHOUSE_PRODUCT_ID) warehouse_product_id: String?
    ): Single<ApiResponse<ProductModel>>

    @FormUrlEncoded
    @POST(Urls.SEARCH_HISTORY)
    fun getSearchHistory(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_ANDROID_DEVICE_ID) android_device_id: String?,
        @Field(PARAM_IOS_DEVICE_ID) ios_device_id: String?
    ): Call<ApiResponse<List<SearchHistoryModel>>>

    @FormUrlEncoded
    @POST(Urls.CHECK_VALID_PRODUCT_ADDED)
    fun chkValidOrder(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_ADDRESS_ID) addressId: String?,
        @Field(PARAM_PRODUCTS) products: JSONArray?,
        @Field(PARAM_WAREHOUSE_ID) warehouse_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Call<ApiResponse<CheckValidOrderModel>>

    @FormUrlEncoded
    @POST(Urls.GET_PROMO_CODE_INFO)
    fun getPromocodeInfo(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_PROMO_CODE) promo_code: String?,
        @Field(PARAM_SUBTOTAL) sub_total: Double,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Call<ApiResponse<PromoCodeModel>>

    @FormUrlEncoded
    @POST(Urls.GET_DELIVERY_SLOT)
    fun getDeliveryslotTimings(
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Single<ApiResponse<DeliverySlotApiModel>>

    @FormUrlEncoded
    @POST(Urls.GET_DELIVERY_SLOT)
    fun getDeliveryslotTimingsTest(
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Single<DeliverySlotResponse>

    @FormUrlEncoded
    @POST(Urls.GET_ORDER_DETAIL)
    fun orderDetail(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_ORDER_ID) order_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<OrderDetailModel>>

    @FormUrlEncoded
    @POST(Urls.FAVOURITE_LIST)
    fun getFavList(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_WAREHOUSE_ID) store_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Observable<ApiResponse<List<ProductModel>>>

    @FormUrlEncoded
    @POST(Urls.PRODUCT_DETAIL)
    fun getProductDetails(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_WAREHOUSE_PRODUCT_ID) warehouse_product_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<ProductDetailModel>>

    @FormUrlEncoded
    @POST(Urls.PRODUCT_DETAIL_NEW)
    fun getProductDetailsNew(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_WAREHOUSE_PRODUCT_ID_) warehouse_product_id: String?,
        @Field(PARAM_GROUP_ID) group_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<ProductDetailModel>>


    @FormUrlEncoded
    @POST(Urls.ADDRESS_DELETE)
    fun deleteAddress(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_ADDRESS_TYPE) address_type: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Observable<ApiResponse<ApiResponseDefault>>

    @FormUrlEncoded
    @POST(Urls.GET_ORDER_LIST)
    fun orderlist(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_PAGE) page: Int,
        @Field(PARAM_STATUS) status: Int,
        @Field(PARAM_IS_RETURN) is_return: Int
    ): Single<ApiResponse<OrderListModel>>

    @FormUrlEncoded
    @POST(Urls.GET_SHIPPING_CHARGES)
    fun getShippingSlots(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_SUBTOTAL) subTotal: Double,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_WAREHOUSE_ID) warehouse_id: String?,
        @Field(PARAM_ORDER_TYPE) order_type: String?,
        @Field("cart_products") cart_products: JSONArray?
    ): Single<ApiResponse<ShippingChargesResponse>>

    @FormUrlEncoded
    @POST(Urls.PLACE_ORDER)
    fun placeOrder(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_WAREHOUSE_ID) warehouse_id: String?,
        @Field(PARAM_ADDRESS_ID) address_id: String?,
        @Field(PARAM_DELIVERY_TIME_TYPE) delivery_time_type: String?,
        @Field(PARAM_DELIVERY_SLOT_FROM_TIME) delivery_slot_from_time: String?,
        @Field(PARAM_DELIVERY_SLOT_TO_TIME) delivery_slot_to_time: String?,
        @Field(PARAM_PAYMENTMODE) paymentMode: String?,
        @Field(PARAM_MOBILE_NUMBER) mobile_number: String?,
        @Field(PARAM_AUTHENTICATION_DEVICE_ID) authentication_device_id: String?,
        @Field(PARAM_LATITUDE) latitude: Double,
        @Field(PARAM_LONGITUDE) longitude: Double,
        @Field(PARAM_DELIVERY_CHARGE) delivery_charge: Double,
        @Field(PARAM_SUBTOTAL) subtotal: Double,
        @Field(PARAM_GRAND_TOTAL) grand_total: Double,
        @Field(PARAM_PRODUCTS) products: JSONArray?,
        @Field(PARAM_IS_WALLET) is_wallet: Int,
        @Field(PARAM_PROMO_CODE) promo_code: String?,
        @Field(PARAM_GIFT_TO_OTHERS) gift_to_others: Int,
        @Field(PARAM_GIFT_TO_OTHERS_AMOUNT) gift_to_others_amount: Double,
        @Field(PARAM_GIFT_FROM) gift_from: String,
        @Field(PARAM_GIFT_TO) gift_to: String,
        @Field(PARAM_GIFT_TO_OTHERS_TEXT) gift_to_others_text: String,
        @Field(PARAM_PICK_UP_FROM_STORE) pick_up_from_store: Int,
        @Field(PARAM_GST_INVOICE) gstEnabled: Int?,
        @Field(PARAM_GST_INVOICE_NO) gstNumber: String?,
        @Field(PARAM_GST_INVOICE_NAME) company_name: String?,
        @Field(PARAM_CF_TOKEN) cfToken: String?,
        @Field(PARAM_DELIVERY_NOTES) order_instruction: String?,
        @Field(PARAM_BILLING_ADDRESS) billing_address: String?,
        @Field(PARAM_BILLING_MOBILE_NO) billing_mobile: String?,
        @Field(PARAM_BILLING_CONTACT_NAME) billing_contact: String?,
        @Field(PARAM_CHK_BILLING_ADDRESS) chk_billing_address: String?
    ): Call<ApiResponse<OrderPlaceModel>>

    @FormUrlEncoded
    @POST(Urls.GET_STORE_INFO)
    fun getStoreInfo(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_LATITUDE) latitude: Double,
        @Field(PARAM_LONGITUDE) longitude: Double,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Call<ApiResponse<StoreModel>>

    @FormUrlEncoded
    @POST(Urls.GET_STORE_INFO)
    fun getStoreInfoSingle(
        @Field(PARAM_LATITUDE) latitude: Double,
        @Field(PARAM_LONGITUDE) longitude: Double,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<StoreModel>>

    @FormUrlEncoded
    @POST(Urls.FAVOURITE_UPDATE)
    fun doFav(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_WAREHOUSE_PRODUCT_ID) warehouse_product_id: String?,
        @Field(PARAM_PRODUCT_ID) product_id: String?,
        @Field(PARAM_GROUP_ID) group_id: String?,
        @Field(PARAM_IS_FAVOURITE) is_favourite: Int
    ): Single<ApiResponse<SetFavModel>>

    @FormUrlEncoded
    @POST(Urls.FAVOURITE_UPDATE)
    fun setFavGrid(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_WAREHOUSE_PRODUCT_ID) warehouse_product_id: String?,
        @Field(PARAM_PRODUCT_ID) product_id: String?,
        @Field(PARAM_IS_FAVOURITE) is_favourite: Int,
        @Field(PARAM_GROUP_ID) group_id: String?
    ): Call<ApiResponse<SetFavModel>>

    @FormUrlEncoded
    @POST(Urls.FAVOURITE_UPDATE)
    fun setFav(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_WAREHOUSE_PRODUCT_ID) warehouse_product_id: String?,
        @Field(PARAM_PRODUCT_ID) product_id: String?,
        @Field(PARAM_IS_FAVOURITE) is_favourite: Int,
        @Field(PARAM_GROUP_ID) group_id: String?
    ): Observable<ApiResponse<SetFavModel>>

    @FormUrlEncoded
    @POST(Urls.FAVOURITE_UPDATE)
    fun setFavourite(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_WAREHOUSE_PRODUCT_ID) warehouse_product_id: String?,
        @Field(PARAM_PRODUCT_ID) product_id: String?,
        @Field(PARAM_IS_FAVOURITE) is_favourite: Int,
        @Field(PARAM_GROUP_ID) group_id: String?
    ): Single<ApiResponse<SetFavModel>>


    @FormUrlEncoded
    @POST(Urls.COMPARE_PRODUCT_DETAIL)
    fun getProductCompare(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_COMPARE_PRODUCTS) products: JSONArray?
    ): Single<ApiResponse<List<ProductModel>>>


    @FormUrlEncoded
    @POST(Urls.HOME_LIST)
    fun getHomeListCall(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_PAGE) page: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field("page_no") page_no: Int?
    ): Call<ApiResponse<List<HomeModel>>>


    @FormUrlEncoded
    @POST(Urls.HOME_LIST)
    fun getHomeListCallFromOfferZone(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_PAGE) page: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field("page_no") page_no: Int?,
        @Field(PARAM_SORT_BY) sort_by: String?,
        @Field(PARAM_ORDER_BY) order_by: String?,
        @FieldMap options: Map<String, String>?,
        @Field("android_device_id") android_device_id: String?,
        @Field("ios_device_id") ios_device_id: String?,
    ): Call<ApiResponse<List<HomeModel>>>


    @FormUrlEncoded
    @POST(Urls.HOME_LIST)
    fun getHomeList(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_PAGE) page: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Observable<ApiResponse<List<HomeModel>>>

    @FormUrlEncoded
    @POST(Urls.HOME_LIST)
    fun getHomeList(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_PAGE) page: String?
    ): Single<ApiResponse<List<HomeModel>>>

    @FormUrlEncoded
    @POST(Urls.GET_PAYMENT_LIST)
    fun getPayments(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<PaymentModeResponse>

    @GET
    fun getPlaceInfoByUrl(@Url url: String?): Observable<JsonObject>

    @GET
    fun doPlaceSearchByUrl(@Url url: String?): Observable<ResponseBody>

    @GET
    fun doPlaceSearchByUrlNew(@Url url: String?): Call<JsonObject>

    @FormUrlEncoded
    @POST(Urls.ADDRESS_ADD)
    fun addAddress(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_ADDRESS_TYPE) address_type: String?,
        @Field(PARAM_ID) address_id: String?,
        @Field(PARAM_HOUSE_NO) house_no: String?,
        @Field(PARAM_APARTMENT_NAME) apartment_name: String?,
        @Field(PARAM_STREET) street_details: String?,
        @Field(PARAM_LANDMARK) landmark: String?,
        @Field(PARAM_MOBILE_NUMBER) mobile_number: String?,
        @Field(PARAM_CONTACT_PERSON) contact_person: String?,
        @Field(PARAM_ADDRESS) address: String?,
        @Field(PARAM_LATITUDE) latitude: Double,
        @Field(PARAM_LONGITUDE) longitude: Double,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Observable<ApiResponse<ApiResponseDefault>>

    @FormUrlEncoded
    @POST(Urls.ADDRESS_LIST)
    fun addressList(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Observable<ApiResponse<List<AddressListModel>>>

    @FormUrlEncoded
    @POST(Urls.SIMILAR_PRODUCT_LIST)
    fun getSimilarProductList(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_GROUP_ID) categoryId: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<List<ProductModel>>>

    @FormUrlEncoded
    @POST(Urls.FREQUENTLY_PRODUCT_LIST)
    fun getFrequentlyProductList(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_WAREHOUSE_ID) store_id: String?
    ): Single<ApiResponse<List<ProductModel>>>

    @FormUrlEncoded
    @POST(Urls.GET_PLACE_HOLDER_DETAILS)
    fun getPlaceHolderDetails(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_PLACEHOLDER_ID) placeholder_id: String?,
        @Field(PARAM_PAGE) page: Int,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<PlaceHolderDetailsApiModel>>

    @FormUrlEncoded
    @POST(Urls.GET_PLACE_HOLDER_DETAILS)
    fun getPlaceHolderDetailsNew(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_PLACEHOLDER_ID) placeholder_id: String?,
        @Field(PARAM_PAGE) page: Int,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_SORT_BY) sort_by: String?,
        @Field(PARAM_ORDER_BY) order_by: String?,
        @Field(PARAM_BRAND_FILTER) brand_filter: String?,
        @FieldMap options: Map<String, String>?
    ): Single<ApiResponse<PlaceHolderDetailsApiModel>>

    @FormUrlEncoded
    @POST(Urls.PRODUCT_LIST)
    fun getProductListNormal(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_CATEGORY_ID) categoryId: String?,
        @Field(PARAM_LEVEL) level: String?,
        @Field(PARAM_PAGE) page: Int,
        @Field(PARAM_SEARCH_PRODUCT) keyword: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?,
        @Field(PARAM_SORT_BY) sort_by: String?,
        @Field(PARAM_ORDER_BY) order_by: String?,
        @Field(PARAM_BRAND_FILTER) brand_filter: String?,
        @FieldMap options: Map<String, String>?
    ): Call<ApiResponse<ProductApiModel>>

    @FormUrlEncoded
    @POST(Urls.PRODUCT_LIST)
    fun getProductList(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_CATEGORY_ID) categoryId: String?,
        @Field(PARAM_LEVEL) level: String?,
        @Field(PARAM_PAGE) page: Int,
        @Field(PARAM_SEARCH_PRODUCT) keyword: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Observable<ApiResponse<ProductApiModel>>

    @FormUrlEncoded
    @POST(Urls.PRODUCT_LIST)
    fun getProductListBySearch(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_STORE_ID) store_id: String?,
        @Field(PARAM_CATEGORY_ID) categoryId: String?,
        @Field(PARAM_LEVEL) level: String?,
        @Field(PARAM_PAGE) page: Int,
        @Field(PARAM_SEARCH_PRODUCT) keyword: String?,
        @Field(PARAM_ANDROID_DEVICE_ID) android_device_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Observable<ApiResponse<ProductApiModel>>

    @FormUrlEncoded
    @POST(Urls.CATEGORY_LIST)
    fun getCategoryList(
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<List<CategoryModel>>>

    @FormUrlEncoded
    @POST(Urls.CATEGORY_LIST)
    fun getCategoryList(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Observable<ApiResponse<List<CategoryModel>>>

    @FormUrlEncoded
    @POST(Urls.NOTIFICATION_LIST)
    fun getNotificationList(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Single<ApiResponse<List<NotificationModel>>>

    @FormUrlEncoded
    @POST(Urls.TRANSACTION_LIST)
    fun getTransactionList(
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?
    ): Single<ApiResponse<List<TransactionListModel>>>


    @FormUrlEncoded
    @POST(Urls.LOGOUT)
    fun apiUserLogout(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_AUTHENTICATION_DEVICE_ID) authentication_device_id: String?,
        @Field(PARAM_LOGOUT_FROM_ALL) logout_from_all: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Observable<ApiResponse<ApiResponseDefault>>

    @FormUrlEncoded
    @POST(Urls.CUSTOMER_DETAIL)
    fun getUserDetail(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field(PARAM_CUSTOMER_ID) customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Observable<ApiResponse<UserModel>>
}