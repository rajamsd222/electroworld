package com.electroworld.android.api.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}