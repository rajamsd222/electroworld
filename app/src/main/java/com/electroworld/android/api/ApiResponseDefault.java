package com.electroworld.android.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.electroworld.android.utils.AppValidator;

public class ApiResponseDefault {

    @SerializedName("http_code")
    @Expose
    private int http_code;

    @SerializedName("message")
    @Expose
    private String message;

    public int getHttp_code() {
        return http_code;
    }

    public String getMessage() {
        return AppValidator.toStr(message);
    }

}
