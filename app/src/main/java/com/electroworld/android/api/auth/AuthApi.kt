package com.electroworld.android.api.auth

import com.electroworld.android.api.ApiConstants.PARAM_AUTHENTICATION_DEVICE_ID
import com.electroworld.android.api.ApiConstants.PARAM_LANGUAGE
import com.electroworld.android.api.ApiConstants.PARAM_TOKEN
import com.electroworld.android.api.ApiConstants.PARAM_VENDOR_ID
import com.electroworld.android.api.ApiResponse
import com.electroworld.android.api.ApiResponseDefault
import com.electroworld.android.api.Urls
import com.electroworld.android.model.ProductModel
import com.electroworld.android.model.SendOTPModel
import com.electroworld.android.model.UpdatePriceModel
import com.electroworld.android.model.UserModel
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthApi {
    @FormUrlEncoded
    @POST(Urls.RESEND_OTP)
    fun resendOTP(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_AUTHENTICATION_DEVICE_ID) authentication_device_id: String?,
        @Field("authentication_id") authentication_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Call<ApiResponseDefault>

    @FormUrlEncoded
    @POST(Urls.GET_UPDATED_PRICE_LIST)
    fun getUpdatedPriceList(
        @Header(PARAM_TOKEN) token: String?,
        @Field(PARAM_TOKEN) tokenField: String?,
        @Field("customer_id") customer_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field("warehouse_product_id") warehouse_product_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Call<ApiResponse<List<UpdatePriceModel>>>

    @FormUrlEncoded
    @POST(Urls.SIGN_UP)
    fun doSignupUser(
        @Header(PARAM_TOKEN) token: String?,
        @Field("role") role: String?,
        @Field("login_type") login_type: String?,
        @Field("social_id") social_id: String?,
        @Field("email") email: String?,
        @Field("country_code") country_code: String?,
        @Field("phone") phone: String?,
        @Field("password") password: String?,
        @Field("firstname") firstname: String?,
        @Field("lastname") lastname: String?,
        @Field("device_type") device_type: String?,
        @Field("device_id") device_id: String?,
        @Field("device_name") device_name: String?,
        @Field("device_version") device_version: String?,
        @Field("app_version") app_version: String?,
        @Field("fcm_token") fcm_token: String?,
        @Field("latitude") latitude: Double?,
        @Field("longitude") longitude: Double?,
        @Field("imei_number") imei_number: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field("dob") dob: String?
    ): Call<ApiResponse<SendOTPModel>>

    @FormUrlEncoded
    @POST(Urls.VERIFY_OTP)
    fun doOTP(
        @Header(PARAM_TOKEN) token: String?,
        @Field("role") role: String?,
        @Field(PARAM_AUTHENTICATION_DEVICE_ID) authentication_device_id: String?,
        @Field("authentication_id") authentication_id: String?,
        @Field("otp") otp: String?,
        @Field("type") type: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Observable<ApiResponse<UserModel>>

    @FormUrlEncoded
    @POST(Urls.CHECK_SOCIAL_ID_EXISTS)
    fun doCheckSocialIdExists(
        @Header(PARAM_TOKEN) token: String?,
        @Field("login_type") login_type: String?,
        @Field("social_id") social_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field("email") email: String?,
        @Field("role") role: String?,
        @Field("device_type") device_type: String?,
        @Field("device_name") device_name: String?,
        @Field("app_version") app_version: String?,
        @Field("fcm_token") fcm_token: String?,
        @Field("latitude") latitude: Double?,
        @Field("longitude") longitude: Double?,
        @Field("imei_number") imei_number: String?,
        @Field("device_id") device_id: String?,
        @Field("device_version") device_version: String?
    ): Call<ApiResponse<UserModel>>

    @FormUrlEncoded
    @POST(Urls.LOGIN)
    fun doLogin(
        @Header(PARAM_TOKEN) token: String?,
        @Field("role") role: String?,
        @Field("user_type") user_type: String?,
        @Field("user_id") mobileNumber: String?,
        @Field("device_id") device_id: String?,
        @Field("device_name") device_name: String?,
        @Field("device_version") device_version: String?,
        @Field("app_version") app_version: String?,
        @Field("device_type") device_type: String?,
        @Field("fcm_token") fcm_token: String?,
        @Field("login_type") login_type: String?,
        @Field("social_id") social_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Call<ApiResponse<UserModel>>


    @FormUrlEncoded
    @POST(Urls.LOGIN)
    fun doLoginNew(
        @Field("role") role: String?,
        @Field("user_type") user_type: String?,
        @Field("user_id") mobileNumber: String?,
        @Field("device_id") device_id: String?,
        @Field("device_name") device_name: String?,
        @Field("device_version") device_version: String?,
        @Field("app_version") app_version: String?,
        @Field("device_type") device_type: String?,
        @Field("fcm_token") fcm_token: String?,
        @Field("login_type") login_type: String?,
        @Field("social_id") social_id: String?,
        @Field(PARAM_VENDOR_ID) vendor_id: String?,
        @Field(PARAM_LANGUAGE) language: String?
    ): Single<ApiResponse<UserModel>>

}