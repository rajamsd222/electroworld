package com.electroworld.android.alert

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.electroworld.android.R
import com.electroworld.android.alert.AlertViewCustomDialog.DialogAdapter.PlanetViewHolder
import com.electroworld.android.model.DeliverySlot
import java.util.*

class AlertViewCustomDialog(
    private val context: Activity,
    private val onItemClickListener: ItemClickListener
) {
    private var isCancelable = false
    private var dialogTitle = ""
    private var dialogMessage = ""
    private var showButtonView = false
    var values: List<DeliverySlot> = ArrayList()
    fun showDialog() {
        val dialog = Dialog(context, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(isCancelable)
        dialog.setCanceledOnTouchOutside(isCancelable)
        dialog.setContentView(R.layout.newcustom_layout)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val alertTitleTV = dialog.findViewById<TextView>(R.id.alertTitleTV)
        val alertMsgTV = dialog.findViewById<TextView>(R.id.alertMsgTV)
        alertTitleTV.text = dialogTitle
        alertMsgTV.text = dialogMessage
        alertTitleTV.visibility = if (dialogTitle.length == 0) View.GONE else View.VISIBLE
        alertMsgTV.visibility = if (dialogMessage.length == 0) View.GONE else View.VISIBLE
        val recyclerView: RecyclerView = dialog.findViewById(R.id.recycler_view)
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        val adapter = DialogAdapter(values, onItemClickListener, dialog)
        recyclerView.adapter = adapter
        val mDialogNo: MaterialButton = dialog.findViewById(R.id.cancelBtn)
        val mDialogOk: MaterialButton = dialog.findViewById(R.id.okbtn)
        val btnLayout = dialog.findViewById<LinearLayout>(R.id.btnLayout)
        btnLayout.visibility = if (showButtonView) View.VISIBLE else View.GONE
        mDialogNo.setOnClickListener { v: View? -> dialog.dismiss() }
        mDialogOk.setOnClickListener { v: View? -> dialog.cancel() }
        dialog.show()
    }

    fun setShowButtonView(showButtonView: Boolean) {
        this.showButtonView = showButtonView
    }

    fun setCancelable(cancelable: Boolean) {
        isCancelable = cancelable
    }

    fun setDialogTitle(dialogTitle: String) {
        this.dialogTitle = dialogTitle
    }

    fun setDialogMessage(dialogMessage: String) {
        this.dialogMessage = dialogMessage
    }

    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    private inner class DialogAdapter internal constructor(
        var dialogList: List<DeliverySlot>,
        private val onItemClickListener: ItemClickListener,
        var dialog: Dialog
    ) : RecyclerView.Adapter<PlanetViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlanetViewHolder {
            val v =
                LayoutInflater.from(parent.context).inflate(R.layout.row_item_dialog, parent, false)
            return PlanetViewHolder(v)
        }

        override fun onBindViewHolder(holder: PlanetViewHolder, position: Int) {
            val (slot) = dialogList[position]
            holder.text.text = slot
            holder.text.setOnClickListener { v: View? ->
                onItemClickListener.onItemClick(v, position)
                dialog.dismiss()
            }
        }

        override fun getItemCount(): Int {
            return dialogList.size
        }

        inner class PlanetViewHolder internal constructor(itemView: View) :
            RecyclerView.ViewHolder(itemView) {
            var text: TextView

            init {
                text = itemView.findViewById(R.id.itemTV)
            }
        }
    }
}