package com.electroworld.android.alert

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.electroworld.android.extentions.setGone
import com.google.android.material.button.MaterialButton
import com.electroworld.android.R

object AlertMakeOrderAsCOD {
    private lateinit var dialogBuilder: AlertDialog.Builder
    private var mListener: OrderSwitchClickListener? = null


    fun showDialog(ctx: Context, screenType: String,iscancelable: Boolean, listener: OrderSwitchClickListener?) {
        mListener = listener
        dialogBuilder = AlertDialog.Builder(ctx)
        dialogBuilder.setCancelable(iscancelable)
        dialogBuilder.setView(R.layout.alert_make_order_ascod)
        val alertDialog = dialogBuilder.create()
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#00141414")))
        alertDialog.show()
        val btnOk = alertDialog.findViewById<MaterialButton>(R.id.btnOk)
        val alertMsgTV = alertDialog.findViewById<TextView>(R.id.alertMsgTV)
        val tvOR = alertDialog.findViewById<TextView>(R.id.tvOR)

        if(screenType == "retry"){
            alertMsgTV?.text = ctx.getString(R.string.payment_failed)
            alertMsgTV?.gravity = Gravity.CENTER
            tvOR?.setGone()
            btnOk?.setGone()
        }

        btnOk?.setOnClickListener {
            alertDialog.dismiss()
            mListener?.onClick(it,"order_detail")
        }
        val btnCOD = alertDialog.findViewById<MaterialButton>(R.id.btnCOD)
        btnCOD?.setOnClickListener {
            alertDialog.dismiss()
            mListener?.onClick(it,"cod")
        }


    }

    interface OrderSwitchClickListener {
        fun onClick(view: View?,tag: String)
    }

}