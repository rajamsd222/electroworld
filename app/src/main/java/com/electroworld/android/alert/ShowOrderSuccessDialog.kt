package com.electroworld.android.alert

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.electroworld.android.R

object ShowOrderSuccessDialog {

    private lateinit var dialogBuilder: AlertDialog.Builder
    private var okClickListener: OKClickListener? = null

    interface OKClickListener {
        fun onClick(view: View?)
        fun onContinueShoppingClick(view: View?)
    }

    fun show(
        ctx: Context,
        orderNumber: String,
        msg: String,
        listener: OKClickListener?
    ) {
        okClickListener = listener
        dialogBuilder = AlertDialog.Builder(ctx)
        dialogBuilder.setCancelable(false)
        dialogBuilder.setView(R.layout.dialog_show_order_success)
        val alertDialog = dialogBuilder.create()
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#00141414")))
        alertDialog.show()
        val btntryAgain = alertDialog.findViewById<TextView>(R.id.btntryAgain)
        val btnContinueShopping = alertDialog.findViewById<TextView>(R.id.btnContinueShopping)
        val alertMsgTV = alertDialog.findViewById<TextView>(R.id.alertMsgTV)
        val tvOrderNumber = alertDialog.findViewById<TextView>(R.id.tvOrderNumber)
        alertMsgTV?.text = msg
        tvOrderNumber?.text = orderNumber
        btntryAgain?.setOnClickListener {
            alertDialog.dismiss()
            okClickListener?.onClick(it)
        }
        btnContinueShopping?.setOnClickListener {
            alertDialog.dismiss()
            okClickListener?.onContinueShoppingClick(it)
        }

    }
}