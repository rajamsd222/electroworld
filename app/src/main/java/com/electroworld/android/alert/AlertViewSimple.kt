package com.electroworld.android.alert

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Html
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.electroworld.android.R
import java.util.*

class AlertViewSimple(
    private val context: Activity,
    private val okClickListener: OKClickListener?
) {
    private var isCancelable = false
    private var dialogTitle = ""
    private var dialogMessage = ""
    private var positiveText = "OK"
    private var negativeText = "Cancel"
    private val showButtonView = true
    private var showNegativeButton = false
    var dialog: Dialog? = null
    fun showDialog() {
        dialog = Dialog(context, R.style.Theme_Dialog)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(isCancelable)
        dialog!!.setCanceledOnTouchOutside(isCancelable)
        dialog!!.setContentView(R.layout.dialog_alert_simple)
        Objects.requireNonNull(dialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val alertTitleTV = dialog!!.findViewById<TextView>(R.id.alertTitleTV)
        val alertMsgTV = dialog!!.findViewById<TextView>(R.id.alertMsgTV)
        alertTitleTV.text = dialogTitle
        alertMsgTV.text = Html.fromHtml(dialogMessage)
        alertTitleTV.visibility = if (dialogTitle.length == 0) View.GONE else View.VISIBLE
        alertMsgTV.visibility = if (dialogMessage.length == 0) View.GONE else View.VISIBLE
        val mDialogOk = dialog!!.findViewById<Button>(R.id.okbtn)
        val mDialogNo = dialog!!.findViewById<Button>(R.id.cancelBtn)
        mDialogOk.text = positiveText
        mDialogNo.text = negativeText
        val btnLayout = dialog!!.findViewById<LinearLayout>(R.id.btnLayout)
        btnLayout.visibility = if (showButtonView) View.VISIBLE else View.GONE
        mDialogNo.visibility = if (showNegativeButton) View.VISIBLE else View.GONE
        mDialogOk.setOnClickListener { v: View? ->
            dialog!!.dismiss()
            okClickListener?.onOKClick(v)
        }
        mDialogNo.setOnClickListener { v: View? -> dialog!!.cancel() }
        dialog!!.show()
    }

    fun dismissDialog() {
        if (dialog != null) dialog!!.dismiss()
    }

    interface OKClickListener {
        fun onOKClick(view: View?)
    }

    fun setShowNegativeButton(showNegativeButton: Boolean) {
        this.showNegativeButton = showNegativeButton
    }

    fun setPositiveText(positiveText: String) {
        this.positiveText = positiveText
    }

    fun setNegativeText(negativeText: String) {
        this.negativeText = negativeText
    }

    fun setCancelable(cancelable: Boolean) {
        isCancelable = cancelable
    }

    fun setDialogTitle(dialogTitle: String) {
        this.dialogTitle = dialogTitle
    }

    fun setDialogMessage(dialogMessage: String) {
        this.dialogMessage = dialogMessage
    }
}