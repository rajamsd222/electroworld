package com.electroworld.android.alert

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.electroworld.android.R
import com.electroworld.android.extentions.setGone
import com.google.android.material.button.MaterialButton

object CustomDialog {

    private lateinit var dialogBuilder: AlertDialog.Builder
    // private lateinit var alertDialog: AlertDialog

    private var okClickListener: OKClickListener? = null

    fun show(
        ctx: Context,
        msg: String?,
        type: AlertType
    ) {


        dialogBuilder = AlertDialog.Builder(ctx)
        dialogBuilder.setCancelable(true)
        dialogBuilder.setView(R.layout.alert_custom_dialog)
        var alertDialog = dialogBuilder.create()
        // magic of transparent background goes here
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        // setting the alertDialog's BackgroundDrawable as the color resource of any color with 1% opacity
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#00141414")))

        // finally displaying the Alertdialog containging the ProgressBar
        alertDialog.show()

        val title = alertDialog.findViewById<TextView>(R.id.alertTitleTV)
        val message = alertDialog.findViewById<TextView>(R.id.alertMsgTV)
        val btnOk = alertDialog.findViewById<MaterialButton>(R.id.btnOk)
        val lineTop = alertDialog.findViewById<View>(R.id.lineTop)
        val lineBottom = alertDialog.findViewById<View>(R.id.lineBottom)
        val topIcon = alertDialog.findViewById<ImageView>(R.id.iconStatus)


        topIcon?.setImageResource(R.drawable.ic_cross_circle_outline_24)

        if (msg.equals("Unable to resolve host \"asm-d-api.izoleap.com\": No address associated with hostname")) {
            message?.text = "Please check your internet connectivity"
        } else {
            message?.text = msg
        }

        // btnOk?.text = ctx.getString(R.string.ok)
        btnOk?.setOnClickListener {
            alertDialog.cancel()
        }

        when (type) {
            AlertType.SUCCESS -> {
                title?.text = ctx.getString(R.string.success)
                title?.setTextColor(ContextCompat.getColor(ctx, R.color.alert_success_color))
                btnOk?.backgroundTintList =
                    ContextCompat.getColorStateList(ctx, R.color.alert_success_color)
                lineTop?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_success_color
                    )
                )
                lineBottom?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_success_color
                    )
                )
                topIcon?.setImageResource(R.mipmap.icon_success_tick)
            }
            AlertType.ERROR -> {
                title?.text = ctx.getString(R.string.error)
                title?.setTextColor(ContextCompat.getColor(ctx, R.color.alert_error_color))
                btnOk?.backgroundTintList =
                    ContextCompat.getColorStateList(ctx, R.color.alert_error_color)
                lineTop?.setBackgroundColor(ContextCompat.getColor(ctx, R.color.alert_error_color))
                lineBottom?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_error_color
                    )
                )
                topIcon?.setImageResource(R.mipmap.icon_error_close)
            }
            AlertType.SERVER_ERROR -> {
                title?.text = ctx.getString(R.string.lbl_error)
                title?.setTextColor(ContextCompat.getColor(ctx, R.color.alert_error_color))
                btnOk?.backgroundTintList =
                    ContextCompat.getColorStateList(ctx, R.color.alert_error_color)
                lineTop?.setBackgroundColor(ContextCompat.getColor(ctx, R.color.alert_error_color))
                lineBottom?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_error_color
                    )
                )
                topIcon?.setImageResource(R.mipmap.icon_error_close)
            }
            AlertType.WARNING -> {
                title?.text = ctx.getString(R.string.oh_snap)
                title?.setTextColor(ContextCompat.getColor(ctx, R.color.alert_warning_color))
                btnOk?.backgroundTintList =
                    ContextCompat.getColorStateList(ctx, R.color.alert_warning_color)
                lineTop?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_warning_color
                    )
                )
                lineBottom?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_warning_color
                    )
                )
                topIcon?.setImageResource(R.mipmap.icon_info_warning)
            }
        }

    }

    fun showDialog(
        ctx: Context,
        titleMsg: String,
        msg: String,
        positiveText: String,
        type: AlertType, listener: OKClickListener?
    ) {
        okClickListener = listener
        dialogBuilder = AlertDialog.Builder(ctx)
        dialogBuilder.setCancelable(false)
        dialogBuilder.setView(R.layout.alert_custom_dialog)
        var alertDialog = dialogBuilder.create()
        // magic of transparent background goes here
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        // setting the alertDialog's BackgroundDrawable as the color resource of any color with 1% opacity
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#00141414")))

        // finally displaying the Alertdialog containging the ProgressBar
        alertDialog.show()

        val title = alertDialog.findViewById<TextView>(R.id.alertTitleTV)
        val message = alertDialog.findViewById<TextView>(R.id.alertMsgTV)
        val btnOk = alertDialog.findViewById<MaterialButton>(R.id.btnOk)
        val lineTop = alertDialog.findViewById<View>(R.id.lineTop)
        val lineBottom = alertDialog.findViewById<View>(R.id.lineBottom)
        val topIcon = alertDialog.findViewById<ImageView>(R.id.iconStatus)


        topIcon?.setImageResource(R.drawable.ic_cross_circle_outline_24)

        btnOk?.setOnClickListener {
            alertDialog.dismiss()
            okClickListener?.onClick(it)
        }
        message?.text = msg
        title?.text = titleMsg
        btnOk?.text = positiveText
        when (type) {
            AlertType.SUCCESS -> {
                title?.text = ctx.getString(R.string.success)
                title?.setTextColor(ContextCompat.getColor(ctx, R.color.alert_success_color))
                btnOk?.backgroundTintList =
                    ContextCompat.getColorStateList(ctx, R.color.alert_success_color)
                lineTop?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_success_color
                    )
                )
                lineBottom?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_success_color
                    )
                )
                topIcon?.setImageResource(R.mipmap.icon_success_tick)
            }
            AlertType.ERROR -> {
                title?.text = ctx.getString(R.string.error)
                title?.setTextColor(ContextCompat.getColor(ctx, R.color.alert_error_color))
                btnOk?.backgroundTintList =
                    ContextCompat.getColorStateList(ctx, R.color.alert_error_color)
                lineTop?.setBackgroundColor(ContextCompat.getColor(ctx, R.color.alert_error_color))
                lineBottom?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_error_color
                    )
                )
                topIcon?.setImageResource(R.mipmap.icon_error_close)
            }
            AlertType.WARNING -> {
                title?.text = ctx.getString(R.string.oh_snap)
                title?.setTextColor(ContextCompat.getColor(ctx, R.color.alert_warning_color))
                btnOk?.backgroundTintList =
                    ContextCompat.getColorStateList(ctx, R.color.alert_warning_color)
                lineTop?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_warning_color
                    )
                )
                lineBottom?.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.alert_warning_color
                    )
                )
                topIcon?.setImageResource(R.mipmap.icon_info_warning)
            }
        }
    }

    fun showCheckOutDialog(
        ctx: Context,
        titleMsg: String,
        msg: String,
        positiveText: String,
        type: AlertType, listener: OKClickListener?
    ) {
        okClickListener = listener
        dialogBuilder = AlertDialog.Builder(ctx)
        dialogBuilder.setCancelable(false)
        dialogBuilder.setView(R.layout.alert_ok)
        val alertDialog = dialogBuilder.create()
        // magic of transparent background goes here
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        // setting the alertDialog's BackgroundDrawable as the color resource of any color with 1% opacity
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#00141414")))

        // finally displaying the Alertdialog containging the ProgressBar
        alertDialog.show()


        val topBg = alertDialog.findViewById<RelativeLayout>(R.id.iconBackgroundView)
        val topIcon = alertDialog.findViewById<ImageView>(R.id.iconStatus)
        val title = alertDialog.findViewById<TextView>(R.id.alertTitleTV)
        val message = alertDialog.findViewById<TextView>(R.id.alertMsgTV)
        val btntryAgain = alertDialog.findViewById<TextView>(R.id.btntryAgain)
        val btnContinueShopping = alertDialog.findViewById<TextView>(R.id.btnContinueShopping)
        btnContinueShopping?.visibility = View.VISIBLE

        btntryAgain?.setOnClickListener {
            alertDialog.dismiss()
            okClickListener?.onClick(it)
        }

        btnContinueShopping?.setOnClickListener {
            alertDialog.dismiss()
            okClickListener?.onContinueShoppingClick(it)
        }

        message?.text = msg
        title?.text = titleMsg
        btntryAgain?.text = positiveText
        when (type) {
            AlertType.SUCCESS -> {
                topBg?.setBackgroundColor(ContextCompat.getColor(ctx, R.color.success_bg))
                topIcon?.setImageResource(R.drawable.ic_circle_outline_24)
            }
            AlertType.ERROR -> {
                topBg?.setBackgroundColor(ContextCompat.getColor(ctx, R.color.error_bg))
                topIcon?.setImageResource(R.drawable.ic_cross_circle_outline_24)
            }
            AlertType.WARNING -> {
                topBg?.setBackgroundColor(ContextCompat.getColor(ctx, R.color.warning_bg))
                topIcon?.setImageResource(R.drawable.ic_cross_circle_outline_24)
            }
            else -> {

            }
        }
    }

    fun showCheckOutWarningDialog(
        ctx: Context,
        msg: String,
        outOfStockList: String,
        unavailableList: String,
        listener: OKClickListener?
    ) {
        okClickListener = listener
        dialogBuilder = AlertDialog.Builder(ctx)
        dialogBuilder.setCancelable(false)
        dialogBuilder.setView(R.layout.alert_ok_cancel)
        var alertDialog = dialogBuilder.create()
        // magic of transparent background goes here
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        // setting the alertDialog's BackgroundDrawable as the color resource of any color with 1% opacity
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#00141414")))

        // finally displaying the Alertdialog containging the ProgressBar
        alertDialog.show()

        val message = alertDialog.findViewById<TextView>(R.id.alertMsgTV)
        val tvOutOfStockList = alertDialog.findViewById<TextView>(R.id.tvOutOfStockList)
        val tvLblUnAvailable = alertDialog.findViewById<TextView>(R.id.tvLblUnAvailable)
        val tvLblOutOfStock = alertDialog.findViewById<TextView>(R.id.tvLblOutOfStock)
        val tvProductList = alertDialog.findViewById<TextView>(R.id.tvProductList)
        val btnOk = alertDialog.findViewById<MaterialButton>(R.id.btnOk)
        val btnCancel = alertDialog.findViewById<MaterialButton>(R.id.btnCancel)

        message?.text = msg
        tvOutOfStockList?.text = outOfStockList
        tvProductList?.text = unavailableList

        if (TextUtils.isEmpty(outOfStockList)) {
            tvLblOutOfStock?.setGone()
            tvOutOfStockList?.setGone()
        }
        if (TextUtils.isEmpty(unavailableList)) {
            tvLblUnAvailable?.setGone()
            tvProductList?.setGone()
        }
        btnOk?.setOnClickListener {
            alertDialog.dismiss()
            okClickListener?.onClick(it)
        }
        btnCancel?.setOnClickListener {
            alertDialog.dismiss()
        }
    }

    interface OKClickListener {
        fun onClick(view: View?)
        fun onContinueShoppingClick(view: View?)
    }
}