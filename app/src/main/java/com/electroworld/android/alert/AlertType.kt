package com.electroworld.android.alert

enum class AlertType {
    SUCCESS, ERROR, WARNING,SERVER_ERROR
}