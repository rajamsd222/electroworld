package com.electroworld.android

import android.content.Context
import androidx.multidex.MultiDex
import com.electroworld.android.di.DaggerAppComponent
import com.splunk.mint.Mint
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class BaseApplication : DaggerApplication() {
    override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
        MultiDex.install(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }

    override fun onCreate() {
        super.onCreate()
        ins = this
        Mint.initAndStartSession(this, "94d5f4cf");
    }

    companion object {
        lateinit var ins: BaseApplication
    }
}