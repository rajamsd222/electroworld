package com.electroworld.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cashfree.pg.CFPaymentService;
import com.electroworld.android.api.ApiResource;
import com.electroworld.android.api.ApiResponse;
import com.electroworld.android.api.auth.AuthApi;
import com.electroworld.android.base.GenericAdapter;
import com.electroworld.android.customclass.CustomTypingEditText;
import com.electroworld.android.databinding.RowNotificationListBinding;
import com.electroworld.android.model.CategoryModel;
import com.electroworld.android.model.ImagesModel;
import com.electroworld.android.model.ProductModel;
import com.electroworld.android.model.SendOTPModel;
import com.electroworld.android.model.filter.FilterData;
import com.electroworld.android.model.filter.FilterModel;
import com.electroworld.android.model.orderlist.OrderListProductItem;
import com.electroworld.android.model.orderlist.OrderStatusHistoryItem;
import com.electroworld.android.ui.main.address.add.locationutil.GetAddressFromLatLng;
import com.electroworld.android.ui.main.cart.models.CartItem;
import com.electroworld.android.ui.main.notifications.NotificationModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import io.reactivex.disposables.CompositeDisposable;

public class TestActivity extends AppCompatActivity {

    MutableLiveData<List<CartItem>> mutableCart = new MutableLiveData<>();

    private LiveData<List<CartItem>> getCart(){
        if(mutableCart.getValue() == null){
            initCart();
        }
        return  mutableCart;
    }

    private void initCart() {
        mutableCart.setValue(new ArrayList<CartItem>());
    }

    private boolean addItemtocart(ProductModel product){
        if(mutableCart.getValue() == null){
            initCart();
        }

        List<CartItem> cartItemList = new ArrayList<>(mutableCart.getValue());
        CartItem item = new CartItem(product,1);
        cartItemList.add(item);
        mutableCart.setValue(cartItemList);
        return true;
    }

    private final CompositeDisposable disposables = new CompositeDisposable();
    private AuthApi authApi;
    String authentication_device_id = "";
    List<CategoryModel> list = new ArrayList<>();
    ProductModel currentModel = null;
    private static final String LOGIN_TYPE_NORMAL = "normal";
    int currentPos = -1;
    private final MutableLiveData<ApiResource<ApiResponse<SendOTPModel>>> responseLiveData = new MutableLiveData<>();
    double latitude;
    double longitude;
    List<FilterModel> mFiltersList = new ArrayList<>();
    List<NotificationModel> mList = new ArrayList<>();
    GetAddressFromLatLng asyncTaskGetAddress;
    private GenericAdapter<NotificationModel, RowNotificationListBinding> mAdapter;
    List<CategoryModel> categoryModels = new ArrayList<>();
    List<ImagesModel> imageList = new ArrayList<>();
    CustomTypingEditText edSearch;

    public void doPayment(Context context, Map<String, String> params, String token, String stage){
        Map<String, String> postData = new HashMap<>();
        postData.put("orderId", "12345");

        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
        cfPaymentService.setOrientation(0);
        cfPaymentService.doPayment(this,postData,token,"TEST");
    }
    CustomTypingEditText.OnTypingModified textWatcher = new CustomTypingEditText.OnTypingModified() {
        @Override
        public void onIsTypingModified(EditText view, boolean isTyping) {
            if(isTyping){
                Log.i("TAG", "onIsTypingModified: User started typing.");
            }else{
                Log.i("TAG", "onIsTypingModified: User stopped typing");
            }
        }
    };
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        edSearch = findViewById(R.id.edSearch);
        asyncTaskGetAddress =  new GetAddressFromLatLng(TestActivity.this);
        asyncTaskGetAddress.setLatLng(latitude,longitude);
        asyncTaskGetAddress.execute();

        edSearch.setOnTypingModified(textWatcher);

/*
        RequestBody someDataBody = null;
        Map<String, RequestBody> params = new HashMap<>();
        params.put("image1", someDataBody);*/

        List<ImagesModel> imageList = new ArrayList<>();
        imageList.add(new ImagesModel("","",""));

        for(int i=0;i<categoryModels.size();i++){
            for(int j=0;j<categoryModels.get(i).getSubcategory().size();j++){
                if(categoryModels.get(i).getName().contains("") ||
                 categoryModels.get(i).getSubcategory().get(j).getName().contains("")){

                }
            }

        }

        mAdapter =
                new GenericAdapter<NotificationModel, RowNotificationListBinding>(R.layout.row_notification_list) {

            @Override
            public void onBindData(NotificationModel model, int position, RowNotificationListBinding dataBinding) {

            }

            @Override
            public void onItemClick(NotificationModel model, int position) {

            }
        };
       /* rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        Log.d("chk", "id" + checkedId);
                        RadioButton button = group.findViewById(checkedId);
                        if (checkedId == R.id.a) {
                            //some code
                        } else if (checkedId == R.id.b) {
                            //some code
                        }

                    }

                });
*/
        CheckBox checkBox = findViewById(R.id.checkBox);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    List<OrderStatusHistoryItem> list = (List<OrderStatusHistoryItem>) v.getTag();
                }
            }
        });


        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) checkBox.getLayoutParams();
        checkBox.setLayoutParams(params);

        RadioGroup rg = new RadioGroup(this);
        RadioButton rb = new RadioButton(this);
        //rb.setText();
        rb.setLayoutParams(checkBox.getLayoutParams());

        if(mFiltersList!=null && mFiltersList.size()>0){
            for(int i=0;i<mFiltersList.size();i++){
                List<FilterData> mFilterDataList = mFiltersList.get(i).getFilter_data();
                if(mFilterDataList!=null && mFilterDataList.size()>0){
                    for(int j=0;j<mFilterDataList.size();i++){
                        FilterData data = mFilterDataList.get(j);
                        if(mFilterDataList.get(j).isSelected()){
                            Log.e("name",mFiltersList.get(i).getKey() +" = "+mFilterDataList.get(j).getName());
                        }

                        mFilterDataList.set(j,data);
                    }
                }
            }
        }






        TagContainerLayout tagLyaout = findViewById(R.id.taglayout);
        tagLyaout.setOnTagClickListener(new TagView.OnTagClickListener() {

            @Override
            public void onTagClick(int position, String text) {
                // ...
            }

            @Override
            public void onTagLongClick(final int position, String text) {
                // ...
            }

            @Override
            public void onSelectedTagDrag(int position, String text){
                // ...
            }

            @Override
            public void onTagCrossClick(int position) {
                // ...
            }
        });

    }



    private int getGroupPos(List<CategoryModel> list,String categoryId){
        if(list!=null && list.size()>0){
            for(int i=0;i<list.size();i++){
                if(list.get(i).getId().equals(categoryId)){
                    return i;
                }
            }
        }
        return -1;
    }


    private String getProductNames(List<OrderListProductItem> list){
        String str = "";
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<list.size();i++){
            sb.append(list.get(i).getName_en());
            if(i==list.size()-1){
                sb.append(", ");
            }
        }
        return str;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            Bundle  bundle = data.getExtras();
            if (bundle != null)
                for (String  key  :  bundle.keySet()) {
                    if (bundle.getString(key) != null) {
                        Log.d("TAG", key + " : " + bundle.getString(key));
                    }
                }
        }
    }

    public void setClick(Button view){
        int pos = (int) view.getTag();
        for(int i=0;i<list.size();i++){
            if(i==pos){
                list.get(i).setSelected(true);
            }
            else{
                list.get(i).setSelected(false);
            }
        }
    }
}
