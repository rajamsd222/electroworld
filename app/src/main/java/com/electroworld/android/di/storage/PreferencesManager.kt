package com.electroworld.android.di.storage

import android.content.SharedPreferences
import com.electroworld.android.model.AddressListModel
import com.electroworld.android.model.DeliverySlot
import com.electroworld.android.model.StoreModel
import com.electroworld.android.model.UserModel
import com.electroworld.android.ui.main.cart.models.CartSingleItem
import com.electroworld.android.ui.main.cart.models.LocalCartModel
import com.electroworld.android.ui.main.cart.models.LocalCompareModel
import com.electroworld.android.utils.AppValidator.toDouble
import com.electroworld.android.utils.AppValidator.toStr
import com.electroworld.android.utils.Constants
import com.electroworld.android.utils.MyUtils
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


/**
 * This class is used for storing all type of  primitive data in key-value pairs. Data Types include Booleans, floats, ints, longs, and strings.
 */
class PreferencesManager(private val myPref: SharedPreferences) {


    private fun setGSTNumber(str: String?) {
        setStringValue(GST_NUMBER, str)
    }

    private fun setCompanyName(str: String?) {
        setStringValue(COMPANY_NAME, str)
    }

    fun setGSTEnabled(isChecked: Boolean, str: String?, companyName: String?) {
        setBooleanValue(GST_CHECKED, isChecked)
        setGSTNumber(str)
        setCompanyName(companyName)
    }

    fun getGSTEnabled(): Boolean {
        return getBooleanValue(GST_CHECKED)
    }

    fun getGSTEnabledValue(): Int {
        return if (getGSTEnabled()) 1 else 0
    }

    fun getGSTNumber(): String {
        return if (getGSTEnabled()) getStringValue(GST_NUMBER) else ""
    }

    fun getCompanyName(): String {
        return if (getGSTEnabled()) getStringValue(COMPANY_NAME) else ""
    }

    fun getFromTime(): String {
        val model = getLocalCart()
        return if (model != null) {
            if (getPickupFromStoreValue() == 1) {
                toStr(model.pickupSlot!!.from_time)
            } else {
                toStr(model.deliverySlot!!.from_time)
            }
        } else ""
    }

    fun getDeliveryTimeType(): String {
        return "as_soon_as"
    }

    fun getToTime(): String {
        val model = getLocalCart()
        return if (model != null) {
            if (getPickupFromStoreValue() == 1) {
                toStr(model.pickupSlot!!.to_time)
            } else {
                toStr(model.deliverySlot!!.to_time)
            }
        } else ""
    }

    fun getPickUpSlotText(): String {
        val model = getLocalCart()
        return if (model != null && model.pickupSlot != null) {
            toStr(model.pickupSlot!!.slot)
        } else ""
    }

    fun setWalletAmount(amount: Double) {
        setStringValue(WALLET_AMOUNT, amount.toString())
    }

    fun getGiftFromMessage(): String {
        val model = getLocalCart()
        return if (model != null) {
            toStr(model.giftFrom)
        } else ""
    }

    fun getGiftToMessage(): String {
        val model = getLocalCart()
        return if (model != null) {
            toStr(model.giftTo)
        } else ""
    }

    fun getGiftMessage(): String {
        val model = getLocalCart()
        return if (model != null) {
            toStr(model.giftMessage)
        } else ""
    }

    fun updateGiftMessages(from: String?, to: String?, message: String?) {
        val model = getLocalCart()
        if (model != null) {
            model.giftFrom = from!!
            model.giftTo = to!!
            model.giftMessage = message!!
            saveCartItems(model)
        }
    }


    fun updateBillingAddressInfo(
        isBillingChecked: Boolean,
        address: String?,
        phone: String?,
        contact_person: String?
    ) {
        val model = getLocalCart()
        if (model != null) {
            model.billing_address = if (isBillingChecked) "" else address.toString()
            model.billing_phone = if (isBillingChecked) "" else phone.toString()
            model.billing_contact_person = if (isBillingChecked) "" else contact_person.toString()
            saveCartItems(model)
        }
    }

    fun getBillingContactName(): String {
        val model = getLocalCart()
        return if (model != null) {
            toStr(model.billing_contact_person)
        } else ""
    }

    fun getBillingPhone(): String {
        val model = getLocalCart()
        return if (model != null) {
            toStr(model.billing_phone)
        } else ""
    }

    fun getBillingAddress(): String {
        val model = getLocalCart()
        return if (model != null) {
            toStr(model.billing_address)
        } else ""
    }

    fun updateBilling(isBillingChecked: Boolean?) {
        val model = getLocalCart()
        if (model != null) {
            model.isBillingAddressEnabled = isBillingChecked!!
            saveCartItems(model)
        }
    }

    fun getBillingChecked(): Boolean {
        val model = getLocalCart()
        return model?.isBillingAddressEnabled ?: false
    }


    fun getPickupFromStoreValue(): Int {
        val model = getLocalCart()
        return if (model != null) {
            if (model.isPickUpFromStoreEnabled) 1 else 0
        } else 0
    }

    fun isPickupFromStoreSelected(): Boolean {
        val model = getLocalCart()
        return model?.isPickUpFromStoreEnabled ?: false
    }

    fun updateFlagPickUpFromStore(isChecked: Boolean) {
        val model = getLocalCart()
        if (model != null) {
            model.isPickUpFromStoreEnabled = isChecked
            saveCartItems(model)
        }
    }

    fun updatePickUpSlot(slot: DeliverySlot?) {
        val model = getLocalCart()
        if (model != null) {
            model.pickupSlot = slot
            saveCartItems(model)
        }
    }

    fun getDeliveryCharge(): Double {
        val deliveryCharge = 0.0
        val model = getLocalCart()
        return if (model != null) {
            if (model.isPickUpFromStoreEnabled) {
                deliveryCharge
            } else {
                model.deliveryCharge
            }
        } else deliveryCharge
    }

    fun updateDeliveryCharge(charge: Double) {
        val model = getLocalCart()
        if (model != null) {
            model.deliveryCharge = charge
            saveCartItems(model)
        }
    }

    fun getSubTotal(): Double {
        var subTotal = 0.0
        val model = getLocalCart()
        if (model != null) {
            if (model.cartItemList.size > 0) {
                for (item in model.cartItemList) {
                    subTotal =
                        subTotal + toDouble(item.productModel.discounted_price) * item.quantity
                }
            }
        }
        subTotal = MyUtils.formatCurrencyFromDoubleWithoutSymbol(subTotal)
        return subTotal
    }

    fun getWalletAmount(): Double {
        return toDouble(getStringValue(WALLET_AMOUNT))
    }

    fun updateWalletSelection(isSelected: Boolean) {
        val model = getLocalCart()
        if (model != null) {
            model.isWalletEnabled = isSelected
        }
        saveCartItems(model)
    }

    fun checkWalletSelected(): Boolean {
        val model = getLocalCart()
        return model?.isWalletEnabled ?: false
    }

    fun getWalletValue(): Int {
        val model = getLocalCart()
        if (model != null) {
            if (model.isWalletEnabled) {
                return 1
            }
        }
        return 0
    }

    fun updateGiftInfo(isGiftSelected: Boolean, giftAmount: Double) {
        val model = getLocalCart()
        if (model != null) {
            model.isGiftEnabled = isGiftSelected
            model.giftAmount = giftAmount
        }
        saveCartItems(model)
    }

    fun checkGiftSelected(): Boolean {
        val model = getLocalCart()
        return model?.isGiftEnabled ?: false
    }

    fun getGiftValue(): Int {
        val model = getLocalCart()
        if (model != null) {
            if (model.isGiftEnabled) {
                return 1
            }
        }
        return 0
    }

    fun getGiftAmount(): Double {
        val model = getLocalCart()
        if (model != null) {
            if (model.isGiftEnabled) {
                return model.giftAmount
            }
        }
        return 0.0
    }

    fun updateDeliverySlot(deliverySlot: DeliverySlot?) {
        val model = getLocalCart()
        if (model != null) {
            model.deliverySlot = deliverySlot
        }
        saveCartItems(model)
    }

    fun getDeliverySlot(): DeliverySlot? {
        val model = getLocalCart()
        return model?.deliverySlot
    }

    fun getPromoCode(): String {
        val model = getLocalCart()
        return model?.promocode ?: ""
    }

    fun getDeliveryInstruction(): String {
        val model = getLocalCart()
        if (model != null) {
            return model.orderInstruction ?: ""
        }
        return ""
    }

    fun getDeliverySlotText(): String {
        val model = getLocalCart()
        if (model != null) {
            if (model.deliverySlot != null) {
                return model.deliverySlot!!.slot
            }
        }
        return ""
    }

    fun resetHomeStoreInfo() {
        setPrefKeyStoreId("")
        setPrefKeyStoreAddress("")
        setPrefKeyStoreName("")
        setHomeAddressTop("")
    }

    fun getPrefKeyStoreId(): String {
        return getStringValue(PREF_KEY_STORE_ID)
    }

    fun setPrefKeyStoreId(value: String?) {
        setStringValue(PREF_KEY_STORE_ID, value)
    }

    fun getPrefKeyStoreAddress(): String {
        return getStringValue(PREF_KEY_STORE_ADDRESS)
    }

    fun setPrefKeyStoreAddress(value: String?) {
        setStringValue(PREF_KEY_STORE_ADDRESS, value)
    }

    fun getPrefKeyStoreName(): String {
        return getStringValue(PREF_KEY_STORE_NAME)
    }

    fun setPrefKeyStoreName(value: String?) {
        setStringValue(PREF_KEY_STORE_NAME, value)
    }

    fun getHomeAddressTop(): String {
        return getStringValue(HOME_ADDRESS_TOP)
    }

    fun setHomeAddressTop(value: String?) {
        setStringValue(HOME_ADDRESS_TOP, value)
    }

    fun getCartName(): String {
        return PREF_KEY_LOCAL_CART/* + "_" + getPrefKeyStoreId()*/
    }

    fun getAddToCompareName(): String {
        return PREF_KEY_LOCAL_ADD_TO_COMPARE/* + "_" + getPrefKeyStoreId()*/
    }


    fun getCartArray(): JSONArray {
        val array = JSONArray()
        val model = getLocalCart()
        if (model != null) {
            val cartlist: List<CartSingleItem> = model.cartItemList
            if (cartlist.size > 0) {
                for (i in cartlist.indices) {
                    val obj = JSONObject()
                    try {
                        obj.put("name_en", cartlist[i].productModel.name_en)
                        obj.put("product_id", cartlist[i].productModel.product_id)
                        obj.put("quantity", cartlist[i].quantity)
                        obj.put(
                            "warehouse_product_id",
                            cartlist[i].productModel.warehouse_product_id
                        )
                        var subTotal =
                            cartlist[i].quantity * toDouble(cartlist[i].productModel.discounted_price)
                        subTotal = MyUtils.formatCurrencyFromDoubleWithoutSymbol(subTotal)
                        obj.put(
                            "subtotal",
                            subTotal
                        )
                        array.put(obj)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        }
        return array
    }

    fun getCartDetailsForFOCProduct(): JSONArray {
        val array = JSONArray()
        val obj = JSONObject()
        val model = getLocalCart()
        if (model != null) {
            val cartList: List<CartSingleItem> = model.cartItemList
            if (cartList.isNotEmpty()) {
                for (i in cartList.indices) {
                    obj.put(cartList[i].productModel.product_id, cartList[i].quantity)
                }
            }
        }
        array.put(obj)
        return array
    }


    fun getLocalCart(): LocalCartModel? {
        return Gson().fromJson(getStringValue(getCartName()), LocalCartModel::class.java)
    }

    fun getAddToCompareData(): LocalCompareModel? {
        return Gson().fromJson(getStringValue(getAddToCompareName()), LocalCompareModel::class.java)
    }

    fun saveAddToCompare(model: LocalCompareModel) {
        val str = Gson().toJson(model, LocalCompareModel::class.java)
        setStringValue(getAddToCompareName(), str)
    }

    fun getCompareProductCount(): Int {
        val model = getAddToCompareData()
        return model?.cartItemList?.size ?: 0
    }

    fun saveCartItems(model: LocalCartModel?) {
        val str = Gson().toJson(model, LocalCartModel::class.java)
        setStringValue(getCartName(), str)
    }

    fun getCartCount(): Int {
        val model = getLocalCart()
        return model?.cartItemList?.size ?: 0
    }


    fun setDefaultAddress(model: AddressListModel?) {
        if (model != null) {
            setStringValue(
                PREF_KEY_ADDRESS_LIST_MODEL,
                Gson().toJson(model, AddressListModel::class.java)
            )
        } else {
            setStringValue(PREF_KEY_ADDRESS_LIST_MODEL, "")
        }
    }


    fun getDefaultAddress(): AddressListModel? {
        return Gson().fromJson(
            getStringValue(PREF_KEY_ADDRESS_LIST_MODEL),
            AddressListModel::class.java
        )
    }

    fun clearLocalCart() {
        Constants.DELIVERY_TIME = ""
        Constants.PAYMENT_METHOD = ""
        Constants.ADDRESS_ID = ""
        Constants.BANK_TRANSFER_NUMBER = ""
        Constants.DELIVERY_SLOT_TEXT = ""
        setStringValue(getCartName(), "")
    }

    fun clearCompareProducts() {
        setStringValue(getAddToCompareName(), "")
    }

    fun getPrefKeyVendorId(): String {
        return getStringValue(PREF_KEY_VENDOR_ID)
    }

    fun setPrefKeyVendorId(id: String?) {
        setStringValue(PREF_KEY_VENDOR_ID, id)
    }


    fun isLogin(): Boolean { /*
        boolean isLogin = false;
        UserModel model = getPrefKeyUserModel();
        if(model!=null){
            isLogin = model.getCustomerId().length()>0;
        }*/
        return getPrefKeyCustomerId().isNotEmpty()
    }

    fun getFCMToken(): String {
        return getStringValue(PREF_KEY_FCM_TOKEN)
    }

    fun saveFCM(fcmToken: String?) {
        setStringValue(PREF_KEY_FCM_TOKEN, fcmToken)
    }

    fun getPrefKeyLanguage(): String {
        return getStringValue(PREF_KEY_LANGUAGE)
    }

    fun isEnglishLanguage(): Boolean {
        return getStringValue(PREF_KEY_LANGUAGE) == "en"
    }

    fun setPrefKeyLanguage(lang: String?) {
        setStringValue(PREF_KEY_LANGUAGE, lang)
    }

    fun getPrefKeyCustomerId(): String {
        return getStringValue(PREF_KEY_CUSTOMER_ID)
    }

    fun setPrefKeyCustomerId(id: String?) {
        setStringValue(PREF_KEY_CUSTOMER_ID, id)
    }

    fun getPrefKeyAccessToken(): String {
        return getStringValue(PREF_KEY_ACCESS_TOKEN)
    }

    fun getPrefKeyAuthenticationId(): String {
        return getStringValue(PREF_KEY_AUTHENTICATION_ID)
    }

    fun getMobileNumber(): String? {
        val model = getPrefKeyUserModel()
        return model?.phone ?: ""
    }

    fun getEmail(): String? {
        val model = getPrefKeyUserModel()
        return model?.email ?: ""
    }

    fun getFirstName(): String? {
        val model = getPrefKeyUserModel()
        return model?.firstname ?: ""
    }

    fun getFullName(): String? {
        val model: UserModel? = getPrefKeyUserModel()
        return if (model != null) {
            toStr(model.firstname) + " " + toStr(model.lastname)
        } else ""
    }

    fun setPrefKeyUserModel(userModel: UserModel?) {
        val str = Gson().toJson(userModel, UserModel::class.java)
        setStringValue(PREF_KEY_USER_MODEL, str)
        val model = getPrefKeyUserModel()
        if (model != null) {
            setWalletAmount(model.wallet_balance)
            if (toStr(model.gst_number).length > 0) {
                setGSTNumber(model.gst_number)
            }
            if (toStr(model.gst_invoice_name).length > 0) {
                setCompanyName(model.gst_invoice_name)
            }
        }
    }

    fun getPrefKeyUserModel(): UserModel? {
        return Gson().fromJson(getStringValue(PREF_KEY_USER_MODEL), UserModel::class.java)
    }

    fun getStoreModel(): StoreModel? {
        return Gson().fromJson(getStringValue(DELIVERY_ADDRESS_STORE_MODEL), StoreModel::class.java)
    }

    fun setStoreModel(storeModel: StoreModel?) {
        setStringValue(
            DELIVERY_ADDRESS_STORE_MODEL,
            Gson().toJson(storeModel, StoreModel::class.java)
        )
    }

    fun getMaxQuantityLimit(): Int {
        if (getIntValue(MAX_QTY_RESTRICTION) == 0) {
            return Constants.DEFAULT_MAX_QTY
        } else {
            return getIntValue(MAX_QTY_RESTRICTION)
        }

    }

    fun setMaxQuantityLimit(value: Int) {
        setIntValue(MAX_QTY_RESTRICTION, value)
    }

    fun setDeliveryAddressStoreId(value: String) {
        setStringValue(DELIVERY_ADDRESS_STORE_ID, value)
    }

    fun getDeliveryAddressStoreId(): String {
        return getStringValue(DELIVERY_ADDRESS_STORE_ID)
    }

    fun checkStoreModelAdded(): String {
        val model = getStoreModel()
        if (model != null) {
            return model.id
        }
        return ""
    }

    fun setStringValue(keyName: String?, value: String?) {
        myPref.edit().putString(keyName, value).apply()
    }

    private fun getStringValue(keyName: String?): String {
        return toStr(myPref.getString(keyName, ""))
    }

    private fun setBooleanValue(keyName: String?, value: Boolean) {
        myPref.edit().putBoolean(keyName, value).apply()
    }

    private fun getBooleanValue(keyName: String?): Boolean {
        return myPref.getBoolean(keyName, false)
    }

    private fun setIntValue(keyName: String?, value: Int) {
        myPref.edit().putInt(keyName, value).apply()
    }

    private fun getIntValue(keyName: String?): Int {
        return myPref.getInt(keyName, 0)
    }


    fun remove(key: String?) {
        myPref.edit().remove(key).apply()
    }

    fun clear(): Boolean {
        return myPref.edit().clear().commit()
    }


    companion object {
        var MAX_QTY_RESTRICTION = "max_qty_restriction"
        var PREF_KEY_ACCESS_TOKEN = "token"
        var PREF_KEY_USER_MODEL = "user_model"
        var PREF_KEY_CUSTOMER_ID = "customer_id"
        var PREF_KEY_AUTHENTICATION_ID = "authentication_id"
        var PREF_KEY_LANGUAGE = "language_selection"
        var PREF_KEY_FCM_TOKEN = "fcm_token"
        var PREF_KEY_LOCAL_CART = "local_cart"
        var PREF_KEY_LOCAL_ADD_TO_COMPARE = "add_to_compare"
        var PREF_KEY_STORE_ID = "store_id"
        var DELIVERY_ADDRESS_STORE_MODEL = "delivery_address_store_model"
        var DELIVERY_ADDRESS_STORE_ID = "delivery_address_store_id"
        var PREF_KEY_STORE_ADDRESS = "store_address"
        var PREF_KEY_STORE_NAME = "store_name"
        var HOME_ADDRESS_TOP = "address_home_top"
        var PREF_KEY_VENDOR_ID = "vendor_id"
        var PREF_KEY_ADDRESS_LIST_MODEL = "address_list_model"
        var WALLET_AMOUNT = "wallet_amount"
        var GST_CHECKED = "gst_checked"
        var GST_NUMBER = "gst_number"
        var COMPANY_NAME = "company_name"
    }
}