package com.electroworld.android.di

import com.electroworld.android.di.auth.AuthFragmentBuildersModule
import com.electroworld.android.di.auth.AuthModule
import com.electroworld.android.di.auth.AuthViewModelsModule
import com.electroworld.android.di.main.MainFragmentBuildersModule
import com.electroworld.android.di.main.MainModule
import com.electroworld.android.di.main.MainScope
import com.electroworld.android.di.main.MainViewModelsModule
import com.electroworld.android.ui.SplashActivity
import com.electroworld.android.ui.auth.AuthActivity
import com.electroworld.android.ui.main.MainActivity
import com.electroworld.android.ui.main.PaymentActivity
import com.electroworld.android.ui.main.products.detail.image.FullScreenImageViewActivity
import com.electroworld.android.ui.main.products.detail.image.VideoPlayerActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {
    @ContributesAndroidInjector(modules = [AuthFragmentBuildersModule::class, AuthViewModelsModule::class, AuthModule::class])
    abstract fun contributeAuthActivity(): AuthActivity

    @ContributesAndroidInjector
    abstract fun contributePaymentActivity(): PaymentActivity

    @ContributesAndroidInjector
    abstract fun contributeFullScreenImageViewActivity(): FullScreenImageViewActivity


    @ContributesAndroidInjector
    abstract fun contributeVideoPlayerActivity(): VideoPlayerActivity

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainFragmentBuildersModule::class, MainViewModelsModule::class, MainModule::class])
    abstract fun contribureMainActivity(): MainActivity
}