package com.electroworld.android.di.main

import androidx.lifecycle.ViewModel
import com.baldystudios.daggerkotlin.di.ViewModelKey
import com.electroworld.android.ui.main.MainActivityViewModel
import com.electroworld.android.ui.main.address.add.MapViewModel
import com.electroworld.android.ui.main.address.deliverylocation.DeliveryLocationViewModel
import com.electroworld.android.ui.main.address.list.AddressListViewModel
import com.electroworld.android.ui.main.cart.CartListViewModel
import com.electroworld.android.ui.main.cashfree.CashFreeViewModel
import com.electroworld.android.ui.main.categories.CategoriesViewModel
import com.electroworld.android.ui.main.checkout.CheckOutViewModel
import com.electroworld.android.ui.main.contactus.ContactUsViewModel
import com.electroworld.android.ui.main.fav.FavouritesViewModel
import com.electroworld.android.ui.main.home.HomeViewModel
import com.electroworld.android.ui.main.myoffersothers.MyOffersAndOthersViewModel
import com.electroworld.android.ui.main.notifications.NotificationViewModel
import com.electroworld.android.ui.main.offers.OffersViewModel
import com.electroworld.android.ui.main.orders.detail.OrderDetailViewModel
import com.electroworld.android.ui.main.orders.list.OrderListViewModel
import com.electroworld.android.ui.main.orders.returnorder.ReturnOrderViewModel
import com.electroworld.android.ui.main.orders.track.TrackOrderViewModel
import com.electroworld.android.ui.main.products.compare.ProductCompareViewModel
import com.electroworld.android.ui.main.products.detail.ProductDetailViewModel
import com.electroworld.android.ui.main.products.list.ProductListViewModel
import com.electroworld.android.ui.main.products.offerpreview.OfferPreviewViewModel
import com.electroworld.android.ui.main.products.search.SearchViewModel
import com.electroworld.android.ui.main.profile.ProfileViewModel
import com.electroworld.android.ui.main.profile.edit.EditProfileViewModel
import com.electroworld.android.ui.main.quickbuy.QuickBuyViewModel
import com.electroworld.android.ui.main.ratingreviews.RatingReviewsViewModel
import com.electroworld.android.ui.main.wallet.WalletViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(CashFreeViewModel::class)
    abstract fun bindCashFreeViewModel(viewModel: CashFreeViewModel): ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(ContactUsViewModel::class)
    abstract fun bindContactUsViewModel(viewModel: ContactUsViewModel): ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(OfferPreviewViewModel::class)
    abstract fun bindOfferPreviewViewModel(viewModel: OfferPreviewViewModel): ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(MyOffersAndOthersViewModel::class)
    abstract fun bindMyOffersAndOthersViewModel(viewModel: MyOffersAndOthersViewModel): ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(ReturnOrderViewModel::class)
    abstract fun bindReturnOrderViewModel(viewModel: ReturnOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RatingReviewsViewModel::class)
    abstract fun bindRatingReviewsViewModel(viewModel: RatingReviewsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun bindMainVM(viewModel: MainActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DeliveryLocationViewModel::class)
    abstract fun bindDeliveryLocationViewModel(viewModel: DeliveryLocationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrackOrderViewModel::class)
    abstract fun bindTrackOrderViewModel(viewModel: TrackOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WalletViewModel::class)
    abstract fun bindWalletViewModel(viewModel: WalletViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OffersViewModel::class)
    abstract fun bindOffersViewModel(viewModel: OffersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QuickBuyViewModel::class)
    abstract fun bindQuickBuyViewModel(viewModel: QuickBuyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditProfileViewModel::class)
    abstract fun bindEditProfileViewModel(viewModel: EditProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavouritesViewModel::class)
    abstract fun bindFavouritesViewModel(viewModel: FavouritesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderDetailViewModel::class)
    abstract fun bindOrderDetailViewModel(viewModel: OrderDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderListViewModel::class)
    abstract fun bindOrderListViewModel(viewModel: OrderListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CheckOutViewModel::class)
    abstract fun bindCheckOutViewModel(viewModel: CheckOutViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CartListViewModel::class)
    abstract fun bindCartListViewModel(viewModel: CartListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailViewModel::class)
    abstract fun bindProductDetailViewModel(viewModel: ProductDetailViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductListViewModel::class)
    abstract fun bindProductListViewModel(viewModel: ProductListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(viewModel: SearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    abstract fun bindAddAddressViewModel(viewModel: MapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddressListViewModel::class)
    abstract fun bindAddressListViewModel(viewModel: AddressListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoriesViewModel::class)
    abstract fun bindCategoriesViewModel(viewModel: CategoriesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NotificationViewModel::class)
    abstract fun bindNotificationViewModel(viewModel: NotificationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(ProductCompareViewModel::class)
    abstract fun bindProductCompareViewModel(viewModel: ProductCompareViewModel): ViewModel
}