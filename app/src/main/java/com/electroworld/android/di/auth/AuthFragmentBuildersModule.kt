package com.electroworld.android.di.auth

import com.electroworld.android.ui.auth.login.LoginFragment
import com.electroworld.android.ui.auth.otp.OTPFragment
import com.electroworld.android.ui.auth.webview.WebViewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AuthFragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeOTPFragment(): OTPFragment

    @ContributesAndroidInjector
    abstract fun contributeWebViewFragment(): WebViewFragment
}