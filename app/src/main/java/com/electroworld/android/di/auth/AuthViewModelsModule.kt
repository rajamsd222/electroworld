package com.electroworld.android.di.auth

import androidx.lifecycle.ViewModel
import com.baldystudios.daggerkotlin.di.ViewModelKey
import com.electroworld.android.ui.auth.login.LoginViewModel
import com.electroworld.android.ui.auth.otp.OTPViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AuthViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(OTPViewModel::class)
    abstract fun bindOTPViewModel(viewModel: OTPViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginModel(viewModel: LoginViewModel): ViewModel
}