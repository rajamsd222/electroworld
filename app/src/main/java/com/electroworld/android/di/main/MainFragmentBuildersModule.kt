package com.electroworld.android.di.main

import com.electroworld.android.ui.auth.webview.WebViewFragment
import com.electroworld.android.ui.main.address.add.MapsFragment
import com.electroworld.android.ui.main.address.deliverylocation.DeliveryLocationSelectionFragment
import com.electroworld.android.ui.main.address.list.AddressListFragment
import com.electroworld.android.ui.main.cart.CartListFragment
import com.electroworld.android.ui.main.cashfree.CashFreePaymentFragment
import com.electroworld.android.ui.main.categories.CategoriesFragment
import com.electroworld.android.ui.main.checkout.CheckOutFragment
import com.electroworld.android.ui.main.contactus.ContactusFragment
import com.electroworld.android.ui.main.fav.FavouritesFragment
import com.electroworld.android.ui.main.home.HomeFragment
import com.electroworld.android.ui.main.myoffersothers.MyOffersAndOthers
import com.electroworld.android.ui.main.notifications.NotificationFragment
import com.electroworld.android.ui.main.offers.OffersFragment
import com.electroworld.android.ui.main.orders.detail.OrderDetailFragment
import com.electroworld.android.ui.main.orders.list.OrderListFragment
import com.electroworld.android.ui.main.orders.returnorder.ReturnOrderFragment
import com.electroworld.android.ui.main.orders.track.TrackOrderFragment
import com.electroworld.android.ui.main.products.compare.ProductCompareFragment
import com.electroworld.android.ui.main.products.detail.ProductDetailFragment
import com.electroworld.android.ui.main.products.list.ProductListFragment
import com.electroworld.android.ui.main.products.offerpreview.OfferPreviewFragment
import com.electroworld.android.ui.main.products.search.SearchFragment
import com.electroworld.android.ui.main.profile.ProfileFragment
import com.electroworld.android.ui.main.profile.edit.EditProfileFragment
import com.electroworld.android.ui.main.quickbuy.QuickBuyFragment
import com.electroworld.android.ui.main.ratingreviews.RatingReviewsListFragment
import com.electroworld.android.ui.main.wallet.WalletFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeCashFreePaymentFragment(): CashFreePaymentFragment

    @ContributesAndroidInjector
    abstract fun contributeContactusFragment(): ContactusFragment

    @ContributesAndroidInjector
    abstract fun contributeOfferPreviewFragment(): OfferPreviewFragment

    @ContributesAndroidInjector
    abstract fun contributeMyOffersAndOthers(): MyOffersAndOthers

    @ContributesAndroidInjector
    abstract fun contributeReturnOrderFragment(): ReturnOrderFragment

    @ContributesAndroidInjector
    abstract fun contributeRatingReviewsListFragment(): RatingReviewsListFragment

    @ContributesAndroidInjector
    abstract fun contributeDeliveryLocationSelectionFragment(): DeliveryLocationSelectionFragment

    @ContributesAndroidInjector
    abstract fun contributeTrackOrderFragment(): TrackOrderFragment

    @ContributesAndroidInjector
    abstract fun contributeWebViewFragment(): WebViewFragment

    @ContributesAndroidInjector
    abstract fun contributeWalletFragment(): WalletFragment

    @ContributesAndroidInjector
    abstract fun contributeOffersFragment(): OffersFragment

    @ContributesAndroidInjector
    abstract fun contributeQuickBuyFragment(): QuickBuyFragment

    @ContributesAndroidInjector
    abstract fun contributeEditProfileFragment(): EditProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeFavouritesFragment(): FavouritesFragment

    @ContributesAndroidInjector
    abstract fun contributeOrderDetailFragment(): OrderDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeOrderListFragment(): OrderListFragment

    @ContributesAndroidInjector
    abstract fun contributeCheckOutFragment(): CheckOutFragment

    @ContributesAndroidInjector
    abstract fun contributeCartListFragment(): CartListFragment

    @ContributesAndroidInjector
    abstract fun contributeProductDetailFragment(): ProductDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeProductListFragment(): ProductListFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment

    @ContributesAndroidInjector
    abstract fun contributeMapsFragment(): MapsFragment

    @ContributesAndroidInjector
    abstract fun contributeAddressListFragment(): AddressListFragment

    @ContributesAndroidInjector
    abstract fun contributeCategoriesFragment(): CategoriesFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeNotificationFragment(): NotificationFragment

    @ContributesAndroidInjector
    abstract fun contributeProductCompareFragment(): ProductCompareFragment
}