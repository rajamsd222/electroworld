package com.baldystudios.daggerkotlin.di


import androidx.lifecycle.ViewModelProvider
import com.electroworld.android.di.ViewModelProviderFactory
import dagger.Binds
import dagger.Module


@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelProviderFactory: ViewModelProviderFactory?):
            ViewModelProvider.Factory?

}